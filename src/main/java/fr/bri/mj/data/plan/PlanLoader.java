/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.data.plan;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.bri.mj.utils.ArrayReader;

public class PlanLoader {

	private static final String PLAN_FILE_URL = "fr/bri/mj/plan/plan.json";

	private static int[] nbMaxSessions;
	private static boolean[] perfectPlans;
	private static int[][][][] plans;

	public static int[] getMaxSessions() {
		if (nbMaxSessions == null) {
			loadFromFile();
		}
		if (nbMaxSessions != null) {
			final int[] copyNBMaxSessions = new int[nbMaxSessions.length];
			for (int sessionIndex = 0; sessionIndex < nbMaxSessions.length; sessionIndex++) {
				copyNBMaxSessions[sessionIndex] = nbMaxSessions[sessionIndex];
			}
			return copyNBMaxSessions;
		} else {
			return null;
		}
	}

	public static boolean[] getPerfectPlans() {
		if (perfectPlans == null) {
			loadFromFile();
		}
		if (perfectPlans != null) {
			final boolean[] copyPerfectPlan = new boolean[perfectPlans.length];
			for (int sessionIndex = 0; sessionIndex < perfectPlans.length; sessionIndex++) {
				copyPerfectPlan[sessionIndex] = perfectPlans[sessionIndex];
			}
			return copyPerfectPlan;
		} else {
			return null;
		}
	}

	public static int[][][] getPlan(final int nbTables) {
		if (plans == null) {
			loadFromFile();
		}
		if (plans != null && nbTables > 0 && nbTables <= plans.length) {
			final int[][][] planToCopy = plans[nbTables - 1];
			final int[][][] copyPlan = new int[planToCopy.length][][];
			for (int sessionIndex = 0; sessionIndex < planToCopy.length; sessionIndex++) {
				copyPlan[sessionIndex] = new int[planToCopy[sessionIndex].length][];
				for (int tableIndex = 0; tableIndex < planToCopy[sessionIndex].length; tableIndex++) {
					copyPlan[sessionIndex][tableIndex] = new int[planToCopy[sessionIndex][tableIndex].length];
					for (int playerIndex = 0; playerIndex < planToCopy[sessionIndex][tableIndex].length; playerIndex++) {
						copyPlan[sessionIndex][tableIndex][playerIndex] = planToCopy[sessionIndex][tableIndex][playerIndex];
					}
				}
			}
			return copyPlan;
		} else {
			return null;
		}
	}

	private static void loadFromFile() {
		final URL planFile = ClassLoader.getSystemResource(PLAN_FILE_URL);
		if (planFile != null) {
			List<?> rawPlans = null;

			try {
				final InputStream is = planFile.openStream();
				rawPlans = ArrayReader.readIntegerArray(new InputStreamReader(is));
				is.close();
			} catch (final Exception e) {
				e.printStackTrace();
				rawPlans = null;
			}

			if (rawPlans != null) {
				final Map<Integer, int[][][]> mapTablePlan = new HashMap<Integer, int[][][]>();
				int maxTables = 0;
				for (int planIndex = 0; planIndex < rawPlans.size(); planIndex++) {
					final Object objectPlan = rawPlans.get(planIndex);
					if (objectPlan instanceof List) {
						final List<?> plan = (List<?>) objectPlan;
						final int[][][] planArray = new int[plan.size()][][];
						for (int sessionIndex = 0; sessionIndex < plan.size(); sessionIndex++) {
							final Object objectSession = plan.get(sessionIndex);
							if (objectSession instanceof List) {
								final List<?> session = (List<?>) objectSession;
								final int[][] sessionArray = new int[session.size()][];
								planArray[sessionIndex] = sessionArray;
								for (int tableIndex = 0; tableIndex < session.size(); tableIndex++) {
									final Object objectTable = session.get(tableIndex);
									if (objectTable instanceof List) {
										final List<?> table = (List<?>) objectTable;
										final int[] tableArray = new int[table.size()];
										sessionArray[tableIndex] = tableArray;
										for (int playerIndex = 0; playerIndex < table.size(); playerIndex++) {
											final Object objectPlayer = table.get(playerIndex);
											if (objectPlayer instanceof Integer) {
												tableArray[playerIndex] = (Integer) objectPlayer;
											}
										}
									}
								}
							}
						}
						mapTablePlan.put(
							planArray[0].length,
							planArray
						);
						maxTables = Math.max(
							maxTables,
							planArray[0].length
						);
					}
				}
				nbMaxSessions = new int[maxTables];
				perfectPlans = new boolean[maxTables];
				plans = new int[maxTables][][][];
				for (final Integer nbTable : mapTablePlan.keySet()) {
					plans[nbTable - 1] = mapTablePlan.get(nbTable);
					nbMaxSessions[nbTable - 1] = plans[nbTable - 1].length;
					perfectPlans[nbTable - 1] = ((nbTable == 5
						? 4
						: nbTable) * 4 - 1) / 3 == nbMaxSessions[nbTable - 1];
				}
			}
		}
	}
}
