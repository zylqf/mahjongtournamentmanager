/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.data.score.aggregated.ranking.calculator.single;

import java.util.EnumMap;
import java.util.Map;

import fr.bri.mj.data.score.aggregated.QualificationMode;
import fr.bri.mj.data.score.aggregated.ranking.comparator.RankingScoreComparatorFactory;
import fr.bri.mj.data.score.aggregated.ranking.comparator.RankingScoreOrder;
import fr.bri.mj.data.score.raw.filter.ScoreFilterMCRFactory;
import fr.bri.mj.data.score.raw.getter.ScoreGetterMCRFactory;
import fr.bri.mj.data.tournament.mcr.ScoreMCR;
import fr.bri.mj.data.tournament.mcr.TournamentScoreMCR;

public class RankingCalculatorSingleMCRFactory implements RankingCalculatorSingleFactory<TournamentScoreMCR, ScoreMCR> {

	private static RankingCalculatorSingleMCRFactory instance = new RankingCalculatorSingleMCRFactory();

	public static RankingCalculatorSingleMCRFactory getInstance() {
		return instance;
	}

	private final Map<QualificationMode, Map<ScoreMCR, RankingCalculatorSingle<TournamentScoreMCR, ScoreMCR>>> buffer;

	private RankingCalculatorSingleMCRFactory() {
		buffer = new EnumMap<QualificationMode, Map<ScoreMCR, RankingCalculatorSingle<TournamentScoreMCR, ScoreMCR>>>(QualificationMode.class);
	}

	@Override
	public RankingCalculatorSingle<TournamentScoreMCR, ScoreMCR> getRankingCalculator(
		final ScoreMCR scoreType,
		final QualificationMode qualificationMode
	) {
		if (qualificationMode != null && scoreType != null) {
			if (!buffer.containsKey(qualificationMode)) {
				buffer.put(
					qualificationMode,
					new EnumMap<ScoreMCR, RankingCalculatorSingle<TournamentScoreMCR, ScoreMCR>>(ScoreMCR.class)
				);
			}

			final Map<ScoreMCR, RankingCalculatorSingle<TournamentScoreMCR, ScoreMCR>> scoreTypeBuffer = buffer.get(qualificationMode);
			if (!scoreTypeBuffer.containsKey(scoreType)) {
				switch (qualificationMode) {
					case WITHOUT:
						switch (scoreType) {
							case MATCH_POINT:
								scoreTypeBuffer.put(
									scoreType,
									new RankingCalculatorSingleWithoutQualification<TournamentScoreMCR, ScoreMCR>(
										ScoreFilterMCRFactory.getScoreFilterAll(),
										ScoreGetterMCRFactory.getScoreGetterMCR(ScoreMCR.MATCH_POINT),
										ScoreGetterMCRFactory.getScoreGetterMCR(ScoreMCR.TABLE_POINT),
										null,
										null,
										RankingScoreComparatorFactory.getRankingScoreComparator(
											RankingScoreOrder.DESCENDANT,
											RankingScoreOrder.DESCENDANT,
											RankingScoreOrder.IGNORE,
											RankingScoreOrder.IGNORE,
											RankingScoreOrder.ASCENDANT
										)
									)
								);
								break;
							case TABLE_POINT:
								scoreTypeBuffer.put(
									scoreType,
									new RankingCalculatorSingleWithoutQualification<TournamentScoreMCR, ScoreMCR>(
										ScoreFilterMCRFactory.getScoreFilterAll(),
										ScoreGetterMCRFactory.getScoreGetterMCR(ScoreMCR.TABLE_POINT),
										ScoreGetterMCRFactory.getScoreGetterMCR(ScoreMCR.MATCH_POINT),
										null,
										null,
										RankingScoreComparatorFactory.getRankingScoreComparator(
											RankingScoreOrder.DESCENDANT,
											RankingScoreOrder.DESCENDANT,
											RankingScoreOrder.IGNORE,
											RankingScoreOrder.IGNORE,
											RankingScoreOrder.ASCENDANT
										)
									)
								);
								break;
						}
						break;
					case WITH:
						switch (scoreType) {
							case MATCH_POINT:
								scoreTypeBuffer.put(
									scoreType,
									new RankingCalculatorSingleWithQualification<TournamentScoreMCR, ScoreMCR>(ScoreGetterMCRFactory.getScoreGetterMCR(ScoreMCR.MATCH_POINT))
								);
								break;
							case TABLE_POINT:
								scoreTypeBuffer.put(
									scoreType,
									new RankingCalculatorSingleWithQualification<TournamentScoreMCR, ScoreMCR>(ScoreGetterMCRFactory.getScoreGetterMCR(ScoreMCR.TABLE_POINT))
								);
								break;
						}
						break;
				}
			}
			return scoreTypeBuffer.get(scoreType);
		} else {
			return null;
		}
	}
}
