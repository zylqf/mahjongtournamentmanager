/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.data.score.aggregated.ranking.calculator.single;

import java.util.EnumMap;
import java.util.Map;

import fr.bri.mj.data.score.aggregated.QualificationMode;
import fr.bri.mj.data.score.aggregated.ranking.comparator.RankingScoreComparatorFactory;
import fr.bri.mj.data.score.aggregated.ranking.comparator.RankingScoreOrder;
import fr.bri.mj.data.score.raw.filter.ScoreFilterRCRFactory;
import fr.bri.mj.data.score.raw.getter.ScoreGetterRCRFactory;
import fr.bri.mj.data.tournament.rcr.ScoreRCR;
import fr.bri.mj.data.tournament.rcr.TournamentScoreRCR;

public class RankingCalculatorSingleRCRFactory implements RankingCalculatorSingleFactory<TournamentScoreRCR, ScoreRCR> {

	private static RankingCalculatorSingleRCRFactory instance = new RankingCalculatorSingleRCRFactory();

	public static RankingCalculatorSingleRCRFactory getInstance() {
		return instance;
	}

	private final Map<QualificationMode, Map<ScoreRCR, RankingCalculatorSingle<TournamentScoreRCR, ScoreRCR>>> buffer;

	private RankingCalculatorSingleRCRFactory() {
		buffer = new EnumMap<QualificationMode, Map<ScoreRCR, RankingCalculatorSingle<TournamentScoreRCR, ScoreRCR>>>(QualificationMode.class);
	}

	@Override
	public RankingCalculatorSingle<TournamentScoreRCR, ScoreRCR> getRankingCalculator(
		final ScoreRCR scoreType,
		final QualificationMode qualificationMode
	) {
		if (qualificationMode != null && scoreType != null) {
			if (!buffer.containsKey(qualificationMode)) {
				buffer.put(
					qualificationMode,
					new EnumMap<ScoreRCR, RankingCalculatorSingle<TournamentScoreRCR, ScoreRCR>>(ScoreRCR.class)
				);
			}

			final Map<ScoreRCR, RankingCalculatorSingle<TournamentScoreRCR, ScoreRCR>> scoreTypeBuffer = buffer.get(qualificationMode);
			if (!scoreTypeBuffer.containsKey(scoreType)) {
				switch (qualificationMode) {
					case WITHOUT:
						switch (scoreType) {
							case FINAL_SCORE:
								scoreTypeBuffer.put(
									scoreType,
									new RankingCalculatorSingleWithoutQualification<TournamentScoreRCR, ScoreRCR>(
										ScoreFilterRCRFactory.getScoreFilterAll(),
										ScoreGetterRCRFactory.getScoreGetterRCR(ScoreRCR.FINAL_SCORE),
										null,
										null,
										null,
										RankingScoreComparatorFactory.getRankingScoreComparator(
											RankingScoreOrder.DESCENDANT,
											RankingScoreOrder.IGNORE,
											RankingScoreOrder.IGNORE,
											RankingScoreOrder.IGNORE,
											RankingScoreOrder.ASCENDANT
										)
									)
								);
								break;
							case GAME_SCORE:
								scoreTypeBuffer.put(
									scoreType,
									new RankingCalculatorSingleWithoutQualification<TournamentScoreRCR, ScoreRCR>(
										ScoreFilterRCRFactory.getScoreFilterAll(),
										ScoreGetterRCRFactory.getScoreGetterRCR(ScoreRCR.GAME_SCORE),
										ScoreGetterRCRFactory.getScoreGetterRCR(ScoreRCR.FINAL_SCORE),
										null,
										null,
										RankingScoreComparatorFactory.getRankingScoreComparator(
											RankingScoreOrder.DESCENDANT,
											RankingScoreOrder.DESCENDANT,
											RankingScoreOrder.IGNORE,
											RankingScoreOrder.IGNORE,
											RankingScoreOrder.ASCENDANT
										)
									)
								);
								break;
							case UMA_SCORE:
								scoreTypeBuffer.put(
									scoreType,
									new RankingCalculatorSingleWithoutQualification<TournamentScoreRCR, ScoreRCR>(
										ScoreFilterRCRFactory.getScoreFilterAll(),
										ScoreGetterRCRFactory.getScoreGetterRCR(ScoreRCR.UMA_SCORE),
										ScoreGetterRCRFactory.getScoreGetterRCR(ScoreRCR.FINAL_SCORE),
										null,
										null,
										RankingScoreComparatorFactory.getRankingScoreComparator(
											RankingScoreOrder.DESCENDANT,
											RankingScoreOrder.DESCENDANT,
											RankingScoreOrder.IGNORE,
											RankingScoreOrder.IGNORE,
											RankingScoreOrder.ASCENDANT
										)
									)
								);
								break;
						}
						break;
					case WITH:
						switch (scoreType) {
							case FINAL_SCORE:
								scoreTypeBuffer.put(
									scoreType,
									new RankingCalculatorSingleWithQualification<TournamentScoreRCR, ScoreRCR>(ScoreGetterRCRFactory.getScoreGetterRCR(ScoreRCR.FINAL_SCORE))
								);
								break;
							case GAME_SCORE:
								scoreTypeBuffer.put(
									scoreType,
									new RankingCalculatorSingleWithQualification<TournamentScoreRCR, ScoreRCR>(ScoreGetterRCRFactory.getScoreGetterRCR(ScoreRCR.GAME_SCORE))
								);
								break;
							case UMA_SCORE:
								scoreTypeBuffer.put(
									scoreType,
									new RankingCalculatorSingleWithQualification<TournamentScoreRCR, ScoreRCR>(ScoreGetterRCRFactory.getScoreGetterRCR(ScoreRCR.UMA_SCORE))
								);
								break;
						}
						break;
				}
			}
			return scoreTypeBuffer.get(scoreType);
		} else {
			return null;
		}
	}
}
