/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.data.score.aggregated.ranking.calculator.single;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import fr.bri.mj.data.score.aggregated.ranking.comparator.RankingScoreComparatorFactory;
import fr.bri.mj.data.score.aggregated.ranking.comparator.RankingScoreOrder;
import fr.bri.mj.data.score.aggregated.ranking.rankingscore.RankingScore;
import fr.bri.mj.data.score.aggregated.ranking.rankingscore.single.RankingScoreSingleIndividual;
import fr.bri.mj.data.score.aggregated.ranking.rankingscore.single.RankingScoreSingleTeam;
import fr.bri.mj.data.score.raw.getter.ScoreGetter;
import fr.bri.mj.data.tournament.Tournament;
import fr.bri.mj.data.tournament.TournamentPlayer;
import fr.bri.mj.data.tournament.TournamentScore;

class RankingCalculatorSingleWithQualification<ScoreClassType extends TournamentScore<ScoreClassType, ScoreEnumType>, ScoreEnumType extends Enum<ScoreEnumType>> implements RankingCalculatorSingle<ScoreClassType, ScoreEnumType> {

	private final ScoreGetter<ScoreClassType, ScoreEnumType> scoreGetter;

	RankingCalculatorSingleWithQualification(final ScoreGetter<ScoreClassType, ScoreEnumType> scoreGetter) {
		this.scoreGetter = scoreGetter;
	}

	@Override
	public List<RankingScoreSingleIndividual> getRankingScoreIndividual(
		final Tournament<ScoreClassType, ScoreEnumType> tournament,
		final int session
	) {
		if (session <= tournament.getNbExistingSessions()) {
			final Map<TournamentPlayer, Integer> mapPlayerToQualificationRanking = new TreeMap<TournamentPlayer, Integer>();
			final List<RankingScoreSingleIndividual> qualificationRanking;

			{
				final Map<TournamentPlayer, RankingScoreSingleIndividual> mapPlayerToQualificationRankingScore = new TreeMap<TournamentPlayer, RankingScoreSingleIndividual>();
				final int qualificationSession = Math.min(
					session,
					tournament.getNbSessions() - 6
				);
				for (final ScoreClassType table : tournament.getTables()) {
					if (table.getSessionId() <= qualificationSession) {
						final TournamentPlayer tournamentPlayer = table.getTournamentPlayer();
						if (!mapPlayerToQualificationRankingScore.containsKey(tournamentPlayer)) {
							final RankingScoreSingleIndividual score = new RankingScoreSingleIndividual(tournamentPlayer);
							score.setUpToDate(true);
							mapPlayerToQualificationRankingScore.put(
								tournamentPlayer,
								score
							);
						}
						final RankingScoreSingleIndividual score = mapPlayerToQualificationRankingScore.get(tournamentPlayer);
						final int qualificationScore = scoreGetter.get(table);
						score.addTotalScore(qualificationScore);
						if (table.getRanking() == 1) {
							score.addTotalScore2(1);
						} else if (table.getRanking() == 4) {
							score.addTotalScore3(1);
						}
						score.setTotalScore4(
							Math.max(
								score.getTotalScore4(),
								qualificationScore
							)
						);
						score.andUpToDate(table.isScoreRecorded());
					}
				}

				qualificationRanking = new ArrayList<RankingScoreSingleIndividual>(mapPlayerToQualificationRankingScore.values());
				final Comparator<RankingScore> qualificationRankingScoreComparator = RankingScoreComparatorFactory.getRankingScoreComparator(
					RankingScoreOrder.DESCENDANT,
					RankingScoreOrder.DESCENDANT,
					RankingScoreOrder.ASCENDANT,
					RankingScoreOrder.DESCENDANT,
					RankingScoreOrder.IGNORE
				);
				Collections.sort(
					qualificationRanking,
					qualificationRankingScoreComparator
				);

				for (int index = 0; index < qualificationRanking.size(); index++) {
					final RankingScoreSingleIndividual score = qualificationRanking.get(index);
					score.setRanking(index + 1);
					mapPlayerToQualificationRanking.put(
						score.getTournamentPlayer(),
						score.getRanking()
					);
				}
			}

			if (session <= tournament.getNbSessions() - 6) {
				// Qualification phase
				return qualificationRanking;
			} else if (session <= tournament.getNbSessions() - 4) {
				// 1/4 final phase
				// Step 1: Score aggregation
				// Step 1.1: Prepare maps from tournament player to score
				final List<Map<TournamentPlayer, RankingScoreSingleIndividual>> mapsPlayerToRankingScore = new ArrayList<Map<TournamentPlayer, RankingScoreSingleIndividual>>();
				for (int index = 0; index < 5; index++) {
					mapsPlayerToRankingScore.add(new TreeMap<TournamentPlayer, RankingScoreSingleIndividual>());
				}
				for (final ScoreClassType table : tournament.getTables()) {
					if (table.getSessionId() == session) {
						final RankingScoreSingleIndividual score = new RankingScoreSingleIndividual(table.getTournamentPlayer());
						score.setUpToDate(true);
						score.setTotalScore2(mapPlayerToQualificationRanking.get(table.getTournamentPlayer()));
						switch (table.getTableId()) {
							case 1:
							case 2:
							case 3:
							case 4:
								mapsPlayerToRankingScore.get(table.getTableId() - 1).put(
									table.getTournamentPlayer(),
									score
								);
								break;
							default:
								mapsPlayerToRankingScore.get(4).put(
									table.getTournamentPlayer(),
									score
								);
								break;
						}
					}
				}

				// Step 1.2:
				// Aggregate the scores session n-5 and n-4 for table 1, 2, 3 and 4
				// Aggregate the scores for the other tables
				for (final ScoreClassType table : tournament.getTables()) {
					final TournamentPlayer tournamentPlayer = table.getTournamentPlayer();
					if (table.getSessionId() <= session) {
						boolean added = false;
						for (int index = 0; !added && index < 4; index++) {
							if (mapsPlayerToRankingScore.get(index).containsKey(tournamentPlayer) && table.getSessionId() > tournament.getNbSessions() - 6) {
								final RankingScoreSingleIndividual score = mapsPlayerToRankingScore.get(index).get(tournamentPlayer);
								score.addTotalScore(scoreGetter.get(table));
								score.andUpToDate(table.isScoreRecorded());
								added = true;
							}
						}
						for (int index = 4; !added && index < 5; index++) {
							if (mapsPlayerToRankingScore.get(index).containsKey(tournamentPlayer)) {
								final RankingScoreSingleIndividual score = mapsPlayerToRankingScore.get(index).get(tournamentPlayer);
								score.addTotalScore(scoreGetter.get(table));
								score.andUpToDate(table.isScoreRecorded());
								added = true;
							}
						}
					}
				}

				final Comparator<RankingScore> finalRankingScoreComparator = RankingScoreComparatorFactory.getRankingScoreComparator(
					RankingScoreOrder.DESCENDANT,
					RankingScoreOrder.ASCENDANT,
					RankingScoreOrder.IGNORE,
					RankingScoreOrder.IGNORE,
					RankingScoreOrder.IGNORE
				);

				// Step 2: Calculate ranking
				final List<List<RankingScoreSingleIndividual>> quarterFinalRankings = new ArrayList<List<RankingScoreSingleIndividual>>();
				for (int index = 0; index < 5; index++) {
					quarterFinalRankings.add(new ArrayList<RankingScoreSingleIndividual>(mapsPlayerToRankingScore.get(index).values()));
				}

				// Step 2.1: Sort individually the scores for table 1, 2, 3 and 4 with qualification ranking as second criteria
				for (int index = 0; index < 4; index++) {
					Collections.sort(
						quarterFinalRankings.get(index),
						finalRankingScoreComparator
					);
				}

				// Step 2.2.1: Take best 2 players from table 1, 2, 3 and 4
				final List<RankingScoreSingleIndividual> quarterFinalRankingFirstHalf = new ArrayList<RankingScoreSingleIndividual>();
				for (int tableIndex = 0; tableIndex < 4; tableIndex++) {
					for (int playerIndex = 0; playerIndex < 2; playerIndex++) {
						quarterFinalRankingFirstHalf.add(quarterFinalRankings.get(tableIndex).get(playerIndex));
					}
				}

				// Step 2.2.2: Sort the score for the first 8 players
				Collections.sort(
					quarterFinalRankingFirstHalf,
					finalRankingScoreComparator
				);
				for (int index = 0; index < quarterFinalRankingFirstHalf.size(); index++) {
					quarterFinalRankingFirstHalf.get(index).setRanking(index + 1);
				}

				// Step 2.3.1: Take last 2 players from table 1, 2, 3 and 4
				final List<RankingScoreSingleIndividual> quarterFinalRankingSecondHalf = new ArrayList<RankingScoreSingleIndividual>();
				for (int tableIndex = 0; tableIndex < 4; tableIndex++) {
					for (int playerIndex = 2; playerIndex < 4; playerIndex++) {
						quarterFinalRankingSecondHalf.add(quarterFinalRankings.get(tableIndex).get(playerIndex));
					}
				}

				// Step 2.3.2: Sort the scores for the second 8 players
				Collections.sort(
					quarterFinalRankingSecondHalf,
					finalRankingScoreComparator
				);
				for (int index = 0; index < quarterFinalRankingSecondHalf.size(); index++) {
					quarterFinalRankingSecondHalf.get(index).setRanking(index + 9);
				}

				// Step 2.4.1: Sort the scores for all the others
				Collections.sort(
					quarterFinalRankings.get(4),
					finalRankingScoreComparator
				);
				for (int index = 0; index < quarterFinalRankings.get(4).size(); index++) {
					quarterFinalRankings.get(4).get(index).setRanking(index + 17);
				}

				// Step 2.5: Aggregate all rankings
				final List<RankingScoreSingleIndividual> quarterFinalRanking = new ArrayList<RankingScoreSingleIndividual>();
				quarterFinalRanking.addAll(quarterFinalRankingFirstHalf);
				quarterFinalRanking.addAll(quarterFinalRankingSecondHalf);
				quarterFinalRanking.addAll(quarterFinalRankings.get(4));
				return quarterFinalRanking;
			} else if (session <= tournament.getNbSessions() - 2) {
				// 1/2 final phase
				// Step 1: Score aggregation
				// Step 1.1: Prepare maps from tournament player to score
				final List<Map<TournamentPlayer, RankingScoreSingleIndividual>> mapsPlayerToRankingScore = new ArrayList<Map<TournamentPlayer, RankingScoreSingleIndividual>>();
				for (int index = 0; index < 4; index++) {
					mapsPlayerToRankingScore.add(new TreeMap<TournamentPlayer, RankingScoreSingleIndividual>());
				}
				for (final ScoreClassType table : tournament.getTables()) {
					if (table.getSessionId() == session) {
						final RankingScoreSingleIndividual score = new RankingScoreSingleIndividual(table.getTournamentPlayer());
						score.setUpToDate(true);
						score.setTotalScore2(mapPlayerToQualificationRanking.get(table.getTournamentPlayer()));
						switch (table.getTableId()) {
							case 1:
							case 2:
								mapsPlayerToRankingScore.get(table.getTableId() - 1).put(
									table.getTournamentPlayer(),
									score
								);
								break;
							case 3:
							case 4:
								mapsPlayerToRankingScore.get(2).put(
									table.getTournamentPlayer(),
									score
								);
								break;
							default:
								mapsPlayerToRankingScore.get(3).put(
									table.getTournamentPlayer(),
									score
								);
								break;
						}
					}
				}

				// Step 1.2:
				// Aggregate the scores session n-3 and n-2 for table 1 and 2
				// Aggregate the scores from session 1 to n-2 for table 3 and 4
				// Aggregate the scores from session 1 to n-2 for the other tables
				for (final ScoreClassType table : tournament.getTables()) {
					final TournamentPlayer tournamentPlayer = table.getTournamentPlayer();
					if (table.getSessionId() <= session) {
						boolean added = false;
						for (int index = 0; !added && index < 2; index++) {
							if (mapsPlayerToRankingScore.get(index).containsKey(tournamentPlayer) && table.getSessionId() > tournament.getNbSessions() - 4) {
								final RankingScoreSingleIndividual score = mapsPlayerToRankingScore.get(index).get(tournamentPlayer);
								score.addTotalScore(scoreGetter.get(table));
								score.andUpToDate(table.isScoreRecorded());
								added = true;
							}
						}
						for (int index = 2; !added && index < 4; index++) {
							if (mapsPlayerToRankingScore.get(index).containsKey(tournamentPlayer)) {
								final RankingScoreSingleIndividual score = mapsPlayerToRankingScore.get(index).get(tournamentPlayer);
								score.addTotalScore(scoreGetter.get(table));
								score.andUpToDate(table.isScoreRecorded());
								added = true;
							}
						}
					}
				}

				final Comparator<RankingScore> finalRankingScoreComparator = RankingScoreComparatorFactory.getRankingScoreComparator(
					RankingScoreOrder.DESCENDANT,
					RankingScoreOrder.ASCENDANT,
					RankingScoreOrder.IGNORE,
					RankingScoreOrder.IGNORE,
					RankingScoreOrder.IGNORE
				);

				// Step 2: Calculate ranking
				final List<List<RankingScoreSingleIndividual>> semiFinalRankings = new ArrayList<List<RankingScoreSingleIndividual>>();
				for (int index = 0; index < 4; index++) {
					semiFinalRankings.add(new ArrayList<RankingScoreSingleIndividual>(mapsPlayerToRankingScore.get(index).values()));
				}

				// Step 2.1: Sort individually the scores for table 1 and 2 with qualification ranking as second criteria
				for (int index = 0; index < 2; index++) {
					Collections.sort(
						semiFinalRankings.get(index),
						finalRankingScoreComparator
					);
				}

				// Step 2.2.1: Take the best 2 players from table 1 and 2
				final List<RankingScoreSingleIndividual> semiFinalRankingFirstHalf = new ArrayList<RankingScoreSingleIndividual>();
				for (int tableIndex = 0; tableIndex < 2; tableIndex++) {
					for (int playerIndex = 0; playerIndex < 2; playerIndex++) {
						semiFinalRankingFirstHalf.add(semiFinalRankings.get(tableIndex).get(playerIndex));
					}
				}

				// Step 2.2.2: Sort the score for the first 4 players
				Collections.sort(
					semiFinalRankingFirstHalf,
					finalRankingScoreComparator
				);
				for (int index = 0; index < semiFinalRankingFirstHalf.size(); index++) {
					semiFinalRankingFirstHalf.get(index).setRanking(index + 1);
				}

				// Step 2.3.1: Take the last 2 players from table 1 and 2
				final List<RankingScoreSingleIndividual> semiFinalRankingSecondHalf = new ArrayList<RankingScoreSingleIndividual>();
				for (int tableIndex = 0; tableIndex < 2; tableIndex++) {
					for (int playerIndex = 2; playerIndex < 4; playerIndex++) {
						semiFinalRankingSecondHalf.add(semiFinalRankings.get(tableIndex).get(playerIndex));
					}
				}

				// Step 2.3.2: Sort the scores for the second 4 players
				Collections.sort(
					semiFinalRankingSecondHalf,
					finalRankingScoreComparator
				);
				for (int index = 0; index < semiFinalRankingSecondHalf.size(); index++) {
					semiFinalRankingSecondHalf.get(index).setRanking(index + 5);
				}

				// Step 2.4: Sort the scores for the next 8 players
				Collections.sort(
					semiFinalRankings.get(2),
					finalRankingScoreComparator
				);
				for (int index = 0; index < semiFinalRankings.get(2).size(); index++) {
					semiFinalRankings.get(2).get(index).setRanking(index + 9);
				}

				// Step 2.5: Sort the scores for all the others
				Collections.sort(
					semiFinalRankings.get(3),
					finalRankingScoreComparator
				);
				for (int index = 0; index < semiFinalRankings.get(3).size(); index++) {
					semiFinalRankings.get(3).get(index).setRanking(index + 17);
				}

				// Step 2.6: Aggregate all rankings
				final List<RankingScoreSingleIndividual> semiFinalRanking = new ArrayList<RankingScoreSingleIndividual>();
				semiFinalRanking.addAll(semiFinalRankingFirstHalf);
				semiFinalRanking.addAll(semiFinalRankingSecondHalf);
				semiFinalRanking.addAll(semiFinalRankings.get(2));
				semiFinalRanking.addAll(semiFinalRankings.get(3));
				return semiFinalRanking;
			} else {
				// Final phase
				// Step 1: Score aggregation
				// Step 1.1: Prepare maps from tournament player to score
				final List<Map<TournamentPlayer, RankingScoreSingleIndividual>> mapsPlayerToRankingScore = new ArrayList<Map<TournamentPlayer, RankingScoreSingleIndividual>>();
				for (int index = 0; index < 4; index++) {
					mapsPlayerToRankingScore.add(new TreeMap<TournamentPlayer, RankingScoreSingleIndividual>());
				}
				for (final ScoreClassType table : tournament.getTables()) {
					if (table.getSessionId() == session) {
						final RankingScoreSingleIndividual score = new RankingScoreSingleIndividual(table.getTournamentPlayer());
						score.setUpToDate(true);
						score.setTotalScore2(mapPlayerToQualificationRanking.get(table.getTournamentPlayer()));
						switch (table.getTableId()) {
							case 1:
							case 2:
								mapsPlayerToRankingScore.get(table.getTableId() - 1).put(
									table.getTournamentPlayer(),
									score
								);
								break;
							case 3:
							case 4:
								mapsPlayerToRankingScore.get(2).put(
									table.getTournamentPlayer(),
									score
								);
								break;
							default:
								mapsPlayerToRankingScore.get(3).put(
									table.getTournamentPlayer(),
									score
								);
								break;
						}
					}
				}

				// Step 1.2:
				// Aggregate the scores session n-1 and n for table 1
				// Aggregate the scores from session n-3 to n-2 for table 2
				// Aggregate the scores for table 3 and 4
				// Aggregate the scores for the other tables
				for (final ScoreClassType table : tournament.getTables()) {
					final TournamentPlayer tournamentPlayer = table.getTournamentPlayer();
					if (table.getSessionId() <= session) {
						boolean added = false;
						for (int index = 0; !added && index < 1; index++) {
							if (mapsPlayerToRankingScore.get(index).containsKey(tournamentPlayer) && table.getSessionId() > tournament.getNbSessions() - 2) {
								final RankingScoreSingleIndividual score = mapsPlayerToRankingScore.get(index).get(tournamentPlayer);
								score.addTotalScore(scoreGetter.get(table));
								score.andUpToDate(table.isScoreRecorded());
								added = true;
							}
						}
						for (int index = 1; !added && index < 2; index++) {
							if (mapsPlayerToRankingScore.get(index).containsKey(tournamentPlayer) && table.getSessionId() > tournament.getNbSessions() - 4) {
								final RankingScoreSingleIndividual score = mapsPlayerToRankingScore.get(index).get(tournamentPlayer);
								score.addTotalScore(scoreGetter.get(table));
								score.andUpToDate(table.isScoreRecorded());
								added = true;
							}
						}
						for (int index = 2; !added && index < 4; index++) {
							if (mapsPlayerToRankingScore.get(index).containsKey(tournamentPlayer)) {
								final RankingScoreSingleIndividual score = mapsPlayerToRankingScore.get(index).get(tournamentPlayer);
								score.addTotalScore(scoreGetter.get(table));
								score.andUpToDate(table.isScoreRecorded());
								added = true;
							}
						}
					}
				}

				final Comparator<RankingScore> finalRankingScoreComparator = RankingScoreComparatorFactory.getRankingScoreComparator(
					RankingScoreOrder.DESCENDANT,
					RankingScoreOrder.ASCENDANT,
					RankingScoreOrder.IGNORE,
					RankingScoreOrder.IGNORE,
					RankingScoreOrder.IGNORE
				);

				// Step 2: Calculate ranking
				final List<List<RankingScoreSingleIndividual>> finalRankings = new ArrayList<List<RankingScoreSingleIndividual>>();
				for (int index = 0; index < 4; index++) {
					finalRankings.add(new ArrayList<RankingScoreSingleIndividual>(mapsPlayerToRankingScore.get(index).values()));
				}

				// Step 2.1: Sort the scores for the first 4 players
				Collections.sort(
					finalRankings.get(0),
					finalRankingScoreComparator
				);
				for (int index = 0; index < finalRankings.get(0).size(); index++) {
					finalRankings.get(0).get(index).setRanking(index + 1);
				}

				// Step 2.2: Sort the scores for the next 4 players
				Collections.sort(
					finalRankings.get(1),
					finalRankingScoreComparator
				);
				for (int index = 0; index < finalRankings.get(1).size(); index++) {
					finalRankings.get(1).get(index).setRanking(index + 5);
				}

				// Step 2.3: Sort the scores for the next 8 players
				Collections.sort(
					finalRankings.get(2),
					finalRankingScoreComparator
				);
				for (int index = 0; index < finalRankings.get(2).size(); index++) {
					finalRankings.get(2).get(index).setRanking(index + 9);
				}

				// Step 2.4: Sort the scores for all the others
				Collections.sort(
					finalRankings.get(3),
					finalRankingScoreComparator
				);
				for (int index = 0; index < finalRankings.get(3).size(); index++) {
					finalRankings.get(3).get(index).setRanking(index + 17);
				}

				// Step 2.5: Aggregate all rankings
				final List<RankingScoreSingleIndividual> finalRanking = new ArrayList<RankingScoreSingleIndividual>();
				finalRanking.addAll(finalRankings.get(0));
				finalRanking.addAll(finalRankings.get(1));
				finalRanking.addAll(finalRankings.get(2));
				finalRanking.addAll(finalRankings.get(3));
				return finalRanking;
			}
		} else {
			return null;
		}
	}

	@Override
	public List<RankingScoreSingleTeam> getRankingScoreTeam(
		final Tournament<ScoreClassType, ScoreEnumType> tournament,
		final int session
	) {
		return null;
	}
}
