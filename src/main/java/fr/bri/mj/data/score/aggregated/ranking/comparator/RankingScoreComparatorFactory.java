/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.data.score.aggregated.ranking.comparator;

public class RankingScoreComparatorFactory {

	private static final RankingScoreComparator buffer[][][][][] = new RankingScoreComparator[RankingScoreOrder.values().length][RankingScoreOrder.values().length][RankingScoreOrder.values().length][RankingScoreOrder.values().length][RankingScoreOrder.values().length];

	public static RankingScoreComparator getRankingScoreComparator(
		final RankingScoreOrder orderScore1,
		final RankingScoreOrder orderScore2,
		final RankingScoreOrder orderScore3,
		final RankingScoreOrder orderScore4,
		final RankingScoreOrder orderName
	) {
		if (buffer[orderScore1.ordinal()][orderScore2.ordinal()][orderScore3.ordinal()][orderScore4.ordinal()][orderName.ordinal()] == null) {
			buffer[orderScore1.ordinal()][orderScore2.ordinal()][orderScore3.ordinal()][orderScore4.ordinal()][orderName.ordinal()] = new RankingScoreComparator(
				orderScore1,
				orderScore2,
				orderScore3,
				orderScore4,
				orderName
			);
		}
		return buffer[orderScore1.ordinal()][orderScore2.ordinal()][orderScore3.ordinal()][orderScore4.ordinal()][orderName.ordinal()];
	}
}
