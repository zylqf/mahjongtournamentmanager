/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.data.score.aggregated.sessionscore.accessor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import fr.bri.mj.data.score.aggregated.sessionscore.score.SessionScoreIndividual;
import fr.bri.mj.data.score.aggregated.sessionscore.score.SessionScoreTeam;
import fr.bri.mj.data.score.raw.getter.ScoreGetter;
import fr.bri.mj.data.tournament.Tournament;
import fr.bri.mj.data.tournament.TournamentPlayer;
import fr.bri.mj.data.tournament.TournamentScore;

class SessionScoreAccessorBase<ScoreClassType extends TournamentScore<ScoreClassType, ScoreEnumType>, ScoreEnumType extends Enum<ScoreEnumType>> implements SessionScoreAccessor<ScoreClassType, ScoreEnumType> {

	private final ScoreGetter<ScoreClassType, ScoreEnumType> scoreGetter;

	SessionScoreAccessorBase(final ScoreGetter<ScoreClassType, ScoreEnumType> scoreGetter) {
		this.scoreGetter = scoreGetter;
	}

	@Override
	final public List<SessionScoreIndividual> getScoreIndividual(
		final Tournament<ScoreClassType, ScoreEnumType> tournament,
		final int session
	) {
		return get(
			tournament,
			session
		);
	}

	@Override
	final public List<SessionScoreTeam> getScoreTeam(
		final Tournament<ScoreClassType, ScoreEnumType> tournament,
		final int session
	) {
		final List<SessionScoreIndividual> listScore = get(
			tournament,
			session
		);

		final Map<String, SessionScoreTeam> mapTeamToScore = new TreeMap<String, SessionScoreTeam>();
		for (int index = 0; index < listScore.size(); index++) {
			final SessionScoreIndividual scoreIndividual = listScore.get(index);
			final TournamentPlayer tournamentPlayer = scoreIndividual.getTournamentPlayer();
			final String teamName = tournamentPlayer.getTeamName();
			if (!mapTeamToScore.containsKey(teamName)) {
				final SessionScoreTeam scoreTeam = new SessionScoreTeam(teamName);
				scoreTeam.setUpToDate(true);
				mapTeamToScore.put(
					teamName,
					scoreTeam
				);
			}

			final SessionScoreTeam scoreTeam = mapTeamToScore.get(teamName);
			if (scoreIndividual != null) {
				scoreTeam.getScores().add(scoreIndividual);
				scoreTeam.addScore(scoreIndividual.getScore());
				if (!scoreIndividual.isUpToDate()) {
					scoreTeam.setUpToDate(false);
				}
			}
		}

		return new ArrayList<SessionScoreTeam>(mapTeamToScore.values());
	}

	private List<SessionScoreIndividual> get(
		final Tournament<ScoreClassType, ScoreEnumType> tournament,
		final int session
	) {
		final List<SessionScoreIndividual> listScore = new ArrayList<SessionScoreIndividual>();
		for (final ScoreClassType table : tournament.getTables()) {
			if (table.getSessionId() == session) {
				final TournamentPlayer tournamentPlayer = table.getTournamentPlayer();
				final SessionScoreIndividual score = new SessionScoreIndividual(tournamentPlayer);
				score.setScore(scoreGetter.get(table));
				score.setUpToDate(table.isScoreRecorded());
				listScore.add(score);
			}
		}
		return listScore;
	}
}
