/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.data.score.aggregated.sessionscore.accessor;

import java.util.EnumMap;

import fr.bri.mj.data.score.raw.getter.ScoreGetterMCRFactory;
import fr.bri.mj.data.tournament.mcr.ScoreMCR;
import fr.bri.mj.data.tournament.mcr.TournamentScoreMCR;

public class SessionScoreAccessorMCRFactory {
	private static final EnumMap<ScoreMCR, SessionScoreAccessor<TournamentScoreMCR, ScoreMCR>> buffer = new EnumMap<ScoreMCR, SessionScoreAccessor<TournamentScoreMCR, ScoreMCR>>(ScoreMCR.class);

	public static SessionScoreAccessor<TournamentScoreMCR, ScoreMCR> getScoreAccessorMCR(final ScoreMCR mode) {
		if (mode != null) {
			if (!buffer.containsKey(mode)) {
				switch (mode) {
					case MATCH_POINT:
						buffer.put(
							mode,
							new SessionScoreAccessorBase<TournamentScoreMCR, ScoreMCR>(ScoreGetterMCRFactory.getScoreGetterMCR(ScoreMCR.MATCH_POINT))
						);
						break;
					case TABLE_POINT:
						buffer.put(
							mode,
							new SessionScoreAccessorBase<TournamentScoreMCR, ScoreMCR>(ScoreGetterMCRFactory.getScoreGetterMCR(ScoreMCR.TABLE_POINT))
						);
						break;
					default:
						break;
				}
			}
			return buffer.get(mode);
		} else {
			return null;
		}
	}
}
