/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.data.score.raw.comparator;

import java.util.Comparator;

import fr.bri.mj.data.score.raw.getter.ScoreGetter;
import fr.bri.mj.data.tournament.TournamentScore;

public class ScoreComparator<ScoreClassType extends TournamentScore<ScoreClassType, ScoreEnumType>, ScoreEnumType extends Enum<ScoreEnumType>> implements Comparator<ScoreClassType> {

	private final ScoreGetter<ScoreClassType, ScoreEnumType> scoreGetter;

	public ScoreComparator(final ScoreGetter<ScoreClassType, ScoreEnumType> scoreGetter) {
		this.scoreGetter = scoreGetter;
	}

	@Override
	public int compare(
		final ScoreClassType o1,
		final ScoreClassType o2
	) {
		return Integer.compare(
			scoreGetter.get(o1),
			scoreGetter.get(o2)
		);
	}

}
