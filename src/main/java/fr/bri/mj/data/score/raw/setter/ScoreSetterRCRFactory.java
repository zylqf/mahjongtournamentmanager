/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.data.score.raw.setter;

import java.util.EnumMap;

import fr.bri.mj.data.tournament.rcr.ScoreRCR;
import fr.bri.mj.data.tournament.rcr.TournamentScoreRCR;

public class ScoreSetterRCRFactory {
	private static final EnumMap<ScoreRCR, ScoreSetter<TournamentScoreRCR, ScoreRCR>> buffer = new EnumMap<ScoreRCR, ScoreSetter<TournamentScoreRCR, ScoreRCR>>(ScoreRCR.class);

	public static ScoreSetter<TournamentScoreRCR, ScoreRCR> getScoreSetterRCR(final ScoreRCR mode) {
		if (mode != null) {
			if (!buffer.containsKey(mode)) {
				switch (mode) {
					case FINAL_SCORE:
						buffer.put(
							mode,
							(
								final TournamentScoreRCR score,
								final int value
							) -> {
								score.setFinalScore(value);
							}
						);
						break;
					case GAME_SCORE:
						buffer.put(
							mode,
							(
								final TournamentScoreRCR score,
								final int value
							) -> {
								score.setGameScore(value);
							}
						);
						break;
					case UMA_SCORE:
						buffer.put(
							mode,
							(
								final TournamentScoreRCR score,
								final int value
							) -> {
								score.setUmaScore(value);
							}
						);
						break;
					default:
						break;
				}
			}
			return buffer.get(mode);
		} else {
			return null;
		}
	}
}
