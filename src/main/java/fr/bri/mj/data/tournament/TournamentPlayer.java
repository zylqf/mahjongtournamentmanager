/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.data.tournament;

import java.text.DecimalFormat;

import fr.bri.mj.data.player.Player;

public class TournamentPlayer implements Comparable<TournamentPlayer> {

	private static final DecimalFormat ID_FORMAT = new DecimalFormat("000");

	private final int tournamentPlayerId;
	private Player player;
	private String teamName;

	public TournamentPlayer(
		final int tournamentPlayerId,
		final Player player,
		final String teamName
	) {
		this.tournamentPlayerId = tournamentPlayerId;
		this.player = player;
		this.teamName = teamName;
	}

	public int getTournamentPlayerId() {
		return tournamentPlayerId;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(final Player player) {
		this.player = player;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(final String teamName) {
		this.teamName = teamName;
	}

	public String getPlayerName(final String emptyPlayer) {
		final StringBuilder name = new StringBuilder();
		if (player != null) {
			name.append(player.getPlayerFirstName());
			name.append(" ");
			name.append(player.getPlayerLastName());
		} else {
			name.append(emptyPlayer);
			name.append(" ");
			name.append(ID_FORMAT.format(tournamentPlayerId));
		}
		return name.toString();
	}

	public String getIDPlayerName(final String emptyPlayer) {
		final StringBuilder name = new StringBuilder();
		name.append(ID_FORMAT.format(tournamentPlayerId));
		name.append(" ");
		if (player != null) {
			name.append(player.getPlayerFirstName());
			name.append(" ");
			name.append(player.getPlayerLastName());
		} else {
			name.append(emptyPlayer);
			name.append(" ");
			name.append(ID_FORMAT.format(tournamentPlayerId));
		}
		return name.toString();
	}

	@Override
	public int compareTo(final TournamentPlayer o) {
		return Integer.compare(
			tournamentPlayerId,
			o.tournamentPlayerId
		);
	}
}
