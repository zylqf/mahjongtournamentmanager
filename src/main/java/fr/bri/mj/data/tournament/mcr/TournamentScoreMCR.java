/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.data.tournament.mcr;

import fr.bri.mj.data.tournament.Tournament;
import fr.bri.mj.data.tournament.TournamentPlayer;
import fr.bri.mj.data.tournament.TournamentScore;

public class TournamentScoreMCR extends TournamentScore<TournamentScoreMCR, ScoreMCR> {

	private int tablePoint;
	private int penalty;
	private int totalPoint;
	private int matchPoint;

	public TournamentScoreMCR(
		final Tournament<TournamentScoreMCR, ScoreMCR> tournament,
		final int sessionId,
		final int tableId,
		final int tablePlayerId,
		final TournamentPlayer tournamentPlayer,
		final boolean scoreRecorded
	) {
		super(
			tournament,
			sessionId,
			tableId,
			tablePlayerId,
			tournamentPlayer,
			scoreRecorded,
			0,
			1
		);
		tablePoint = 0;
		penalty = 0;
		totalPoint = 0;
		matchPoint = 0;
	}

	public TournamentScoreMCR(
		final Tournament<TournamentScoreMCR, ScoreMCR> tournament,
		final int sessionId,
		final int tableId,
		final int tablePlayerId,
		final TournamentPlayer tournamentPlayer,
		final int substituted,
		final int ranking,
		final int tablePoint,
		final int penalty,
		final int totalPoint,
		final int matchPoint,
		final boolean scoreRecorded
	) {
		super(
			tournament,
			sessionId,
			tableId,
			tablePlayerId,
			tournamentPlayer,
			scoreRecorded,
			substituted,
			ranking
		);
		this.tablePoint = tablePoint;
		this.penalty = penalty;
		this.totalPoint = totalPoint;
		this.matchPoint = matchPoint;
	}

	public int getTablePoint() {
		return tablePoint;
	}

	public void setTablePoint(final int tablePoint) {
		this.tablePoint = tablePoint;
	}

	public int getPenalty() {
		return penalty;
	}

	public void setPenalty(final int penalty) {
		this.penalty = penalty;
	}

	public int getTotalPoint() {
		return totalPoint;
	}

	public void setTotalPoint(final int totalPoint) {
		this.totalPoint = totalPoint;
	}

	public int getMatchPoint() {
		return matchPoint;
	}

	public void setMatchPoint(final int matchPoint) {
		this.matchPoint = matchPoint;
	}
}
