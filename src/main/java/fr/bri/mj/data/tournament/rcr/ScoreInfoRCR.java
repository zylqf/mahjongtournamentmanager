package fr.bri.mj.data.tournament.rcr;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ScoreInfoRCR {

	public static final ScoreInfoRCR SCORE_INFO_RCR_15K;
	public static final ScoreInfoRCR SCORE_INFO_RCR_30K;

	static {
		{
			final List<Integer> uma15k = new ArrayList<Integer>();
			uma15k.add(15000);
			uma15k.add(5000);
			uma15k.add(-5000);
			uma15k.add(-15000);
			SCORE_INFO_RCR_15K = new ScoreInfoRCR(
				"15000",
				-15000,
				-15000,
				Collections.unmodifiableList(uma15k)
			);
		}

		{
			final List<Integer> uma30k = new ArrayList<Integer>();
			uma30k.add(30000);
			uma30k.add(10000);
			uma30k.add(-10000);
			uma30k.add(-30000);
			SCORE_INFO_RCR_30K = new ScoreInfoRCR(
				"30000",
				-30000,
				-30000,
				Collections.unmodifiableList(uma30k)
			);
		}
	}

	private final String name;
	private final int substitutedPlayerScore;
	private final int substitutedPlayerUMA;
	private final List<Integer> uma;

	private ScoreInfoRCR(
		final String name,
		final int substitutedPlayerScore,
		final int substitutedPlayerUMA,
		final List<Integer> uma
	) {
		super();
		this.name = name;
		this.substitutedPlayerScore = substitutedPlayerScore;
		this.substitutedPlayerUMA = substitutedPlayerUMA;
		this.uma = uma;
	}

	public String getName() {
		return name;
	}

	public int getSubstitutedPlayerScore() {
		return substitutedPlayerScore;
	}

	public int getSubstitutedPlayerUMA() {
		return substitutedPlayerUMA;
	}

	public List<Integer> getUma() {
		return uma;
	}
}
