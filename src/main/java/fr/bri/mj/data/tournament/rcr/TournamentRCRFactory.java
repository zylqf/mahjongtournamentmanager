/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.data.tournament.rcr;

import fr.bri.mj.data.tournament.Tournament;
import fr.bri.mj.data.tournament.TournamentFactory;
import fr.bri.mj.data.tournament.TournamentPlayer;

public class TournamentRCRFactory extends TournamentFactory<TournamentScoreRCR, ScoreRCR> {

	private static TournamentRCRFactory instance = new TournamentRCRFactory();

	public static TournamentRCRFactory getInstance() {
		return instance;
	}

	private TournamentRCRFactory() {
	}

	@Override
	public Tournament<TournamentScoreRCR, ScoreRCR> createTournament(
		final int id,
		final String name,
		final String leagueName,
		final int nbSessions,
		final int nbRoundRobinSessions,
		final int nbTables,
		final boolean withQualification,
		final boolean archived
	) {
		return new TournamentRuleSetRCR(
			id,
			name,
			leagueName,
			nbSessions,
			nbRoundRobinSessions,
			nbTables,
			withQualification,
			archived
		);
	}

	@Override
	public TournamentScoreRCR createTournamentScore(
		final Tournament<TournamentScoreRCR, ScoreRCR> tournament,
		final int sessionId,
		final int tableId,
		final int tablePlayerId,
		final TournamentPlayer tournamentPlayer,
		final boolean scoreRecorded
	) {
		return new TournamentScoreRCR(
			tournament,
			sessionId,
			tableId,
			tablePlayerId,
			tournamentPlayer,
			scoreRecorded
		);
	}
}
