/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.data.tournament.rcr;

import fr.bri.mj.data.tournament.Tournament;
import fr.bri.mj.data.tournament.TournamentPlayer;
import fr.bri.mj.data.tournament.TournamentScore;

public class TournamentScoreRCR extends TournamentScore<TournamentScoreRCR, ScoreRCR> {

	private int gameScore;
	private int umaScore;
	private int penalty;
	private int finalScore;

	public TournamentScoreRCR(
		final Tournament<TournamentScoreRCR, ScoreRCR> tournament,
		final int sessionId,
		final int tableId,
		final int tablePlayerId,
		final TournamentPlayer tournamentPlayer,
		final boolean scoreRecorded
	) {
		super(
			tournament,
			sessionId,
			tableId,
			tablePlayerId,
			tournamentPlayer,
			scoreRecorded,
			0,
			1
		);
		gameScore = 0;
		umaScore = 0;
		penalty = 0;
		finalScore = 0;
	}

	public TournamentScoreRCR(
		final Tournament<TournamentScoreRCR, ScoreRCR> tournament,
		final int sessionId,
		final int tableId,
		final int tablePlayerId,
		final TournamentPlayer tournamentPlayer,
		final int substituted,
		final int ranking,
		final int gameScore,
		final int umaScore,
		final int penalty,
		final int finalScore,
		final boolean scoreRecorded
	) {
		super(
			tournament,
			sessionId,
			tableId,
			tablePlayerId,
			tournamentPlayer,
			scoreRecorded,
			substituted,
			ranking
		);
		this.gameScore = gameScore;
		this.umaScore = umaScore;
		this.penalty = penalty;
		this.finalScore = finalScore;
	}

	public int getGameScore() {
		return gameScore;
	}

	public void setGameScore(final int gameScore) {
		this.gameScore = gameScore;
	}

	public int getUmaScore() {
		return umaScore;
	}

	public void setUmaScore(final int umaScore) {
		this.umaScore = umaScore;
	}

	public int getPenalty() {
		return penalty;
	}

	public void setPenalty(final int penalty) {
		this.penalty = penalty;
	}

	public int getFinalScore() {
		return finalScore;
	}

	public void setFinalScore(final int finalScore) {
		this.finalScore = finalScore;
	}
}
