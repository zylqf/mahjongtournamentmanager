/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.dataaccess.database;

import java.util.List;

import fr.bri.mj.dataaccess.database.entity.DatabasePlayer;
import fr.bri.mj.dataaccess.database.entity.DatabaseTournamentMCR;
import fr.bri.mj.dataaccess.database.entity.DatabaseTournamentMCRPlanning;
import fr.bri.mj.dataaccess.database.entity.DatabaseTournamentMCRPlayer;
import fr.bri.mj.dataaccess.database.entity.DatabaseTournamentMCRTable;
import fr.bri.mj.dataaccess.database.entity.DatabaseTournamentMCRTableId;
import fr.bri.mj.dataaccess.database.entity.DatabaseTournamentRCR;
import fr.bri.mj.dataaccess.database.entity.DatabaseTournamentRCRPlanning;
import fr.bri.mj.dataaccess.database.entity.DatabaseTournamentRCRPlayer;
import fr.bri.mj.dataaccess.database.entity.DatabaseTournamentRCRTable;
import fr.bri.mj.dataaccess.database.entity.DatabaseTournamentRCRTableId;
import fr.bri.mj.dataaccess.database.entity.DatabaseUIConfig;

class DataBaseDataAccess extends DatabaseConnection {

	// UI Configuration
	DatabaseUIConfig getUIConfig() {
		try {
			final DatabaseUIConfig config = session.find(
				DatabaseUIConfig.class,
				DatabaseUIConfig.DEFAULT_ID
			);
			if (config != null) {
				session.evict(config);
			}
			return config;
		} catch (final Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	void saveUIConfig(final DatabaseUIConfig config) {
		try {
			session.getTransaction().begin();
			session.saveOrUpdate(config);
			session.getTransaction().commit();
		} catch (final Exception e) {
			if (session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}
	}

	// Players
	boolean addPlayer(final DatabasePlayer player) {
		try {
			session.getTransaction().begin();
			session.save(player);
			session.getTransaction().commit();
			return true;
		} catch (final Exception e) {
			if (session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			return false;
		}
	}

	DatabasePlayer getPlayer(final int id) {
		try {
			return session.find(
				DatabasePlayer.class,
				id
			);
		} catch (final Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	List<DatabasePlayer> getAllPlayers() {
		try {
			return session.createQuery(
				"SELECT player FROM Player player",
				DatabasePlayer.class
			).getResultList();
		} catch (final Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	boolean updatePlayer(final DatabasePlayer player) {
		try {
			if (player != null) {
				session.getTransaction().begin();
				session.update(player);
				session.getTransaction().commit();
				return true;
			} else {
				if (session.getTransaction().isActive()) {
					session.getTransaction().rollback();
				}
				return false;
			}
		} catch (final Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	boolean deletePlayer(final DatabasePlayer player) {
		try {
			if (player != null) {
				session.getTransaction().begin();
				session.remove(player);
				session.getTransaction().commit();
				session.clear();
				return true;
			} else {
				return false;
			}
		} catch (final Exception e) {
			if (session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			return false;
		}
	}

	// Tournament MCR
	boolean addTournamentMCR(final DatabaseTournamentMCR tournament) {
		try {
			session.getTransaction().begin();
			session.save(tournament);
			for (final DatabaseTournamentMCRPlanning dbPlanning : tournament.getPlannings()) {
				session.save(dbPlanning);
			}
			for (final DatabaseTournamentMCRPlayer dbTournamentPlayer : tournament.getTournamentPlayers()) {
				session.save(dbTournamentPlayer);
			}
			for (final DatabaseTournamentMCRTable dbTable : tournament.getTables()) {
				session.save(dbTable);
			}
			session.getTransaction().commit();
			return true;
		} catch (final Exception e) {
			if (session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			return false;
		}
	}

	DatabaseTournamentMCR getTournamentMCR(final int id) {
		return session.find(
			DatabaseTournamentMCR.class,
			id
		);
	}

	List<DatabaseTournamentMCR> getAllTournamentMCR() {
		try {
			return session.createQuery(
				"SELECT tournamentMCR FROM TournamentMCR tournamentMCR",
				DatabaseTournamentMCR.class
			).getResultList();
		} catch (final Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	boolean updateTournamentMCR(final DatabaseTournamentMCR tournament) {
		try {
			if (tournament != null) {
				session.getTransaction().begin();
				session.update(tournament);
				for (final DatabaseTournamentMCRPlanning planning : tournament.getPlannings()) {
					session.update(planning);
				}
				for (final DatabaseTournamentMCRPlayer tournamentPlayer : tournament.getTournamentPlayers()) {
					session.update(tournamentPlayer);
				}
				session.getTransaction().commit();
				session.clear();
				return true;
			} else {
				return false;
			}
		} catch (final Exception e) {
			if (session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			return false;
		}
	}

	boolean deleteTournamentMCR(final DatabaseTournamentMCR tournament) {
		try {
			if (tournament != null) {
				session.getTransaction().begin();
				for (final DatabaseTournamentMCRTable dbTable : tournament.getTables()) {
					session.delete(dbTable);
				}
				for (final DatabaseTournamentMCRPlayer dbTournamentPlayer : tournament.getTournamentPlayers()) {
					session.delete(dbTournamentPlayer);
				}
				for (final DatabaseTournamentMCRPlanning dbPlanning : tournament.getPlannings()) {
					session.delete(dbPlanning);
				}
				session.delete(tournament);
				session.getTransaction().commit();
				session.clear();
				return true;
			} else {
				return false;
			}
		} catch (final Exception e) {
			if (session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			return false;
		}
	}

	// Tournament Score MCR
	boolean addTournamentMCRScore(final DatabaseTournamentMCRTable score) {
		try {
			if (score != null) {
				session.getTransaction().begin();
				session.save(score);
				session.getTransaction().commit();
				return true;
			} else {
				return false;
			}
		} catch (final Exception e) {
			if (session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			return false;
		}
	}

	DatabaseTournamentMCRTable getTournamentMCRScore(final DatabaseTournamentMCRTableId dbTableId) {
		return session.find(
			DatabaseTournamentMCRTable.class,
			dbTableId
		);
	}

	boolean updateTournamentMCRScore(final DatabaseTournamentMCRTable score) {
		try {
			if (score != null) {
				session.getTransaction().begin();
				session.update(score);
				session.getTransaction().commit();
				return true;
			} else {
				return false;
			}
		} catch (final Exception e) {
			if (session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			return false;
		}
	}

	boolean deleteTournamentMCRScore(final DatabaseTournamentMCRTable score) {
		try {
			if (score != null) {
				session.getTransaction().begin();
				session.delete(score);
				session.getTransaction().commit();
				return true;
			} else {
				return false;
			}
		} catch (final Exception e) {
			if (session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			return false;
		}
	}

	// Tournament RCR
	boolean addTournamentRCR(final DatabaseTournamentRCR tournament) {
		try {
			session.getTransaction().begin();
			session.save(tournament);
			for (final DatabaseTournamentRCRPlanning dbPlanning : tournament.getPlannings()) {
				session.save(dbPlanning);
			}
			for (final DatabaseTournamentRCRPlayer dbTournamentPlayer : tournament.getTournamentPlayers()) {
				session.save(dbTournamentPlayer);
			}
			for (final DatabaseTournamentRCRTable dbTable : tournament.getTables()) {
				session.save(dbTable);
			}
			session.getTransaction().commit();
			return true;
		} catch (final Exception e) {
			if (session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			return false;
		}
	}

	DatabaseTournamentRCR getTournamentRCR(final int id) {
		return session.find(
			DatabaseTournamentRCR.class,
			id
		);
	}

	List<DatabaseTournamentRCR> getAllTournamentRCR() {
		try {
			return session.createQuery(
				"SELECT tournamentRCR FROM TournamentRCR tournamentRCR",
				DatabaseTournamentRCR.class
			).getResultList();
		} catch (final Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	boolean updateTournamentRCR(final DatabaseTournamentRCR tournament) {
		try {
			if (tournament != null) {
				session.getTransaction().begin();
				session.update(tournament);
				for (final DatabaseTournamentRCRPlanning planning : tournament.getPlannings()) {
					session.update(planning);
				}
				for (final DatabaseTournamentRCRPlayer tournamentPlayer : tournament.getTournamentPlayers()) {
					session.update(tournamentPlayer);
				}
				session.getTransaction().commit();
				session.clear();
				return true;
			} else {
				return false;
			}
		} catch (final Exception e) {
			if (session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			return false;
		}
	}

	boolean deleteTournamentRCR(final DatabaseTournamentRCR tournament) {
		try {
			if (tournament != null) {
				session.getTransaction().begin();
				for (final DatabaseTournamentRCRTable dbTable : tournament.getTables()) {
					session.delete(dbTable);
				}
				for (final DatabaseTournamentRCRPlayer dbTournamentPlayer : tournament.getTournamentPlayers()) {
					session.delete(dbTournamentPlayer);
				}
				for (final DatabaseTournamentRCRPlanning dbPlanning : tournament.getPlannings()) {
					session.delete(dbPlanning);
				}
				session.delete(tournament);
				session.getTransaction().commit();
				session.clear();
				return true;
			} else {
				return false;
			}
		} catch (final Exception e) {
			if (session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			return false;
		}
	}

	// Tournament Score RCR
	boolean addTournamentRCRScore(final DatabaseTournamentRCRTable score) {
		try {
			if (score != null) {
				session.getTransaction().begin();
				session.save(score);
				session.getTransaction().commit();
				return true;
			} else {
				return false;
			}
		} catch (final Exception e) {
			if (session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			return false;
		}
	}

	DatabaseTournamentRCRTable getTournamentRCRScore(final DatabaseTournamentRCRTableId dbTableId) {
		return session.find(
			DatabaseTournamentRCRTable.class,
			dbTableId
		);
	}

	boolean updateTournamentRCRScore(final DatabaseTournamentRCRTable score) {
		try {
			if (score != null) {
				session.getTransaction().begin();
				session.update(score);
				session.getTransaction().commit();
				return true;
			} else {
				return false;
			}
		} catch (final Exception e) {
			if (session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			return false;
		}
	}

	boolean deleteTournamentRCRScore(final DatabaseTournamentRCRTable score) {
		try {
			if (score != null) {
				session.getTransaction().begin();
				session.delete(score);
				session.getTransaction().commit();
				return true;
			} else {
				return false;
			}
		} catch (final Exception e) {
			if (session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			return false;
		}
	}
}
