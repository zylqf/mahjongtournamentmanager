/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.dataaccess.database;

import fr.bri.mj.data.tournament.mcr.ScoreMCR;
import fr.bri.mj.data.tournament.mcr.TournamentScoreMCR;
import fr.bri.mj.data.tournament.rcr.ScoreRCR;
import fr.bri.mj.data.tournament.rcr.TournamentScoreRCR;
import fr.bri.mj.dataaccess.ConfigurationManager;
import fr.bri.mj.dataaccess.ConnectionManager;
import fr.bri.mj.dataaccess.ManagerFactory;
import fr.bri.mj.dataaccess.PlayerManager;
import fr.bri.mj.dataaccess.TournamentManager;

public class DataBaseManagerFactory implements ManagerFactory {

	private static DataBaseManagerFactory instance;

	public static DataBaseManagerFactory getInstance() {
		if (instance == null) {
			instance = new DataBaseManagerFactory();
		}
		return instance;
	}

	private final ConnectionManager connectionManager;
	private final ConfigurationManager configurationManager;
	private final PlayerManager playerManager;
	private final TournamentManager<TournamentScoreMCR, ScoreMCR> tournamentManagerMCR;
	private final TournamentManager<TournamentScoreRCR, ScoreRCR> tournamentManagerRCR;

	private DataBaseManagerFactory() {
		final DataBaseDataAccess databaseDataAccess = new DataBaseDataAccess();
		connectionManager = new DataBaseConnectionManager(databaseDataAccess);
		configurationManager = new DataBaseConfigurationManager(databaseDataAccess);
		playerManager = new DataBasePlayerManager(databaseDataAccess);
		tournamentManagerMCR = new DataBaseTournamentManagerMCR(
			databaseDataAccess,
			playerManager
		);
		tournamentManagerRCR = new DataBaseTournamentManagerRCR(
			databaseDataAccess,
			playerManager
		);
	}

	@Override
	public ConnectionManager getConnectionManager() {
		return connectionManager;
	}

	@Override
	public ConfigurationManager getConfigurationManager() {
		return configurationManager;
	}

	@Override
	public PlayerManager getPlayerManager() {
		return playerManager;
	}

	@Override
	public TournamentManager<TournamentScoreMCR, ScoreMCR> getTournamentManagerMCR() {
		return tournamentManagerMCR;
	}

	@Override
	public TournamentManager<TournamentScoreRCR, ScoreRCR> getTournamentManagerRCR() {
		return tournamentManagerRCR;
	}
}
