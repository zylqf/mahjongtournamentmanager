/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.dataaccess.database;

import java.util.Collections;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import fr.bri.mj.data.player.Player;
import fr.bri.mj.dataaccess.PlayerManager;
import fr.bri.mj.dataaccess.database.entity.DatabasePlayer;

class DataBasePlayerManager implements PlayerManager {

	private final DataBaseDataAccess databaseDataAccess;

	private boolean playerCacheInitialized;
	private final SortedSet<Player> playerCacheSet;
	private final Set<Player> playerCacheSetExternal;

	DataBasePlayerManager(final DataBaseDataAccess databaseDataAccess) {
		this.databaseDataAccess = databaseDataAccess;

		playerCacheInitialized = false;
		playerCacheSet = new TreeSet<Player>();
		playerCacheSetExternal = Collections.unmodifiableSet(playerCacheSet);
	}

	@Override
	public int getAvailablePlayerId() {
		initializePlayerCache();
		int id = 1;
		for (final Player existingPlayer : playerCacheSet) {
			if (existingPlayer.getId() == id) {
				id++;
			} else {
				break;
			}
		}
		return id;
	}

	@Override
	public boolean addPlayer(final Player player) {
		initializePlayerCache();
		if (player != null && !playerCacheSet.contains(player)) {
			DatabasePlayer dbPlayer;
			dbPlayer = new DatabasePlayer(
				player.getId(),
				player.getPlayerFirstName(),
				player.getPlayerLastName(),
				player.getPlayerNationality(),
				player.getClub(),
				player.getLicense()
			);

			final boolean added = databaseDataAccess.addPlayer(dbPlayer);
			if (added) {
				playerCacheSet.add(player);
			}
			return added;
		} else {
			return false;
		}
	}

	@Override
	public Set<Player> getAllPlayers() {
		initializePlayerCache();
		return playerCacheSetExternal;
	}

	@Override
	public boolean updatePlayer(final Player player) {
		if (player != null && playerCacheSet.contains(player)) {
			final DatabasePlayer dbPlayer = databaseDataAccess.getPlayer(player.getId());
			if (dbPlayer != null) {
				dbPlayer.setPlayerFirstName(player.getPlayerFirstName());
				dbPlayer.setPlayerLastName(player.getPlayerLastName());
				dbPlayer.setPlayerNationality(player.getPlayerNationality());
				dbPlayer.setClub(player.getClub());
				dbPlayer.setLicense(player.getLicense());
				return databaseDataAccess.updatePlayer(dbPlayer);
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@Override
	public boolean deletePlayer(final Player player) {
		if (player != null && playerCacheSet.contains(player)) {
			final DatabasePlayer dbPlayer = databaseDataAccess.getPlayer(player.getId());
			if (dbPlayer != null) {
				final boolean deleted = databaseDataAccess.deletePlayer(dbPlayer);
				if (deleted) {
					playerCacheSet.remove(player);
				}
				return deleted;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	private void initializePlayerCache() {
		if (!playerCacheInitialized) {
			for (final DatabasePlayer dbPlayer : databaseDataAccess.getAllPlayers()) {
				final Player player = new Player(
					dbPlayer.getId(),
					dbPlayer.getPlayerFirstName(),
					dbPlayer.getPlayerLastName(),
					dbPlayer.getPlayerNationality(),
					dbPlayer.getClub(),
					dbPlayer.getLicense()
				);
				playerCacheSet.add(player);
			}
			playerCacheInitialized = true;
		}
	}
}
