/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.dataaccess.database;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import fr.bri.mj.data.player.Player;
import fr.bri.mj.data.tournament.Tournament;
import fr.bri.mj.data.tournament.TournamentPlayer;
import fr.bri.mj.data.tournament.mcr.ScoreMCR;
import fr.bri.mj.data.tournament.mcr.TournamentRuleSetMCR;
import fr.bri.mj.data.tournament.mcr.TournamentScoreMCR;
import fr.bri.mj.dataaccess.PlayerManager;
import fr.bri.mj.dataaccess.TournamentManager;
import fr.bri.mj.dataaccess.database.entity.DatabaseTournamentMCR;
import fr.bri.mj.dataaccess.database.entity.DatabaseTournamentMCRPlanning;
import fr.bri.mj.dataaccess.database.entity.DatabaseTournamentMCRPlayer;
import fr.bri.mj.dataaccess.database.entity.DatabaseTournamentMCRTable;
import fr.bri.mj.dataaccess.database.entity.DatabaseTournamentMCRTableId;

class DataBaseTournamentManagerMCR implements TournamentManager<TournamentScoreMCR, ScoreMCR> {

	private final DataBaseDataAccess databaseDataAccess;
	private final PlayerManager playerManager;

	private boolean tournamentCacheInitialized;
	private final SortedSet<Tournament<TournamentScoreMCR, ScoreMCR>> tournamentCacheSet;
	private final Set<Tournament<TournamentScoreMCR, ScoreMCR>> tournamentCacheSetExternal;

	DataBaseTournamentManagerMCR(
		final DataBaseDataAccess databaseDataAccess,
		final PlayerManager playerManager
	) {
		this.databaseDataAccess = databaseDataAccess;
		this.playerManager = playerManager;

		tournamentCacheInitialized = false;
		tournamentCacheSet = new TreeSet<Tournament<TournamentScoreMCR, ScoreMCR>>();
		tournamentCacheSetExternal = Collections.unmodifiableSet(tournamentCacheSet);
	}

	@Override
	public int getAvailableTournamentId() {
		initializeTournamentCache();
		int id = 1;
		if (tournamentCacheSet.size() > 0) {
			id = tournamentCacheSet.first().getId() + 1;
		}
		return id;
	}

	@Override
	public boolean addTournament(final Tournament<TournamentScoreMCR, ScoreMCR> tournament) {
		initializeTournamentCache();
		if (tournament != null && !tournamentCacheSet.contains(tournament)) {
			final DatabaseTournamentMCR dbTournament = new DatabaseTournamentMCR(
				tournament.getId(),
				tournament.getName(),
				tournament.getLeagueName(),
				tournament.getNbSessions(),
				tournament.getNbRoundRobinSessions(),
				tournament.getNbTables(),
				tournament.isWithQualification(),
				tournament.isArchived()
			);

			for (final Integer sessionId : tournament.getPlannings().keySet()) {
				final DatabaseTournamentMCRPlanning dbPlanning = new DatabaseTournamentMCRPlanning(
					dbTournament,
					sessionId,
					tournament.getPlannings().get(sessionId)
				);
				dbTournament.getPlannings().add(dbPlanning);
			}

			final Map<TournamentPlayer, DatabaseTournamentMCRPlayer> mapTournamentPlayerToDbTournamentPlayer = new HashMap<TournamentPlayer, DatabaseTournamentMCRPlayer>();
			for (final TournamentPlayer tournamentPlayer : tournament.getTournamentPlayers()) {
				final Integer playerId = tournamentPlayer.getPlayer() != null
					? tournamentPlayer.getPlayer().getId()
					: null;
				final DatabaseTournamentMCRPlayer dbTournamentPlayer = new DatabaseTournamentMCRPlayer(
					dbTournament,
					tournamentPlayer.getTournamentPlayerId(),
					playerId,
					tournamentPlayer.getTeamName()
				);
				dbTournament.getTournamentPlayers().add(dbTournamentPlayer);
				mapTournamentPlayerToDbTournamentPlayer.put(
					tournamentPlayer,
					dbTournamentPlayer
				);
			}

			for (final TournamentScoreMCR table : tournament.getTables()) {
				final DatabaseTournamentMCRTable dbTable = new DatabaseTournamentMCRTable(
					dbTournament,
					table.getSessionId(),
					table.getTableId(),
					table.getTablePlayerId(),
					mapTournamentPlayerToDbTournamentPlayer.get(table.getTournamentPlayer()),
					table.getSubstituted(),
					table.getRanking(),
					table.getTablePoint(),
					table.getPenalty(),
					table.getTotalPoint(),
					table.getMatchPoint(),
					table.isScoreRecorded()
				);
				dbTournament.getTables().add(dbTable);
			}

			final boolean added = databaseDataAccess.addTournamentMCR(dbTournament);
			if (added) {
				tournamentCacheSet.add(tournament);
			}
			return added;
		} else {
			return false;
		}
	}

	@Override
	public Set<Tournament<TournamentScoreMCR, ScoreMCR>> getAllTournament() {
		initializeTournamentCache();
		return tournamentCacheSetExternal;
	}

	@Override
	public boolean updateTournament(final Tournament<TournamentScoreMCR, ScoreMCR> tournament) {
		if (tournament != null && tournamentCacheSet.contains(tournament)) {
			final DatabaseTournamentMCR dbTournament = databaseDataAccess.getTournamentMCR(tournament.getId());
			if (dbTournament != null) {
				final Map<Integer, TournamentPlayer> mapTournamentPlayerIdToTournamentPlayer = new HashMap<Integer, TournamentPlayer>();
				for (final TournamentPlayer tournamentPlayer : tournament.getTournamentPlayers()) {
					mapTournamentPlayerIdToTournamentPlayer.put(
						tournamentPlayer.getTournamentPlayerId(),
						tournamentPlayer
					);
				}

				final Map<Integer, DatabaseTournamentMCRPlayer> mapTournamentPlayerIdToDatabaseTournamentPlayer = new HashMap<Integer, DatabaseTournamentMCRPlayer>();
				for (final DatabaseTournamentMCRPlayer databaseTournamentPlayer : dbTournament.getTournamentPlayers()) {
					mapTournamentPlayerIdToDatabaseTournamentPlayer.put(
						databaseTournamentPlayer.getTournamentPlayerId(),
						databaseTournamentPlayer
					);
				}

				final Map<DatabaseTournamentMCRTableId, TournamentScoreMCR> mapScoreIdToTable = new HashMap<DatabaseTournamentMCRTableId, TournamentScoreMCR>();
				for (final TournamentScoreMCR table : tournament.getTables()) {
					final DatabaseTournamentMCRTableId id = new DatabaseTournamentMCRTableId(
						tournament.getId(),
						table.getSessionId(),
						table.getTableId(),
						table.getTablePlayerId()
					);
					mapScoreIdToTable.put(
						id,
						table
					);
				}

				final Map<DatabaseTournamentMCRTableId, DatabaseTournamentMCRTable> mapScoreIdToDatabaseTable = new HashMap<DatabaseTournamentMCRTableId, DatabaseTournamentMCRTable>();
				for (final DatabaseTournamentMCRTable table : dbTournament.getTables()) {
					final DatabaseTournamentMCRTableId id = new DatabaseTournamentMCRTableId(
						tournament.getId(),
						table.getSessionId(),
						table.getTableId(),
						table.getTablePlayerId()
					);
					mapScoreIdToDatabaseTable.put(
						id,
						table
					);
				}

				final Set<DatabaseTournamentMCRTableId> tableToDelete = new HashSet<DatabaseTournamentMCRTableId>(mapScoreIdToDatabaseTable.keySet());
				tableToDelete.removeAll(mapScoreIdToTable.keySet());

				final Set<DatabaseTournamentMCRTableId> tableToAdd = new HashSet<DatabaseTournamentMCRTableId>(mapScoreIdToTable.keySet());
				tableToAdd.removeAll(mapScoreIdToDatabaseTable.keySet());

				dbTournament.setName(tournament.getName());
				dbTournament.setLeagueName(tournament.getLeagueName());
				dbTournament.setArchived(tournament.isArchived());
				for (final DatabaseTournamentMCRPlanning dbPlanning : dbTournament.getPlannings()) {
					dbPlanning.setStartTime(tournament.getPlannings().get(dbPlanning.getSessionId()));
				}
				for (final DatabaseTournamentMCRPlayer dbTournamentPlayer : dbTournament.getTournamentPlayers()) {
					final TournamentPlayer tournamentPlayer = mapTournamentPlayerIdToTournamentPlayer.get(dbTournamentPlayer.getTournamentPlayerId());
					final Integer playerId = tournamentPlayer.getPlayer() != null
						? tournamentPlayer.getPlayer().getId()
						: null;
					dbTournamentPlayer.setPlayerId(playerId);
					dbTournamentPlayer.setTeamName(tournamentPlayer.getTeamName());
				}
				for (final DatabaseTournamentMCRTableId id : tableToDelete) {
					final DatabaseTournamentMCRTable databaseTable = mapScoreIdToDatabaseTable.get(id);
					databaseDataAccess.deleteTournamentMCRScore(databaseTable);
				}
				for (final DatabaseTournamentMCRTableId id : tableToAdd) {
					final TournamentScoreMCR table = mapScoreIdToTable.get(id);
					final DatabaseTournamentMCRTable dbTable = new DatabaseTournamentMCRTable(
						dbTournament,
						table.getSessionId(),
						table.getTableId(),
						table.getTablePlayerId(),
						mapTournamentPlayerIdToDatabaseTournamentPlayer.get(table.getTournamentPlayer().getTournamentPlayerId()),
						table.getSubstituted(),
						table.getRanking(),
						table.getTablePoint(),
						table.getPenalty(),
						table.getTotalPoint(),
						table.getMatchPoint(),
						table.isScoreRecorded()
					);
					databaseDataAccess.addTournamentMCRScore(dbTable);
				}

				return databaseDataAccess.updateTournamentMCR(dbTournament);
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@Override
	public boolean updateTournamentScore(final TournamentScoreMCR score) {
		final DatabaseTournamentMCRTableId dbTableId = new DatabaseTournamentMCRTableId(
			score.getTournament().getId(),
			score.getSessionId(),
			score.getTableId(),
			score.getTablePlayerId()
		);
		final DatabaseTournamentMCRTable dbTable = databaseDataAccess.getTournamentMCRScore(dbTableId);

		if (dbTable != null) {
			dbTable.setSubstituted(score.getSubstituted());
			dbTable.setRanking(score.getRanking());
			dbTable.setTablePoint(score.getTablePoint());
			dbTable.setPenalty(score.getPenalty());
			dbTable.setTotalPoint(score.getTotalPoint());
			dbTable.setMatchPoint(score.getMatchPoint());
			dbTable.setScoreRecorded(score.isScoreRecorded());
			return databaseDataAccess.updateTournamentMCRScore(dbTable);
		} else {
			return false;
		}
	}

	@Override
	public boolean deleteTournament(final Tournament<TournamentScoreMCR, ScoreMCR> tournament) {
		initializeTournamentCache();
		if (tournament != null && tournamentCacheSet.contains(tournament)) {
			final DatabaseTournamentMCR dbTournament = databaseDataAccess.getTournamentMCR(tournament.getId());
			if (dbTournament != null) {
				final boolean deleted = databaseDataAccess.deleteTournamentMCR(dbTournament);
				if (deleted) {
					tournamentCacheSet.remove(tournament);
				}
				return deleted;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	private void initializeTournamentCache() {
		if (!tournamentCacheInitialized) {
			final Map<Integer, Player> mapPlayerIdToPlayer = new HashMap<Integer, Player>();
			final Set<Player> players = playerManager.getAllPlayers();
			for (final Player player : players) {
				mapPlayerIdToPlayer.put(
					player.getId(),
					player
				);
			}

			final List<DatabaseTournamentMCR> dbTournaments = databaseDataAccess.getAllTournamentMCR();
			for (final DatabaseTournamentMCR dbTournament : dbTournaments) {
				final Tournament<TournamentScoreMCR, ScoreMCR> tournament = new TournamentRuleSetMCR(
					dbTournament.getId(),
					dbTournament.getName(),
					dbTournament.getLeagueName(),
					dbTournament.getNbSessions(),
					dbTournament.getNbRoundRobinSessions(),
					dbTournament.getNbTables(),
					dbTournament.isWithQualification(),
					dbTournament.isArchived()
				);
				for (final DatabaseTournamentMCRPlanning dbPlanning : dbTournament.getPlannings()) {
					tournament.getPlannings().put(
						dbPlanning.getSessionId(),
						dbPlanning.getStartTime()
					);
				}
				final Map<DatabaseTournamentMCRPlayer, TournamentPlayer> mapDbTournamentPlayerToTournamentPlayer = new HashMap<DatabaseTournamentMCRPlayer, TournamentPlayer>();
				for (final DatabaseTournamentMCRPlayer dbTournamentPlayer : dbTournament.getTournamentPlayers()) {
					final Player player = dbTournamentPlayer.getPlayerId() != null
						? mapPlayerIdToPlayer.get(dbTournamentPlayer.getPlayerId())
						: null;
					final TournamentPlayer tournamentPlayer = new TournamentPlayer(
						dbTournamentPlayer.getTournamentPlayerId(),
						player,
						dbTournamentPlayer.getTeamName()
					);
					tournament.getTournamentPlayers().add(tournamentPlayer);
					mapDbTournamentPlayerToTournamentPlayer.put(
						dbTournamentPlayer,
						tournamentPlayer
					);
				}
				for (final DatabaseTournamentMCRTable dbTable : dbTournament.getTables()) {
					final TournamentScoreMCR score = new TournamentScoreMCR(
						tournament,
						dbTable.getSessionId(),
						dbTable.getTableId(),
						dbTable.getTablePlayerId(),
						mapDbTournamentPlayerToTournamentPlayer.get(dbTable.getTournamentPlayer()),
						dbTable.getSubstituted(),
						dbTable.getRanking(),
						dbTable.getTablePoint(),
						dbTable.getPenalty(),
						dbTable.getTotalPoint(),
						dbTable.getMatchPoint(),
						dbTable.isScoreRecorded()
					);
					tournament.getTables().add(score);
				}
				tournamentCacheSet.add(tournament);
			}
			tournamentCacheInitialized = true;
		}
	}
}
