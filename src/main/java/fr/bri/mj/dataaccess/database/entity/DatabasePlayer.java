/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.dataaccess.database.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import fr.bri.mj.data.player.CountryCode;

@Entity(name = "Player")
@Table(name = "PLAYER")
public class DatabasePlayer extends DatabaseEntity implements Comparable<DatabasePlayer> {

	@Id
	@Column(
		name = "ID",
		nullable = false,
		updatable = false
	)
	private int id;

	@Column(
		name = "FIRST_NAME",
		nullable = false,
		length = 64
	)
	private String playerFirstName;

	@Column(
		name = "LAST_NAME",
		nullable = false,
		length = 64
	)
	private String playerLastName;

	@Column(
		name = "NATIONALITY",
		nullable = false
	)
	@Enumerated(EnumType.ORDINAL)
	private CountryCode playerNationality;

	@Column(
		name = "CLUB",
		length = 8
	)
	private String club;

	@Column(
		name = "LICENSE",
		length = 16
	)
	private String license;

	public DatabasePlayer() {
	}

	public DatabasePlayer(
		final int id,
		final String playerFirstName,
		final String playerLastName,
		final CountryCode playerNationality,
		final String club,
		final String license
	) {
		this.id = id;
		this.playerFirstName = playerFirstName;
		this.playerLastName = playerLastName;
		this.playerNationality = playerNationality;
		this.club = club;
		this.license = license;
	}

	private DatabasePlayer(
		final int id,
		final String playerFirstName,
		final String playerLastName,
		final int playerNationality,
		final String club,
		final String license
	) {
		this.id = id;
		this.playerFirstName = playerFirstName;
		this.playerLastName = playerLastName;
		this.playerNationality = CountryCode.values()[playerNationality];
		this.club = club;
		this.license = license;
	}

	public int getId() {
		return id;
	}

	public String getPlayerFirstName() {
		return playerFirstName;
	}

	public void setPlayerFirstName(final String playerFirstName) {
		if (playerFirstName != null) {
			this.playerFirstName = playerFirstName;
		}
	}

	public String getPlayerLastName() {
		return playerLastName;
	}

	public void setPlayerLastName(final String playerLastName) {
		if (playerLastName != null) {
			this.playerLastName = playerLastName;
		}
	}

	public CountryCode getPlayerNationality() {
		return playerNationality;
	}

	public void setPlayerNationality(final CountryCode playerNationality) {
		if (playerNationality != null) {
			this.playerNationality = playerNationality;
		}
	}

	public String getClub() {
		return club;
	}

	public void setClub(final String club) {
		this.club = club;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(final String license) {
		this.license = license;
	}

	private static final String[] TITLES = {
		"ID",
		"FIRST_NAME",
		"LAST_NAME",
		"NATIONALITY",
		"CLUB",
		"LICENSE"
	};

	public static String getTitleString() {
		return String.join(
			FIELD_SEPARATOR,
			TITLES
		);
	}

	public static String toFieldString(final DatabasePlayer player) {
		return String.join(
			FIELD_SEPARATOR,
			Integer.toString(player.id),
			player.playerFirstName,
			player.playerLastName,
			Integer.toString(player.playerNationality.ordinal()),
			player.club,
			player.license
		);
	}

	public static DatabasePlayer fromFieldString(final String fieldString) {
		final String fields[] = fieldString.split(
			FIELD_SEPARATOR,
			-1
		);
		return new DatabasePlayer(
			Integer.parseInt(fields[0]),
			fields[1],
			fields[2],
			Integer.parseInt(fields[3]),
			fields[4],
			fields[5]
		);
	}

	@Override
	public int compareTo(final DatabasePlayer o) {
		return Integer.compare(
			id,
			o.id
		);
	}
}
