/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.dataaccess.database.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name = "TournamentMCR")
@Table(name = "MCR_TOURNAMENT")
public class DatabaseTournamentMCR extends DatabaseEntity implements Comparable<DatabaseTournamentMCR> {

	@Id
	@Column(
		name = "ID",
		nullable = false,
		updatable = false
	)
	private int id;

	@Column(
		name = "NAME",
		length = 64,
		nullable = false,
		unique = true
	)
	private String name;

	@Column(
		name = "LEAGUE_NAME",
		length = 64,
		nullable = false
	)
	private String leagueName;

	@Column(
		name = "NB_SESSIONS",
		nullable = false,
		updatable = false
	)
	private int nbSessions;

	@Column(
		name = "NB_ROUNDROBIN_SESSIONS",
		nullable = false,
		updatable = false
	)
	private int nbRoundRobinSessions;

	@Column(
		name = "NB_TABLES",
		nullable = false,
		updatable = false
	)
	private int nbTables;

	@Column(
		name = "WITH_QUALIFICATION",
		nullable = false
	)
	private boolean withQualification;

	@Column(
		name = "IS_ARCHIVED",
		nullable = false
	)
	private boolean archived;

	@OneToMany(
		mappedBy = "tournament",
		fetch = FetchType.EAGER
	)
	private Set<DatabaseTournamentMCRPlanning> plannings;

	@OneToMany(
		mappedBy = "tournament",
		fetch = FetchType.EAGER
	)
	private Set<DatabaseTournamentMCRPlayer> tournamentPlayers;

	@OneToMany(
		mappedBy = "tournament",
		fetch = FetchType.EAGER
	)
	private Set<DatabaseTournamentMCRTable> tables;

	public DatabaseTournamentMCR() {
	}

	public DatabaseTournamentMCR(
		final int id,
		final String name,
		final String leagueName,
		final int nbSessions,
		final int nbRoundRobinSessions,
		final int nbTables,
		final boolean withQualification,
		final boolean archived
	) {
		this.id = id;
		this.name = name;
		this.leagueName = leagueName;
		this.nbSessions = nbSessions;
		this.nbRoundRobinSessions = nbRoundRobinSessions;
		this.nbTables = nbTables;
		this.withQualification = withQualification;
		this.archived = archived;
		plannings = new HashSet<DatabaseTournamentMCRPlanning>();
		tournamentPlayers = new HashSet<DatabaseTournamentMCRPlayer>();
		tables = new HashSet<DatabaseTournamentMCRTable>();
	}

	public int getId() {
		return id;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getLeagueName() {
		return leagueName;
	}

	public void setLeagueName(final String leagueName) {
		this.leagueName = leagueName;
	}

	public int getNbSessions() {
		return nbSessions;
	}

	public int getNbTables() {
		return nbTables;
	}

	public boolean isWithQualification() {
		return withQualification;
	}

	public int getNbRoundRobinSessions() {
		return nbRoundRobinSessions;
	}

	public boolean isArchived() {
		return archived;
	}

	public void setArchived(final boolean archived) {
		this.archived = archived;
	}

	public Set<DatabaseTournamentMCRPlanning> getPlannings() {
		return plannings;
	}

	public Set<DatabaseTournamentMCRPlayer> getTournamentPlayers() {
		return tournamentPlayers;
	}

	public Set<DatabaseTournamentMCRTable> getTables() {
		return tables;
	}

	private static final String[] TITLES = {
		"ID",
		"NAME",
		"LEAGUE_NAME",
		"NB_SESSIONS",
		"NB_ROUND_ROBIN_SESSIONS",
		"NB_TABLES",
		"WITH_QUALIFICATION",
		"NB_QUALIFICATION_SESSIONS",
		"IS_ARCHIVED"
	};

	public static String getTitleString() {
		return String.join(
			FIELD_SEPARATOR,
			TITLES
		);
	}

	public static String toFieldString(final DatabaseTournamentMCR tournament) {
		return String.join(
			FIELD_SEPARATOR,
			Integer.toString(tournament.id),
			tournament.name,
			tournament.leagueName,
			Integer.toString(tournament.nbSessions),
			Integer.toString(tournament.nbRoundRobinSessions),
			Integer.toString(tournament.nbTables),
			Boolean.toString(tournament.withQualification),
			Boolean.toString(tournament.archived)
		);
	}

	public static DatabaseTournamentMCR fromFieldString(final String fieldString) {
		final String fields[] = fieldString.split(
			FIELD_SEPARATOR,
			-1
		);
		return new DatabaseTournamentMCR(
			Integer.parseInt(fields[0]),
			fields[1],
			fields[2],
			Integer.parseInt(fields[3]),
			Integer.parseInt(fields[4]),
			Integer.parseInt(fields[5]),
			Boolean.parseBoolean(fields[6]),
			Boolean.parseBoolean(fields[7])
		);
	}

	@Override
	public int compareTo(final DatabaseTournamentMCR o) {
		return Integer.compare(
			id,
			o.id
		);
	}
}
