/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.dataaccess.database.entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name = "TournamentMCRPlanning")
@Table(name = "MCR_TOURNAMENT_PLANNING")
public class DatabaseTournamentMCRPlanning extends DatabaseEntity implements Comparable<DatabaseTournamentMCRPlanning> {

	@EmbeddedId
	private DatabaseTournamentMCRPlanningId id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(
		name = "MCR_TOURNAMENT_ID",
		referencedColumnName = "ID",
		nullable = false,
		insertable = false,
		updatable = false
	)
	private DatabaseTournamentMCR tournament;

	@Column(
		name = "START_TIME",
		nullable = false
	)
	@Temporal(TemporalType.TIMESTAMP)
	private Date startTime;

	public DatabaseTournamentMCRPlanning() {
	}

	public DatabaseTournamentMCRPlanning(
		final DatabaseTournamentMCR tournament,
		final int sessionId,
		final Date startTime
	) {
		id = new DatabaseTournamentMCRPlanningId(
			tournament.getId(),
			sessionId
		);
		this.tournament = tournament;
		this.startTime = startTime;
	}

	private DatabaseTournamentMCRPlanning(
		final int tournamentId,
		final int sessionId,
		final Date startTime
	) {
		id = new DatabaseTournamentMCRPlanningId(
			tournamentId,
			sessionId
		);
		this.startTime = startTime;
	}

	public DatabaseTournamentMCRPlanningId getId() {
		return id;
	}

	public DatabaseTournamentMCR getTournament() {
		return tournament;
	}

	public int getSessionId() {
		return id.getSessionId();
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(final Date startTime) {
		this.startTime = startTime;
	}

	private static final String[] TITLES = {
		"MCR_TOURNAMENT_ID",
		"SESSION_ID",
		"START_TIME"
	};

	public static String getTitleString() {
		return String.join(
			FIELD_SEPARATOR,
			TITLES
		);
	}

	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public static String toFieldString(final DatabaseTournamentMCRPlanning planning) {
		return String.join(
			FIELD_SEPARATOR,
			Integer.toString(planning.id.getTournamentId()),
			Integer.toString(planning.id.getSessionId()),
			DATE_FORMAT.format(planning.startTime)
		);
	}

	public static DatabaseTournamentMCRPlanning fromFieldString(final String fieldString) {
		final String fields[] = fieldString.split(
			FIELD_SEPARATOR,
			-1
		);
		try {
			return new DatabaseTournamentMCRPlanning(
				Integer.parseInt(fields[0]),
				Integer.parseInt(fields[1]),
				DATE_FORMAT.parse(fields[2])
			);
		} catch (final ParseException e) {
			return null;
		}
	}

	@Override
	public int compareTo(final DatabaseTournamentMCRPlanning o) {
		return id.compareTo(o.id);
	}
}
