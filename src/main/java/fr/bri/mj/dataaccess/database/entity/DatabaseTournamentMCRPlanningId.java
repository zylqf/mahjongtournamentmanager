/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.dataaccess.database.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DatabaseTournamentMCRPlanningId implements Serializable, Comparable<DatabaseTournamentMCRPlanningId> {

	private static final long serialVersionUID = -4306191768134933347L;

	@Column(
		name = "MCR_TOURNAMENT_ID",
		nullable = false,
		updatable = false
	)
	private int tournamentId;

	@Column(
		name = "SESSION_ID",
		nullable = false,
		updatable = false
	)
	private int sessionId;

	public DatabaseTournamentMCRPlanningId() {
	}

	public DatabaseTournamentMCRPlanningId(
		final int tournamentId,
		final int sessionId
	) {
		this.tournamentId = tournamentId;
		this.sessionId = sessionId;
	}

	public int getTournamentId() {
		return tournamentId;
	}

	public void setTournamentId(final int tournamentId) {
		this.tournamentId = tournamentId;
	}

	public int getSessionId() {
		return sessionId;
	}

	@Override
	public int hashCode() {
		return tournamentId * 100000 + sessionId * 1000;
	}

	@Override
	public boolean equals(final Object obj) {
		return obj instanceof DatabaseTournamentMCRPlanningId && hashCode() == obj.hashCode();
	}

	@Override
	public int compareTo(final DatabaseTournamentMCRPlanningId o) {
		final int result = Integer.compare(
			tournamentId,
			o.tournamentId
		);
		if (result == 0) {
			return Integer.compare(
				sessionId,
				o.sessionId
			);
		} else {
			return result;
		}
	}
}
