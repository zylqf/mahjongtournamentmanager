/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.dataaccess.database.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity(name = "TournamentMCRPlayer")
@Table(name = "MCR_TOURNAMENT_PLAYER")
public class DatabaseTournamentMCRPlayer extends DatabaseEntity implements Comparable<DatabaseTournamentMCRPlayer> {

	@EmbeddedId
	private DatabaseTournamentMCRPlayerId id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(
		name = "MCR_TOURNAMENT_ID",
		referencedColumnName = "ID",
		nullable = false,
		insertable = false,
		updatable = false
	)
	private DatabaseTournamentMCR tournament;

	@Column(
		name = "PLAYER_ID",
		nullable = true
	)
	private Integer playerId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(
		name = "PLAYER_ID",
		referencedColumnName = "ID",
		nullable = false,
		insertable = false,
		updatable = false
	)

	private DatabasePlayer player;
	@Column(
		name = "TEAM_NAME",
		length = 64,
		nullable = false
	)
	private String teamName;

	public DatabaseTournamentMCRPlayer() {
	}

	public DatabaseTournamentMCRPlayer(
		final DatabaseTournamentMCR tournament,
		final int tournamentPlayerId,
		final Integer playerId,
		final String teamName
	) {
		id = new DatabaseTournamentMCRPlayerId(
			tournament.getId(),
			tournamentPlayerId
		);
		this.tournament = tournament;
		this.playerId = playerId;
		this.teamName = teamName;
	}

	private DatabaseTournamentMCRPlayer(
		final int tournamentId,
		final int tournamentPlayerId,
		final Integer playerId,
		final String teamName
	) {
		id = new DatabaseTournamentMCRPlayerId(
			tournamentId,
			tournamentPlayerId
		);
		this.playerId = playerId;
		this.teamName = teamName;
	}

	public DatabaseTournamentMCRPlayerId getId() {
		return id;
	}

	public DatabaseTournamentMCR getTournament() {
		return tournament;
	}

	public int getTournamentPlayerId() {
		return id.getTournamentPlayerId();
	}

	public Integer getPlayerId() {
		return playerId;
	}

	public void setPlayerId(final Integer playerId) {
		this.playerId = playerId;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(final String teamName) {
		this.teamName = teamName;
	}

	private static final String[] TITLES = {
		"MCR_TOURNAMENT_ID",
		"MCR_TOURNAMENT_PLAYER_ID",
		"PLAYER_ID",
		"TEAM_NAME"
	};

	public static String getTitleString() {
		return String.join(
			FIELD_SEPARATOR,
			TITLES
		);
	}

	public static String toFieldString(final DatabaseTournamentMCRPlayer tournamentPlayer) {
		return String.join(
			FIELD_SEPARATOR,
			Integer.toString(tournamentPlayer.id.getTournamentId()),
			Integer.toString(tournamentPlayer.getTournamentPlayerId()),
			tournamentPlayer.playerId == null
				? NULL_VALUE
				: Integer.toString(tournamentPlayer.playerId),
			tournamentPlayer.teamName
		);
	}

	public static DatabaseTournamentMCRPlayer fromFieldString(final String fieldString) {
		final String fields[] = fieldString.split(
			FIELD_SEPARATOR,
			-1
		);
		return new DatabaseTournamentMCRPlayer(
			Integer.parseInt(fields[0]),
			Integer.parseInt(fields[1]),
			NULL_VALUE.equals(fields[2])
				? null
				: Integer.parseInt(fields[2]),
			fields[3]
		);
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public boolean equals(final Object obj) {
		return obj instanceof DatabaseTournamentMCRPlayer && hashCode() == obj.hashCode();
	}

	@Override
	public int compareTo(final DatabaseTournamentMCRPlayer o) {
		return id.compareTo(o.id);
	}
}
