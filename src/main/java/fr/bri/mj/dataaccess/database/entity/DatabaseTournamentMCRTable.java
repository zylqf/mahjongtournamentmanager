/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.dataaccess.database.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity(name = "TournamentMCRTable")
@Table(name = "MCR_TOURNAMENT_TABLE")
public class DatabaseTournamentMCRTable extends DatabaseEntity implements Comparable<DatabaseTournamentMCRTable> {

	@EmbeddedId
	private DatabaseTournamentMCRTableId id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(
		name = "MCR_TOURNAMENT_ID",
		referencedColumnName = "id",
		nullable = false,
		insertable = false,
		updatable = false
	)
	private DatabaseTournamentMCR tournament;

	@Column(
		name = "MCR_TOURNAMENT_PLAYER_ID",
		nullable = false,
		updatable = false
	)
	private int tournamentPlayerId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumns(
		{
			@JoinColumn(
				name = "MCR_TOURNAMENT_ID",
				referencedColumnName = "MCR_TOURNAMENT_ID",
				nullable = false,
				insertable = false,
				updatable = false
			),
			@JoinColumn(
				name = "MCR_TOURNAMENT_PLAYER_ID",
				referencedColumnName = "MCR_TOURNAMENT_PLAYER_ID",
				nullable = false,
				insertable = false,
				updatable = false
			)
		}
	)
	private DatabaseTournamentMCRPlayer tournamentPlayer;

	@Column(
		name = "SUBSTITUTED",
		nullable = false
	)
	private int substituted;

	@Column(
		name = "RANKING",
		nullable = false
	)
	private int ranking;

	@Column(
		name = "TABLE_POINT",
		nullable = false
	)
	private int tablePoint;

	@Column(
		name = "PENALTY",
		nullable = false
	)
	private int penalty;

	@Column(
		name = "TOTAL_POINT",
		nullable = false
	)
	private int totalPoint;

	@Column(
		name = "MATCH_POINT",
		nullable = false
	)
	private int matchPoint;

	@Column(
		name = "SCORE_RECORDED",
		nullable = false
	)
	private boolean scoreRecorded;

	public DatabaseTournamentMCRTable() {
	}

	public DatabaseTournamentMCRTable(
		final DatabaseTournamentMCR tournament,
		final int sessionId,
		final int tableId,
		final int tablePlayerId,
		final DatabaseTournamentMCRPlayer tournamentPlayer,
		final int substituted,
		final int ranking,
		final int tablePoint,
		final int penalty,
		final int totalPoint,
		final int matchPoint,
		final boolean scoreRecorded
	) {
		id = new DatabaseTournamentMCRTableId(
			tournament.getId(),
			sessionId,
			tableId,
			tablePlayerId
		);
		this.tournament = tournament;
		tournamentPlayerId = tournamentPlayer.getTournamentPlayerId();
		this.tournamentPlayer = tournamentPlayer;
		this.substituted = substituted;
		this.ranking = ranking;
		this.tablePoint = tablePoint;
		this.penalty = penalty;
		this.totalPoint = totalPoint;
		this.matchPoint = matchPoint;
		this.scoreRecorded = scoreRecorded;
	}

	private DatabaseTournamentMCRTable(
		final int tournamentId,
		final int sessionId,
		final int tableId,
		final int tablePlayerId,
		final int tournamentPlayerId,
		final int substituted,
		final int ranking,
		final int tablePoint,
		final int penalty,
		final int totalPoint,
		final int matchPoint,
		final boolean scoreRecorded
	) {
		id = new DatabaseTournamentMCRTableId(
			tournamentId,
			sessionId,
			tableId,
			tablePlayerId
		);
		this.tournamentPlayerId = tournamentId;
		this.tournamentPlayerId = tournamentPlayerId;
		this.substituted = substituted;
		this.ranking = ranking;
		this.tablePoint = tablePoint;
		this.penalty = penalty;
		this.totalPoint = totalPoint;
		this.matchPoint = matchPoint;
		this.scoreRecorded = scoreRecorded;
	}

	public DatabaseTournamentMCRTableId getId() {
		return id;
	}

	public DatabaseTournamentMCR getTournament() {
		return tournament;
	}

	public int getSessionId() {
		return id.getSessionId();
	}

	public int getTableId() {
		return id.getTableId();
	}

	public int getTablePlayerId() {
		return id.getTablePlayerId();
	}

	public DatabaseTournamentMCRPlayer getTournamentPlayer() {
		return tournamentPlayer;
	}

	public int getSubstituted() {
		return substituted;
	}

	public void setSubstituted(final int substituted) {
		this.substituted = substituted;
	}

	public int getRanking() {
		return ranking;
	}

	public void setRanking(final int ranking) {
		this.ranking = ranking;
	}

	public int getTablePoint() {
		return tablePoint;
	}

	public void setTablePoint(final int tablePoint) {
		this.tablePoint = tablePoint;
	}

	public int getPenalty() {
		return penalty;
	}

	public void setPenalty(final int penalty) {
		this.penalty = penalty;
	}

	public int getTotalPoint() {
		return totalPoint;
	}

	public void setTotalPoint(final int totalPoint) {
		this.totalPoint = totalPoint;
	}

	public int getMatchPoint() {
		return matchPoint;
	}

	public void setMatchPoint(final int matchPoint) {
		this.matchPoint = matchPoint;
	}

	public boolean isScoreRecorded() {
		return scoreRecorded;
	}

	public void setScoreRecorded(final boolean scoreRecorded) {
		this.scoreRecorded = scoreRecorded;
	}

	private static final String[] TITLES = {
		"MCR_TOURNAMENT_ID",
		"SESSION_ID",
		"TABLE_ID",
		"TABLE_PLAYER_ID",
		"MCR_TOURNAMENT_PLAYER_ID",
		"SUBSTITUTED",
		"RANKING",
		"TABLE_POINT",
		"PENALTY",
		"TOTAL_POINT",
		"MATCH_POINT",
		"SCORE_RECORDED"
	};

	public static String getTitleString() {
		return String.join(
			FIELD_SEPARATOR,
			TITLES
		);
	}

	public static String toFieldString(final DatabaseTournamentMCRTable table) {
		return String.join(
			FIELD_SEPARATOR,
			Integer.toString(table.id.getTournamentId()),
			Integer.toString(table.id.getSessionId()),
			Integer.toString(table.id.getTableId()),
			Integer.toString(table.id.getTablePlayerId()),
			Integer.toString(table.tournamentPlayerId),
			Integer.toString(table.substituted),
			Integer.toString(table.ranking),
			Integer.toString(table.tablePoint),
			Integer.toString(table.penalty),
			Integer.toString(table.totalPoint),
			Integer.toString(table.matchPoint),
			Boolean.toString(table.scoreRecorded)
		);
	}

	public static DatabaseTournamentMCRTable fromFieldString(final String fieldString) {
		final String fields[] = fieldString.split(
			FIELD_SEPARATOR,
			-1
		);
		return new DatabaseTournamentMCRTable(
			Integer.parseInt(fields[0]),
			Integer.parseInt(fields[1]),
			Integer.parseInt(fields[2]),
			Integer.parseInt(fields[3]),
			Integer.parseInt(fields[4]),
			Integer.parseInt(fields[5]),
			Integer.parseInt(fields[6]),
			Integer.parseInt(fields[7]),
			Integer.parseInt(fields[8]),
			Integer.parseInt(fields[9]),
			Integer.parseInt(fields[10]),
			Boolean.parseBoolean(fields[11])
		);
	}

	@Override
	public int compareTo(final DatabaseTournamentMCRTable o) {
		return id.compareTo(o.id);
	}
}
