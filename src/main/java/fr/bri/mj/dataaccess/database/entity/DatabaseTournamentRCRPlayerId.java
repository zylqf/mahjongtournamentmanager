/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.dataaccess.database.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DatabaseTournamentRCRPlayerId implements Serializable, Comparable<DatabaseTournamentRCRPlayerId> {

	private static final long serialVersionUID = -7959872520674474115L;

	@Column(
		name = "RCR_TOURNAMENT_ID",
		nullable = false,
		updatable = false
	)
	private int tournamentId;

	@Column(
		name = "RCR_TOURNAMENT_PLAYER_ID",
		nullable = false,
		updatable = false
	)
	private int tournamentPlayerId;

	public DatabaseTournamentRCRPlayerId() {
	}

	public DatabaseTournamentRCRPlayerId(
		final int tournamentId,
		final int tournamentPlayerId
	) {
		this.tournamentId = tournamentId;
		this.tournamentPlayerId = tournamentPlayerId;
	}

	public int getTournamentId() {
		return tournamentId;
	}

	public void setTournamentId(final int tournamentId) {
		this.tournamentId = tournamentId;
	}

	public int getTournamentPlayerId() {
		return tournamentPlayerId;
	}

	@Override
	public int hashCode() {
		return tournamentId * 100000 + tournamentPlayerId;
	}

	@Override
	public boolean equals(final Object obj) {
		return obj instanceof DatabaseTournamentRCRPlayerId && hashCode() == obj.hashCode();
	}

	@Override
	public int compareTo(final DatabaseTournamentRCRPlayerId o) {
		final int result = Integer.compare(
			tournamentId,
			o.tournamentId
		);
		if (result == 0) {
			return Integer.compare(
				tournamentPlayerId,
				o.tournamentPlayerId
			);
		} else {
			return result;
		}
	}
}
