/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.dataaccess.database.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity(name = "TournamentRCRTable")
@Table(name = "RCR_TOURNAMENT_TABLE")
public class DatabaseTournamentRCRTable extends DatabaseEntity implements Comparable<DatabaseTournamentRCRTable> {

	@EmbeddedId
	private DatabaseTournamentRCRTableId id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(
		name = "RCR_TOURNAMENT_ID",
		referencedColumnName = "ID",
		nullable = false,
		insertable = false,
		updatable = false
	)
	private DatabaseTournamentRCR tournament;

	@Column(
		name = "RCR_TOURNAMENT_PLAYER_ID",
		nullable = false,
		updatable = false
	)
	private int tournamentPlayerId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumns(
		{
			@JoinColumn(
				name = "RCR_TOURNAMENT_ID",
				referencedColumnName = "RCR_TOURNAMENT_ID",
				nullable = false,
				insertable = false,
				updatable = false
			),
			@JoinColumn(
				name = "RCR_TOURNAMENT_PLAYER_ID",
				referencedColumnName = "RCR_TOURNAMENT_PLAYER_ID",
				nullable = false,
				insertable = false,
				updatable = false
			)
		}
	)
	private DatabaseTournamentRCRPlayer tournamentPlayer;

	@Column(
		name = "SUBSTITUTED",
		nullable = false
	)
	private int substituted;

	@Column(
		name = "RANKING",
		nullable = false
	)
	private int ranking;

	@Column(
		name = "GAME_SCORE",
		nullable = false
	)
	private int gameScore;

	@Column(
		name = "UMA_SCORE",
		nullable = false
	)
	private int umaScore;

	@Column(
		name = "PENALTY",
		nullable = false
	)
	private int penalty;

	@Column(
		name = "FINAL_SCORE",
		nullable = false
	)
	private int finalScore;

	@Column(
		name = "SCORE_RECORDED",
		nullable = false
	)
	private boolean scoreRecorded;

	public DatabaseTournamentRCRTable() {
	}

	public DatabaseTournamentRCRTable(
		final DatabaseTournamentRCR tournament,
		final int sessionId,
		final int tableId,
		final int tablePlayerId,
		final DatabaseTournamentRCRPlayer tournamentPlayer,
		final int substituted,
		final int ranking,
		final int gameScore,
		final int umaScore,
		final int penalty,
		final int finalScore,
		final boolean scoreRecorded
	) {
		id = new DatabaseTournamentRCRTableId(
			tournament.getId(),
			sessionId,
			tableId,
			tablePlayerId
		);
		this.tournament = tournament;
		tournamentPlayerId = tournamentPlayer.getTournamentPlayerId();
		this.tournamentPlayer = tournamentPlayer;
		this.substituted = substituted;
		this.ranking = ranking;
		this.gameScore = gameScore;
		this.umaScore = umaScore;
		this.penalty = penalty;
		this.finalScore = finalScore;
		this.scoreRecorded = scoreRecorded;
	}

	private DatabaseTournamentRCRTable(
		final int tournamentId,
		final int sessionId,
		final int tableId,
		final int tablePlayerId,
		final int tournamentPlayerId,
		final int substituted,
		final int ranking,
		final int gameScore,
		final int umaScore,
		final int penalty,
		final int finalScore,
		final boolean scoreRecorded
	) {
		id = new DatabaseTournamentRCRTableId(
			tournamentId,
			sessionId,
			tableId,
			tablePlayerId
		);
		this.tournamentPlayerId = tournamentPlayerId;
		this.substituted = substituted;
		this.ranking = ranking;
		this.gameScore = gameScore;
		this.umaScore = umaScore;
		this.penalty = penalty;
		this.finalScore = finalScore;
		this.scoreRecorded = scoreRecorded;
	}

	public DatabaseTournamentRCRTableId getId() {
		return id;
	}

	public DatabaseTournamentRCR getTournament() {
		return tournament;
	}

	public int getSessionId() {
		return id.getSessionId();
	}

	public int getTableId() {
		return id.getTableId();
	}

	public int getTablePlayerId() {
		return id.getTablePlayerId();
	}

	public DatabaseTournamentRCRPlayer getTournamentPlayer() {
		return tournamentPlayer;
	}

	public int getSubstituted() {
		return substituted;
	}

	public void setSubstituted(final int substituted) {
		this.substituted = substituted;
	}

	public int getRanking() {
		return ranking;
	}

	public void setRanking(final int ranking) {
		this.ranking = ranking;
	}

	public int getGameScore() {
		return gameScore;
	}

	public void setGameScore(final int gameScore) {
		this.gameScore = gameScore;
	}

	public int getUmaScore() {
		return umaScore;
	}

	public void setUmaScore(final int umaScore) {
		this.umaScore = umaScore;
	}

	public int getPenalty() {
		return penalty;
	}

	public void setPenalty(final int penalty) {
		this.penalty = penalty;
	}

	public int getFinalScore() {
		return finalScore;
	}

	public void setFinalScore(final int finalScore) {
		this.finalScore = finalScore;
	}

	public boolean isScoreRecorded() {
		return scoreRecorded;
	}

	public void setScoreRecorded(final boolean scoreRecorded) {
		this.scoreRecorded = scoreRecorded;
	}

	private static final String[] TITLES = {
		"RCR_TOURNAMENT_ID",
		"SESSION_ID",
		"TABLE_ID",
		"TABLE_PLAYER_ID",
		"RCR_TOURNAMENT_PLAYER_ID",
		"SUBSTITUTED",
		"RANKING",
		"GAME_SCORE",
		"UMA_SCORE",
		"PENALTY",
		"FINAL_SCORE",
		"SCORE_RECORDED"
	};

	public static String getTitleString() {
		return String.join(
			FIELD_SEPARATOR,
			TITLES
		);
	}

	public static String toFieldString(final DatabaseTournamentRCRTable table) {
		return String.join(
			FIELD_SEPARATOR,
			Integer.toString(table.id.getTournamentId()),
			Integer.toString(table.id.getSessionId()),
			Integer.toString(table.id.getTableId()),
			Integer.toString(table.id.getTablePlayerId()),
			Integer.toString(table.tournamentPlayerId),
			Integer.toString(table.substituted),
			Integer.toString(table.ranking),
			Integer.toString(table.gameScore),
			Integer.toString(table.umaScore),
			Integer.toString(table.penalty),
			Integer.toString(table.finalScore),
			Boolean.toString(table.scoreRecorded)
		);
	}

	public static DatabaseTournamentRCRTable fromFieldString(final String fieldString) {
		final String fields[] = fieldString.split(
			FIELD_SEPARATOR,
			-1
		);
		return new DatabaseTournamentRCRTable(
			Integer.parseInt(fields[0]),
			Integer.parseInt(fields[1]),
			Integer.parseInt(fields[2]),
			Integer.parseInt(fields[3]),
			Integer.parseInt(fields[4]),
			Integer.parseInt(fields[5]),
			Integer.parseInt(fields[6]),
			Integer.parseInt(fields[7]),
			Integer.parseInt(fields[8]),
			Integer.parseInt(fields[9]),
			Integer.parseInt(fields[10]),
			Boolean.parseBoolean(fields[11])
		);
	}

	@Override
	public int compareTo(final DatabaseTournamentRCRTable o) {
		return id.compareTo(o.id);
	}
}
