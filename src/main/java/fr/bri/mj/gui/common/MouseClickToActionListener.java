/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.common;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MouseClickToActionListener extends MouseAdapter {

	private final ActionListener listener;

	public MouseClickToActionListener(final ActionListener listener) {
		this.listener = listener;
	}

	@Override
	public void mousePressed(final MouseEvent e) {
		if (listener != null) {
			final ActionEvent event = new ActionEvent(
				e.getSource(),
				0,
				"clicked"
			);
			listener.actionPerformed(event);
		}
	}
}
