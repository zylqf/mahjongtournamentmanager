/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.common;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import fr.bri.mj.gui.config.UIConfig;
import fr.bri.mj.gui.config.item.UIConfigFontSize;
import fr.bri.mj.gui.language.UITextTranslator;

public abstract class UITabPanel extends JPanel {

	private static final long serialVersionUID = 2361857210829688208L;

	protected static final int NORTH_PANEL_ROWS_NUMBER = 1;
	protected static final int NORTH_PANEL_COLUMNS_NUMBER = 11;
	protected static final int NORTH_PANEL_HORIZONTAL_GAP = 8;
	protected static final int NORTH_PANEL_VERTICAL_GAP = 2;

	protected static final int NORTH_PANEL_TOURNAMENT_TITLE_INDEX = 0;
	protected static final int NORTH_PANEL_TOURNAMENT_COMBOBOX_INDEX = 1;
	protected static final int NORTH_PANEL_MODE_TITLE_INDEX = 2;
	protected static final int NORTH_PANEL_MODE_COMBOBOX_INDEX = 3;
	protected static final int NORTH_PANEL_RANKING_TITLE_INDEX = 4;
	protected static final int NORTH_PANEL_RANKING_COMBOBOX_INDEX = 5;
	protected static final int NORTH_PANEL_SESSION_TITLE_INDEX = 6;
	protected static final int NORTH_PANEL_SESSION_COMBOBOX_INDEX = 7;
	protected static final int NORTH_PANEL_BUTTON_SPACER_INDEX = 8;
	protected static final int NORTH_PANEL_BUTTON_EXPORT_IMAGE_INDEX = 9;
	protected static final int NORTH_PANEL_BUTTON_EXPORT_CSV_INDEX = 10;

	protected static final int NORTH_PANEL_TOURNAMENT_TITLE_WEIGHT = 2;
	protected static final int NORTH_PANEL_TOURNAMENT_COMBOBOX_WEIGHT = 6;
	protected static final int NORTH_PANEL_MODE_TITLE_WEIGHT = 1;
	protected static final int NORTH_PANEL_MODE_COMBOBOX_WEIGHT = 3;
	protected static final int NORTH_PANEL_RANKING_TITLE_WEIGHT = 1;
	protected static final int NORTH_PANEL_RANKING_COMBOBOX_WEIGHT = 3;
	protected static final int NORTH_PANEL_SESSION_TITLE_WEIGHT = 1;
	protected static final int NORTH_PANEL_SESSION_COMBOBOX_WEIGHT = 3;
	protected static final int NORTH_PANEL_BUTTON_SPACER_WEIGHT = 2;
	protected static final int NORTH_PANEL_BUTTON_EXPORT_IMAGE_WEIGHT = 2;
	protected static final int NORTH_PANEL_BUTTON_EXPORT_CSV_WEIGHT = 2;

	protected static final DecimalFormat SESSION_ID_FORMAT = new DecimalFormat("00");
	protected static final DecimalFormat TABLE_ID_FORMAT = new DecimalFormat("00");
	protected static final DecimalFormat PLAYER_ID_FORMAT = new DecimalFormat("000");
	protected static final DecimalFormat PLAYER_LICENSE_FORMAT = new DecimalFormat("00000000");

	public static final Font CUSTOM_FONT;
	static {
		Font font = null;
		try {
			font = Font.createFont(
				Font.TRUETYPE_FONT,
				ClassLoader.getSystemResourceAsStream("fr/bri/mj/font/OxygenMono-Regular.ttf")
			);
		} catch (final FontFormatException e) {
			font = null;
			e.printStackTrace();
		} catch (final IOException e) {
			font = null;
			e.printStackTrace();
		}

		CUSTOM_FONT = font;
	}

	private final Set<ChangeListener> setListener;
	private final ChangeEvent event;
	protected File lastChoosenFolder;
	protected final UIConfig config;
	protected final UITextTranslator translator;

	protected UITabPanel(
		final UIConfig config,
		final UITextTranslator translator
	) {
		this.config = config;
		this.translator = translator;

		setListener = new HashSet<ChangeListener>();
		event = new ChangeEvent(this);

		try {
			lastChoosenFolder = new File(getClass().getProtectionDomain().getCodeSource().getLocation().toURI()).getParentFile();
		} catch (final URISyntaxException e) {
			lastChoosenFolder = null;
		}
	}

	public abstract String getTabName();

	public void refresh(final boolean hard) {
	};

	public void remoteRefresh() {
	};

	public void stopRunningThreads() {
	};

	public boolean hasUnsavedChanges() {
		return false;
	};

	public void addChangeListener(final ChangeListener l) {
		if (l != null) {
			setListener.add(l);
		}
	}

	public void removeChangeListener(final ChangeListener l) {
		if (l != null) {
			setListener.remove(l);
		}
	}

	protected void informChange() {
		for (final ChangeListener l : setListener) {
			l.stateChanged(event);
		}
	}

	protected String regulateString(final String original) {
		return original.replaceAll(
			"[ /\\\\:*?\"<>|]",
			"_"
		);
	}

	protected int getConfigFontSize() {
		return getFontSize(config.getFontSize());
	}

	protected int getFontSize(final UIConfigFontSize fontSize) {
		switch (fontSize) {
			case BIG:
				return 24;
			case MEDIUM:
				return 20;
			case SMALL:
				return 16;
			default:
				return 16;
		}
	}

	protected String fraction(
		final int numerator,
		final int denominator
	) {
		final int quotient = numerator / denominator;
		final int remainder = numerator % denominator;
		if (remainder != 0) {
			int a = remainder;
			int bcd = denominator;
			int t;
			while (a != 0) {
				t = a;
				a = bcd;
				bcd = t;
				a = a % bcd;
			}
			return Integer.toString(quotient)
				+ " "
				+ Integer.toString(remainder / bcd)
				+ "/"
				+ Integer.toString(denominator / bcd);
		} else {
			return Integer.toString(quotient);
		}
	}
}
