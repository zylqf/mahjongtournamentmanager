/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.common;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import fr.bri.mj.gui.config.UIConfig;
import fr.bri.mj.gui.icon.ImageLoaderScoreType;
import fr.bri.mj.gui.language.UITextTranslator;

public abstract class UITabPanelWithWaterMark extends UITabPanel {

	private static final long serialVersionUID = -5969293695082533535L;

	private final int WATERMARK_MARGIN = 16;
	private final Image watermark;

	protected UITabPanelWithWaterMark(
		final UIConfig config,
		final UITextTranslator translator
	) {
		super(
			config,
			translator
		);
		setOpaque(true);

		switch (config.getRuleSet()) {
			case MCR:
				watermark = ImageLoaderScoreType.getIconImageMCR();
				break;
			case RCR:
				watermark = ImageLoaderScoreType.getIconImageRCR();
				break;
			default:
				watermark = null;
				break;
		}
	}

	@Override
	public void paintComponent(final Graphics g) {
		super.paintComponent(g);
		if (watermark != null) {
			final Dimension panelSize = getSize();
			final int x = panelSize.width - watermark.getWidth(null) - WATERMARK_MARGIN;
			final int y = panelSize.height - watermark.getHeight(null) - WATERMARK_MARGIN;
			g.drawImage(
				watermark,
				x,
				y,
				null
			);
		}
	}
}
