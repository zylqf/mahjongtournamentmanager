/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.config;

import fr.bri.mj.gui.config.item.UIConfigColorProfile;
import fr.bri.mj.gui.config.item.UIConfigDisplayLanguage;
import fr.bri.mj.gui.config.item.UIConfigFontSize;
import fr.bri.mj.gui.config.item.UIConfigNationalMode;
import fr.bri.mj.gui.config.item.UIConfigRuleSet;
import fr.bri.mj.gui.config.item.UIConfigTimerDisplayMode;
import fr.bri.mj.gui.config.item.UIConfigTimerMode;
import fr.bri.mj.gui.config.item.UIConfigVoiceGender;
import fr.bri.mj.gui.config.item.UIConfigVoiceLanguage;

public class UIConfig {

	private UIConfigRuleSet ruleSet;

	private UIConfigDisplayLanguage displayLanguage;
	private UIConfigNationalMode nationalMode;

	private UIConfigColorProfile colorProfile;
	private UIConfigFontSize fontSize;

	private UIConfigTimerMode timerMode;
	private UIConfigTimerDisplayMode timerDisplayMode;
	private UIConfigVoiceLanguage voiceLanguage;
	private UIConfigVoiceGender voiceGender;

	private boolean majorChange;
	private boolean minorChange;

	public UIConfig() {
		ruleSet = UIConfigRuleSet.values()[0];

		displayLanguage = UIConfigDisplayLanguage.values()[0];
		nationalMode = UIConfigNationalMode.values()[0];

		colorProfile = UIConfigColorProfile.values()[0];
		fontSize = UIConfigFontSize.values()[0];

		timerMode = UIConfigTimerMode.values()[0];
		timerDisplayMode = UIConfigTimerDisplayMode.values()[0];
		voiceLanguage = UIConfigVoiceLanguage.values()[0];
		voiceGender = UIConfigVoiceGender.values()[0];

		majorChange = true;
		minorChange = true;
	}

	public UIConfig(
		final int ruleSetIndex,
		final int displayLanguageIndex,
		final int nationalModeIndex,
		final int colorProfileIndex,
		final int fontSizeIndex,
		final int timerModeIndex,
		final int timerDisplayModeIndex,
		final int voiceLanguageIndex,
		final int voiceGenderIndex
	) {
		ruleSet = UIConfigRuleSet.values()[ruleSetIndex];

		displayLanguage = UIConfigDisplayLanguage.values()[displayLanguageIndex];
		nationalMode = UIConfigNationalMode.values()[nationalModeIndex];

		colorProfile = UIConfigColorProfile.values()[colorProfileIndex];
		fontSize = UIConfigFontSize.values()[fontSizeIndex];

		timerMode = UIConfigTimerMode.values()[timerModeIndex];
		timerDisplayMode = UIConfigTimerDisplayMode.values()[timerDisplayModeIndex];
		voiceLanguage = UIConfigVoiceLanguage.values()[voiceLanguageIndex];
		voiceGender = UIConfigVoiceGender.values()[voiceGenderIndex];

		majorChange = true;
		minorChange = true;
	}

	public UIConfigRuleSet getRuleSet() {
		return ruleSet;
	}

	public void setRuleSet(final UIConfigRuleSet ruleSet) {
		if (ruleSet != null && this.ruleSet != ruleSet) {
			this.ruleSet = ruleSet;
			majorChange = true;
		}
	}

	public UIConfigDisplayLanguage getDisplayLanguage() {
		return displayLanguage;
	}

	public void setDisplayLanguage(final UIConfigDisplayLanguage displayLanguage) {
		if (displayLanguage != null && this.displayLanguage != displayLanguage) {
			this.displayLanguage = displayLanguage;
			majorChange = true;
		}
	}

	public UIConfigNationalMode getNationalMode() {
		return nationalMode;
	}

	public void setNationalMode(final UIConfigNationalMode nationalMode) {
		if (nationalMode != null && this.nationalMode != nationalMode) {
			this.nationalMode = nationalMode;
			majorChange = true;
		}
	}

	public UIConfigColorProfile getColorProfile() {
		return colorProfile;
	}

	public void setColorProfile(final UIConfigColorProfile colorProfile) {
		if (colorProfile != null && this.colorProfile != colorProfile) {
			this.colorProfile = colorProfile;
			minorChange = true;
		}
	}

	public UIConfigFontSize getFontSize() {
		return fontSize;
	}

	public void setFontSize(final UIConfigFontSize fontSize) {
		if (fontSize != null && this.fontSize != fontSize) {
			this.fontSize = fontSize;
			minorChange = true;
		}
	}

	public UIConfigTimerMode getTimerMode() {
		return timerMode;
	}

	public void setTimerMode(final UIConfigTimerMode timerMode) {
		if (timerMode != null && this.timerMode != timerMode) {
			this.timerMode = timerMode;
			majorChange = true;
		}
	}

	public UIConfigTimerDisplayMode getTimerDisplayMode() {
		return timerDisplayMode;
	}

	public void setTimerDisplayMode(final UIConfigTimerDisplayMode timerDisplayMode) {
		if (timerDisplayMode != null && this.timerDisplayMode != timerDisplayMode) {
			this.timerDisplayMode = timerDisplayMode;
			majorChange = true;
		}
	}

	public UIConfigVoiceLanguage getVoiceLanguage() {
		return voiceLanguage;
	}

	public void setVoiceLanguage(final UIConfigVoiceLanguage voiceLanguage) {
		if (voiceLanguage != null && this.voiceLanguage != voiceLanguage) {
			this.voiceLanguage = voiceLanguage;
			minorChange = true;
		}
	}

	public UIConfigVoiceGender getVoiceGender() {
		return voiceGender;
	}

	public void setVoiceGender(final UIConfigVoiceGender voiceGender) {
		if (voiceGender != null && this.voiceGender != voiceGender) {
			this.voiceGender = voiceGender;
			minorChange = true;
		}
	}

	public void resetChanges() {
		majorChange = false;
		minorChange = false;
	}

	public boolean isMajorChange() {
		return majorChange;
	}

	public boolean isMinorChange() {
		return minorChange;
	}
}
