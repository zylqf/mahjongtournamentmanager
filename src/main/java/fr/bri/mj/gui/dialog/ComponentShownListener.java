package fr.bri.mj.gui.dialog;

import java.awt.event.ComponentEvent;

@FunctionalInterface
public interface ComponentShownListener {

	public void componentShown(final ComponentEvent e);

}
