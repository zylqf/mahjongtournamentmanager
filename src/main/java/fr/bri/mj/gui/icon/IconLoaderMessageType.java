/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.icon;

import java.awt.Image;
import java.util.EnumMap;
import java.util.Map;

import javax.imageio.ImageIO;

import fr.bri.mj.gui.dialog.UIMessageDialog;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogMessageType;

public class IconLoaderMessageType {

	private static final String ICON_URL_PREFIX = "fr/bri/mj/icon/message_type_";
	private static final String ICON_URL_SUFFIX = ".png";

	private static Map<MessageDialogMessageType, Image> ICON_IMAGE_BUFFER = new EnumMap<UIMessageDialog.MessageDialogMessageType, Image>(UIMessageDialog.MessageDialogMessageType.class);

	public static Image getIconImage(final MessageDialogMessageType messageType) {
		if (!ICON_IMAGE_BUFFER.containsKey(messageType)) {
			try {
				final Image image = ImageIO.read(ClassLoader.getSystemResource(ICON_URL_PREFIX + messageType.getName() + ICON_URL_SUFFIX));
				ICON_IMAGE_BUFFER.put(
					messageType,
					image
				);
			} catch (final Exception e) {
			}
		}
		return ICON_IMAGE_BUFFER.get(messageType);
	}
}
