/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.icon;

import java.awt.Image;

import javax.imageio.ImageIO;

public class IconLoaderTreeNode {

	private static final String ICON_URL_TOURNAMENT = "fr/bri/mj/icon/tree_node_tournament.png";
	private static final String ICON_URL_SESSION = "fr/bri/mj/icon/tree_node_session.png";

	private static Image ICON_IMAGE_TOURNAMENT;
	private static Image ICON_IMAGE_SESSION;

	public static Image getIconImageTournament() {
		if (ICON_IMAGE_TOURNAMENT == null) {
			try {
				ICON_IMAGE_TOURNAMENT = ImageIO.read(ClassLoader.getSystemResource(ICON_URL_TOURNAMENT));
			} catch (final Exception e) {
				ICON_IMAGE_TOURNAMENT = null;
			}
		}
		return ICON_IMAGE_TOURNAMENT;
	}

	public static Image getIconImageSession() {
		if (ICON_IMAGE_SESSION == null) {
			try {
				ICON_IMAGE_SESSION = ImageIO.read(ClassLoader.getSystemResource(ICON_URL_SESSION));
			} catch (final Exception e) {
				ICON_IMAGE_SESSION = null;
			}
		}
		return ICON_IMAGE_SESSION;
	}
}
