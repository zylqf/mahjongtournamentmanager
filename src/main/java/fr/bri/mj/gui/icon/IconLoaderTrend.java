/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.icon;

import java.awt.Image;

import javax.imageio.ImageIO;

public class IconLoaderTrend {

	private static final String ICON_URL_ARROW_UP_UP = "fr/bri/mj/icon/arrow_up_up.png";
	private static final String ICON_URL_ARROW_UP = "fr/bri/mj/icon/arrow_up.png";
	private static final String ICON_URL_ARROW_FLAT = "fr/bri/mj/icon/arrow_flat.png";
	private static final String ICON_URL_ARROW_DOWN = "fr/bri/mj/icon/arrow_down.png";
	private static final String ICON_URL_ARROW_DOWN_DOWN = "fr/bri/mj/icon/arrow_down_down.png";

	private static Image ICON_IMAGE_ARROW_UP_UP;
	private static Image ICON_IMAGE_ARROW_UP;
	private static Image ICON_IMAGE_ARROW_FLAT;
	private static Image ICON_IMAGE_ARROW_DOWN;
	private static Image ICON_IMAGE_ARROW_DOWN_DOWN;

	public static Image getIconImageArrowUpUp() {
		if (ICON_IMAGE_ARROW_UP_UP == null) {
			try {
				ICON_IMAGE_ARROW_UP_UP = ImageIO.read(ClassLoader.getSystemResource(ICON_URL_ARROW_UP_UP));
			} catch (final Exception e) {
				ICON_IMAGE_ARROW_UP_UP = null;
			}
		}
		return ICON_IMAGE_ARROW_UP_UP;
	}

	public static Image getIconImageArrowUp() {
		if (ICON_IMAGE_ARROW_UP == null) {
			try {
				ICON_IMAGE_ARROW_UP = ImageIO.read(ClassLoader.getSystemResource(ICON_URL_ARROW_UP));
			} catch (final Exception e) {
				ICON_IMAGE_ARROW_UP = null;
			}
		}
		return ICON_IMAGE_ARROW_UP;
	}

	public static Image getIconImageArrowFlat() {
		if (ICON_IMAGE_ARROW_FLAT == null) {
			try {
				ICON_IMAGE_ARROW_FLAT = ImageIO.read(ClassLoader.getSystemResource(ICON_URL_ARROW_FLAT));
			} catch (final Exception e) {
				ICON_IMAGE_ARROW_FLAT = null;
			}
		}
		return ICON_IMAGE_ARROW_FLAT;
	}

	public static Image getIconImageArrowDown() {
		if (ICON_IMAGE_ARROW_DOWN == null) {
			try {
				ICON_IMAGE_ARROW_DOWN = ImageIO.read(ClassLoader.getSystemResource(ICON_URL_ARROW_DOWN));
			} catch (final Exception e) {
				ICON_IMAGE_ARROW_DOWN = null;
			}
		}
		return ICON_IMAGE_ARROW_DOWN;
	}

	public static Image getIconImageArrowDownDown() {
		if (ICON_IMAGE_ARROW_DOWN_DOWN == null) {
			try {
				ICON_IMAGE_ARROW_DOWN_DOWN = ImageIO.read(ClassLoader.getSystemResource(ICON_URL_ARROW_DOWN_DOWN));
			} catch (final Exception e) {
				ICON_IMAGE_ARROW_DOWN_DOWN = null;
			}
		}
		return ICON_IMAGE_ARROW_DOWN_DOWN;
	}
}
