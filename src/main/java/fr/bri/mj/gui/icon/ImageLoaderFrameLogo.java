/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.icon;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

public class ImageLoaderFrameLogo {

	private static final String ICON_URLS_FRAME_LOGO[] = {
		"fr/bri/mj/image/logo_016.png",
		"fr/bri/mj/image/logo_024.png",
		"fr/bri/mj/image/logo_032.png"
	};

	private static List<Image> ICON_IMAGES_FRAME_LOGO;

	public static List<Image> getIconImageFrameLogo() {
		if (ICON_IMAGES_FRAME_LOGO == null) {
			ICON_IMAGES_FRAME_LOGO = new ArrayList<Image>();
			for (int index = 0; index < ICON_URLS_FRAME_LOGO.length; index++) {
				try {
					final URL iconURL = ClassLoader.getSystemResource(ICON_URLS_FRAME_LOGO[index]);
					final BufferedImage iconImage = ImageIO.read(iconURL);
					ICON_IMAGES_FRAME_LOGO.add(iconImage);
				} catch (final Exception e) {
				}
			}
		}
		return ICON_IMAGES_FRAME_LOGO;
	}
}
