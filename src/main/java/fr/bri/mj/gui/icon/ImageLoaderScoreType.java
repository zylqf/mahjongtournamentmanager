/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.icon;

import java.awt.Image;

import javax.imageio.ImageIO;

public class ImageLoaderScoreType {

	private static final String ICON_URL_MCR = "fr/bri/mj/image/mcr.png";
	private static final String ICON_URL_RCR = "fr/bri/mj/image/rcr.png";

	private static Image ICON_IMAGE_MCR;
	private static Image ICON_IMAGE_RCR;

	public static Image getIconImageMCR() {
		if (ICON_IMAGE_MCR == null) {
			try {
				ICON_IMAGE_MCR = ImageIO.read(ClassLoader.getSystemResource(ICON_URL_MCR));
			} catch (final Exception e) {
				ICON_IMAGE_MCR = null;
			}
		}
		return ICON_IMAGE_MCR;
	}

	public static Image getIconImageRCR() {
		if (ICON_IMAGE_RCR == null) {
			try {
				ICON_IMAGE_RCR = ImageIO.read(ClassLoader.getSystemResource(ICON_URL_RCR));
			} catch (final Exception e) {
				ICON_IMAGE_RCR = null;
			}
		}
		return ICON_IMAGE_RCR;
	}
}
