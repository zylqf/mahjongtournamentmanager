/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.language;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import fr.bri.mj.gui.config.UIConfig;
import fr.bri.mj.gui.config.item.UIConfigDisplayLanguage;

public class UITextTranslator {

	private static final String FILE_URL_TRANSLATION_ENGLISH = "fr/bri/mj/lang/english.properties";
	private static final String FILE_URL_TRANSLATION_FRENCH = "fr/bri/mj/lang/francais.properties";

	private final UIConfig config;

	private final EnumMap<UIConfigDisplayLanguage, EnumMap<UIText, String>> texts;

	public UITextTranslator(final UIConfig config) {
		this.config = config;

		texts = new EnumMap<UIConfigDisplayLanguage, EnumMap<UIText, String>>(UIConfigDisplayLanguage.class);
		texts.put(
			UIConfigDisplayLanguage.ENGLISH,
			readMap(FILE_URL_TRANSLATION_ENGLISH)
		);
		texts.put(
			UIConfigDisplayLanguage.FRENCH,
			readMap(FILE_URL_TRANSLATION_FRENCH)
		);
	}

	private EnumMap<UIText, String> readMap(final String url) {
		final EnumMap<UIText, String> map = new EnumMap<UIText, String>(UIText.class);
		try {
			final BufferedReader reader = new BufferedReader(
				new InputStreamReader(
					ClassLoader.getSystemResource(url).openStream(),
					StandardCharsets.UTF_8
				)
			);

			final Map<String, String> rawMap = new HashMap<String, String>(512);
			String line = reader.readLine();
			while (line != null) {
				if (!line.isEmpty()) {
					final String[] fields = line.split("=");
					if (fields.length == 2) {
						rawMap.put(
							fields[0].trim(),
							fields[1].trim()
						);
					}
				}
				line = reader.readLine();
			}
			reader.close();

			for (final UIText text : UIText.values()) {
				if (rawMap.containsKey(text.getKey())) {
					map.put(
						text,
						rawMap.get(text.getKey())
					);
				} else {
					map.put(
						text,
						""
					);
				}
			}
		} catch (final IOException e) {
		}
		return map;
	}

	public String translate(final UIText text) {
		if (text != null) {
			return texts.get(config.getDisplayLanguage()).get(text);
		} else {
			return null;
		}
	}

	public String translate(
		final UIConfigDisplayLanguage language,
		final UIText text
	) {
		if (language != null && text != null) {
			return texts.get(language).get(text);
		} else {
			return null;
		}
	}
}
