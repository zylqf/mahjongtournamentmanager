/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.primary;

import fr.bri.mj.data.score.aggregated.ranking.calculator.single.RankingCalculatorSingleMCRFactory;
import fr.bri.mj.data.score.aggregated.ranking.calculator.single.RankingCalculatorSingleRCRFactory;
import fr.bri.mj.data.tournament.mcr.ScoreMCR;
import fr.bri.mj.data.tournament.mcr.TournamentMCRFactory;
import fr.bri.mj.data.tournament.mcr.TournamentScoreMCR;
import fr.bri.mj.data.tournament.rcr.ScoreRCR;
import fr.bri.mj.data.tournament.rcr.TournamentRCRFactory;
import fr.bri.mj.data.tournament.rcr.TournamentScoreRCR;
import fr.bri.mj.dataaccess.ManagerFactory;
import fr.bri.mj.gui.common.UITabPanel;
import fr.bri.mj.gui.config.UIConfig;
import fr.bri.mj.gui.config.item.UIConfigDisplayLanguage;
import fr.bri.mj.gui.config.item.UIConfigRuleSet;
import fr.bri.mj.gui.language.UITextTranslator;
import fr.bri.mj.gui.primary.inputscore.mcr.UITabPanelInputScoreMCR;
import fr.bri.mj.gui.primary.inputscore.rcr.UITabPanelInputScoreRCR;
import fr.bri.mj.gui.primary.managetournament.UITabPanelManageTournament;
import fr.bri.mj.gui.primary.managetournamentplayer.UITabPanelManageTournamentPlayer;
import fr.bri.mj.gui.primary.newtournament.UITabPanelNewTournament;
import fr.bri.mj.gui.primary.player.UITabPanelManagePlayer;
import fr.bri.mj.gui.primary.score.mcr.UITabPanelDisplayTableScoreMCR;
import fr.bri.mj.gui.primary.score.rcr.UITabPanelDisplayTableScoreRCR;

public class UITabPanelPrimaryFactory {

	private static UIConfigDisplayLanguage lastDisplayLanguage = null;

	public static void checkDisplayLanguage(final UIConfigDisplayLanguage currentDisplayLanguage) {
		if (currentDisplayLanguage != null && currentDisplayLanguage != lastDisplayLanguage) {
			BUFFER_PLAYER = null;
			for (int index1 = 0; index1 < UIConfigRuleSet.values().length; index1++) {
				BUFFER_MANAGEMENT_TOURNAMENT[index1] = null;
				BUFFER_MANAGEMENT_TOURNAMENT_PLAYER[index1] = null;
				BUFFER_NEW_TOURNAMENT[index1] = null;
				BUFFER_INPUT_SCORE[index1] = null;
				BUFFER_DISPLAY_SCORE[index1] = null;
			}
			lastDisplayLanguage = currentDisplayLanguage;
		}
	}

	private static UITabPanel BUFFER_PLAYER = null;

	public static UITabPanel getTabPanelPlayer(
		final ManagerFactory managerFactory,
		final UIConfig config,
		final UITextTranslator translator
	) {
		checkDisplayLanguage(config.getDisplayLanguage());
		if (BUFFER_PLAYER == null) {
			BUFFER_PLAYER = new UITabPanelManagePlayer(
				managerFactory.getPlayerManager(),
				config,
				translator
			);

		}
		return BUFFER_PLAYER;
	}

	private static final UITabPanel BUFFER_NEW_TOURNAMENT[] = new UITabPanel[UIConfigRuleSet.values().length];

	public static UITabPanel getTabPanelNewTournament(
		final ManagerFactory managerFactory,
		final UIConfig config,
		final UITextTranslator translator
	) {
		checkDisplayLanguage(config.getDisplayLanguage());
		if (BUFFER_NEW_TOURNAMENT[config.getRuleSet().ordinal()] == null) {
			switch (config.getRuleSet()) {
				case MCR:
					BUFFER_NEW_TOURNAMENT[config.getRuleSet().ordinal()] = new UITabPanelNewTournament<TournamentScoreMCR, ScoreMCR>(
						managerFactory.getTournamentManagerMCR(),
						config,
						translator,
						TournamentMCRFactory.getInstance()
					);
					break;
				case RCR:
					BUFFER_NEW_TOURNAMENT[config.getRuleSet().ordinal()] = new UITabPanelNewTournament<TournamentScoreRCR, ScoreRCR>(
						managerFactory.getTournamentManagerRCR(),
						config,
						translator,
						TournamentRCRFactory.getInstance()
					);
					break;
			}
		}
		return BUFFER_NEW_TOURNAMENT[config.getRuleSet().ordinal()];
	}

	private static final UITabPanel BUFFER_MANAGEMENT_TOURNAMENT[] = new UITabPanel[UIConfigRuleSet.values().length];

	public static UITabPanel getTabPanelManagementTournament(
		final ManagerFactory managerFactory,
		final UIConfig config,
		final UITextTranslator translator
	) {
		checkDisplayLanguage(config.getDisplayLanguage());
		if (BUFFER_MANAGEMENT_TOURNAMENT[config.getRuleSet().ordinal()] == null) {
			switch (config.getRuleSet()) {
				case MCR:
					BUFFER_MANAGEMENT_TOURNAMENT[config.getRuleSet().ordinal()] = new UITabPanelManageTournament<TournamentScoreMCR, ScoreMCR>(
						managerFactory.getTournamentManagerMCR(),
						config,
						translator,
						TournamentMCRFactory.getInstance(),
						RankingCalculatorSingleMCRFactory.getInstance(),
						ScoreMCR.MATCH_POINT
					);
					break;
				case RCR:
					BUFFER_MANAGEMENT_TOURNAMENT[config.getRuleSet().ordinal()] = new UITabPanelManageTournament<TournamentScoreRCR, ScoreRCR>(
						managerFactory.getTournamentManagerRCR(),
						config,
						translator,
						TournamentRCRFactory.getInstance(),
						RankingCalculatorSingleRCRFactory.getInstance(),
						ScoreRCR.FINAL_SCORE
					);
					break;
			}
		}
		return BUFFER_MANAGEMENT_TOURNAMENT[config.getRuleSet().ordinal()];
	}

	private static final UITabPanel BUFFER_MANAGEMENT_TOURNAMENT_PLAYER[] = new UITabPanel[UIConfigRuleSet.values().length];

	public static UITabPanel getTabPanelManagementTournamentPlayer(
		final ManagerFactory managerFactory,
		final UIConfig config,
		final UITextTranslator translator
	) {
		checkDisplayLanguage(config.getDisplayLanguage());
		if (BUFFER_MANAGEMENT_TOURNAMENT_PLAYER[config.getRuleSet().ordinal()] == null) {
			switch (config.getRuleSet()) {
				case MCR:
					BUFFER_MANAGEMENT_TOURNAMENT_PLAYER[config.getRuleSet().ordinal()] = new UITabPanelManageTournamentPlayer<TournamentScoreMCR, ScoreMCR>(
						managerFactory.getPlayerManager(),
						managerFactory.getTournamentManagerMCR(),
						config,
						translator
					);
					break;
				case RCR:
					BUFFER_MANAGEMENT_TOURNAMENT_PLAYER[config.getRuleSet().ordinal()] = new UITabPanelManageTournamentPlayer<TournamentScoreRCR, ScoreRCR>(
						managerFactory.getPlayerManager(),
						managerFactory.getTournamentManagerRCR(),
						config,
						translator
					);
					break;
			}
		}
		return BUFFER_MANAGEMENT_TOURNAMENT_PLAYER[config.getRuleSet().ordinal()];
	}

	private static final UITabPanel BUFFER_INPUT_SCORE[] = new UITabPanel[UIConfigRuleSet.values().length];

	public static UITabPanel getTabPanelInputScore(
		final ManagerFactory managerFactory,
		final UIConfig config,
		final UITextTranslator translator
	) {
		checkDisplayLanguage(config.getDisplayLanguage());
		if (BUFFER_INPUT_SCORE[config.getRuleSet().ordinal()] == null) {
			switch (config.getRuleSet()) {
				case MCR:
					BUFFER_INPUT_SCORE[config.getRuleSet().ordinal()] = new UITabPanelInputScoreMCR(
						managerFactory.getTournamentManagerMCR(),
						config,
						translator
					);
					break;
				case RCR:
					BUFFER_INPUT_SCORE[config.getRuleSet().ordinal()] = new UITabPanelInputScoreRCR(
						managerFactory.getTournamentManagerRCR(),
						config,
						translator
					);
					break;
			}
		}
		return BUFFER_INPUT_SCORE[config.getRuleSet().ordinal()];
	}

	private static final UITabPanel BUFFER_DISPLAY_SCORE[] = new UITabPanel[UIConfigRuleSet.values().length];

	public static UITabPanel getTabPanelDisplayScore(
		final ManagerFactory managerFactory,
		final UIConfig config,
		final UITextTranslator translator
	) {
		checkDisplayLanguage(config.getDisplayLanguage());
		if (BUFFER_DISPLAY_SCORE[config.getRuleSet().ordinal()] == null) {
			switch (config.getRuleSet()) {
				case MCR:
					BUFFER_DISPLAY_SCORE[config.getRuleSet().ordinal()] = new UITabPanelDisplayTableScoreMCR(
						managerFactory.getTournamentManagerMCR(),
						config,
						translator
					);
					break;
				case RCR:
					BUFFER_DISPLAY_SCORE[config.getRuleSet().ordinal()] = new UITabPanelDisplayTableScoreRCR(
						managerFactory.getTournamentManagerRCR(),
						config,
						translator
					);
					break;
			}
		}
		return BUFFER_DISPLAY_SCORE[config.getRuleSet().ordinal()];
	}
}
