/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.primary.managetournamentplayer;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import fr.bri.awt.ProportionalGridLayoutConstraint;
import fr.bri.awt.ProportionalGridLayoutInteger;
import fr.bri.mj.data.player.Player;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;

public class UISelectPlayerDialog extends JDialog {

	private static final long serialVersionUID = -4819540301307619166L;

	private final JFrame parent;
	private final JTextField textFilterPlayer;
	private final JButton buttonFilterPlayer;
	private final JComboBox<String> comboBoxPlayer;

	private final List<Player> listPlayers;
	private final List<String> listDisplayString;
	private final List<String> listNormalizedDisplayString;
	private final List<Player> listFilteredPlayers;

	private boolean playerSelected;
	private Player selectedPlayer;

	public UISelectPlayerDialog(
		final JFrame parent,
		final String playerId,
		final List<Player> listPlayers,
		final List<String> listDisplayString,
		final List<String> listNormalizedDisplayString,
		final UITextTranslator translator
	) {
		super(
			parent,
			translator.translate(UIText.GUI_SELECT_PLAYER_DIALOG_TITLE)
				+ " "
				+ playerId,
			true
		);
		this.parent = parent;
		this.listPlayers = listPlayers;
		this.listDisplayString = listDisplayString;
		this.listNormalizedDisplayString = listNormalizedDisplayString;

		listFilteredPlayers = new ArrayList<Player>();

		final Container pane = getContentPane();
		pane.setLayout(new BorderLayout());

		{
			final JPanel centerPanel = new JPanel();
			final ProportionalGridLayoutInteger layout = new ProportionalGridLayoutInteger(
				5,
				6,
				0,
				0
			);
			layout.setWeightY(
				1,
				4,
				1,
				4,
				1
			);
			centerPanel.setLayout(layout);

			final ProportionalGridLayoutConstraint constraint = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			constraint.y = 1;
			constraint.x = 0;
			constraint.gridWidth = 1;
			centerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_SELECT_PLAYER_DIALOG_TITLE_SEARCH)
						+ " : ",
					SwingConstants.RIGHT
				),
				constraint
			);

			constraint.x = 1;
			constraint.gridWidth = 4;
			textFilterPlayer = new JTextField();
			centerPanel.add(
				textFilterPlayer,
				constraint
			);

			constraint.x = 5;
			constraint.gridWidth = 1;
			buttonFilterPlayer = new JButton(translator.translate(UIText.GUI_SELECT_PLAYER_DIALOG_BUTTON_FILTER));
			centerPanel.add(
				buttonFilterPlayer,
				constraint
			);

			constraint.y = 3;
			constraint.x = 0;
			constraint.gridWidth = 1;
			centerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_SELECT_PLAYER_DIALOG_TITLE_PLAYER)
						+ " : ",
					SwingConstants.RIGHT
				),
				constraint
			);
			constraint.x = 1;
			constraint.gridWidth = 5;
			comboBoxPlayer = new JComboBox<String>();
			centerPanel.add(
				comboBoxPlayer,
				constraint
			);

			pane.add(
				centerPanel,
				BorderLayout.CENTER
			);

			buttonFilterPlayer.addActionListener((final ActionEvent e) -> {
				filterPlayers();
			});
		}

		{
			final JPanel southPanel = new JPanel();
			final ProportionalGridLayoutInteger layout = new ProportionalGridLayoutInteger(
				3,
				5,
				0,
				0
			);
			layout.setWeightY(
				1,
				4,
				1
			);
			southPanel.setLayout(layout);

			final ProportionalGridLayoutConstraint constraint = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			constraint.y = 1;
			constraint.x = 1;
			final JButton buttonOk = new JButton(
				translator.translate(UIText.GUI_SELECT_PLAYER_DIALOG_BUTTON_OK)

			);
			southPanel.add(
				buttonOk,
				constraint
			);

			constraint.x = 3;
			final JButton buttonCancel = new JButton(
				translator.translate(UIText.GUI_SELECT_PLAYER_DIALOG_BUTTON_CANCEL)

			);
			southPanel.add(
				buttonCancel,
				constraint
			);

			pane.add(
				southPanel,
				BorderLayout.SOUTH
			);

			buttonOk.addActionListener((final ActionEvent e) -> {
				playerSelected = true;
				setVisible(false);
			});

			buttonCancel.addActionListener((final ActionEvent e) -> {
				setVisible(false);
			});
		}

		setPreferredSize(
			new Dimension(
				640,
				160
			)
		);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setResizable(false);
		pack();
	}

	private void filterPlayers() {
		comboBoxPlayer.removeAllItems();
		listFilteredPlayers.clear();

		final String searchText = textFilterPlayer.getText().trim().toLowerCase();
		if (searchText.isEmpty()) {
			for (int playerIndex = 0; playerIndex < listPlayers.size(); playerIndex++) {
				final Player player = listPlayers.get(playerIndex);
				listFilteredPlayers.add(player);
				comboBoxPlayer.addItem(listDisplayString.get(playerIndex));
			}
		} else {
			for (int playerIndex = 0; playerIndex < listPlayers.size(); playerIndex++) {
				final Player player = listPlayers.get(playerIndex);
				if (listNormalizedDisplayString.get(playerIndex).contains(searchText)) {
					listFilteredPlayers.add(player);
					comboBoxPlayer.addItem(listDisplayString.get(playerIndex));
				}
			}
		}

		if (listFilteredPlayers.size() > 0) {
			comboBoxPlayer.setSelectedIndex(0);
		} else {
			comboBoxPlayer.setSelectedIndex(-1);
		}
	}

	public boolean showDialog(final Player preselectedPlayer) {
		final Dimension size = getSize();
		if (parent != null && (parent.getExtendedState() & Frame.ICONIFIED) == 0) {
			final Point parentLocation = parent.getLocationOnScreen();
			final Dimension parentSize = parent.getSize();
			final int x = parentLocation.x + (parentSize.width - size.width) / 2;
			final int y = parentLocation.y + (parentSize.height - size.height) / 2;
			setLocation(
				x,
				y
			);
		} else {
			final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			final int x = (screenSize.width - size.width) / 2;
			final int y = (screenSize.height - size.height) / 2;
			setLocation(
				x,
				y
			);
		}

		textFilterPlayer.setText("");
		filterPlayers();
		final int selectedPlayerIndex = listFilteredPlayers.indexOf(preselectedPlayer);
		if (selectedPlayerIndex >= 0) {
			comboBoxPlayer.setSelectedIndex(selectedPlayerIndex);
		} else {
			comboBoxPlayer.setSelectedIndex(0);
		}
		setVisible(true);

		if (playerSelected) {
			selectedPlayer = listFilteredPlayers.get(comboBoxPlayer.getSelectedIndex());
		}
		return playerSelected;
	}

	public Player getSelectedPlayer() {
		return selectedPlayer;
	}
}
