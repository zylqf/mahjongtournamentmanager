/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.primary.newtournament;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import fr.bri.awt.ProportionalGridLayoutConstraint;
import fr.bri.awt.ProportionalGridLayoutInteger;
import fr.bri.mj.data.plan.PlanLoader;
import fr.bri.mj.data.tournament.Tournament;
import fr.bri.mj.data.tournament.TournamentFactory;
import fr.bri.mj.data.tournament.TournamentPlayer;
import fr.bri.mj.data.tournament.TournamentScore;
import fr.bri.mj.dataaccess.TournamentManager;
import fr.bri.mj.gui.common.UITabPanelWithWaterMark;
import fr.bri.mj.gui.config.UIConfig;
import fr.bri.mj.gui.dialog.UIMessageDialog;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogMessageType;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogOption;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogOptionsType;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;

public class UITabPanelNewTournament<ScoreClassType extends TournamentScore<ScoreClassType, ScoreEnumType>, ScoreEnumType extends Enum<ScoreEnumType>> extends UITabPanelWithWaterMark {

	private static final long serialVersionUID = 3958950430315412906L;

	private static final int NB_MIN_SESSIONS = 1;
	private static final int NB_MAX_SESSIONS = 20;
	private static final int NB_DEFAULT_SESSIONS = 5;

	private static final int QUALIFICATION_NB_MIN_ROUND_ROBIN_SESSIONS = 2;
	private static final int QUALIFICATION_NB_FINALE_SESSIONS = 6;
	private static final int QUALIFICATION_NB_MIN_SESSIONS = QUALIFICATION_NB_MIN_ROUND_ROBIN_SESSIONS + QUALIFICATION_NB_FINALE_SESSIONS;

	private final int[] maxSessions;
	private final TournamentFactory<ScoreClassType, ScoreEnumType> tournamentFactory;
	private final TournamentManager<ScoreClassType, ScoreEnumType> tournamentManager;

	private final JTextField textIndividualNewTournamentName;
	private final JTextField textIndividualNewTournamentLeagueName;
	private final JSpinner spinnerIndividualNbSessions;
	private final SpinnerNumberModel spinnerModeleIndividualNbSessions;
	private final ChangeListener spinnerIndividualNbSessionsListener;
	private final JSpinner spinnerIndividualNbRoundRobinSessions;
	private final SpinnerNumberModel spinnerModeleIndividualNbRoundRobinSessions;
	private final JComboBox<String> comboIndividualNbTables;
	private final List<Integer> individualNbTables;
	private final JComboBox<Integer> comboIndividualTeamSize;
	private final JCheckBox checkWithQualification;
	private final JLabel labelIndividualCollision;
	private final JButton buttonIndividualAddTournament;

	private final JTextField textTeamNewTournamentName;
	private final JTextField textTeamNewTournamentLeagueName;
	private final JSpinner spinnerTeamNbSessions;
	private final JSpinner spinnerTeamNbTables;
	private final JLabel labelTeamCollision;
	private final JButton buttonTeamAddTournament;

	public UITabPanelNewTournament(
		final TournamentManager<ScoreClassType, ScoreEnumType> tournamentManager,
		final UIConfig config,
		final UITextTranslator translator,
		final TournamentFactory<ScoreClassType, ScoreEnumType> tournamentFactory
	) {
		super(
			config,
			translator
		);
		this.tournamentFactory = tournamentFactory;
		this.tournamentManager = tournamentManager;

		final JPanel tournamentPanel = new JPanel();
		tournamentPanel.setOpaque(false);
		tournamentPanel.setLayout(new GridBagLayout());
		final GridBagConstraints tournamentPanelC = new GridBagConstraints(
			0,
			0,
			1,
			1,
			1.0,
			0.0,
			GridBagConstraints.CENTER,
			GridBagConstraints.HORIZONTAL,
			new Insets(
				16,
				0,
				16,
				0
			),
			0,
			0
		);

		{
			maxSessions = PlanLoader.getMaxSessions();
			final boolean perfectPlan[] = PlanLoader.getPerfectPlans();
			individualNbTables = new ArrayList<Integer>();
			final Vector<String> listAvailablePlanForTables = new Vector<String>();
			for (int planIndex = 0; planIndex < maxSessions.length; planIndex++) {
				if (maxSessions[planIndex] != 0) {
					individualNbTables.add(planIndex + 1);
					if (perfectPlan[planIndex]) {
						listAvailablePlanForTables.add(Integer.toString(planIndex + 1));
					} else {
						listAvailablePlanForTables.add(
							Integer.toString(planIndex + 1)
								+ "*"
						);
					}
				}
			}

			final JPanel panelIndividualTournament = new JPanel();
			panelIndividualTournament.setBorder(
				BorderFactory.createTitledBorder(
					BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
					translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_TITLE_CREATE_INDIVIDUAL_TOURNAMENT)
				)
			);
			panelIndividualTournament.setLayout(
				new ProportionalGridLayoutInteger(
					3,
					10,
					2,
					2
				)
			);
			final ProportionalGridLayoutConstraint northPanelC = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			{
				northPanelC.y = 0;
				northPanelC.x = 0;
				northPanelC.gridWidth = 1;
				panelIndividualTournament.add(
					new JLabel(
						translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_TITLE_NAME),
						SwingConstants.RIGHT
					),
					northPanelC
				);
				textIndividualNewTournamentName = new JTextField();
				northPanelC.x = 1;
				northPanelC.gridWidth = 7;
				panelIndividualTournament.add(
					textIndividualNewTournamentName,
					northPanelC
				);

				northPanelC.x = 8;
				northPanelC.gridWidth = 1;
				labelIndividualCollision = new JLabel(
					"",
					SwingConstants.CENTER
				);
				labelIndividualCollision.setForeground(Color.RED);
				panelIndividualTournament.add(
					labelIndividualCollision,
					northPanelC
				);

				northPanelC.x = 9;
				northPanelC.gridWidth = 1;
				buttonIndividualAddTournament = new JButton(translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_BUTTON_CREATE));
				panelIndividualTournament.add(
					buttonIndividualAddTournament,
					northPanelC
				);
			}

			{
				northPanelC.y = 1;
				northPanelC.x = 0;
				northPanelC.gridWidth = 1;
				panelIndividualTournament.add(
					new JLabel(
						translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_TITLE_LEAGUE_NAME),
						SwingConstants.RIGHT
					),
					northPanelC
				);
				textIndividualNewTournamentLeagueName = new JTextField();
				northPanelC.x = 1;
				northPanelC.gridWidth = 7;
				panelIndividualTournament.add(
					textIndividualNewTournamentLeagueName,
					northPanelC
				);
			}

			{
				northPanelC.y = 2;
				northPanelC.x = 0;
				northPanelC.gridWidth = 1;
				panelIndividualTournament.add(
					new JLabel(
						translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_TITLE_SESSIONS),
						SwingConstants.RIGHT
					),
					northPanelC
				);

				northPanelC.x = 1;
				northPanelC.gridWidth = 1;
				spinnerModeleIndividualNbSessions = new SpinnerNumberModel(
					NB_DEFAULT_SESSIONS,
					NB_MIN_SESSIONS,
					NB_MAX_SESSIONS,
					1
				);
				spinnerIndividualNbSessions = new JSpinner(spinnerModeleIndividualNbSessions);
				panelIndividualTournament.add(
					spinnerIndividualNbSessions,
					northPanelC
				);

				northPanelC.x = 2;
				northPanelC.gridWidth = 1;
				panelIndividualTournament.add(
					new JLabel(
						translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_TITLE_ROUND_ROBIN_SESSIONS),
						SwingConstants.RIGHT
					),
					northPanelC
				);

				northPanelC.x = 3;
				northPanelC.gridWidth = 1;
				spinnerModeleIndividualNbRoundRobinSessions = new SpinnerNumberModel(
					NB_DEFAULT_SESSIONS,
					NB_MIN_SESSIONS,
					NB_DEFAULT_SESSIONS,
					1
				);
				spinnerIndividualNbRoundRobinSessions = new JSpinner(spinnerModeleIndividualNbRoundRobinSessions);
				panelIndividualTournament.add(
					spinnerIndividualNbRoundRobinSessions,
					northPanelC
				);

				northPanelC.x = 4;
				northPanelC.gridWidth = 1;
				panelIndividualTournament.add(
					new JLabel(
						translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_TITLE_TABLES),
						SwingConstants.RIGHT
					),
					northPanelC
				);

				northPanelC.x = 5;
				northPanelC.gridWidth = 1;
				comboIndividualNbTables = new JComboBox<String>(listAvailablePlanForTables);
				comboIndividualNbTables.setSelectedItem("4");
				panelIndividualTournament.add(
					comboIndividualNbTables,
					northPanelC
				);

				northPanelC.x = 6;
				northPanelC.gridWidth = 1;
				panelIndividualTournament.add(
					new JLabel(
						translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_TITLE_TEAM_SIZE),
						SwingConstants.RIGHT
					),
					northPanelC
				);

				northPanelC.x = 7;
				northPanelC.gridWidth = 1;
				comboIndividualTeamSize = new JComboBox<Integer>();
				comboIndividualTeamSize.addItem(1);
				panelIndividualTournament.add(
					comboIndividualTeamSize,
					northPanelC
				);

				northPanelC.x = 9;
				northPanelC.gridWidth = 1;
				checkWithQualification = new JCheckBox(translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_TITLE_WITH_QUALIFICATION));
				panelIndividualTournament.add(
					checkWithQualification,
					northPanelC
				);
			}

			tournamentPanelC.gridy = 0;
			tournamentPanel.add(
				panelIndividualTournament,
				tournamentPanelC
			);
		}

		{
			final JPanel panelTeamTournament = new JPanel();
			panelTeamTournament.setBorder(
				BorderFactory.createTitledBorder(
					BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
					translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_TITLE_CREATE_TEAM_TOURNAMENT)
				)
			);
			panelTeamTournament.setLayout(
				new ProportionalGridLayoutInteger(
					3,
					10,
					2,
					2
				)
			);
			final ProportionalGridLayoutConstraint teamPanelC = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			{
				teamPanelC.y = 0;
				teamPanelC.x = 0;
				teamPanelC.gridWidth = 1;
				panelTeamTournament.add(
					new JLabel(
						translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_TITLE_NAME),
						SwingConstants.RIGHT
					),
					teamPanelC
				);
				textTeamNewTournamentName = new JTextField();
				teamPanelC.x = 1;
				teamPanelC.gridWidth = 7;
				panelTeamTournament.add(
					textTeamNewTournamentName,
					teamPanelC
				);

				teamPanelC.x = 8;
				teamPanelC.gridWidth = 1;
				labelTeamCollision = new JLabel(
					"",
					SwingConstants.CENTER
				);
				labelTeamCollision.setForeground(Color.RED);
				panelTeamTournament.add(
					labelTeamCollision,
					teamPanelC
				);

				teamPanelC.x = 9;
				teamPanelC.gridWidth = 1;
				buttonTeamAddTournament = new JButton(translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_BUTTON_CREATE));
				panelTeamTournament.add(
					buttonTeamAddTournament,
					teamPanelC
				);
			}

			{
				tournamentPanelC.gridy = 1;
				tournamentPanel.add(
					panelTeamTournament,
					tournamentPanelC
				);

				teamPanelC.y = 1;
				teamPanelC.x = 0;
				teamPanelC.gridWidth = 1;
				panelTeamTournament.add(
					new JLabel(
						translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_TITLE_LEAGUE_NAME),
						SwingConstants.RIGHT
					),
					teamPanelC
				);
				textTeamNewTournamentLeagueName = new JTextField();
				teamPanelC.x = 1;
				teamPanelC.gridWidth = 7;
				panelTeamTournament.add(
					textTeamNewTournamentLeagueName,
					teamPanelC
				);
			}

			{
				teamPanelC.y = 2;
				teamPanelC.x = 0;
				teamPanelC.gridWidth = 1;
				panelTeamTournament.add(
					new JLabel(
						translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_TITLE_SESSIONS),
						SwingConstants.RIGHT
					),
					teamPanelC
				);

				teamPanelC.x = 1;
				teamPanelC.gridWidth = 1;
				spinnerTeamNbSessions = new JSpinner(
					new SpinnerNumberModel(
						NB_DEFAULT_SESSIONS,
						NB_MIN_SESSIONS,
						NB_MAX_SESSIONS,
						1
					)
				);
				panelTeamTournament.add(
					spinnerTeamNbSessions,
					teamPanelC
				);

				teamPanelC.x = 4;
				teamPanelC.gridWidth = 1;
				panelTeamTournament.add(
					new JLabel(
						translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_TITLE_TABLES),
						SwingConstants.RIGHT
					),
					teamPanelC
				);

				teamPanelC.x = 5;
				teamPanelC.gridWidth = 1;
				spinnerTeamNbTables = new JSpinner(
					new SpinnerNumberModel(
						5,
						1,
						20,
						1
					)
				);
				panelTeamTournament.add(
					spinnerTeamNbTables,
					teamPanelC
				);
			}
		}

		final JPanel supportPanel = new JPanel(new GridBagLayout());
		supportPanel.setOpaque(false);
		final GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.anchor = GridBagConstraints.NORTH;
		c.fill = GridBagConstraints.NONE;
		supportPanel.add(
			tournamentPanel,
			c
		);

		setLayout(new BorderLayout());
		add(
			supportPanel,
			BorderLayout.CENTER
		);

		spinnerIndividualNbSessionsListener = (final ChangeEvent e) -> {
			synchronizeRoundRobinSessionSpinner();
			checkIndividualPlan();
		};
		spinnerIndividualNbSessions.addChangeListener(spinnerIndividualNbSessionsListener);
		spinnerIndividualNbRoundRobinSessions.addChangeListener((final ChangeEvent e) -> {
			checkIndividualPlan();
		});
		comboIndividualNbTables.addActionListener((final ActionEvent e) -> {
			checkIndividualPlan();
		});
		checkWithQualification.addActionListener((final ActionEvent e) -> {
			toggleQualificationMode();
		});
		buttonIndividualAddTournament.addActionListener((final ActionEvent e) -> {
			addIndividualTournament();
		});

		spinnerTeamNbSessions.addChangeListener((final ChangeEvent e) -> {
			checkTeamPlan();
		});
		spinnerTeamNbTables.addChangeListener((final ChangeEvent e) -> {
			checkTeamPlan();
		});
		buttonTeamAddTournament.addActionListener((final ActionEvent e) -> {
			addTeamTournament();
		});

		checkTeamPlan();
	}

	@Override
	public String getTabName() {
		return translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_TAB_NAME);
	}

	private void toggleQualificationMode() {
		spinnerIndividualNbSessions.removeChangeListener(spinnerIndividualNbSessionsListener);
		final boolean withQualification = checkWithQualification.isSelected();
		if (withQualification) {
			final int currentSessions = (Integer) spinnerModeleIndividualNbSessions.getValue();
			final int currentRoundRobinSessions = (Integer) spinnerModeleIndividualNbRoundRobinSessions.getValue();

			final int newSessions;
			final int newRoundRobinSessions;
			if (currentSessions >= QUALIFICATION_NB_MIN_SESSIONS) {
				newSessions = currentSessions;
				newRoundRobinSessions = currentSessions - QUALIFICATION_NB_FINALE_SESSIONS;
			} else {
				newSessions = QUALIFICATION_NB_MIN_SESSIONS;
				newRoundRobinSessions = QUALIFICATION_NB_MIN_ROUND_ROBIN_SESSIONS;
			}

			if (newSessions > currentSessions) {
				spinnerModeleIndividualNbSessions.setValue(newSessions);
			}
			spinnerModeleIndividualNbSessions.setMinimum(QUALIFICATION_NB_MIN_SESSIONS);

			if (newRoundRobinSessions > currentRoundRobinSessions) {
				spinnerModeleIndividualNbRoundRobinSessions.setMaximum(newRoundRobinSessions);
				spinnerModeleIndividualNbRoundRobinSessions.setValue(newRoundRobinSessions);
				spinnerModeleIndividualNbRoundRobinSessions.setMinimum(newRoundRobinSessions);
			} else if (newRoundRobinSessions < currentRoundRobinSessions) {
				spinnerModeleIndividualNbRoundRobinSessions.setMinimum(newRoundRobinSessions);
				spinnerModeleIndividualNbRoundRobinSessions.setValue(newRoundRobinSessions);
				spinnerModeleIndividualNbRoundRobinSessions.setMaximum(newRoundRobinSessions);
			}
		} else {
			final int currentSessions = (Integer) spinnerModeleIndividualNbSessions.getValue();
			spinnerModeleIndividualNbSessions.setMinimum(NB_MIN_SESSIONS);
			spinnerModeleIndividualNbRoundRobinSessions.setMaximum(currentSessions);
			spinnerModeleIndividualNbRoundRobinSessions.setMinimum(NB_MIN_SESSIONS);
		}
		spinnerIndividualNbSessions.addChangeListener(spinnerIndividualNbSessionsListener);
	}

	private void synchronizeRoundRobinSessionSpinner() {
		final boolean withQualification = checkWithQualification.isSelected();
		if (withQualification) {
			final int currentSessions = (Integer) spinnerModeleIndividualNbSessions.getValue();
			final int currentRoundRobinSessions = (Integer) spinnerModeleIndividualNbRoundRobinSessions.getValue();
			final int newRoundRobinSessions = currentSessions - QUALIFICATION_NB_FINALE_SESSIONS;
			if (currentSessions > currentRoundRobinSessions) {
				spinnerModeleIndividualNbRoundRobinSessions.setMaximum(newRoundRobinSessions);
				spinnerModeleIndividualNbRoundRobinSessions.setValue(newRoundRobinSessions);
				spinnerModeleIndividualNbRoundRobinSessions.setMinimum(newRoundRobinSessions);
			} else if (currentSessions < currentRoundRobinSessions) {
				spinnerModeleIndividualNbRoundRobinSessions.setMinimum(newRoundRobinSessions);
				spinnerModeleIndividualNbRoundRobinSessions.setValue(newRoundRobinSessions);
				spinnerModeleIndividualNbRoundRobinSessions.setMaximum(newRoundRobinSessions);
			}
		} else {
			final int currentDifference = (Integer) spinnerModeleIndividualNbRoundRobinSessions.getMaximum() - (Integer) spinnerModeleIndividualNbRoundRobinSessions.getValue();
			final int newSessions = (Integer) spinnerModeleIndividualNbSessions.getValue();
			final int newDifference = newSessions - currentDifference;
			if (newDifference > 0) {
				spinnerModeleIndividualNbRoundRobinSessions.setValue(newDifference);
			} else {
				spinnerModeleIndividualNbRoundRobinSessions.setValue(1);
			}
			spinnerModeleIndividualNbRoundRobinSessions.setMaximum(newSessions);
		}
	}

	private void checkIndividualPlan() {
		final int nbTables = individualNbTables.get(comboIndividualNbTables.getSelectedIndex());
		final int nbPlayers = nbTables * 4;
		final int nbSessions = (Integer) spinnerModeleIndividualNbRoundRobinSessions.getValue();
		final int selectedTeamSizeIndex = comboIndividualTeamSize.getSelectedIndex();
		comboIndividualTeamSize.removeAllItems();

		comboIndividualTeamSize.addItem(0);
		if (nbSessions > maxSessions[nbTables - 1]) {
			labelIndividualCollision.setText(translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_TITLE_COLLISION));
			comboIndividualTeamSize.addItem(1);
		} else {
			labelIndividualCollision.setText("");
			final int maxTeamSize = Math.min(
				Math.max(
					nbPlayers - nbSessions * 3,
					1
				),
				nbPlayers / 2
			);
			int teamSize = 1;
			while (teamSize <= maxTeamSize) {
				if (nbPlayers % teamSize == 0) {
					comboIndividualTeamSize.addItem(teamSize);
				}
				teamSize = teamSize << 1;
			}
		}
		if (selectedTeamSizeIndex >= 0 && selectedTeamSizeIndex < comboIndividualTeamSize.getItemCount()) {
			comboIndividualTeamSize.setSelectedIndex(selectedTeamSizeIndex);
		}
	}

	private void addIndividualTournament() {
		final String tournamentName = textIndividualNewTournamentName.getText().trim();
		if (tournamentName.length() > 0) {
			final String tournamentLeagueName = textIndividualNewTournamentLeagueName.getText().trim();
			final int nbSessions = (Integer) spinnerModeleIndividualNbSessions.getValue();
			final int nbRoundRobinSessions = (Integer) spinnerModeleIndividualNbRoundRobinSessions.getValue();
			final int nbTables = individualNbTables.get(comboIndividualNbTables.getSelectedIndex());
			final int nbPlayers = nbTables * 4;
			final int selectedTeamSize = (Integer) comboIndividualTeamSize.getSelectedItem();
			final boolean withQualification = checkWithQualification.isSelected();

			final Tournament<ScoreClassType, ScoreEnumType> tournament = tournamentFactory.createTournament(
				tournamentManager.getAvailableTournamentId(),
				tournamentName,
				tournamentLeagueName,
				nbSessions,
				nbRoundRobinSessions,
				nbTables,
				withQualification,
				false
			);

			final SortedSet<TournamentPlayer> tournamentPlayerSet = tournament.getTournamentPlayers();
			final TournamentPlayer tournamentPlayers[] = new TournamentPlayer[nbPlayers];
			for (int index = 0; index < nbPlayers; index++) {
				final TournamentPlayer tournamentPlayer = new TournamentPlayer(
					index + 1,
					null,
					""
				);
				tournamentPlayerSet.add(tournamentPlayer);
				tournamentPlayers[index] = tournamentPlayer;
			}

			final int planPlayerIndex[][][] = PlanLoader.getPlan(nbTables);
			final int nbMaxSessions = planPlayerIndex.length;
			final Integer tablePlan[] = createIndex(nbTables);
			final Integer sessionPlan[] = createIndex(nbMaxSessions);
			final boolean encounter[][] = new boolean[nbPlayers][nbPlayers];

			final SortedSet<ScoreClassType> tables = tournament.getTables();
			for (int sessionIndex = 0; sessionIndex < nbRoundRobinSessions; sessionIndex++) {
				final int sessionPlanIndex = sessionIndex % nbMaxSessions;
				if (sessionPlanIndex == 0) {
					shuffle(tournamentPlayers);
					shuffle(sessionPlan);
				}
				shuffle(tablePlan);

				for (int tableIndex = 0; tableIndex < nbTables; tableIndex++) {
					for (int player1Index = 0; player1Index < 4; player1Index++) {
						final ScoreClassType table = tournamentFactory.createTournamentScore(
							tournament,
							sessionIndex + 1,
							tableIndex + 1,
							player1Index + 1,
							tournamentPlayers[planPlayerIndex[sessionPlan[sessionPlanIndex]][tablePlan[tableIndex]][player1Index]],
							false
						);
						tables.add(table);
						for (int player2Index = player1Index + 1; player2Index < 4; player2Index++) {
							encounter[planPlayerIndex[sessionPlan[sessionPlanIndex]][tablePlan[tableIndex]][player1Index]][planPlayerIndex[sessionPlan[sessionPlanIndex]][tablePlan[tableIndex]][player2Index]] = true;
							encounter[planPlayerIndex[sessionPlan[sessionPlanIndex]][tablePlan[tableIndex]][player2Index]][planPlayerIndex[sessionPlan[sessionPlanIndex]][tablePlan[tableIndex]][player1Index]] = true;
						}
					}
				}
			}

			if (selectedTeamSize == 0) {
				for (int playerIndex = 0; playerIndex < nbPlayers; playerIndex++) {
					tournamentPlayers[playerIndex].setTeamName("");
				}
			} else {
				int nbPlayersTeam = selectedTeamSize;
				int[][] teamsIndex = generateTeam(
					encounter,
					nbPlayersTeam
				);
				while (teamsIndex == null) {
					nbPlayersTeam = nbPlayersTeam >> 1;
					teamsIndex = generateTeam(
						encounter,
						nbPlayersTeam
					);
				}

				final int nbTeams = nbPlayers / nbPlayersTeam;
				final Integer teamPlan[] = createIndex(nbTeams);
				shuffle(teamPlan);

				for (int teamIndex = 0; teamIndex < nbTeams; teamIndex++) {
					final String teamName = translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_TITLE_TEAM)
						+ " "
						+ Integer.toString(teamPlan[teamIndex] + 1);
					for (int playerIndex = 0; playerIndex < nbPlayersTeam; playerIndex++) {
						tournamentPlayers[teamsIndex[teamIndex][playerIndex]].setTeamName(teamName);
					}
				}
			}

			final Calendar calendar = Calendar.getInstance();
			calendar.set(
				Calendar.MILLISECOND,
				0
			);
			calendar.set(
				Calendar.SECOND,
				0
			);
			calendar.set(
				Calendar.MINUTE,
				0
			);
			calendar.set(
				Calendar.HOUR_OF_DAY,
				0
			);
			calendar.add(
				Calendar.DAY_OF_MONTH,
				1
			);
			final Date today = calendar.getTime();

			final SortedMap<Integer, Date> plannings = tournament.getPlannings();
			for (int sessionIndex = 0; sessionIndex < nbSessions; sessionIndex++) {
				plannings.put(
					sessionIndex + 1,
					today
				);
			}

			if (tournamentManager.addTournament(tournament)) {
				new UIMessageDialog(
					(JFrame) SwingUtilities.getWindowAncestor(this),
					translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_ADD_RESULT_TOURNAMENT_ADDED_MESSAGE),
					translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_ADD_RESULT_TOURNAMENT_ADDED_TITLE),
					MessageDialogMessageType.INFORMATION,
					MessageDialogOptionsType.OK,
					MessageDialogOption.OK,
					translator
				).showOptionDialog();
				informChange();
			} else {
				new UIMessageDialog(
					(JFrame) SwingUtilities.getWindowAncestor(this),
					translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_ADD_RESULT_TOURNAMENT_NOT_ADDED_MESSAGE),
					translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_ADD_RESULT_TOURNAMENT_NOT_ADDED_TITLE),
					MessageDialogMessageType.ERROR,
					MessageDialogOptionsType.OK,
					MessageDialogOption.OK,
					translator
				).showOptionDialog();
			}
		} else {
			new UIMessageDialog(
				(JFrame) SwingUtilities.getWindowAncestor(this),
				translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_ADD_RESULT_TOURNAMENT_NAME_EMPTY_MESSAGE),
				translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_ADD_RESULT_TOURNAMENT_NAME_EMPTY_TITLE),
				MessageDialogMessageType.ERROR,
				MessageDialogOptionsType.OK,
				MessageDialogOption.OK,
				translator
			).showOptionDialog();
		}
	}

	private void checkTeamPlan() {
		final int nbTables = (Integer) spinnerTeamNbTables.getValue();
		if ((Integer) spinnerTeamNbSessions.getValue() > nbTables * 2 - 1) {
			labelTeamCollision.setText(translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_TITLE_COLLISION));
			buttonTeamAddTournament.setEnabled(false);
		} else {
			labelTeamCollision.setText("");
			buttonTeamAddTournament.setEnabled(true);
		}
	}

	private void addTeamTournament() {
		final String tournamentName = textTeamNewTournamentName.getText().trim();
		if (tournamentName.length() > 0) {
			final String tournamentLeagueName = textTeamNewTournamentLeagueName.getText().trim();
			final int nbSessions = (Integer) spinnerTeamNbSessions.getValue();
			final int nbTables = (Integer) spinnerTeamNbTables.getValue();
			final int nbTeams = nbTables * 2;
			final int maxSessions = nbTeams - 1;
			final int nbPlayers = nbTables * 4;

			final int teamPlan[][][] = new int[maxSessions][nbTables][2];
			for (int sessionIndex = 0; sessionIndex < maxSessions; sessionIndex++) {
				teamPlan[sessionIndex][0][0] = nbTeams - 1;
				teamPlan[sessionIndex][0][1] = sessionIndex;

				for (int tableIndex = 1; tableIndex < nbTables; tableIndex++) {
					teamPlan[sessionIndex][tableIndex][0] = (sessionIndex + tableIndex) % maxSessions;
					teamPlan[sessionIndex][tableIndex][1] = (maxSessions + sessionIndex - tableIndex) % maxSessions;
				}
			}

			final Tournament<ScoreClassType, ScoreEnumType> tournament = tournamentFactory.createTournament(
				tournamentManager.getAvailableTournamentId(),
				tournamentName,
				tournamentLeagueName,
				nbSessions,
				nbSessions,
				nbTables,
				false,
				false
			);

			final SortedSet<TournamentPlayer> tournamentPlayerSet = tournament.getTournamentPlayers();
			final TournamentPlayer tournamentPlayers[] = new TournamentPlayer[nbPlayers];
			for (int playerIndex = 0; playerIndex < nbPlayers; playerIndex++) {
				final TournamentPlayer tournamentPlayer = new TournamentPlayer(
					playerIndex + 1,
					null,
					translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_TITLE_TEAM)
						+ " "
						+ (playerIndex / 2 + 1)
				);
				tournamentPlayerSet.add(tournamentPlayer);
				tournamentPlayers[playerIndex] = tournamentPlayer;
			}

			final Integer sessionPlan[] = createIndex(maxSessions);
			final Integer tablePlan[] = createIndex(nbTables);
			shuffle(sessionPlan);

			final SortedSet<ScoreClassType> tables = tournament.getTables();
			for (int sessionIndex = 0; sessionIndex < nbSessions; sessionIndex++) {
				shuffle(tablePlan);
				for (int tableIndex = 0; tableIndex < nbTables; tableIndex++) {
					final ScoreClassType score1 = tournamentFactory.createTournamentScore(
						tournament,
						sessionIndex + 1,
						tableIndex + 1,
						1,
						tournamentPlayers[teamPlan[sessionPlan[sessionIndex]][tablePlan[tableIndex]][0] * 2],
						false
					);
					tables.add(score1);
					final ScoreClassType score3 = tournamentFactory.createTournamentScore(
						tournament,
						sessionIndex + 1,
						tableIndex + 1,
						3,
						tournamentPlayers[teamPlan[sessionPlan[sessionIndex]][tablePlan[tableIndex]][0] * 2 + 1],
						false
					);
					tables.add(score3);
					final ScoreClassType score2 = tournamentFactory.createTournamentScore(
						tournament,
						sessionIndex + 1,
						tableIndex + 1,
						2,
						tournamentPlayers[teamPlan[sessionPlan[sessionIndex]][tablePlan[tableIndex]][1] * 2],
						false
					);
					tables.add(score2);
					final ScoreClassType score4 = tournamentFactory.createTournamentScore(
						tournament,
						sessionIndex + 1,
						tableIndex + 1,
						4,
						tournamentPlayers[teamPlan[sessionPlan[sessionIndex]][tablePlan[tableIndex]][1] * 2 + 1],
						false
					);
					tables.add(score4);
				}
			}

			final Calendar calendar = Calendar.getInstance();
			calendar.set(
				Calendar.MILLISECOND,
				0
			);
			calendar.set(
				Calendar.SECOND,
				0
			);
			calendar.set(
				Calendar.MINUTE,
				0
			);
			calendar.set(
				Calendar.HOUR_OF_DAY,
				0
			);
			calendar.add(
				Calendar.DAY_OF_MONTH,
				1
			);
			final Date today = calendar.getTime();

			final SortedMap<Integer, Date> plannings = tournament.getPlannings();
			for (int sessionIndex = 0; sessionIndex < nbSessions; sessionIndex++) {
				plannings.put(
					sessionIndex + 1,
					today
				);
			}

			if (tournamentManager.addTournament(tournament)) {
				new UIMessageDialog(
					(JFrame) SwingUtilities.getWindowAncestor(this),
					translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_ADD_RESULT_TOURNAMENT_ADDED_MESSAGE),
					translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_ADD_RESULT_TOURNAMENT_ADDED_TITLE),
					MessageDialogMessageType.INFORMATION,
					MessageDialogOptionsType.OK,
					MessageDialogOption.OK,
					translator
				).showOptionDialog();
				informChange();
			} else {
				new UIMessageDialog(
					(JFrame) SwingUtilities.getWindowAncestor(this),
					translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_ADD_RESULT_TOURNAMENT_NOT_ADDED_MESSAGE),
					translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_ADD_RESULT_TOURNAMENT_NOT_ADDED_TITLE),
					MessageDialogMessageType.ERROR,
					MessageDialogOptionsType.OK,
					MessageDialogOption.OK,
					translator
				).showOptionDialog();
			}
		} else {
			new UIMessageDialog(
				(JFrame) SwingUtilities.getWindowAncestor(this),
				translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_ADD_RESULT_TOURNAMENT_NAME_EMPTY_MESSAGE),
				translator.translate(UIText.GUI_PRIMARY_NEW_TOURNAMENT_ADD_RESULT_TOURNAMENT_NAME_EMPTY_TITLE),
				MessageDialogMessageType.ERROR,
				MessageDialogOptionsType.OK,
				MessageDialogOption.OK,
				translator
			).showOptionDialog();
		}
	}

	private int[][] generateTeam(
		final boolean[][] encounter,
		final int nbPlayersTeam
	) {
		final int nbPlayers = encounter.length;
		if (nbPlayers % nbPlayersTeam == 0) {
			final int nbTeams = nbPlayers / nbPlayersTeam;
			final int teams[][] = new int[nbTeams][nbPlayersTeam];

			final List<Integer> availablePlayers = new ArrayList<Integer>();
			for (int player = 0; player < nbPlayers; player++) {
				availablePlayers.add(player);
			}
			if (recursiveGenerateTeam(
				availablePlayers,
				encounter,
				teams,
				0
			)) {
				return teams;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	private boolean recursiveGenerateTeam(
		final List<Integer> availablePlayers,
		final boolean[][] encounter,
		final int teams[][],
		final int currentTeamIndex
	) {
		if (currentTeamIndex == teams.length) {
			return true;
		} else {
			final int teamSize = teams[currentTeamIndex].length;
			final int basePlayer = availablePlayers.get(0);
			final List<Integer> suitablePlayers = new ArrayList<Integer>();
			for (int player = 0; player < encounter[basePlayer].length; player++) {
				if (!encounter[basePlayer][player] && availablePlayers.contains(player)) {
					suitablePlayers.add(player);
				}
			}
			if (suitablePlayers.size() >= teamSize) {
				final int[] combinationIndex = new int[teamSize];
				for (int index = 0; index < teamSize; index++) {
					combinationIndex[index] = index;
				}

				boolean finished = false;
				final List<Integer> newAvailablePlayers = new ArrayList<Integer>();
				while (!finished) {
					boolean noCollision = true;
					for (int index1 = 1; index1 < teamSize - 1; index1++) {
						for (int index2 = index1 + 1; index2 < teamSize; index2++) {
							if (encounter[suitablePlayers.get(combinationIndex[index1])][suitablePlayers.get(combinationIndex[index2])]) {
								noCollision = false;
							}
						}
					}

					if (noCollision) {
						newAvailablePlayers.addAll(availablePlayers);
						for (int index = 0; index < teamSize; index++) {
							final Integer player = suitablePlayers.get(combinationIndex[index]);
							teams[currentTeamIndex][index] = player;
							newAvailablePlayers.remove(player);
						}
						if (recursiveGenerateTeam(
							newAvailablePlayers,
							encounter,
							teams,
							currentTeamIndex + 1
						)) {
							return true;
						}
						newAvailablePlayers.clear();
					}

					int recursifIndex = teamSize - 1;
					while (recursifIndex > 0 && combinationIndex[recursifIndex] == suitablePlayers.size() - (teamSize - recursifIndex)) {
						recursifIndex--;
					}

					if (recursifIndex == 0) {
						finished = true;
					} else {
						combinationIndex[recursifIndex]++;
						for (int index = recursifIndex + 1; index < teamSize; index++) {
							combinationIndex[index] = combinationIndex[index - 1] + 1;
						}
					}
				}
			}
			return false;
		}
	}

	private Integer[] createIndex(final int nbElements) {
		final Integer array[] = new Integer[nbElements];
		for (int index = 0; index < nbElements; index++) {
			array[index] = index;
		}
		return array;
	}

	private <T> void shuffle(final T[] array) {
		final Random random = new Random();
		int index1, index2;
		for (int index = array.length; index > 1; index--) {
			index1 = index - 1;
			index2 = random.nextInt(index);
			final T e = array[index1];
			array[index1] = array[index2];
			array[index2] = e;
		}
	}
}
