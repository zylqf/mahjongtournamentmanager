/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.primary.player;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EtchedBorder;

import fr.bri.awt.ProportionalGridLayoutConstraint;
import fr.bri.awt.ProportionalGridLayoutInteger;
import fr.bri.mj.data.player.CountryCode;
import fr.bri.mj.data.player.Player;
import fr.bri.mj.dataaccess.PlayerManager;
import fr.bri.mj.gui.common.UITabPanel;
import fr.bri.mj.gui.config.UIConfig;
import fr.bri.mj.gui.dialog.UIMessageDialog;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogMessageType;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogOption;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogOptionsType;
import fr.bri.mj.gui.dialog.UIProgressDialog;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;
import fr.bri.mj.utils.CSVFileFilter;
import fr.bri.mj.utils.CSVReader;
import fr.bri.mj.utils.CSVWriter;
import fr.bri.mj.utils.StringNormalizer;

public class UITabPanelManagePlayer extends UITabPanel {

	private static final long serialVersionUID = 7048797807866931022L;

	private final PlayerManager playerManager;

	private final JTextField textNewPlayerFirstName;
	private final JTextField textNewPlayerLastName;
	private final JTextField textNewPlayerLicense;
	private final JComboBox<CountryCode> comboBoxNewPlayerNationality;
	private final JTextField textNewPlayerClub;
	private final JButton buttonAddPlayer;

	private final JTextField textFilterPlayer;
	private final JButton buttonFilterPlayer;
	private final JComboBox<String> comboBoxPlayer;
	private final ActionListener comboBoxPlayerActionListener;
	private final JTextField textModifyPlayerFirstName;
	private final JTextField textModifyPlayerLastName;
	private final JTextField textModifyPlayerLicense;
	private final JComboBox<CountryCode> comboBoxModifyPlayerNationality;
	private final JTextField textModifyPlayerClub;
	private final JButton buttonModifyPlayer;
	private final JButton buttonDeletePlayer;

	private final JButton buttonImportPlayers;
	private final JButton buttonUpdatePlayers;
	private final JButton buttonExportPlayers;
	private final JButton buttonDeleteAllPlayers;

	private final List<Player> listPlayer;
	private final List<String> listPlayerDisplayString;
	private final List<String> listNormalizedPlayerDisplayString;

	private final List<Player> filteredPlayer;

	public UITabPanelManagePlayer(
		final PlayerManager playerManager,
		final UIConfig config,
		final UITextTranslator translator
	) {
		super(
			config,
			translator
		);

		final List<CountryCode> listCountryCodes = new ArrayList<CountryCode>();
		for (final CountryCode countryCode : CountryCode.values()) {
			if (!countryCode.isDummy()) {
				listCountryCodes.add(countryCode);
			}
		}
		Collections.sort(
			listCountryCodes,
			(
				final CountryCode o1,
				final CountryCode o2
			) -> {
				return o1.getCountryName().compareTo(o2.getCountryName());
			}
		);
		final CountryCode[] countryCodes = listCountryCodes.toArray(new CountryCode[0]);

		this.playerManager = playerManager;

		final JPanel playerPanel = new JPanel();
		playerPanel.setLayout(new GridBagLayout());
		final GridBagConstraints playerC = new GridBagConstraints(
			0,
			0,
			1,
			1,
			1.0,
			0.0,
			GridBagConstraints.CENTER,
			GridBagConstraints.HORIZONTAL,
			new Insets(
				16,
				0,
				16,
				0
			),
			0,
			0
		);
		{
			final JPanel addPlayerPanel = new JPanel();
			final ProportionalGridLayoutInteger addPlayerPanelLayout = new ProportionalGridLayoutInteger(
				5,
				6,
				2,
				2
			);
			addPlayerPanel.setLayout(addPlayerPanelLayout);
			addPlayerPanel.setBorder(
				BorderFactory.createTitledBorder(
					BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
					translator.translate(UIText.GUI_PRIMARY_PLAYER_TITLE_ADD_PLAYER)
				)
			);
			final ProportionalGridLayoutConstraint addPlayerC = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			addPlayerC.y = 0;
			addPlayerC.x = 0;
			addPlayerC.gridWidth = 1;
			addPlayerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_PLAYER_TITLE_FIRST_NAME),
					SwingConstants.RIGHT
				),
				addPlayerC
			);
			addPlayerC.x = 1;
			addPlayerC.gridWidth = 2;
			textNewPlayerFirstName = new JTextField();
			addPlayerPanel.add(
				textNewPlayerFirstName,
				addPlayerC
			);

			addPlayerC.x = 3;
			addPlayerC.gridWidth = 1;
			addPlayerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_PLAYER_TITLE_LAST_NAME),
					SwingConstants.RIGHT
				),
				addPlayerC
			);
			addPlayerC.x = 4;
			addPlayerC.gridWidth = 2;
			textNewPlayerLastName = new JTextField();
			addPlayerPanel.add(
				textNewPlayerLastName,
				addPlayerC
			);

			addPlayerC.y = 1;
			addPlayerC.x = 0;
			addPlayerC.gridWidth = 1;
			addPlayerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_PLAYER_TITLE_NATIONALITY),
					SwingConstants.RIGHT
				),
				addPlayerC
			);
			addPlayerC.x = 1;
			addPlayerC.gridWidth = 2;
			comboBoxNewPlayerNationality = new JComboBox<CountryCode>(countryCodes);
			addPlayerPanel.add(
				comboBoxNewPlayerNationality,
				addPlayerC
			);

			addPlayerC.x = 3;
			addPlayerC.gridWidth = 1;
			addPlayerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_PLAYER_TITLE_LICENSE),
					SwingConstants.RIGHT
				),
				addPlayerC
			);
			addPlayerC.x = 4;
			addPlayerC.gridWidth = 2;
			textNewPlayerLicense = new JTextField();
			addPlayerPanel.add(
				textNewPlayerLicense,
				addPlayerC
			);

			addPlayerC.y = 2;

			addPlayerC.x = 3;
			addPlayerC.gridWidth = 1;
			addPlayerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_PLAYER_TITLE_CLUB),
					SwingConstants.RIGHT
				),
				addPlayerC
			);
			addPlayerC.x = 4;
			addPlayerC.gridWidth = 2;
			textNewPlayerClub = new JTextField();
			addPlayerPanel.add(
				textNewPlayerClub,
				addPlayerC
			);

			addPlayerC.y = 4;
			addPlayerC.x = 5;
			addPlayerC.gridWidth = 1;
			buttonAddPlayer = new JButton(translator.translate(UIText.GUI_PRIMARY_PLAYER_BUTTON_ADD));
			addPlayerPanel.add(
				buttonAddPlayer,
				addPlayerC
			);

			playerC.gridy = 0;
			playerPanel.add(
				addPlayerPanel,
				playerC
			);
		}

		{
			final JPanel modifyPlayerPanel = new JPanel();
			final ProportionalGridLayoutInteger modifyPlayerPanelLayout = new ProportionalGridLayoutInteger(
				7,
				6,
				2,
				2
			);

			modifyPlayerPanel.setLayout(modifyPlayerPanelLayout);
			modifyPlayerPanel.setBorder(
				BorderFactory.createTitledBorder(
					BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
					translator.translate(UIText.GUI_PRIMARY_PLAYER_TITLE_MODIFY_PLAYER)
				)
			);
			final ProportionalGridLayoutConstraint modifyPlayerC = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			modifyPlayerC.y = 0;
			modifyPlayerC.x = 0;
			modifyPlayerC.gridWidth = 1;
			modifyPlayerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_PLAYER_TITLE_SEARCH),
					SwingConstants.RIGHT
				),
				modifyPlayerC
			);
			modifyPlayerC.x = 1;
			modifyPlayerC.gridWidth = 4;
			textFilterPlayer = new JTextField();
			modifyPlayerPanel.add(
				textFilterPlayer,
				modifyPlayerC
			);
			modifyPlayerC.x = 5;
			modifyPlayerC.gridWidth = 1;
			buttonFilterPlayer = new JButton(translator.translate(UIText.GUI_PRIMARY_PLAYER_BUTTON_FILTER));
			modifyPlayerPanel.add(
				buttonFilterPlayer,
				modifyPlayerC
			);

			modifyPlayerC.y = 1;
			modifyPlayerC.x = 0;
			modifyPlayerC.gridWidth = 1;
			modifyPlayerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_PLAYER_TITLE_PLAYER),
					SwingConstants.RIGHT
				),
				modifyPlayerC
			);
			modifyPlayerC.x = 1;
			modifyPlayerC.gridWidth = 5;
			comboBoxPlayer = new JComboBox<String>();
			modifyPlayerPanel.add(
				comboBoxPlayer,
				modifyPlayerC
			);

			modifyPlayerC.y = 2;
			modifyPlayerC.x = 0;
			modifyPlayerC.gridWidth = 1;
			modifyPlayerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_PLAYER_TITLE_FIRST_NAME),
					SwingConstants.RIGHT
				),
				modifyPlayerC
			);
			modifyPlayerC.x = 1;
			modifyPlayerC.gridWidth = 2;
			textModifyPlayerFirstName = new JTextField();
			modifyPlayerPanel.add(
				textModifyPlayerFirstName,
				modifyPlayerC
			);

			modifyPlayerC.x = 3;
			modifyPlayerC.gridWidth = 1;
			modifyPlayerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_PLAYER_TITLE_LAST_NAME),
					SwingConstants.RIGHT
				),
				modifyPlayerC
			);
			modifyPlayerC.x = 4;
			modifyPlayerC.gridWidth = 2;
			textModifyPlayerLastName = new JTextField();
			modifyPlayerPanel.add(
				textModifyPlayerLastName,
				modifyPlayerC
			);

			modifyPlayerC.y = 3;
			modifyPlayerC.x = 0;
			modifyPlayerC.gridWidth = 1;
			modifyPlayerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_PLAYER_TITLE_NATIONALITY),
					SwingConstants.RIGHT
				),
				modifyPlayerC
			);
			modifyPlayerC.x = 1;
			modifyPlayerC.gridWidth = 2;
			comboBoxModifyPlayerNationality = new JComboBox<CountryCode>(countryCodes);
			modifyPlayerPanel.add(
				comboBoxModifyPlayerNationality,
				modifyPlayerC
			);

			modifyPlayerC.x = 3;
			modifyPlayerC.gridWidth = 1;
			modifyPlayerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_PLAYER_TITLE_LICENSE),
					SwingConstants.RIGHT
				),
				modifyPlayerC
			);
			modifyPlayerC.x = 4;
			modifyPlayerC.gridWidth = 2;
			textModifyPlayerLicense = new JTextField();
			modifyPlayerPanel.add(
				textModifyPlayerLicense,
				modifyPlayerC
			);

			modifyPlayerC.y = 4;

			modifyPlayerC.x = 3;
			modifyPlayerC.gridWidth = 1;
			modifyPlayerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_PLAYER_TITLE_CLUB),
					SwingConstants.RIGHT
				),
				modifyPlayerC
			);
			modifyPlayerC.x = 4;
			modifyPlayerC.gridWidth = 2;
			textModifyPlayerClub = new JTextField();
			modifyPlayerPanel.add(
				textModifyPlayerClub,
				modifyPlayerC
			);

			modifyPlayerC.y = 6;
			modifyPlayerC.x = 0;
			modifyPlayerC.gridWidth = 1;
			buttonDeletePlayer = new JButton(translator.translate(UIText.GUI_PRIMARY_PLAYER_BUTTON_DELETE));
			modifyPlayerPanel.add(
				buttonDeletePlayer,
				modifyPlayerC
			);

			modifyPlayerC.x = 5;
			modifyPlayerC.gridWidth = 1;
			buttonModifyPlayer = new JButton(translator.translate(UIText.GUI_PRIMARY_PLAYER_BUTTON_MODIFY));
			modifyPlayerPanel.add(
				buttonModifyPlayer,
				modifyPlayerC
			);

			playerC.gridy = 1;
			playerPanel.add(
				modifyPlayerPanel,
				playerC
			);
		}

		{
			final JPanel batchPanel = new JPanel();
			final ProportionalGridLayoutInteger batchPanelLayout = new ProportionalGridLayoutInteger(
				1,
				7,
				0,
				0
			);
			batchPanel.setLayout(batchPanelLayout);
			batchPanel.setBorder(
				BorderFactory.createTitledBorder(
					BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
					translator.translate(UIText.GUI_PRIMARY_PLAYER_TITLE_BATCH)
				)
			);
			final ProportionalGridLayoutConstraint batchC = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			batchC.y = 0;
			batchC.gridWidth = 1;

			batchC.x = 0;
			buttonImportPlayers = new JButton(translator.translate(UIText.GUI_PRIMARY_PLAYER_BUTTON_IMPORT_PLAYERS));
			batchPanel.add(
				buttonImportPlayers,
				batchC
			);

			batchC.x = 2;
			buttonUpdatePlayers = new JButton(translator.translate(UIText.GUI_PRIMARY_PLAYER_BUTTON_UPDATE_PLAYERS));
			batchPanel.add(
				buttonUpdatePlayers,
				batchC
			);

			batchC.x = 4;
			buttonExportPlayers = new JButton(translator.translate(UIText.GUI_PRIMARY_PLAYER_BUTTON_EXPORT_PLAYERS));
			batchPanel.add(
				buttonExportPlayers,
				batchC
			);

			batchC.x = 6;
			buttonDeleteAllPlayers = new JButton(translator.translate(UIText.GUI_PRIMARY_PLAYER_BUTTON_DELETE_PLAYERS));
			batchPanel.add(
				buttonDeleteAllPlayers,
				batchC
			);

			playerC.gridy = 2;
			playerPanel.add(
				batchPanel,
				playerC
			);
		}

		setLayout(new GridBagLayout());
		final GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.anchor = GridBagConstraints.NORTH;
		c.fill = GridBagConstraints.NONE;
		add(
			playerPanel,
			c
		);

		listPlayer = new ArrayList<Player>();
		listPlayerDisplayString = new ArrayList<String>();
		listNormalizedPlayerDisplayString = new ArrayList<String>();

		filteredPlayer = new ArrayList<Player>();

		buttonFilterPlayer.addActionListener((final ActionEvent e) -> {
			filterPlayers();
		});
		comboBoxPlayerActionListener = (final ActionEvent e) -> {
			displayPlayer();
		};
		comboBoxPlayer.addActionListener(comboBoxPlayerActionListener);
		buttonAddPlayer.addActionListener((final ActionEvent e) -> {
			addPlayer();
		});
		buttonModifyPlayer.addActionListener((final ActionEvent e) -> {
			modifyPlayer();
		});
		buttonDeletePlayer.addActionListener((final ActionEvent e) -> {
			deletePlayer();
		});
		buttonImportPlayers.addActionListener((final ActionEvent e) -> {
			importPlayers();
		});
		buttonUpdatePlayers.addActionListener((final ActionEvent e) -> {
			updatePlayers();
		});
		buttonExportPlayers.addActionListener((final ActionEvent e) -> {
			exportPlayers();
		});
		buttonDeleteAllPlayers.addActionListener((final ActionEvent e) -> {
			deleteAllPlayers();
		});
	}

	@Override
	public String getTabName() {
		return translator.translate(UIText.GUI_PRIMARY_PLAYER_TAB_NAME);
	}

	@Override
	public void remoteRefresh() {
		refresh(false);
	}

	@Override
	public void refresh(final boolean hard) {
		refreshPlayer();
	}

	private void refreshPlayer() {
		listPlayer.clear();
		listPlayer.addAll(playerManager.getAllPlayers());
		Collections.sort(listPlayer);

		listPlayerDisplayString.clear();
		listNormalizedPlayerDisplayString.clear();
		for (int playerIndex = 0; playerIndex < listPlayer.size(); playerIndex++) {
			final Player player = listPlayer.get(playerIndex);
			final String displayString = Integer.toString(player.getId())
				+ " - "
				+ player.getPlayerFirstName()
				+ " "
				+ player.getPlayerLastName();
			final String normalizedDisplayString = StringNormalizer.normalize(displayString).toLowerCase();
			listPlayerDisplayString.add(displayString);
			listNormalizedPlayerDisplayString.add(normalizedDisplayString);
		}

		filterPlayers();
	}

	private void filterPlayers() {
		comboBoxPlayer.removeActionListener(comboBoxPlayerActionListener);
		comboBoxPlayer.removeAllItems();

		final String searchText = textFilterPlayer.getText().trim().toLowerCase();
		filteredPlayer.clear();
		for (int playerIndex = 0; playerIndex < listPlayer.size(); playerIndex++) {
			final Player player = listPlayer.get(playerIndex);
			if (listNormalizedPlayerDisplayString.get(playerIndex).contains(searchText)) {
				filteredPlayer.add(player);
				comboBoxPlayer.addItem(listPlayerDisplayString.get(playerIndex));
			}
		}

		comboBoxPlayer.addActionListener(comboBoxPlayerActionListener);
		if (filteredPlayer.size() > 0) {
			comboBoxPlayer.setSelectedIndex(0);
		} else {
			comboBoxPlayer.setSelectedIndex(-1);
		}
	}

	private void addPlayer() {
		final String playerFirstName = textNewPlayerFirstName.getText().trim();
		final String playerLastName = textNewPlayerLastName.getText().trim();
		final CountryCode playerNationality = (CountryCode) comboBoxNewPlayerNationality.getSelectedItem();
		final String playerClub = textNewPlayerClub.getText().trim();
		final String playerLicense = textNewPlayerLicense.getText().trim();
		if (playerFirstName != null && playerFirstName.length() > 0 && playerLastName != null && playerLastName.length() > 0) {
			final Player newPlayer = new Player(
				playerManager.getAvailablePlayerId(),
				playerFirstName,
				playerLastName,
				playerNationality,
				playerClub,
				playerLicense
			);
			if (playerManager.addPlayer(newPlayer)) {
				new UIMessageDialog(
					(JFrame) SwingUtilities.getWindowAncestor(this),
					translator.translate(UIText.GUI_PRIMARY_PLAYER_ADD_RESULT_PLAYER_ADDED_MESSAGE),
					translator.translate(UIText.GUI_PRIMARY_PLAYER_ADD_RESULT_PLAYER_ADDED_TITLE),
					MessageDialogMessageType.INFORMATION,
					MessageDialogOptionsType.OK,
					MessageDialogOption.OK,
					translator
				).showOptionDialog();
				textNewPlayerFirstName.setText("");
				textNewPlayerLastName.setText("");
				textNewPlayerClub.setText("");
				textNewPlayerLicense.setText("");
				refreshPlayer();
			} else {
				new UIMessageDialog(
					(JFrame) SwingUtilities.getWindowAncestor(this),
					translator.translate(UIText.GUI_PRIMARY_PLAYER_ADD_RESULT_PLAYER_NOT_ADDED_MESSAGE),
					translator.translate(UIText.GUI_PRIMARY_PLAYER_ADD_RESULT_PLAYER_NOT_ADDED_TITLE),
					MessageDialogMessageType.ERROR,
					MessageDialogOptionsType.OK,
					MessageDialogOption.OK,
					translator
				).showOptionDialog();
			}
		} else {
			new UIMessageDialog(
				(JFrame) SwingUtilities.getWindowAncestor(this),
				translator.translate(UIText.GUI_PRIMARY_PLAYER_ADD_RESULT_PLAYER_NAME_EMPTY_MESSAGE),
				translator.translate(UIText.GUI_PRIMARY_PLAYER_ADD_RESULT_PLAYER_NAME_EMPTY_TITLE),
				MessageDialogMessageType.ERROR,
				MessageDialogOptionsType.OK,
				MessageDialogOption.OK,
				translator
			).showOptionDialog();
		}
	}

	private void displayPlayer() {
		final int selectedPlayerIndex = comboBoxPlayer.getSelectedIndex();
		if (selectedPlayerIndex != -1) {
			final Player player = filteredPlayer.get(selectedPlayerIndex);
			textModifyPlayerFirstName.setText(player.getPlayerFirstName());
			textModifyPlayerLastName.setText(player.getPlayerLastName());
			comboBoxModifyPlayerNationality.setSelectedItem(player.getPlayerNationality());
			textModifyPlayerClub.setText(player.getClub());
			textModifyPlayerLicense.setText(player.getLicense());
		} else {
			textModifyPlayerFirstName.setText("");
			textModifyPlayerLastName.setText("");
			comboBoxModifyPlayerNationality.setSelectedIndex(-1);
			textModifyPlayerClub.setText("");
			textModifyPlayerLicense.setText("");
		}
	}

	private void modifyPlayer() {
		final int selectedPlayerIndex = comboBoxPlayer.getSelectedIndex();
		if (selectedPlayerIndex != -1) {
			final Player player = filteredPlayer.get(selectedPlayerIndex);
			final String playerFirstName = textModifyPlayerFirstName.getText().trim();
			final String playerLastName = textModifyPlayerLastName.getText().trim();
			final CountryCode playerNationality = (CountryCode) comboBoxModifyPlayerNationality.getSelectedItem();
			final String playerClub = textModifyPlayerClub.getText().trim();
			final String playerLicense = textModifyPlayerLicense.getText().trim();
			if (playerFirstName != null && playerFirstName.length() > 0 && playerLastName != null && playerLastName.length() > 0) {
				player.setPlayerFirstName(playerFirstName);
				player.setPlayerLastName(playerLastName);
				player.setPlayerNationality(playerNationality);
				player.setClub(playerClub);
				player.setLicense(playerLicense);
				if (playerManager.updatePlayer(player)) {
					new UIMessageDialog(
						(JFrame) SwingUtilities.getWindowAncestor(this),
						translator.translate(UIText.GUI_PRIMARY_PLAYER_MODIFY_RESULT_PLAYER_MODIFIED_MESSAGE),
						translator.translate(UIText.GUI_PRIMARY_PLAYER_MODIFY_RESULT_PLAYER_MODIFIED_TITLE),
						MessageDialogMessageType.INFORMATION,
						MessageDialogOptionsType.OK,
						MessageDialogOption.OK,
						translator
					).showOptionDialog();
					refreshPlayer();
					informChange();
				} else {
					new UIMessageDialog(
						(JFrame) SwingUtilities.getWindowAncestor(this),
						translator.translate(UIText.GUI_PRIMARY_PLAYER_MODIFY_RESULT_PLAYER_NOT_MODIFIED_MESSAGE),
						translator.translate(UIText.GUI_PRIMARY_PLAYER_MODIFY_RESULT_PLAYER_NOT_MODIFIED_TITLE),
						MessageDialogMessageType.ERROR,
						MessageDialogOptionsType.OK,
						MessageDialogOption.OK,
						translator
					).showOptionDialog();
				}
			} else {
				new UIMessageDialog(
					(JFrame) SwingUtilities.getWindowAncestor(this),
					translator.translate(UIText.GUI_PRIMARY_PLAYER_MODIFY_RESULT_PLAYER_NAME_EMPTY_MESSAGE),
					translator.translate(UIText.GUI_PRIMARY_PLAYER_MODIFY_RESULT_PLAYER_NAME_EMPTY_TITLE),
					MessageDialogMessageType.ERROR,
					MessageDialogOptionsType.OK,
					MessageDialogOption.OK,
					translator
				).showOptionDialog();
			}
		}
	}

	private void deletePlayer() {
		final int selectedPlayerIndex = comboBoxPlayer.getSelectedIndex();
		if (selectedPlayerIndex != -1) {
			final Player player = filteredPlayer.get(selectedPlayerIndex);
			if (playerManager.deletePlayer(player)) {
				new UIMessageDialog(
					(JFrame) SwingUtilities.getWindowAncestor(this),
					translator.translate(UIText.GUI_PRIMARY_PLAYER_DELETE_RESULT_PLAYER_DELETED_MESSAGE),
					translator.translate(UIText.GUI_PRIMARY_PLAYER_DELETE_RESULT_PLAYER_DELETED_TITLE),
					MessageDialogMessageType.INFORMATION,
					MessageDialogOptionsType.OK,
					MessageDialogOption.OK,
					translator
				).showOptionDialog();
				refreshPlayer();
			} else {
				new UIMessageDialog(
					(JFrame) SwingUtilities.getWindowAncestor(this),
					translator.translate(UIText.GUI_PRIMARY_PLAYER_DELETE_RESULT_PLAYER_NOT_DELETED_MESSAGE),
					translator.translate(UIText.GUI_PRIMARY_PLAYER_DELETE_RESULT_PLAYER_NOT_DELETED_TITLE),
					MessageDialogMessageType.ERROR,
					MessageDialogOptionsType.OK,
					MessageDialogOption.OK,
					translator
				).showOptionDialog();
			}
		}
	}

	private static final int BATCH_CSV_FORMAT_NB_COLUMNS = 6;
	private static final int BATCH_CSV_FORMAT_INDEX_ID = 0;
	private static final int BATCH_CSV_FORMAT_INDEX_FIRST_NAME = 1;
	private static final int BATCH_CSV_FORMAT_INDEX_LAST_NAME = 2;
	private static final int BATCH_CSV_FORMAT_INDEX_NATIONALY = 3;
	private static final int BATCH_CSV_FORMAT_INDEX_CLUB = 4;
	private static final int BATCH_CSV_FORMAT_INDEX_LISENCE = 5;
	private static final int BATCH_CSV_FORMAT_NON_EMPTY_INDEX[] = {
		BATCH_CSV_FORMAT_INDEX_ID,
		BATCH_CSV_FORMAT_INDEX_FIRST_NAME,
		BATCH_CSV_FORMAT_INDEX_LAST_NAME,
		BATCH_CSV_FORMAT_INDEX_NATIONALY
	};

	private void importPlayers() {
		JFileChooser csvFileChooser = new JFileChooser();
		if (lastChoosenFolder != null) {
			csvFileChooser = new JFileChooser(lastChoosenFolder);
		} else {
			csvFileChooser = new JFileChooser();
		}
		csvFileChooser.setMultiSelectionEnabled(false);
		csvFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		csvFileChooser.setFileFilter(new CSVFileFilter());

		final int answer = csvFileChooser.showOpenDialog(this);
		if (answer == JFileChooser.APPROVE_OPTION) {
			final File dataFile = csvFileChooser.getSelectedFile();
			if (dataFile != null && dataFile.exists()) {
				final List<List<String>> csvData = CSVReader.read(
					dataFile,
					BATCH_CSV_FORMAT_NB_COLUMNS
				);
				if (!csvData.isEmpty()) {
					final UIProgressDialog progressDialog = new UIProgressDialog(
						(JFrame) SwingUtilities.getWindowAncestor(this),
						csvData.size(),
						translator
					);
					progressDialog.setComponentShownListener((final ComponentEvent componentEvent) -> {
						new Thread(() -> {
							try {
								final CountryCode countryCodeValues[] = CountryCode.values();

								int totalPlayers = 0;
								int addedPlayers = 0;
								for (int indexCsv = 0; indexCsv < csvData.size(); indexCsv++) {
									final List<String> csvLine = csvData.get(indexCsv);
									if (csvLine != null && csvLine.size() == BATCH_CSV_FORMAT_NB_COLUMNS) {
										boolean empty = false;
										for (int indexNonEmpty = 0; indexNonEmpty < BATCH_CSV_FORMAT_NON_EMPTY_INDEX.length; indexNonEmpty++) {
											empty = empty || csvLine.get(BATCH_CSV_FORMAT_NON_EMPTY_INDEX[indexNonEmpty]).isEmpty();
										}
										if (!empty) {
											final Player player = new Player(
												Integer.parseInt(csvLine.get(BATCH_CSV_FORMAT_INDEX_ID)),
												csvLine.get(BATCH_CSV_FORMAT_INDEX_FIRST_NAME),
												csvLine.get(BATCH_CSV_FORMAT_INDEX_LAST_NAME),
												countryCodeValues[Integer.parseInt(csvLine.get(BATCH_CSV_FORMAT_INDEX_NATIONALY))],
												csvLine.get(BATCH_CSV_FORMAT_INDEX_CLUB),
												csvLine.get(BATCH_CSV_FORMAT_INDEX_LISENCE)
											);
											totalPlayers++;
											if (playerManager.addPlayer(player)) {
												addedPlayers++;
											}
										}
									}
									progressDialog.setProgress(indexCsv);
								}

								progressDialog.setVisible(false);
								new UIMessageDialog(
									(JFrame) SwingUtilities.getWindowAncestor(this),
									Integer.toString(addedPlayers)
										+ " "
										+ translator.translate(UIText.GUI_PRIMARY_PLAYER_IMPORT_RESULT_PLAYER_IMPORTED_MESSAGE_1)
										+ " "
										+ Integer.toString(totalPlayers)
										+ " "
										+ translator.translate(UIText.GUI_PRIMARY_PLAYER_IMPORT_RESULT_PLAYER_IMPORTED_MESSAGE_2),
									translator.translate(UIText.GUI_PRIMARY_PLAYER_IMPORT_RESULT_PLAYER_IMPORTED_TITLE),
									MessageDialogMessageType.INFORMATION,
									MessageDialogOptionsType.OK,
									MessageDialogOption.OK,
									translator
								).showOptionDialog();

								refreshPlayer();
							} catch (final Exception exception) {
								exception.printStackTrace();
								progressDialog.setVisible(false);
								new UIMessageDialog(
									(JFrame) SwingUtilities.getWindowAncestor(this),
									translator.translate(UIText.GUI_PRIMARY_PLAYER_IMPORT_RESULT_FILE_FORMAT_MESSAGE),
									translator.translate(UIText.GUI_PRIMARY_PLAYER_IMPORT_RESULT_FILE_FORMAT_TITLE),
									MessageDialogMessageType.ERROR,
									MessageDialogOptionsType.OK,
									MessageDialogOption.OK,
									translator
								).showOptionDialog();
							}
						}).start();
					});
					progressDialog.showProgressDialog();
				} else {
					new UIMessageDialog(
						(JFrame) SwingUtilities.getWindowAncestor(this),
						translator.translate(UIText.GUI_PRIMARY_PLAYER_IMPORT_RESULT_FILE_FORMAT_MESSAGE),
						translator.translate(UIText.GUI_PRIMARY_PLAYER_IMPORT_RESULT_FILE_FORMAT_TITLE),
						MessageDialogMessageType.ERROR,
						MessageDialogOptionsType.OK,
						MessageDialogOption.OK,
						translator
					).showOptionDialog();
				}
			} else {
				new UIMessageDialog(
					(JFrame) SwingUtilities.getWindowAncestor(this),
					translator.translate(UIText.GUI_PRIMARY_PLAYER_IMPORT_RESULT_READ_ERROR_MESSAGE),
					translator.translate(UIText.GUI_PRIMARY_PLAYER_IMPORT_RESULT_READ_ERROR_TITLE),
					MessageDialogMessageType.ERROR,
					MessageDialogOptionsType.OK,
					MessageDialogOption.OK,
					translator
				).showOptionDialog();
			}
		}
	}

	private void updatePlayers() {
		JFileChooser csvFileChooser = new JFileChooser();
		if (lastChoosenFolder != null) {
			csvFileChooser = new JFileChooser(lastChoosenFolder);
		} else {
			csvFileChooser = new JFileChooser();
		}
		csvFileChooser.setMultiSelectionEnabled(false);
		csvFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		csvFileChooser.setFileFilter(new CSVFileFilter());

		final int answer = csvFileChooser.showOpenDialog(this);
		if (answer == JFileChooser.APPROVE_OPTION) {
			final File dataFile = csvFileChooser.getSelectedFile();
			if (dataFile != null && dataFile.exists()) {
				lastChoosenFolder = dataFile.getParentFile();
				final List<List<String>> csvData = CSVReader.read(
					dataFile,
					BATCH_CSV_FORMAT_NB_COLUMNS
				);
				if (!csvData.isEmpty()) {
					final UIProgressDialog progressDialog = new UIProgressDialog(
						(JFrame) SwingUtilities.getWindowAncestor(this),
						csvData.size(),
						translator
					);
					progressDialog.setComponentShownListener((final ComponentEvent componentEvent) -> {
						new Thread(() -> {
							try {
								final CountryCode countryCodeValues[] = CountryCode.values();

								final SortedMap<Integer, Player> mapIdToPlayer = new TreeMap<Integer, Player>();
								for (final Player player : listPlayer) {
									mapIdToPlayer.put(
										player.getId(),
										player
									);
								}

								int totalPlayers = 0;
								int modifiedPlayers = 0;
								for (int indexCsv = 0; indexCsv < csvData.size(); indexCsv++) {
									final List<String> csvLine = csvData.get(indexCsv);
									if (csvLine != null && csvLine.size() == BATCH_CSV_FORMAT_NB_COLUMNS) {
										boolean empty = false;
										for (int indexNonEmpty = 0; indexNonEmpty < BATCH_CSV_FORMAT_NON_EMPTY_INDEX.length; indexNonEmpty++) {
											empty = empty || csvLine.get(BATCH_CSV_FORMAT_NON_EMPTY_INDEX[indexNonEmpty]).isEmpty();
										}
										if (!empty) {
											final Player player = mapIdToPlayer.get(Integer.parseInt(csvLine.get(BATCH_CSV_FORMAT_INDEX_ID)));
											if (player != null) {
												player.setPlayerFirstName(csvLine.get(BATCH_CSV_FORMAT_INDEX_FIRST_NAME));
												player.setPlayerLastName(csvLine.get(BATCH_CSV_FORMAT_INDEX_LAST_NAME));
												player.setPlayerNationality(countryCodeValues[Integer.parseInt(csvLine.get(BATCH_CSV_FORMAT_INDEX_NATIONALY))]);
												player.setClub(csvLine.get(BATCH_CSV_FORMAT_INDEX_CLUB));
												player.setLicense(csvLine.get(BATCH_CSV_FORMAT_INDEX_LISENCE));

												totalPlayers++;
												if (playerManager.updatePlayer(player)) {
													modifiedPlayers++;
												}
											}
										}
									}
									progressDialog.setProgress(indexCsv);
								}

								progressDialog.setVisible(false);
								new UIMessageDialog(
									(JFrame) SwingUtilities.getWindowAncestor(this),
									Integer.toString(modifiedPlayers)
										+ " "
										+ translator.translate(UIText.GUI_PRIMARY_PLAYER_UPDATE_RESULT_PLAYER_UPDATED_MESSAGE_1)
										+ " "
										+ Integer.toString(totalPlayers)
										+ " "
										+ translator.translate(UIText.GUI_PRIMARY_PLAYER_UPDATE_RESULT_PLAYER_UPDATED_MESSAGE_2),
									translator.translate(UIText.GUI_PRIMARY_PLAYER_UPDATE_RESULT_PLAYER_UPDATED_TITLE),
									MessageDialogMessageType.INFORMATION,
									MessageDialogOptionsType.OK,
									MessageDialogOption.OK,
									translator
								).showOptionDialog();

								refreshPlayer();
								informChange();
							} catch (final Exception exception) {
								exception.printStackTrace();
								progressDialog.setVisible(false);
								new UIMessageDialog(
									(JFrame) SwingUtilities.getWindowAncestor(this),
									translator.translate(UIText.GUI_PRIMARY_PLAYER_UPDATE_RESULT_FILE_FORMAT_MESSAGE),
									translator.translate(UIText.GUI_PRIMARY_PLAYER_UPDATE_RESULT_FILE_FORMAT_TITLE),
									MessageDialogMessageType.ERROR,
									MessageDialogOptionsType.OK,
									MessageDialogOption.OK,
									translator
								).showOptionDialog();
							}
						}).start();
					});
					progressDialog.showProgressDialog();
				} else {
					new UIMessageDialog(
						(JFrame) SwingUtilities.getWindowAncestor(this),
						translator.translate(UIText.GUI_PRIMARY_PLAYER_UPDATE_RESULT_FILE_FORMAT_MESSAGE),
						translator.translate(UIText.GUI_PRIMARY_PLAYER_UPDATE_RESULT_FILE_FORMAT_TITLE),
						MessageDialogMessageType.ERROR,
						MessageDialogOptionsType.OK,
						MessageDialogOption.OK,
						translator
					).showOptionDialog();
				}
			} else {
				new UIMessageDialog(
					(JFrame) SwingUtilities.getWindowAncestor(this),
					translator.translate(UIText.GUI_PRIMARY_PLAYER_UPDATE_RESULT_READ_ERROR_MESSAGE),
					translator.translate(UIText.GUI_PRIMARY_PLAYER_UPDATE_RESULT_READ_ERROR_TITLE),
					MessageDialogMessageType.ERROR,
					MessageDialogOptionsType.OK,
					MessageDialogOption.OK,
					translator
				).showOptionDialog();
			}
		}
	}

	private void exportPlayers() {
		final JFileChooser csvFileChooser = new JFileChooser();
		csvFileChooser.setMultiSelectionEnabled(false);
		csvFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		if (lastChoosenFolder != null) {
			csvFileChooser.setSelectedFile(
				new File(
					lastChoosenFolder,
					"Player.csv"
				)
			);
		} else {
			csvFileChooser.setSelectedFile(new File("Player.csv"));
		}
		File csvFile = null;
		boolean toContinue = true;
		while (toContinue) {
			final int fileChooserAnswer = csvFileChooser.showSaveDialog(this);
			if (fileChooserAnswer == JFileChooser.APPROVE_OPTION) {
				csvFile = csvFileChooser.getSelectedFile();
				if (csvFile.exists()) {
					final MessageDialogOption overwriteAnswer = new UIMessageDialog(
						(JFrame) SwingUtilities.getWindowAncestor(this),
						translator.translate(UIText.GUI_PRIMARY_PLAYER_EXPORT_OVERWRITE_CONFIRM_MESSAGE),
						translator.translate(UIText.GUI_PRIMARY_PLAYER_EXPORT_OVERWRITE_CONFIRM_TITLE),
						MessageDialogMessageType.WARNING,
						MessageDialogOptionsType.YES_NO_CANCEL,
						MessageDialogOption.NO,
						translator
					).showOptionDialog();
					switch (overwriteAnswer) {
						case YES:
							toContinue = false;
							break;
						case NO:
							csvFile = null;
							break;
						case CANCEL:
						case CLOSED:
							csvFile = null;
							toContinue = false;
							break;
						default:
							break;
					}
				} else {
					toContinue = false;
				}
			} else {
				csvFile = null;
				toContinue = false;
			}
		}

		if (csvFile != null) {
			lastChoosenFolder = csvFileChooser.getCurrentDirectory();
			final String[][] csvContent = new String[listPlayer.size()][BATCH_CSV_FORMAT_NB_COLUMNS];
			for (int indexPlayer = 0; indexPlayer < listPlayer.size(); indexPlayer++) {
				final Player player = listPlayer.get(indexPlayer);
				csvContent[indexPlayer][BATCH_CSV_FORMAT_INDEX_ID] = Integer.toString(player.getId());
				csvContent[indexPlayer][BATCH_CSV_FORMAT_INDEX_FIRST_NAME] = player.getPlayerFirstName();
				csvContent[indexPlayer][BATCH_CSV_FORMAT_INDEX_LAST_NAME] = player.getPlayerLastName();
				csvContent[indexPlayer][BATCH_CSV_FORMAT_INDEX_NATIONALY] = Integer.toString(player.getPlayerNationality().ordinal());
				csvContent[indexPlayer][BATCH_CSV_FORMAT_INDEX_CLUB] = player.getClub();
				csvContent[indexPlayer][BATCH_CSV_FORMAT_INDEX_LISENCE] = player.getLicense();
			}

			if (CSVWriter.write(
				csvFile,
				csvContent,
				null
			)) {
				new UIMessageDialog(
					(JFrame) SwingUtilities.getWindowAncestor(this),
					translator.translate(UIText.GUI_PRIMARY_PLAYER_EXPORT_RESULT_PLAYER_EXPORTED_MESSAGE),
					translator.translate(UIText.GUI_PRIMARY_PLAYER_EXPORT_RESULT_PLAYER_EXPORTED_TITLE),
					MessageDialogMessageType.INFORMATION,
					MessageDialogOptionsType.OK,
					MessageDialogOption.OK,
					translator
				).showOptionDialog();
			} else {
				new UIMessageDialog(
					(JFrame) SwingUtilities.getWindowAncestor(this),
					translator.translate(UIText.GUI_PRIMARY_PLAYER_EXPORT_RESULT_WRITE_ERROR_MESSAGE),
					translator.translate(UIText.GUI_PRIMARY_PLAYER_EXPORT_RESULT_WRITE_ERROR_TITLE),
					MessageDialogMessageType.ERROR,
					MessageDialogOptionsType.OK,
					MessageDialogOption.OK,
					translator
				).showOptionDialog();
			}
		}
	}

	private void deleteAllPlayers() {
		MessageDialogOption answer = new UIMessageDialog(
			(JFrame) SwingUtilities.getWindowAncestor(this),
			translator.translate(UIText.GUI_PRIMARY_PLAYER_DELETE_ALL_CONFIRM_1_MESSAGE),
			translator.translate(UIText.GUI_PRIMARY_PLAYER_DELETE_ALL_CONFIRM_1_TITLE),
			MessageDialogMessageType.QUESTION,
			MessageDialogOptionsType.YES_NO,
			MessageDialogOption.NO,
			translator
		).showOptionDialog();
		if (answer == MessageDialogOption.YES) {
			answer = new UIMessageDialog(
				(JFrame) SwingUtilities.getWindowAncestor(this),
				translator.translate(UIText.GUI_PRIMARY_PLAYER_DELETE_ALL_CONFIRM_2_MESSAGE),
				translator.translate(UIText.GUI_PRIMARY_PLAYER_DELETE_ALL_CONFIRM_2_TITLE),
				MessageDialogMessageType.QUESTION,
				MessageDialogOptionsType.YES_NO,
				MessageDialogOption.NO,
				translator
			).showOptionDialog();
			if (answer == MessageDialogOption.YES) {
				final UIProgressDialog progressDialog = new UIProgressDialog(
					(JFrame) SwingUtilities.getWindowAncestor(this),
					listPlayer.size(),
					translator
				);
				progressDialog.setComponentShownListener((final ComponentEvent componentEvent) -> {
					new Thread(() -> {
						boolean success = true;
						for (int indexPlayer = 0; success && indexPlayer < listPlayer.size(); indexPlayer++) {
							final Player player = listPlayer.get(indexPlayer);
							success = success && playerManager.deletePlayer(player);
							progressDialog.setProgress(indexPlayer);
						}

						progressDialog.setVisible(false);
						if (success) {
							new UIMessageDialog(
								(JFrame) SwingUtilities.getWindowAncestor(this),
								translator.translate(UIText.GUI_PRIMARY_PLAYER_DELETE_ALL_RESULT_PLAYER_DELETED_MESSAGE),
								translator.translate(UIText.GUI_PRIMARY_PLAYER_DELETE_ALL_RESULT_PLAYER_DELETED_TITLE),
								MessageDialogMessageType.INFORMATION,
								MessageDialogOptionsType.OK,
								MessageDialogOption.OK,
								translator
							).showOptionDialog();
						} else {
							new UIMessageDialog(
								(JFrame) SwingUtilities.getWindowAncestor(this),
								translator.translate(UIText.GUI_PRIMARY_PLAYER_DELETE_ALL_RESULT_PLAYER_NOT_DELETED_MESSAGE),
								translator.translate(UIText.GUI_PRIMARY_PLAYER_DELETE_ALL_RESULT_PLAYER_NOT_DELETED_TITLE),
								MessageDialogMessageType.ERROR,
								MessageDialogOptionsType.OK,
								MessageDialogOption.OK,
								translator
							).showOptionDialog();
						}
						refreshPlayer();
					}).start();
				});
				progressDialog.showProgressDialog();
			}
		}
	}
}
