/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.primary.score;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import fr.bri.awt.ProportionalGridLayoutConstraint;
import fr.bri.awt.ProportionalGridLayoutInteger;
import fr.bri.mj.data.tournament.Tournament;
import fr.bri.mj.data.tournament.TournamentScore;
import fr.bri.mj.dataaccess.TournamentManager;
import fr.bri.mj.gui.common.UITabPanelWithWaterMark;
import fr.bri.mj.gui.config.UIConfig;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;
import fr.bri.mj.gui.utils.UITreeNodeRenderer;

public abstract class UITabPanelDisplayTableScore<ScoreClassType extends TournamentScore<ScoreClassType, ScoreEnumType>, ScoreEnumType extends Enum<ScoreEnumType>> extends UITabPanelWithWaterMark {

	private static final long serialVersionUID = 4188743143053758558L;

	private final TournamentManager<ScoreClassType, ScoreEnumType> tournamentManager;

	private final JComboBox<String> comboTournament;
	private final ActionListener tournamentComboBoxActionListener;
	private final JTree treeIds;
	private final DefaultTreeModel treeModel;
	private final JPanel panelDisplay;

	private final List<Tournament<ScoreClassType, ScoreEnumType>> listTournament;
	protected ScoreClassType scoreHighest;
	protected ScoreClassType scoreLowest;

	public UITabPanelDisplayTableScore(
		final TournamentManager<ScoreClassType, ScoreEnumType> tournamentManager,
		final UIConfig config,
		final UITextTranslator translator
	) {
		super(
			config,
			translator
		);
		this.tournamentManager = tournamentManager;

		final ProportionalGridLayoutInteger layout = new ProportionalGridLayoutInteger(
			1,
			2,
			4,
			0
		);
		layout.setWeightX(
			1,
			3
		);
		setLayout(layout);

		final ProportionalGridLayoutConstraint constraint = new ProportionalGridLayoutConstraint(
			0,
			1,
			0,
			1
		);

		{
			final JPanel leftComponent = new JPanel();
			leftComponent.setLayout(new GridBagLayout());

			final GridBagConstraints c = new GridBagConstraints();
			c.gridy = 0;
			c.gridx = 0;
			c.fill = GridBagConstraints.NONE;
			c.weighty = 0.0;
			c.weightx = 0.0;
			leftComponent.add(
				new JLabel(translator.translate(UIText.GUI_PRIMARY_DISPLAY_SCORE_TITLE_TOURNAMENT)),
				c
			);

			comboTournament = new JComboBox<String>();
			comboTournament.setEditable(false);
			c.gridx = 1;
			c.fill = GridBagConstraints.HORIZONTAL;
			c.weightx = 1.0;
			leftComponent.add(
				comboTournament,
				c
			);

			treeModel = new DefaultTreeModel(null);
			treeIds = new JTree(treeModel);
			treeIds.setRootVisible(true);
			treeIds.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
			treeIds.setCellRenderer(new UITreeNodeRenderer());
			final JScrollPane scrollList = new JScrollPane(
				treeIds,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER
			);
			c.gridy = 1;
			c.gridx = 0;
			c.gridwidth = 2;
			c.fill = GridBagConstraints.BOTH;
			c.weighty = 1.0;
			c.weightx = 1.0;
			leftComponent.add(
				scrollList,
				c
			);

			constraint.x = 0;
			add(
				leftComponent,
				constraint
			);
		}

		{
			panelDisplay = new JPanel();
			panelDisplay.setOpaque(false);

			final JPanel panelDisplaySupport = new JPanel();
			panelDisplaySupport.setOpaque(false);
			panelDisplaySupport.setLayout(new GridBagLayout());

			final GridBagConstraints supportConstraints = new GridBagConstraints();
			supportConstraints.gridx = 0;
			supportConstraints.gridy = 0;
			supportConstraints.gridwidth = 1;
			supportConstraints.gridheight = 1;
			supportConstraints.weightx = 1.0;
			supportConstraints.weighty = 1.0;
			supportConstraints.anchor = GridBagConstraints.NORTH;
			supportConstraints.fill = GridBagConstraints.HORIZONTAL;

			panelDisplaySupport.add(
				panelDisplay,
				supportConstraints
			);

			final JScrollPane scrollDisplay = new JScrollPane(
				panelDisplaySupport,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED
			);
			scrollDisplay.getVerticalScrollBar().setUnitIncrement(16);
			scrollDisplay.getViewport().setOpaque(false);

			constraint.x = 1;
			add(
				scrollDisplay,
				constraint
			);
		}

		listTournament = new ArrayList<Tournament<ScoreClassType, ScoreEnumType>>();

		tournamentComboBoxActionListener = (final ActionEvent e) -> {
			changeTournament();
		};
		comboTournament.addActionListener(tournamentComboBoxActionListener);
		treeIds.getSelectionModel().addTreeSelectionListener((final TreeSelectionEvent e) -> {
			displayGame();
		});
	}

	@Override
	public String getTabName() {
		return translator.translate(UIText.GUI_PRIMARY_DISPLAY_SCORE_TAB_NAME);
	}

	@Override
	public void remoteRefresh() {
		refresh(false);
	}

	@Override
	public void refresh(final boolean hard) {
		int selectedTournamentIndex = comboTournament.getSelectedIndex();
		Tournament<ScoreClassType, ScoreEnumType> selectedTournament = null;
		if (selectedTournamentIndex >= 0) {
			selectedTournament = listTournament.get(selectedTournamentIndex);
		}

		comboTournament.removeActionListener(tournamentComboBoxActionListener);
		comboTournament.removeAllItems();
		listTournament.clear();
		for (final Tournament<ScoreClassType, ScoreEnumType> tournament : tournamentManager.getAllTournament()) {
			if (!tournament.isArchived()) {
				listTournament.add(tournament);
			}
		}

		if (listTournament.size() > 0) {
			Collections.sort(listTournament);

			selectedTournamentIndex = 0;
			for (int index = 0; index < listTournament.size(); index++) {
				final Tournament<ScoreClassType, ScoreEnumType> tournament = listTournament.get(index);
				if (tournament == selectedTournament) {
					selectedTournamentIndex = index;
				}
				comboTournament.addItem(tournament.getName());
			}

			comboTournament.setSelectedIndex(selectedTournamentIndex);
		}
		changeTournament();
		comboTournament.addActionListener(tournamentComboBoxActionListener);
	}

	private void changeTournament() {
		invalidate();
		final int selectedTournamentIndex = comboTournament.getSelectedIndex();
		if (listTournament.size() > 0 && selectedTournamentIndex >= 0) {
			final Tournament<ScoreClassType, ScoreEnumType> tournament = listTournament.get(selectedTournamentIndex);
			final DefaultMutableTreeNode root = new DefaultMutableTreeNode(tournament);
			for (int session = 1; session <= tournament.getNbExistingSessions(); session++) {
				final DefaultMutableTreeNode nodeSession = new DefaultMutableTreeNode(
					new SessionId(
						session,
						translator.translate(UIText.GUI_PRIMARY_DISPLAY_SCORE_TITLE_SESSION)
					)
				);
				root.add(nodeSession);
			}
			treeModel.setRoot(root);
		} else {
			treeModel.setRoot(null);
		}
		validate();
		repaint();
	}

	private void displayGame() {
		invalidate();
		panelDisplay.removeAll();

		final int selectedTournamentIndex = comboTournament.getSelectedIndex();
		if (listTournament.size() > 0 && selectedTournamentIndex >= 0) {
			final Tournament<ScoreClassType, ScoreEnumType> tournament = listTournament.get(selectedTournamentIndex);

			final TreePath selectedPath = treeIds.getSelectionPath();
			if (selectedPath != null) {
				final Object[] path = selectedPath.getPath();
				if (path.length == 2 && path[path.length - 1] != null && path[path.length - 1] instanceof DefaultMutableTreeNode) {
					final Object sessionObject = ((DefaultMutableTreeNode) path[path.length - 1]).getUserObject();
					if (sessionObject instanceof SessionId) {
						final SessionId sessionId = (SessionId) sessionObject;
						updateHighestLowestScore(
							tournament,
							sessionId.getSession()
						);
						final SortedMap<Integer, SortedSet<ScoreClassType>> mapTableToScores = new TreeMap<Integer, SortedSet<ScoreClassType>>();
						for (final ScoreClassType table : tournament.getTables()) {
							if (table.getSessionId() == sessionId.getSession()) {
								if (!mapTableToScores.containsKey(table.getTableId())) {
									mapTableToScores.put(
										table.getTableId(),
										new TreeSet<ScoreClassType>()
									);
								}
								mapTableToScores.get(table.getTableId()).add(table);
							}
						}

						final ProportionalGridLayoutInteger layout = new ProportionalGridLayoutInteger(
							mapTableToScores.keySet().size(),
							1,
							4,
							0,
							4,
							0,
							0,
							8
						);
						panelDisplay.setLayout(layout);

						final ProportionalGridLayoutConstraint constraint = new ProportionalGridLayoutConstraint(
							0,
							1,
							0,
							1
						);
						int index = 0;
						for (final Integer numTable : mapTableToScores.keySet()) {
							final UIDisplayTableScoreChart<ScoreClassType, ScoreEnumType> displayScoreTable = createDisplay();
							displayScoreTable.setOpaque(false);
							displayScoreTable.setTableScores(mapTableToScores.get(numTable));

							constraint.y = index;
							panelDisplay.add(
								displayScoreTable,
								constraint
							);

							index++;
						}
					}
				}
			}
		}
		validate();
		repaint();
	}

	protected abstract void updateHighestLowestScore(
		Tournament<ScoreClassType, ScoreEnumType> tournament,
		int session
	);

	protected abstract UIDisplayTableScoreChart<ScoreClassType, ScoreEnumType> createDisplay();
}
