/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.league;

import fr.bri.mj.gui.language.UIText;

public enum UILeagueMode {

	STRICT(UIText.GUI_LEAGUE_MODE_STRICT, true),
	INCLUSIVE(UIText.GUI_LEAGUE_MODE_INCLUSIVE, false);

	private final UIText uiText;
	private final boolean strict;

	private UILeagueMode(
		final UIText uiText,
		final boolean strict
	) {
		this.uiText = uiText;
		this.strict = strict;
	}

	public UIText getUIText() {
		return uiText;
	}

	public boolean isStrict() {
		return strict;
	}
}
