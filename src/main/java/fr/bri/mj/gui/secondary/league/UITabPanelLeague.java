/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.league;

import java.awt.BorderLayout;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import fr.bri.awt.ProportionalGridLayoutConstraint;
import fr.bri.awt.ProportionalGridLayoutInteger;
import fr.bri.mj.data.score.aggregated.ranking.calculator.multi.RankingCalculatorMultiFactory;
import fr.bri.mj.data.score.aggregated.ranking.rankingscore.multi.RankingScoreMulti;
import fr.bri.mj.data.tournament.Tournament;
import fr.bri.mj.data.tournament.TournamentScore;
import fr.bri.mj.dataaccess.TournamentManager;
import fr.bri.mj.gui.common.UITabPanel;
import fr.bri.mj.gui.config.UIConfig;
import fr.bri.mj.gui.config.item.UIConfigDisplayLanguage;
import fr.bri.mj.gui.dialog.UIMessageDialog;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogMessageType;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogOption;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogOptionsType;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;
import fr.bri.mj.gui.secondary.ranking.UIRankingMode;

public abstract class UITabPanelLeague<ScoreClassType extends TournamentScore<ScoreClassType, ScoreEnumType>, ScoreEnumType extends Enum<ScoreEnumType>> extends UITabPanel {

	private static final long serialVersionUID = -3018682907315213483L;

	protected final TournamentManager<ScoreClassType, ScoreEnumType> tournamentManager;
	protected final RankingCalculatorMultiFactory<ScoreClassType, ScoreEnumType> rankingCalculatorMultiFactory;

	protected final JComboBox<String> comboLeague;
	protected final JComboBox<String> comboLeagueMode;
	protected final JComboBox<String> comboRankingMode;
	protected final JButton buttonExportImage;
	protected final JButton buttonExportCSV;

	protected final ActionListener leagueComboBoxActionListener;
	protected final ActionListener modeComboBoxActionListener;

	protected final JPanel panelCenterSupport;
	protected final JPanel panelLeague;
	protected final JScrollPane scrollLeague;
	protected final JToggleButton buttonAutoScroll;

	protected final List<UILeagueMode> leagueModes;
	protected final List<UIRankingMode<ScoreEnumType>> rankingModes;

	protected final List<String> listLeague;
	protected Map<String, Set<Tournament<ScoreClassType, ScoreEnumType>>> mapLeagueToTournament;
	protected List<RankingScoreMulti> scoreListMulti;

	protected Thread threadAutoScroll;

	protected UITabPanelLeague(
		final TournamentManager<ScoreClassType, ScoreEnumType> tournamentManager,
		final RankingCalculatorMultiFactory<ScoreClassType, ScoreEnumType> rankingCalculatorMultiFactory,
		final List<UIRankingMode<ScoreEnumType>> rankingModes,
		final UIConfig config,
		final UITextTranslator translator
	) {
		super(
			config,
			translator
		);
		this.tournamentManager = tournamentManager;
		this.rankingCalculatorMultiFactory = rankingCalculatorMultiFactory;
		this.rankingModes = rankingModes;

		setLayout(new BorderLayout());
		{
			final JPanel panelNorth = new JPanel();
			final ProportionalGridLayoutInteger northLayout = new ProportionalGridLayoutInteger(
				NORTH_PANEL_ROWS_NUMBER,
				NORTH_PANEL_COLUMNS_NUMBER,
				NORTH_PANEL_HORIZONTAL_GAP,
				NORTH_PANEL_VERTICAL_GAP
			);
			northLayout.setWeightX(
				NORTH_PANEL_TOURNAMENT_TITLE_WEIGHT,
				NORTH_PANEL_TOURNAMENT_COMBOBOX_WEIGHT,
				NORTH_PANEL_MODE_TITLE_WEIGHT,
				NORTH_PANEL_MODE_COMBOBOX_WEIGHT,
				NORTH_PANEL_RANKING_TITLE_WEIGHT,
				NORTH_PANEL_RANKING_COMBOBOX_WEIGHT,
				NORTH_PANEL_SESSION_TITLE_WEIGHT,
				NORTH_PANEL_SESSION_COMBOBOX_WEIGHT,
				NORTH_PANEL_BUTTON_SPACER_WEIGHT,
				NORTH_PANEL_BUTTON_EXPORT_IMAGE_WEIGHT,
				NORTH_PANEL_BUTTON_EXPORT_CSV_WEIGHT
			);
			panelNorth.setLayout(northLayout);
			panelNorth.setBorder(BorderFactory.createLoweredBevelBorder());
			add(
				panelNorth,
				BorderLayout.NORTH
			);
			final ProportionalGridLayoutConstraint c = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			{
				c.y = 0;
				c.x = NORTH_PANEL_TOURNAMENT_TITLE_INDEX;
				panelNorth.add(
					new JLabel(
						translator.translate(UIText.GUI_SECONDARY_LEAGUE_TITLE_LEAGUE),
						SwingConstants.RIGHT
					),
					c
				);
				comboLeague = new JComboBox<String>();
				comboLeague.setEditable(false);
				c.x = NORTH_PANEL_TOURNAMENT_COMBOBOX_INDEX;
				panelNorth.add(
					comboLeague,
					c
				);

				c.x = NORTH_PANEL_MODE_TITLE_INDEX;
				panelNorth.add(
					new JLabel(
						translator.translate(UIText.GUI_SECONDARY_TREND_TITLE_MODE),
						SwingConstants.RIGHT
					),
					c
				);

				final UILeagueMode[] leagueModesValues = UILeagueMode.values();
				leagueModes = new ArrayList<UILeagueMode>();
				final Vector<String> leagueModeStrings = new Vector<String>();
				for (int index = 0; index < leagueModesValues.length; index++) {
					leagueModes.add(leagueModesValues[index]);
					leagueModeStrings.add(translator.translate(leagueModesValues[index].getUIText()));
				}
				comboLeagueMode = new JComboBox<String>(leagueModeStrings);
				c.x = NORTH_PANEL_MODE_COMBOBOX_INDEX;
				panelNorth.add(
					comboLeagueMode,
					c
				);

				c.x = NORTH_PANEL_RANKING_TITLE_INDEX;
				panelNorth.add(
					new JLabel(
						translator.translate(UIText.GUI_SECONDARY_LEAGUE_TITLE_RANKING),
						SwingConstants.RIGHT
					),
					c
				);

				final Vector<String> rankingModeStrings = new Vector<String>();
				for (int index = 0; index < rankingModes.size(); index++) {
					rankingModeStrings.add(translator.translate(rankingModes.get(index).getUIText()));
				}
				comboRankingMode = new JComboBox<String>(rankingModeStrings);
				c.x = NORTH_PANEL_RANKING_COMBOBOX_INDEX;
				panelNorth.add(
					comboRankingMode,
					c
				);

				c.x = NORTH_PANEL_BUTTON_EXPORT_IMAGE_INDEX;
				buttonExportImage = new JButton(translator.translate(UIText.GUI_SECONDARY_LEAGUE_BUTTON_EXPORT_IMAGE));
				panelNorth.add(
					buttonExportImage,
					c
				);

				c.x = NORTH_PANEL_BUTTON_EXPORT_CSV_INDEX;
				buttonExportCSV = new JButton(translator.translate(UIText.GUI_SECONDARY_LEAGUE_BUTTON_EXPORT_CSV));
				panelNorth.add(
					buttonExportCSV,
					c
				);
			}
		}

		{
			panelCenterSupport = new JPanel();
			panelCenterSupport.setLayout(new GridBagLayout());
			panelCenterSupport.setOpaque(true);
			panelCenterSupport.setBackground(getColorProfile().getTabBackgroundColor());

			final GridBagConstraints constraints = new GridBagConstraints();
			constraints.gridx = 0;
			constraints.gridy = 0;
			constraints.gridwidth = 1;
			constraints.gridheight = 1;
			constraints.weightx = 1.0;
			constraints.weighty = 1.0;
			constraints.anchor = GridBagConstraints.NORTH;
			constraints.fill = GridBagConstraints.HORIZONTAL;

			panelLeague = new JPanel();
			panelLeague.setOpaque(false);
			panelCenterSupport.add(
				panelLeague,
				constraints
			);

			scrollLeague = new JScrollPane(
				panelCenterSupport,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED
			);
			scrollLeague.getVerticalScrollBar().setUnitIncrement(16);

			add(
				scrollLeague,
				BorderLayout.CENTER
			);
		}

		{
			final JPanel panelSouth = new JPanel();
			final ProportionalGridLayoutInteger southLayout = new ProportionalGridLayoutInteger(
				1,
				2,
				0,
				0
			);
			southLayout.setWeightX(
				7,
				1
			);
			panelSouth.setLayout(southLayout);

			final ProportionalGridLayoutConstraint c = new ProportionalGridLayoutConstraint(
				1,
				1,
				0,
				1
			);

			buttonAutoScroll = new JToggleButton(translator.translate(UIText.GUI_SECONDARY_LEAGUE_BUTTON_AUTO_SCROLL));
			panelSouth.add(
				buttonAutoScroll,
				c
			);

			add(
				panelSouth,
				BorderLayout.SOUTH
			);
		}

		listLeague = new ArrayList<String>();
		mapLeagueToTournament = new TreeMap<String, Set<Tournament<ScoreClassType, ScoreEnumType>>>();
		leagueComboBoxActionListener = (final ActionEvent e) -> {
			displayLeague();
		};
		modeComboBoxActionListener = (final ActionEvent e) -> {
			displayLeague();
		};
		buttonExportImage.addActionListener((final ActionEvent e) -> {
			exportImage();
		});
		buttonExportCSV.addActionListener((final ActionEvent e) -> {
			exportCSV();
		});
		buttonAutoScroll.addActionListener((final ActionEvent e) -> {
			if (buttonAutoScroll.isSelected()) {
				enableAutoScroll();
			} else {
				disableAutoScroll();
			}
		});
	}

	@Override
	public String getTabName() {
		return translator.translate(UIText.GUI_SECONDARY_LEAGUE_TAB_NAME);
	}

	protected UILeagueColorProfile getColorProfile() {
		switch (config.getColorProfile()) {
			case WHITE_ON_BLACK:
				return UILeagueColorProfile.PROFILE_WHITE_ON_BLACK;
			case BLACK_ON_WHITE:
				return UILeagueColorProfile.PROFILE_BLACK_ON_WHITE;
			default:
				return null;
		}
	}

	@Override
	public void refresh(final boolean hard) {
		int selectedLeagueIndex = comboLeague.getSelectedIndex();
		String selectedLeague = null;
		if (selectedLeagueIndex >= 0) {
			selectedLeague = listLeague.get(selectedLeagueIndex);
		}

		comboRankingMode.removeActionListener(modeComboBoxActionListener);
		comboLeagueMode.removeActionListener(modeComboBoxActionListener);
		comboLeague.removeActionListener(leagueComboBoxActionListener);
		comboLeague.removeAllItems();

		listLeague.clear();
		mapLeagueToTournament.clear();
		final Set<String> archivedLeagueNames = new HashSet<String>();
		for (final Tournament<ScoreClassType, ScoreEnumType> tournament : tournamentManager.getAllTournament()) {
			final String leagueName = tournament.getLeagueName();
			if (leagueName != null && !leagueName.isEmpty()) {
				if (tournament.isArchived()) {
					archivedLeagueNames.add(leagueName);
				}
				if (!mapLeagueToTournament.containsKey(leagueName)) {
					mapLeagueToTournament.put(
						leagueName,
						new TreeSet<Tournament<ScoreClassType, ScoreEnumType>>()
					);
				}
				mapLeagueToTournament.get(leagueName).add(tournament);
			}
		}
		listLeague.addAll(mapLeagueToTournament.keySet());
		listLeague.removeAll(archivedLeagueNames);

		if (listLeague.size() > 0) {
			Collections.sort(listLeague);

			selectedLeagueIndex = -1;
			for (int index = 0; index < listLeague.size(); index++) {
				final String league = listLeague.get(index);
				if (league.equals(selectedLeague)) {
					selectedLeagueIndex = index;
				}
				comboLeague.addItem(league);
			}

			if (selectedLeagueIndex >= 0) {
				comboLeague.setSelectedIndex(selectedLeagueIndex);
				displayLeague();
			} else {
				comboLeague.setSelectedIndex(0);
				displayLeague();
			}
		} else {
			displayLeague();
		}

		comboRankingMode.addActionListener(modeComboBoxActionListener);
		comboLeagueMode.addActionListener(modeComboBoxActionListener);
		comboLeague.addActionListener(leagueComboBoxActionListener);
	}

	protected abstract void displayLeague();

	@Override
	public void remoteRefresh() {
		displayLeague();
	}

	@Override
	public void stopRunningThreads() {
		disableAutoScroll();
	}

	private void enableAutoScroll() {
		threadAutoScroll = new Thread(() -> {
			autoScroll();
		});
		threadAutoScroll.start();
	}

	private void disableAutoScroll() {
		if (threadAutoScroll != null) {
			threadAutoScroll.interrupt();
			threadAutoScroll = null;
		}
	}

	private void autoScroll() {
		while (true) {
			try {
				Thread.sleep(5000);
			} catch (final InterruptedException e) {
				break;
			}

			synchronized (scrollLeague) {
				final JScrollBar verticalScrollBar = scrollLeague.getVerticalScrollBar();
				final int value = verticalScrollBar.getModel().getValue();
				final int extent = verticalScrollBar.getModel().getExtent();
				final int minimum = verticalScrollBar.getModel().getMinimum();
				final int maximum = verticalScrollBar.getModel().getMaximum();
				int newValue;
				if (value + extent >= maximum) {
					newValue = minimum;
				} else {
					newValue = value + extent;
				}
				verticalScrollBar.setValue(newValue);
			}
		}
	}

	private void exportImage() {
		final int selectedLeagueIndex = comboLeague.getSelectedIndex();
		if (selectedLeagueIndex != -1) {
			final String league = listLeague.get(selectedLeagueIndex);
			final UIRankingMode<ScoreEnumType> rankingMode = rankingModes.get(comboRankingMode.getSelectedIndex());
			final UILeagueMode leagueMode = leagueModes.get(comboLeagueMode.getSelectedIndex());
			final JFileChooser pngFileChooser = new JFileChooser();
			pngFileChooser.setMultiSelectionEnabled(false);
			pngFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			final String saveFileName = "League_"
				+ regulateString(league)
				+ "_Score_"
				+ regulateString(
					translator.translate(
						UIConfigDisplayLanguage.ENGLISH,
						rankingMode.getUIText()
					)
				)
				+ "_"
				+ regulateString(
					translator.translate(
						UIConfigDisplayLanguage.ENGLISH,
						leagueMode.getUIText()
					)
				)
				+ ".png";
			if (lastChoosenFolder != null) {
				pngFileChooser.setSelectedFile(
					new File(
						lastChoosenFolder,
						saveFileName
					)
				);
			} else {
				pngFileChooser.setSelectedFile(new File(saveFileName));
			}
			File pngFile = null;
			boolean toContinue = true;
			while (toContinue) {
				final int fileChooserAnswer = pngFileChooser.showSaveDialog(this);
				if (fileChooserAnswer == JFileChooser.APPROVE_OPTION) {
					pngFile = pngFileChooser.getSelectedFile();
					if (pngFile.exists()) {
						final MessageDialogOption overwriteAnswer = new UIMessageDialog(
							(JFrame) SwingUtilities.getWindowAncestor(this),
							translator.translate(UIText.GUI_SECONDARY_LEAGUE_EXPORT_OVERWRITE_CONFIRM_MESSAGE),
							translator.translate(UIText.GUI_SECONDARY_LEAGUE_EXPORT_OVERWRITE_CONFIRM_TITLE),
							MessageDialogMessageType.WARNING,
							MessageDialogOptionsType.YES_NO_CANCEL,
							MessageDialogOption.NO,
							translator
						).showOptionDialog();
						switch (overwriteAnswer) {
							case YES:
								toContinue = false;
								break;
							case NO:
								pngFile = null;
								break;
							case CANCEL:
							case CLOSED:
								pngFile = null;
								toContinue = false;
								break;
							default:
								break;
						}
					} else {
						toContinue = false;
					}
				} else {
					pngFile = null;
					toContinue = false;
				}
			}
			if (pngFile != null) {
				lastChoosenFolder = pngFileChooser.getCurrentDirectory();
				final BufferedImage screenShot = new BufferedImage(
					panelLeague.getWidth(),
					panelLeague.getHeight(),
					BufferedImage.TYPE_INT_RGB
				);
				final Graphics2D graphics = screenShot.createGraphics();
				graphics.setBackground(getColorProfile().getTabBackgroundColor());
				graphics.clearRect(
					0,
					0,
					panelLeague.getWidth(),
					panelLeague.getHeight()
				);
				panelLeague.paintAll(graphics);
				try {
					if (ImageIO.write(
						screenShot,
						"png",
						pngFile
					)) {
						new UIMessageDialog(
							(JFrame) SwingUtilities.getWindowAncestor(this),
							translator.translate(UIText.GUI_SECONDARY_LEAGUE_EXPORT_RESULT_FILE_SAVED_MESSAGE),
							translator.translate(UIText.GUI_SECONDARY_LEAGUE_EXPORT_RESULT_FILE_SAVED_TITLE),
							MessageDialogMessageType.INFORMATION,
							MessageDialogOptionsType.OK,
							MessageDialogOption.OK,
							translator
						).showOptionDialog();
					} else {
						new UIMessageDialog(
							(JFrame) SwingUtilities.getWindowAncestor(this),
							translator.translate(UIText.GUI_SECONDARY_LEAGUE_EXPORT_RESULT_FILE_NOT_SAVED_MESSAGE),
							translator.translate(UIText.GUI_SECONDARY_LEAGUE_EXPORT_RESULT_FILE_NOT_SAVED_TITLE),
							MessageDialogMessageType.ERROR,
							MessageDialogOptionsType.OK,
							MessageDialogOption.OK,
							translator
						).showOptionDialog();
					}
				} catch (final IOException e) {
					new UIMessageDialog(
						(JFrame) SwingUtilities.getWindowAncestor(this),
						translator.translate(UIText.GUI_SECONDARY_LEAGUE_EXPORT_RESULT_FILE_NOT_SAVED_MESSAGE),
						translator.translate(UIText.GUI_SECONDARY_LEAGUE_EXPORT_RESULT_FILE_NOT_SAVED_TITLE),
						MessageDialogMessageType.ERROR,
						MessageDialogOptionsType.OK,
						MessageDialogOption.OK,
						translator
					).showOptionDialog();
					e.printStackTrace();
				}
			}
		}
	}

	protected abstract void exportCSV();
}
