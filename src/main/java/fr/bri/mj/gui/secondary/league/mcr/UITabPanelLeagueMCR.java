/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.league.mcr;

import java.awt.Font;
import java.io.File;
import java.util.Set;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import fr.bri.awt.ProportionalGridLayoutConstraint;
import fr.bri.awt.ProportionalGridLayoutInteger;
import fr.bri.mj.data.player.Player;
import fr.bri.mj.data.score.aggregated.QualificationMode;
import fr.bri.mj.data.score.aggregated.TeamMode;
import fr.bri.mj.data.score.aggregated.ranking.calculator.multi.RankingCalculatorMultiMCRFactory;
import fr.bri.mj.data.score.aggregated.ranking.rankingscore.multi.RankingScoreMulti;
import fr.bri.mj.data.tournament.Tournament;
import fr.bri.mj.data.tournament.mcr.ScoreMCR;
import fr.bri.mj.data.tournament.mcr.TournamentScoreMCR;
import fr.bri.mj.dataaccess.TournamentManager;
import fr.bri.mj.gui.config.UIConfig;
import fr.bri.mj.gui.config.item.UIConfigDisplayLanguage;
import fr.bri.mj.gui.dialog.UIMessageDialog;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogMessageType;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogOption;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogOptionsType;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;
import fr.bri.mj.gui.secondary.league.UILeagueColorProfile;
import fr.bri.mj.gui.secondary.league.UILeagueMode;
import fr.bri.mj.gui.secondary.league.UITabPanelLeague;
import fr.bri.mj.gui.secondary.ranking.UIRankingMode;
import fr.bri.mj.gui.secondary.ranking.mcr.UIRankingModeMCR;
import fr.bri.mj.utils.CSVWriter;

public class UITabPanelLeagueMCR extends UITabPanelLeague<TournamentScoreMCR, ScoreMCR> {

	private static final long serialVersionUID = -4814628925257750716L;

	public UITabPanelLeagueMCR(
		final TournamentManager<TournamentScoreMCR, ScoreMCR> dataAccess,
		final UIConfig config,
		final UITextTranslator translator
	) {
		super(
			dataAccess,
			RankingCalculatorMultiMCRFactory.getInstance(),
			UIRankingModeMCR.getRankingModes(
				TeamMode.INDIVIDUAL,
				QualificationMode.WITHOUT
			),
			config,
			translator
		);
	}

	@Override
	protected void displayLeague() {
		final UILeagueColorProfile leagueColorProfile = getColorProfile();
		invalidate();
		panelCenterSupport.setBackground(leagueColorProfile.getTabBackgroundColor());
		panelLeague.removeAll();

		final int selectedLeagueIndex = comboLeague.getSelectedIndex();
		if (selectedLeagueIndex != -1) {
			final Set<Tournament<TournamentScoreMCR, ScoreMCR>> leagueTournaments = mapLeagueToTournament.get(listLeague.get(selectedLeagueIndex));
			final UILeagueMode leagueMode = leagueModes.get(comboLeagueMode.getSelectedIndex());
			final UIRankingMode<ScoreMCR> rankingMode = rankingModes.get(comboRankingMode.getSelectedIndex());
			scoreListMulti = rankingCalculatorMultiFactory.getRankingCalculator(rankingMode.getScore()).getRankingScore(
				leagueTournaments,
				leagueMode.isStrict()
			);
			if (scoreListMulti != null && scoreListMulti.size() > 0) {
				int session = 0;
				for (final Tournament<TournamentScoreMCR, ScoreMCR> tournament : leagueTournaments) {
					session += tournament.getNbSessions();
				}
				int average = 0;
				switch (rankingMode.getScore()) {
					case MATCH_POINT:
						average = 21 * session;
						break;
					case TABLE_POINT:
						average = 0;
						break;
					default:
						break;
				}

				final int fontSize = getConfigFontSize();
				final Font labelFont = (CUSTOM_FONT != null
					? CUSTOM_FONT
					: getFont()).deriveFont(
						Font.PLAIN,
						fontSize
					);

				final ProportionalGridLayoutInteger layout = new ProportionalGridLayoutInteger(
					scoreListMulti.size() + 1,
					1,
					0,
					0
				);
				panelLeague.setLayout(layout);

				final ProportionalGridLayoutConstraint constraint = new ProportionalGridLayoutConstraint(
					0,
					1,
					0,
					1
				);

				constraint.x = 0;
				final UILeagueChartTitleMCR leagueTableTitle = new UILeagueChartTitleMCR(
					rankingMode,
					config.getNationalMode(),
					leagueColorProfile,
					labelFont,
					translator
				);
				constraint.y = 0;
				panelLeague.add(
					leagueTableTitle,
					constraint
				);

				for (int index = 0; index < scoreListMulti.size(); index++) {
					final RankingScoreMulti record = scoreListMulti.get(index);
					final UILeagueChartRowMCR rankingTableRow = new UILeagueChartRowMCR(
						rankingMode,
						config.getNationalMode(),
						leagueColorProfile,
						labelFont,
						fontSize,
						translator
					);

					final boolean darken = index % 2 == 0;
					rankingTableRow.setScore(
						record,
						average,
						darken
					);

					constraint.y = index + 1;
					panelLeague.add(
						rankingTableRow,
						constraint
					);
				}
			}
		}
		validate();
		scrollLeague.getVerticalScrollBar().setValue(scrollLeague.getVerticalScrollBar().getModel().getMinimum());
		repaint();
	}

	@Override
	protected void exportCSV() {
		final int selectedLeagueIndex = comboLeague.getSelectedIndex();
		if (selectedLeagueIndex != -1) {
			final String leagueName = listLeague.get(selectedLeagueIndex);
			final UILeagueMode leagueMode = leagueModes.get(comboLeagueMode.getSelectedIndex());
			final UIRankingMode<ScoreMCR> rankingMode = rankingModes.get(comboRankingMode.getSelectedIndex());

			if (scoreListMulti != null && scoreListMulti.size() > 0) {
				final JFileChooser csvFileChooser = new JFileChooser();
				csvFileChooser.setMultiSelectionEnabled(false);
				csvFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				final String saveFileName = "League_"
					+ regulateString(leagueName)
					+ "_Ranking_"
					+ regulateString(
						translator.translate(
							UIConfigDisplayLanguage.ENGLISH,
							rankingMode.getUIText()
						)
					)
					+ "_"
					+ regulateString(
						translator.translate(
							UIConfigDisplayLanguage.ENGLISH,
							leagueMode.getUIText()
						)
					)
					+ ".csv";
				if (lastChoosenFolder != null) {
					csvFileChooser.setSelectedFile(
						new File(
							lastChoosenFolder,
							saveFileName
						)
					);
				} else {
					csvFileChooser.setSelectedFile(new File(saveFileName));
				}
				File csvFile = null;
				boolean toContinue = true;
				while (toContinue) {
					final int fileChooserAnswer = csvFileChooser.showSaveDialog(this);
					if (fileChooserAnswer == JFileChooser.APPROVE_OPTION) {
						csvFile = csvFileChooser.getSelectedFile();
						if (csvFile.exists()) {
							final MessageDialogOption overwriteAnswer = new UIMessageDialog(
								(JFrame) SwingUtilities.getWindowAncestor(this),
								translator.translate(UIText.GUI_SECONDARY_LEAGUE_EXPORT_OVERWRITE_CONFIRM_MESSAGE),
								translator.translate(UIText.GUI_SECONDARY_LEAGUE_EXPORT_OVERWRITE_CONFIRM_TITLE),
								MessageDialogMessageType.WARNING,
								MessageDialogOptionsType.YES_NO_CANCEL,
								MessageDialogOption.NO,
								translator
							).showOptionDialog();
							switch (overwriteAnswer) {
								case YES:
									toContinue = false;
									break;
								case NO:
									csvFile = null;
									break;
								case CANCEL:
								case CLOSED:
									csvFile = null;
									toContinue = false;
									break;
								default:
									break;
							}
						} else {
							toContinue = false;
						}
					} else {
						csvFile = null;
						toContinue = false;
					}
				}

				if (csvFile != null) {
					lastChoosenFolder = csvFileChooser.getCurrentDirectory();

					final String csvContent[][] = new String[scoreListMulti.size() + 2][];
					csvContent[0] = new String[] {
						"League",
						leagueName,
						"Ranking",
						translator.translate(
							UIConfigDisplayLanguage.ENGLISH,
							rankingMode.getUIText()
						)
					};

					switch (rankingMode.getScore()) {
						case MATCH_POINT: {
							csvContent[1] = new String[] {
								"Ranking",
								"First Name",
								"Last Name",
								"Nationality",
								"License",
								translator.translate(
									UIConfigDisplayLanguage.ENGLISH,
									UIText.GUI_SECONDARY_LEAGUE_MCR_ROW_TITLE_MATCH_POINT
								),
								translator.translate(
									UIConfigDisplayLanguage.ENGLISH,
									UIText.GUI_SECONDARY_LEAGUE_MCR_ROW_TITLE_TABLE_POINT
								)
							};
							for (int index = 0; index < scoreListMulti.size(); index++) {
								final RankingScoreMulti score = scoreListMulti.get(index);
								final Player player = score.getPlayer();
								csvContent[index + 2] = new String[] {
									Integer.toString(score.getRanking()),
									player.getPlayerFirstName(),
									player.getPlayerLastName(),
									player.getPlayerNationality().getCountryCodeAlpha3(),
									player.getLicense(),
									fraction(
										score.getTotalScore(),
										12
									),
									Integer.toString(score.getTotalScore2())
								};
							}
						}
							break;
						case TABLE_POINT: {
							csvContent[1] = new String[] {
								"Ranking",
								"First Name",
								"Last Name",
								"Nationality",
								"License",
								translator.translate(
									UIConfigDisplayLanguage.ENGLISH,
									UIText.GUI_SECONDARY_LEAGUE_MCR_ROW_TITLE_TABLE_POINT
								),
								translator.translate(
									UIConfigDisplayLanguage.ENGLISH,
									UIText.GUI_SECONDARY_LEAGUE_MCR_ROW_TITLE_MATCH_POINT
								)
							};
							for (int index = 0; index < scoreListMulti.size(); index++) {
								final RankingScoreMulti score = scoreListMulti.get(index);
								final Player player = score.getPlayer();
								csvContent[index + 2] = new String[] {
									Integer.toString(score.getRanking()),
									player.getPlayerFirstName(),
									player.getPlayerLastName(),
									player.getPlayerNationality().getCountryCodeAlpha3(),
									player.getLicense(),
									Integer.toString(score.getTotalScore()),
									fraction(
										score.getTotalScore2(),
										12
									)
								};
							}
						}
							break;
						default:
							break;
					}

					if (CSVWriter.write(
						csvFile,
						csvContent,
						null
					)) {
						new UIMessageDialog(
							(JFrame) SwingUtilities.getWindowAncestor(this),
							translator.translate(UIText.GUI_SECONDARY_LEAGUE_EXPORT_RESULT_FILE_SAVED_MESSAGE),
							translator.translate(UIText.GUI_SECONDARY_LEAGUE_EXPORT_RESULT_FILE_SAVED_TITLE),
							MessageDialogMessageType.INFORMATION,
							MessageDialogOptionsType.OK,
							MessageDialogOption.OK,
							translator
						).showOptionDialog();
					} else {
						new UIMessageDialog(
							(JFrame) SwingUtilities.getWindowAncestor(this),
							translator.translate(UIText.GUI_SECONDARY_LEAGUE_EXPORT_RESULT_FILE_NOT_SAVED_MESSAGE),
							translator.translate(UIText.GUI_SECONDARY_LEAGUE_EXPORT_RESULT_FILE_NOT_SAVED_TITLE),
							MessageDialogMessageType.ERROR,
							MessageDialogOptionsType.OK,
							MessageDialogOption.OK,
							translator
						).showOptionDialog();
					}
				}
			}
		}
	}
}
