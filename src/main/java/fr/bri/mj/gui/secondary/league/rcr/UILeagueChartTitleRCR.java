/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.league.rcr;

import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import fr.bri.awt.ProportionalGridLayoutConstraint;
import fr.bri.awt.ProportionalGridLayoutInteger;
import fr.bri.mj.data.tournament.rcr.ScoreRCR;
import fr.bri.mj.data.tournament.rcr.TournamentScoreRCR;
import fr.bri.mj.gui.config.item.UIConfigNationalMode;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;
import fr.bri.mj.gui.secondary.league.UILeagueChartTitle;
import fr.bri.mj.gui.secondary.league.UILeagueColorProfile;
import fr.bri.mj.gui.secondary.ranking.UIRankingMode;

public class UILeagueChartTitleRCR extends UILeagueChartTitle<TournamentScoreRCR, ScoreRCR> {

	private static final long serialVersionUID = -5748786941025344447L;

	public UILeagueChartTitleRCR(
		final UIRankingMode<ScoreRCR> rankingMode,
		final UIConfigNationalMode nationalMode,
		final UILeagueColorProfile colorProfile,
		final Font labelFont,
		final UITextTranslator translator
	) {
		super(translator);
		setOpaque(false);

		final ProportionalGridLayoutInteger layout = new ProportionalGridLayoutInteger(
			1,
			7,
			0,
			0
		);
		layout.setWeightX(
			2,
			3,
			3,
			3,
			10,
			14,
			2
		);
		setLayout(layout);

		final ProportionalGridLayoutConstraint constraints = new ProportionalGridLayoutConstraint(
			0,
			1,
			0,
			1
		);

		constraints.y = 0;
		{
			constraints.x = 1;
			final JLabel labelTitleRanking = new JLabel(
				translator.translate(UIText.GUI_SECONDARY_LEAGUE_RCR_ROW_TITLE_RANKING),
				SwingConstants.CENTER
			);
			labelTitleRanking.setFont(labelFont);
			labelTitleRanking.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
			add(
				labelTitleRanking,
				constraints
			);
		}
		{
			constraints.x = 2;
			String titleNationality = null;
			switch (nationalMode) {
				case NATIONALITY:
					titleNationality = translator.translate(UIText.GUI_SECONDARY_LEAGUE_MCR_ROW_TITLE_NATIONALITY);
					break;
				case CLUB_NAME:
					titleNationality = translator.translate(UIText.GUI_SECONDARY_LEAGUE_MCR_ROW_TITLE_CLUB);
					break;
			}

			final JLabel labelTitleFlag = new JLabel(
				titleNationality,
				SwingConstants.CENTER
			);
			labelTitleFlag.setFont(labelFont);
			labelTitleFlag.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
			add(
				labelTitleFlag,
				constraints
			);
		}
		{
			constraints.x = 4;
			final JLabel labelTitlePlayerName = new JLabel(
				translator.translate(UIText.GUI_SECONDARY_LEAGUE_RCR_ROW_TITLE_PLAYER_NAME),
				SwingConstants.LEADING
			);
			labelTitlePlayerName.setFont(labelFont);
			labelTitlePlayerName.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
			add(
				labelTitlePlayerName,
				constraints
			);
		}
		{
			constraints.x = 5;
			final JPanel panelScore = new JPanel();
			panelScore.setOpaque(false);

			final ProportionalGridLayoutConstraint panelScoreLayoutConstraints = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			final JLabel labelScore1 = new JLabel(
				"",
				SwingConstants.CENTER
			);
			labelScore1.setFont(labelFont);

			final JLabel labelScore2 = new JLabel(
				"",
				SwingConstants.CENTER
			);
			labelScore2.setFont(labelFont);

			switch (rankingMode.getScore()) {
				case FINAL_SCORE: {
					final ProportionalGridLayoutInteger panelScoreLayout = new ProportionalGridLayoutInteger(
						1,
						1,
						0,
						2
					);
					panelScoreLayout.setWeightX(1);
					panelScore.setLayout(panelScoreLayout);

					panelScoreLayoutConstraints.x = 0;
					panelScoreLayoutConstraints.gridWidth = 1;
					labelScore1.setText(translator.translate(UIText.GUI_SECONDARY_LEAGUE_RCR_ROW_TITLE_FINAL_SCORE));
					panelScore.add(
						labelScore1,
						panelScoreLayoutConstraints
					);
				}
					break;
				case GAME_SCORE: {
					final ProportionalGridLayoutInteger panelScoreLayout = new ProportionalGridLayoutInteger(
						1,
						2,
						0,
						2
					);
					panelScore.setLayout(panelScoreLayout);

					panelScoreLayoutConstraints.x = 0;
					labelScore1.setText(translator.translate(UIText.GUI_SECONDARY_LEAGUE_RCR_ROW_TITLE_GAME_SCORE));
					panelScore.add(
						labelScore1,
						panelScoreLayoutConstraints
					);
					panelScoreLayoutConstraints.x = 1;
					labelScore2.setText(translator.translate(UIText.GUI_SECONDARY_LEAGUE_RCR_ROW_TITLE_FINAL_SCORE));
					panelScore.add(
						labelScore2,
						panelScoreLayoutConstraints
					);
				}
					break;
				case UMA_SCORE: {
					final ProportionalGridLayoutInteger panelScoreLayout = new ProportionalGridLayoutInteger(
						1,
						2,
						0,
						2
					);
					panelScore.setLayout(panelScoreLayout);

					panelScoreLayoutConstraints.x = 0;
					labelScore1.setText(translator.translate(UIText.GUI_SECONDARY_LEAGUE_RCR_ROW_TITLE_UMA_SCORE));
					panelScore.add(
						labelScore1,
						panelScoreLayoutConstraints
					);
					panelScoreLayoutConstraints.x = 1;
					labelScore2.setText(translator.translate(UIText.GUI_SECONDARY_LEAGUE_RCR_ROW_TITLE_FINAL_SCORE));
					panelScore.add(
						labelScore2,
						panelScoreLayoutConstraints
					);
				}
					break;
			}

			labelScore1.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
			labelScore2.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());

			add(
				panelScore,
				constraints
			);
		}
	}
}
