/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.plan;

import java.awt.Color;

public class UIPlanTableColorProfile {

	public static final UIPlanTableColorProfile PROFILE_BLACK_ON_WHITE = new UIPlanTableColorProfile(
		new Color(
			255,
			255,
			255,
			255
		),
		new Color(
			223,
			223,
			223,
			255
		),
		new Color(
			0,
			0,
			0,
			255
		)
	);

	public static final UIPlanTableColorProfile PROFILE_WHITE_ON_BLACK = new UIPlanTableColorProfile(
		new Color(
			0,
			0,
			0,
			255
		),
		new Color(
			31,
			31,
			31,
			255
		),
		new Color(
			255,
			255,
			255,
			255
		)
	);

	private final Color tabBackgroundColor;
	private final Color darkenLineBackgroundColor;
	private final Color fontColor;

	private UIPlanTableColorProfile(
		final Color tabBackgroundColor,
		final Color darkenLineBackgroundColor,
		final Color fontColor
	) {
		this.tabBackgroundColor = tabBackgroundColor;
		this.darkenLineBackgroundColor = darkenLineBackgroundColor;
		this.fontColor = fontColor;
	}

	public Color getTabBackgroundColor() {
		return tabBackgroundColor;
	}

	public Color getDarkenLineBackgroundColor() {
		return darkenLineBackgroundColor;
	}

	public Color getFontColor() {
		return fontColor;
	}

}
