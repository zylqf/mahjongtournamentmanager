/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.plan.mcr;

import java.io.File;

import fr.bri.mj.data.tournament.Tournament;
import fr.bri.mj.data.tournament.mcr.ScoreMCR;
import fr.bri.mj.data.tournament.mcr.TournamentScoreMCR;
import fr.bri.mj.dataaccess.TournamentManager;
import fr.bri.mj.gui.config.UIConfig;
import fr.bri.mj.gui.language.UITextTranslator;
import fr.bri.mj.gui.secondary.plan.UITabPanelDisplayPlan;
import fr.bri.mj.printing.PaperSizeType;

public class UITabPanelDisplayPlanMCR extends UITabPanelDisplayPlan<TournamentScoreMCR, ScoreMCR> {

	private static final long serialVersionUID = -8824949882517409787L;

	public UITabPanelDisplayPlanMCR(
		final TournamentManager<TournamentScoreMCR, ScoreMCR> dataAccessAdaptor,
		final UIConfig config,
		final UITextTranslator translator
	) {
		super(
			dataAccessAdaptor,
			config,
			translator
		);
	}

	@Override
	protected boolean isWriteScoreSheetSupported() {
		return false;
	}

	@Override
	protected boolean writeScoreSheet(
		final Tournament<TournamentScoreMCR, ScoreMCR> tournament,
		final PaperSizeType paperSize,
		final PaperSizeType formSize,
		final boolean drawLineBetweenForms,
		final File pdfFile
	) {
		return false;
	}
}
