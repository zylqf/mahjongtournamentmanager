/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.ranking;

import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.bri.awt.ProportionalGridLayoutConstraint;
import fr.bri.awt.ProportionalGridLayoutInteger;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;

public class UIRankingChartSeparator extends JPanel {

	private static final long serialVersionUID = 3698996165750295571L;

	public UIRankingChartSeparator(
		final UIRankingColorProfile colorProfile,
		final Font labelFont,
		final UIText phase,
		final UITextTranslator translator
	) {
		setOpaque(false);

		final ProportionalGridLayoutInteger layout = new ProportionalGridLayoutInteger(
			1,
			7,
			0,
			0
		);
		layout.setWeightX(
			2,
			14,
			3,
			3,
			10,
			3,
			2
		);
		setLayout(layout);

		final ProportionalGridLayoutConstraint constraints = new ProportionalGridLayoutConstraint(
			1,
			1,
			0,
			1
		);

		final JLabel labelRankingPhase = new JLabel(translator.translate(phase));
		labelRankingPhase.setOpaque(true);
		labelRankingPhase.setFont(labelFont);
		labelRankingPhase.setBackground(colorProfile.getSeparatorBackgroundColor());
		labelRankingPhase.setForeground(colorProfile.getSeparatorFontColor());
		constraints.x = 1;
		constraints.gridWidth = 1;
		add(
			labelRankingPhase,
			constraints
		);

		final JLabel spacer = new JLabel();
		spacer.setOpaque(true);
		spacer.setBackground(colorProfile.getSeparatorBackgroundColor());
		constraints.x = 2;
		constraints.gridWidth = 4;
		add(
			spacer,
			constraints
		);
	}
}
