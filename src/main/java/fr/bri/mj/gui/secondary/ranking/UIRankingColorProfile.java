/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.ranking;

import java.awt.Color;

public class UIRankingColorProfile {

	public static final UIRankingColorProfile PROFILE_BLACK_ON_WHITE = new UIRankingColorProfile(
		new Color(
			255,
			255,
			255,
			255
		),
		new Color(
			223,
			223,
			223,
			255
		),
		new Color(
			0,
			0,
			0,
			255
		),
		new Color(
			255,
			255,
			255,
			255
		),
		new Color(
			0,
			0,
			0,
			255
		),
		new Color(
			191,
			191,
			191,
			255
		),
		new Color(
			255,
			0,
			0,
			255
		),
		new Color(
			255,
			191,
			191,
			255
		)
	);

	public static final UIRankingColorProfile PROFILE_WHITE_ON_BLACK = new UIRankingColorProfile(
		new Color(
			0,
			0,
			0,
			255
		),
		new Color(
			31,
			31,
			31,
			255
		),
		new Color(
			255,
			255,
			255,
			255
		),
		new Color(
			0,
			0,
			0,
			255
		),
		new Color(
			255,
			255,
			255,
			255
		),
		new Color(
			63,
			63,
			63,
			255
		),
		new Color(
			255,
			0,
			0,
			255
		),
		new Color(
			63,
			0,
			0,
			255
		)
	);

	private final Color tabBackgroundColor;
	private final Color darkenLineBackgroundColor;
	private final Color separatorBackgroundColor;
	private final Color separatorFontColor;
	private final Color upToDatePositiveScoreFontColor;
	private final Color delayedPositiveScoreFontColor;
	private final Color upToDateNegativeScoreFontColor;
	private final Color delayedNegativeScoreFontColor;

	private UIRankingColorProfile(
		final Color tabBackgroundColor,
		final Color darkenLineBackgroundColor,
		final Color separatorBackgroundColor,
		final Color separatorFontColor,
		final Color upToDatePositiveScoreFontColor,
		final Color delayedPositiveScoreFontColor,
		final Color upToDateNegativeScoreFontColor,
		final Color delayedNegativeScoreFontColor
	) {
		this.tabBackgroundColor = tabBackgroundColor;
		this.darkenLineBackgroundColor = darkenLineBackgroundColor;
		this.separatorBackgroundColor = separatorBackgroundColor;
		this.separatorFontColor = separatorFontColor;
		this.upToDatePositiveScoreFontColor = upToDatePositiveScoreFontColor;
		this.delayedPositiveScoreFontColor = delayedPositiveScoreFontColor;
		this.upToDateNegativeScoreFontColor = upToDateNegativeScoreFontColor;
		this.delayedNegativeScoreFontColor = delayedNegativeScoreFontColor;
	}

	public Color getTabBackgroundColor() {
		return tabBackgroundColor;
	}

	public Color getDarkenLineBackgroundColor() {
		return darkenLineBackgroundColor;
	}

	public Color getSeparatorBackgroundColor() {
		return separatorBackgroundColor;
	}

	public Color getSeparatorFontColor() {
		return separatorFontColor;
	}

	public Color getUpToDatePositiveScoreFontColor() {
		return upToDatePositiveScoreFontColor;
	}

	public Color getDelayedPositiveScoreFontColor() {
		return delayedPositiveScoreFontColor;
	}

	public Color getUpToDateNegativeScoreFontColor() {
		return upToDateNegativeScoreFontColor;
	}

	public Color getDelayedNegativeScoreFontColor() {
		return delayedNegativeScoreFontColor;
	}
}
