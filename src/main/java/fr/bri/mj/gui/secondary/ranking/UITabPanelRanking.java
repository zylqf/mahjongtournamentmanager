/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.ranking;

import java.awt.BorderLayout;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import fr.bri.awt.ProportionalGridLayoutConstraint;
import fr.bri.awt.ProportionalGridLayoutInteger;
import fr.bri.mj.data.score.aggregated.ranking.calculator.single.RankingCalculatorSingleFactory;
import fr.bri.mj.data.score.aggregated.ranking.rankingscore.single.RankingScoreSingleIndividual;
import fr.bri.mj.data.score.aggregated.ranking.rankingscore.single.RankingScoreSingleTeam;
import fr.bri.mj.data.tournament.Tournament;
import fr.bri.mj.data.tournament.TournamentScore;
import fr.bri.mj.dataaccess.TournamentManager;
import fr.bri.mj.gui.common.UITabPanel;
import fr.bri.mj.gui.config.UIConfig;
import fr.bri.mj.gui.config.item.UIConfigDisplayLanguage;
import fr.bri.mj.gui.dialog.UIMessageDialog;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogMessageType;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogOption;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogOptionsType;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;

public abstract class UITabPanelRanking<ScoreClassType extends TournamentScore<ScoreClassType, ScoreEnumType>, ScoreEnumType extends Enum<ScoreEnumType>> extends UITabPanel {

	private static final long serialVersionUID = -8731825816965389702L;

	protected final TournamentManager<ScoreClassType, ScoreEnumType> tournamentManager;
	protected final RankingCalculatorSingleFactory<ScoreClassType, ScoreEnumType> rankingCalculatorSingleFactory;

	protected final JComboBox<String> comboTournament;
	protected final JComboBox<String> comboSession;
	protected final JComboBox<String> comboMode;
	protected final JButton buttonExportImage;
	protected final JButton buttonExportCSV;

	protected final ActionListener tournamentComboBoxActionListener;
	protected final ActionListener sessionComboBoxActionListener;

	protected final JPanel panelCenterSupport;
	protected final JPanel panelRanking;
	protected VirticalBoxMovingRowLayout panelRankingLayout;
	protected final JScrollPane scrollRanking;
	protected final JToggleButton buttonAutoScroll;

	protected List<UIRankingMode<ScoreEnumType>> rankingModes;
	protected final List<UIRankingMode<ScoreEnumType>> rankingModesWithQualification;
	protected final List<UIRankingMode<ScoreEnumType>> rankingModesWithoutQualification;

	protected UIRankingChartTitle<ScoreClassType, ScoreEnumType> rankingTableTitle;
	protected Map<Integer, Integer> mapPlayerIdRowIndex;
	protected Map<Integer, Integer> targetMapPlayerIdRowIndex;
	protected List<UIRankingChartRow<ScoreClassType, ScoreEnumType>> rankingTableRows;
	protected List<UIRankingChartRow<ScoreClassType, ScoreEnumType>> targetRankingTableRows;

	protected List<Tournament<ScoreClassType, ScoreEnumType>> listTournament;
	protected List<RankingScoreSingleIndividual> scoreListIndividual;
	protected List<RankingScoreSingleIndividual> targetScoreListIndividual;
	protected List<RankingScoreSingleTeam> scoreListTeam;
	protected List<RankingScoreSingleTeam> targetScoreListTeam;

	protected Thread threadAutoScroll;

	protected UITabPanelRanking(
		final TournamentManager<ScoreClassType, ScoreEnumType> tournamentManager,
		final RankingCalculatorSingleFactory<ScoreClassType, ScoreEnumType> rankingCalculatorSingleFactory,
		final List<UIRankingMode<ScoreEnumType>> rankingModesWithQualification,
		final List<UIRankingMode<ScoreEnumType>> rankingModesWithoutQualification,
		final UIConfig config,
		final UITextTranslator translator
	) {
		super(
			config,
			translator
		);
		this.tournamentManager = tournamentManager;
		this.rankingCalculatorSingleFactory = rankingCalculatorSingleFactory;
		this.rankingModesWithQualification = rankingModesWithQualification;
		this.rankingModesWithoutQualification = rankingModesWithoutQualification;

		setLayout(new BorderLayout());
		{
			final JPanel panelNorth = new JPanel();
			final ProportionalGridLayoutInteger northLayout = new ProportionalGridLayoutInteger(
				NORTH_PANEL_ROWS_NUMBER,
				NORTH_PANEL_COLUMNS_NUMBER,
				NORTH_PANEL_HORIZONTAL_GAP,
				NORTH_PANEL_VERTICAL_GAP
			);
			northLayout.setWeightX(
				NORTH_PANEL_TOURNAMENT_TITLE_WEIGHT,
				NORTH_PANEL_TOURNAMENT_COMBOBOX_WEIGHT,
				NORTH_PANEL_MODE_TITLE_WEIGHT,
				NORTH_PANEL_MODE_COMBOBOX_WEIGHT,
				NORTH_PANEL_RANKING_TITLE_WEIGHT,
				NORTH_PANEL_RANKING_COMBOBOX_WEIGHT,
				NORTH_PANEL_SESSION_TITLE_WEIGHT,
				NORTH_PANEL_SESSION_COMBOBOX_WEIGHT,
				NORTH_PANEL_BUTTON_SPACER_WEIGHT,
				NORTH_PANEL_BUTTON_EXPORT_IMAGE_WEIGHT,
				NORTH_PANEL_BUTTON_EXPORT_CSV_WEIGHT
			);
			panelNorth.setLayout(northLayout);
			panelNorth.setBorder(BorderFactory.createLoweredBevelBorder());
			add(
				panelNorth,
				BorderLayout.NORTH
			);
			final ProportionalGridLayoutConstraint c = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			{
				c.y = 0;
				c.x = NORTH_PANEL_TOURNAMENT_TITLE_INDEX;
				panelNorth.add(
					new JLabel(
						translator.translate(UIText.GUI_SECONDARY_RANKING_TITLE_TOURNAMENT),
						SwingConstants.RIGHT
					),
					c
				);
				comboTournament = new JComboBox<String>();
				comboTournament.setEditable(false);
				c.x = NORTH_PANEL_TOURNAMENT_COMBOBOX_INDEX;
				panelNorth.add(
					comboTournament,
					c
				);

				c.x = NORTH_PANEL_RANKING_TITLE_INDEX;
				panelNorth.add(
					new JLabel(
						translator.translate(UIText.GUI_SECONDARY_RANKING_TITLE_RANKING),
						SwingConstants.RIGHT
					),
					c
				);

				comboMode = new JComboBox<String>();
				c.x = NORTH_PANEL_RANKING_COMBOBOX_INDEX;
				panelNorth.add(
					comboMode,
					c
				);

				c.x = NORTH_PANEL_SESSION_TITLE_INDEX;
				panelNorth.add(
					new JLabel(
						translator.translate(UIText.GUI_SECONDARY_RANKING_TITLE_SESSION),
						SwingConstants.RIGHT
					),
					c
				);
				comboSession = new JComboBox<String>();
				c.x = NORTH_PANEL_SESSION_COMBOBOX_INDEX;
				panelNorth.add(
					comboSession,
					c
				);

				c.x = NORTH_PANEL_BUTTON_EXPORT_IMAGE_INDEX;
				buttonExportImage = new JButton(translator.translate(UIText.GUI_SECONDARY_RANKING_BUTTON_EXPORT_IMAGE));
				panelNorth.add(
					buttonExportImage,
					c
				);

				c.x = NORTH_PANEL_BUTTON_EXPORT_CSV_INDEX;
				buttonExportCSV = new JButton(translator.translate(UIText.GUI_SECONDARY_RANKING_BUTTON_EXPORT_CSV));
				panelNorth.add(
					buttonExportCSV,
					c
				);
			}
		}

		{
			panelCenterSupport = new JPanel();
			panelCenterSupport.setLayout(new GridBagLayout());
			panelCenterSupport.setOpaque(true);
			panelCenterSupport.setBackground(getColorProfile().getTabBackgroundColor());

			final GridBagConstraints constraints = new GridBagConstraints();
			constraints.gridx = 0;
			constraints.gridy = 0;
			constraints.gridwidth = 1;
			constraints.gridheight = 1;
			constraints.weightx = 1.0;
			constraints.weighty = 1.0;
			constraints.anchor = GridBagConstraints.NORTH;
			constraints.fill = GridBagConstraints.HORIZONTAL;

			panelRanking = new JPanel();
			panelRanking.setOpaque(false);
			panelCenterSupport.add(
				panelRanking,
				constraints
			);

			scrollRanking = new JScrollPane(
				panelCenterSupport,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED
			);
			scrollRanking.getVerticalScrollBar().setUnitIncrement(16);

			add(
				scrollRanking,
				BorderLayout.CENTER
			);
		}

		{
			final JPanel panelSouth = new JPanel();
			final ProportionalGridLayoutInteger southLayout = new ProportionalGridLayoutInteger(
				1,
				2,
				0,
				0
			);
			southLayout.setWeightX(
				7,
				1
			);
			panelSouth.setLayout(southLayout);

			final ProportionalGridLayoutConstraint c = new ProportionalGridLayoutConstraint(
				1,
				1,
				0,
				1
			);

			buttonAutoScroll = new JToggleButton(translator.translate(UIText.GUI_SECONDARY_RANKING_BUTTON_AUTO_SCROLL));
			panelSouth.add(
				buttonAutoScroll,
				c
			);

			add(
				panelSouth,
				BorderLayout.SOUTH
			);
		}

		listTournament = new ArrayList<Tournament<ScoreClassType, ScoreEnumType>>();
		tournamentComboBoxActionListener = (final ActionEvent e) -> {
			changeTournament();
		};
		sessionComboBoxActionListener = (final ActionEvent e) -> {
			displayRanking();
		};
		buttonExportImage.addActionListener((final ActionEvent e) -> {
			exportImage();
		});
		buttonExportCSV.addActionListener((final ActionEvent e) -> {
			exportCSV();
		});
		buttonAutoScroll.addActionListener((final ActionEvent e) -> {
			if (buttonAutoScroll.isSelected()) {
				enableAutoScroll();
			} else {
				disableAutoScroll();
			}
		});
	}

	@Override
	public String getTabName() {
		return translator.translate(UIText.GUI_SECONDARY_RANKING_TAB_NAME);
	}

	protected UIRankingColorProfile getColorProfile() {
		switch (config.getColorProfile()) {
			case WHITE_ON_BLACK:
				return UIRankingColorProfile.PROFILE_WHITE_ON_BLACK;
			case BLACK_ON_WHITE:
				return UIRankingColorProfile.PROFILE_BLACK_ON_WHITE;
			default:
				return null;
		}
	}

	@Override
	public void refresh(final boolean hard) {
		int selectedTournamentIndex = comboTournament.getSelectedIndex();
		Tournament<ScoreClassType, ScoreEnumType> selectedTournament = null;
		if (selectedTournamentIndex >= 0) {
			selectedTournament = listTournament.get(selectedTournamentIndex);
		}

		comboTournament.removeActionListener(tournamentComboBoxActionListener);
		comboTournament.removeAllItems();
		listTournament.clear();
		for (final Tournament<ScoreClassType, ScoreEnumType> tournament : tournamentManager.getAllTournament()) {
			if (!tournament.isArchived()) {
				listTournament.add(tournament);
			}
		}

		if (listTournament.size() > 0) {
			Collections.sort(listTournament);

			selectedTournamentIndex = -1;
			for (int index = 0; index < listTournament.size(); index++) {
				final Tournament<ScoreClassType, ScoreEnumType> tournament = listTournament.get(index);
				if (tournament == selectedTournament) {
					selectedTournamentIndex = index;
				}
				comboTournament.addItem(tournament.getName());
			}

			if (selectedTournamentIndex >= 0) {
				comboTournament.setSelectedIndex(selectedTournamentIndex);
				if (hard) {
					changeTournament();
				} else {
					displayRanking();
				}
			} else {
				comboTournament.setSelectedIndex(0);
				changeTournament();
			}
		} else {
			changeTournament();
		}
		comboTournament.addActionListener(tournamentComboBoxActionListener);
	}

	private void changeTournament() {
		final int selectedTournamentIndex = comboTournament.getSelectedIndex();
		comboMode.removeActionListener(sessionComboBoxActionListener);
		comboMode.removeAllItems();
		comboSession.removeActionListener(sessionComboBoxActionListener);
		comboSession.removeAllItems();
		int selectSessionIndex = -1;
		if (listTournament.size() > 0 && selectedTournamentIndex >= 0) {
			final Tournament<ScoreClassType, ScoreEnumType> tournament = listTournament.get(selectedTournamentIndex);
			for (int session = 1; session <= tournament.getNbExistingSessions(); session++) {
				comboSession.addItem(SESSION_ID_FORMAT.format(session));
			}

			if (tournament.isWithQualification()) {
				rankingModes = rankingModesWithQualification;
			} else {
				rankingModes = rankingModesWithoutQualification;
			}
			for (int index = 0; index < rankingModes.size(); index++) {
				comboMode.addItem(translator.translate(rankingModes.get(index).getUIText()));
			}

			selectSessionIndex = 0;
		}
		comboMode.addActionListener(sessionComboBoxActionListener);
		comboSession.addActionListener(sessionComboBoxActionListener);
		comboSession.setSelectedIndex(selectSessionIndex);
	}

	protected abstract void displayRanking();

	@Override
	public void remoteRefresh() {
		updateRanking();
	}

	protected abstract void updateRanking();

	protected void animate() {
		panelRankingLayout.startTransition();
		panelRankingLayout.setProgress(0.0);

		try {
			Thread.sleep(256);
		} catch (final InterruptedException e) {
		}
		for (double x = -0x1p-3; x < 0x1p-3; x += 0x1p-6) {
			final double progress = 0x1p-1 + Math.cbrt(x);
			panelRankingLayout.setProgress(progress);
			repaint();
			try {
				Thread.sleep(64);
			} catch (final InterruptedException e) {
			}
		}
		panelRankingLayout.finishTransition();
		scoreListIndividual = targetScoreListIndividual;
		scoreListTeam = targetScoreListTeam;
		rankingTableRows = targetRankingTableRows;
		mapPlayerIdRowIndex = targetMapPlayerIdRowIndex;
	}

	@Override
	public void stopRunningThreads() {
		disableAutoScroll();
	}

	private void enableAutoScroll() {
		threadAutoScroll = new Thread(() -> {
			autoScroll();
		});
		threadAutoScroll.start();
	}

	private void disableAutoScroll() {
		if (threadAutoScroll != null) {
			threadAutoScroll.interrupt();
			threadAutoScroll = null;
		}
	}

	private void autoScroll() {
		while (true) {
			try {
				Thread.sleep(10000);
			} catch (final InterruptedException e) {
				break;
			}

			synchronized (scrollRanking) {
				final JScrollBar verticalScrollBar = scrollRanking.getVerticalScrollBar();
				final int value = verticalScrollBar.getModel().getValue();
				final int extent = verticalScrollBar.getModel().getExtent();
				final int minimum = verticalScrollBar.getModel().getMinimum();
				final int maximum = verticalScrollBar.getModel().getMaximum();
				int newValue;
				if (value + extent >= maximum) {
					newValue = minimum;
				} else {
					newValue = value + extent;
				}
				verticalScrollBar.setValue(newValue);
			}
		}
	}

	private void exportImage() {
		final int selectedTournamentIndex = comboTournament.getSelectedIndex();
		final int selectedSessionIndex = comboSession.getSelectedIndex();
		if (selectedTournamentIndex != -1 && selectedSessionIndex != -1) {
			final Tournament<ScoreClassType, ScoreEnumType> tournament = listTournament.get(selectedTournamentIndex);
			final UIRankingMode<ScoreEnumType> ranking = rankingModes.get(comboMode.getSelectedIndex());
			final int session = comboSession.getSelectedIndex() + 1;
			final JFileChooser pngFileChooser = new JFileChooser();
			pngFileChooser.setMultiSelectionEnabled(false);
			pngFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			final String saveFileName = regulateString(tournament.getName())
				+ "_Ranking_"
				+ regulateString(
					translator.translate(
						UIConfigDisplayLanguage.ENGLISH,
						ranking.getUIText()
					)
				)
				+ "_Session_"
				+ Integer.toString(session)
				+ ".png";
			if (lastChoosenFolder != null) {
				pngFileChooser.setSelectedFile(
					new File(
						lastChoosenFolder,
						saveFileName
					)
				);
			} else {
				pngFileChooser.setSelectedFile(new File(saveFileName));
			}
			File pngFile = null;
			boolean toContinue = true;
			while (toContinue) {
				final int fileChooserAnswer = pngFileChooser.showSaveDialog(this);
				if (fileChooserAnswer == JFileChooser.APPROVE_OPTION) {
					pngFile = pngFileChooser.getSelectedFile();
					if (pngFile.exists()) {
						final MessageDialogOption overwriteAnswer = new UIMessageDialog(
							(JFrame) SwingUtilities.getWindowAncestor(this),
							translator.translate(UIText.GUI_SECONDARY_RANKING_EXPORT_OVERWRITE_CONFIRM_MESSAGE),
							translator.translate(UIText.GUI_SECONDARY_RANKING_EXPORT_OVERWRITE_CONFIRM_TITLE),
							MessageDialogMessageType.WARNING,
							MessageDialogOptionsType.YES_NO_CANCEL,
							MessageDialogOption.NO,
							translator
						).showOptionDialog();
						switch (overwriteAnswer) {
							case YES:
								toContinue = false;
								break;
							case NO:
								pngFile = null;
								break;
							case CANCEL:
							case CLOSED:
								pngFile = null;
								toContinue = false;
								break;
							default:
								break;
						}
					} else {
						toContinue = false;
					}
				} else {
					pngFile = null;
					toContinue = false;
				}
			}
			if (pngFile != null) {
				lastChoosenFolder = pngFileChooser.getCurrentDirectory();
				final BufferedImage screenShot = new BufferedImage(
					panelRanking.getWidth(),
					panelRanking.getHeight(),
					BufferedImage.TYPE_INT_RGB
				);
				final Graphics2D graphics = screenShot.createGraphics();
				graphics.setBackground(getColorProfile().getTabBackgroundColor());
				graphics.clearRect(
					0,
					0,
					panelRanking.getWidth(),
					panelRanking.getHeight()
				);
				panelRanking.paintAll(graphics);
				try {
					if (ImageIO.write(
						screenShot,
						"png",
						pngFile
					)) {
						new UIMessageDialog(
							(JFrame) SwingUtilities.getWindowAncestor(this),
							translator.translate(UIText.GUI_SECONDARY_RANKING_EXPORT_RESULT_FILE_SAVED_MESSAGE),
							translator.translate(UIText.GUI_SECONDARY_RANKING_EXPORT_RESULT_FILE_SAVED_TITLE),
							MessageDialogMessageType.INFORMATION,
							MessageDialogOptionsType.OK,
							MessageDialogOption.OK,
							translator
						).showOptionDialog();
					} else {
						new UIMessageDialog(
							(JFrame) SwingUtilities.getWindowAncestor(this),
							translator.translate(UIText.GUI_SECONDARY_RANKING_EXPORT_RESULT_FILE_NOT_SAVED_MESSAGE),
							translator.translate(UIText.GUI_SECONDARY_RANKING_EXPORT_RESULT_FILE_NOT_SAVED_TITLE),
							MessageDialogMessageType.ERROR,
							MessageDialogOptionsType.OK,
							MessageDialogOption.OK,
							translator
						).showOptionDialog();
					}
				} catch (final IOException e) {
					new UIMessageDialog(
						(JFrame) SwingUtilities.getWindowAncestor(this),
						translator.translate(UIText.GUI_SECONDARY_RANKING_EXPORT_RESULT_FILE_NOT_SAVED_MESSAGE),
						translator.translate(UIText.GUI_SECONDARY_RANKING_EXPORT_RESULT_FILE_NOT_SAVED_TITLE),
						MessageDialogMessageType.ERROR,
						MessageDialogOptionsType.OK,
						MessageDialogOption.OK,
						translator
					).showOptionDialog();
					e.printStackTrace();
				}
			}
		}
	}

	protected abstract void exportCSV();
}
