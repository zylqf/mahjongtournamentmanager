/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.ranking.mcr;

import java.awt.Font;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import fr.bri.awt.ProportionalGridLayoutConstraint;
import fr.bri.awt.ProportionalGridLayoutInteger;
import fr.bri.mj.data.score.aggregated.ranking.rankingscore.single.RankingScoreSingleIndividual;
import fr.bri.mj.data.score.aggregated.ranking.rankingscore.single.RankingScoreSingleTeam;
import fr.bri.mj.data.tournament.mcr.ScoreMCR;
import fr.bri.mj.data.tournament.mcr.TournamentScoreMCR;
import fr.bri.mj.gui.config.item.UIConfigNationalMode;
import fr.bri.mj.gui.icon.IconLoaderCountry;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;
import fr.bri.mj.gui.secondary.ranking.UIRankingChartRow;
import fr.bri.mj.gui.secondary.ranking.UIRankingColorProfile;
import fr.bri.mj.gui.secondary.ranking.UIRankingMode;

public class UIRankingChartRowMCR extends UIRankingChartRow<TournamentScoreMCR, ScoreMCR> {

	private static final long serialVersionUID = 8718332367026192601L;

	private static final int SCORE_DIGITS = 4;
	private static final int SCORE_GROUP_WIDTH = 10;
	private static final char SCORE_GROUP_SEPARATOR = ' ';

	private final UIRankingMode<ScoreMCR> rankingMode;
	private final UIConfigNationalMode nationalMode;
	private final UIRankingColorProfile colorProfile;
	private final int fontSize;

	private final JLabel labelRanking;
	private final JLabel labelFlag;
	private final JLabel labelPlayerId;
	private final JLabel labelPlayerName;
	private final List<JLabel> labelTeamMemberNames;
	private final JPanel panelScore;
	private final JLabel labelScore1;
	private final JLabel labelScore2;
	private final JLabel labelScore3;
	private final List<JLabel> labelTeamMemberScores;

	public UIRankingChartRowMCR(
		final UIRankingMode<ScoreMCR> rankingMode,
		final UIConfigNationalMode nationalMode,
		final int nbRows,
		final UIRankingColorProfile colorProfile,
		final Font labelFont,
		final int fontSize,
		final UITextTranslator translator
	) {
		super(translator);
		this.rankingMode = rankingMode;
		this.nationalMode = nationalMode;
		this.colorProfile = colorProfile;
		this.fontSize = fontSize;

		setOpaque(false);

		final ProportionalGridLayoutInteger layout = new ProportionalGridLayoutInteger(
			nbRows,
			7,
			0,
			0
		);
		layout.setWeightX(
			2,
			3,
			3,
			3,
			10,
			14,
			2
		);
		setLayout(layout);

		final ProportionalGridLayoutConstraint constraints = new ProportionalGridLayoutConstraint(
			0,
			1,
			0,
			1
		);

		{
			constraints.y = 0;
			constraints.x = 1;
			constraints.gridHeight = nbRows;
			labelRanking = new JLabel(
				"",
				SwingConstants.CENTER
			);
			labelRanking.setFont(labelFont);
			labelRanking.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
			labelRanking.setBackground(colorProfile.getDarkenLineBackgroundColor());
			add(
				labelRanking,
				constraints
			);
		}
		{
			constraints.y = 0;
			constraints.x = 2;
			constraints.gridHeight = nbRows;
			labelFlag = new JLabel(
				"",
				SwingConstants.CENTER
			);
			labelFlag.setFont(labelFont);
			labelFlag.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
			labelFlag.setBackground(colorProfile.getDarkenLineBackgroundColor());
			add(
				labelFlag,
				constraints
			);
		}
		{
			constraints.y = 0;
			constraints.x = 3;
			constraints.gridHeight = nbRows;
			labelPlayerId = new JLabel(
				"",
				SwingConstants.CENTER
			);
			labelPlayerId.setFont(labelFont);
			labelPlayerId.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
			labelPlayerId.setBackground(colorProfile.getDarkenLineBackgroundColor());
			add(
				labelPlayerId,
				constraints
			);
		}
		{
			constraints.y = 0;
			constraints.x = 4;
			constraints.gridHeight = 1;
			labelPlayerName = new JLabel(
				"",
				SwingConstants.LEADING
			);
			labelPlayerName.setFont(labelFont);
			labelPlayerName.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
			labelPlayerName.setBackground(colorProfile.getDarkenLineBackgroundColor());
			add(
				labelPlayerName,
				constraints
			);

			labelTeamMemberNames = new ArrayList<JLabel>();
			for (int nbRow = 1; nbRow < nbRows; nbRow++) {
				constraints.y = nbRow;
				final JLabel labelPlayerTeamMemberName = new JLabel(
					"",
					SwingConstants.LEADING
				);
				labelPlayerTeamMemberName.setFont(labelFont);
				labelPlayerTeamMemberName.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
				labelPlayerTeamMemberName.setBackground(colorProfile.getDarkenLineBackgroundColor());
				labelTeamMemberNames.add(labelPlayerTeamMemberName);
				add(
					labelPlayerTeamMemberName,
					constraints
				);
			}
		}
		{
			constraints.y = 0;
			constraints.x = 5;
			constraints.gridHeight = nbRows;
			panelScore = new JPanel();
			panelScore.setBackground(colorProfile.getDarkenLineBackgroundColor());

			labelScore1 = new JLabel(
				"",
				SwingConstants.CENTER
			);
			labelScore1.setFont(labelFont);

			labelScore2 = new JLabel(
				"",
				SwingConstants.CENTER
			);
			labelScore2.setFont(labelFont);

			labelScore3 = new JLabel(
				"",
				SwingConstants.CENTER
			);
			labelScore3.setFont(labelFont);

			final ProportionalGridLayoutConstraint panelScoreLayoutConstraints = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			labelTeamMemberScores = new ArrayList<JLabel>();
			switch (rankingMode.getScore()) {
				case MATCH_POINT:
				case TABLE_POINT: {
					final ProportionalGridLayoutInteger panelScoreLayout = new ProportionalGridLayoutInteger(
						nbRows,
						2,
						0,
						2
					);
					panelScore.setLayout(panelScoreLayout);

					panelScoreLayoutConstraints.y = 0;
					panelScoreLayoutConstraints.x = 0;
					panelScore.add(
						labelScore1,
						panelScoreLayoutConstraints
					);
					panelScoreLayoutConstraints.x = 1;
					panelScore.add(
						labelScore2,
						panelScoreLayoutConstraints
					);

					panelScoreLayoutConstraints.x = 0;
					for (int nbRow = 1; nbRow < nbRows; nbRow++) {
						panelScoreLayoutConstraints.y = nbRow;
						final JLabel labelTeamMemberScore = new JLabel(
							"",
							SwingConstants.CENTER
						);
						labelTeamMemberScore.setFont(labelFont);
						labelTeamMemberScores.add(labelTeamMemberScore);
						panelScore.add(
							labelTeamMemberScore,
							panelScoreLayoutConstraints
						);
					}
				}
					break;
			}

			add(
				panelScore,
				constraints
			);
		}
	}

	@Override
	public void setScoreIndividual(
		final RankingScoreSingleIndividual score,
		final int average,
		final boolean darken
	) {
		labelRanking.setText(Integer.toString(score.getRanking()));

		switch (rankingMode.getScore()) {
			case MATCH_POINT:
			case TABLE_POINT:
				switch (nationalMode) {
					case NATIONALITY:
						if (score.getTournamentPlayer().getPlayer() != null) {
							final Image iconImage = IconLoaderCountry.getIconImageCountry(
								score.getTournamentPlayer().getPlayer().getPlayerNationality(),
								fontSize
							);
							labelFlag.setIcon(new ImageIcon(iconImage));
						} else {
							labelFlag.setIcon(null);
						}
						labelFlag.setText("");
						break;
					case CLUB_NAME:
						if (score.getTournamentPlayer().getPlayer() != null) {
							labelFlag.setText(score.getTournamentPlayer().getPlayer().getClub());
						} else {
							labelFlag.setText("");
						}
						labelFlag.setIcon(null);
						break;
				}
				break;
		}

		labelPlayerId.setText(Integer.toString(score.getTournamentPlayer().getTournamentPlayerId()));

		String playerName = null;
		switch (rankingMode.getScore()) {
			case MATCH_POINT:
			case TABLE_POINT:
				playerName = score.getName(translator.translate(UIText.GUI_SECONDARY_RANKING_TITLE_PLAYER_EMPTY));
				break;
		}
		labelPlayerName.setText(playerName);

		switch (rankingMode.getScore()) {
			case MATCH_POINT:
				labelScore1.setText(
					fraction(
						score.getTotalScore(),
						12
					)
				);
				labelScore2.setText(
					format(
						score.getTotalScore2(),
						SCORE_DIGITS,
						SCORE_GROUP_WIDTH,
						SCORE_GROUP_SEPARATOR
					)
				);
				break;
			case TABLE_POINT:
				labelScore1.setText(
					format(
						score.getTotalScore(),
						SCORE_DIGITS,
						SCORE_GROUP_WIDTH,
						SCORE_GROUP_SEPARATOR
					)
				);
				labelScore2.setText(
					fraction(
						score.getTotalScore2(),
						12
					)
				);
				break;
			default:
				break;
		}

		if (score.isUpToDate()) {
			if (score.getTotalScore() >= average) {
				labelScore1.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
				labelScore2.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
				labelScore3.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
			} else {
				labelScore1.setForeground(colorProfile.getUpToDateNegativeScoreFontColor());
				labelScore2.setForeground(colorProfile.getUpToDateNegativeScoreFontColor());
				labelScore3.setForeground(colorProfile.getUpToDateNegativeScoreFontColor());
			}
		} else {
			if (score.getTotalScore() >= average) {
				labelScore1.setForeground(colorProfile.getDelayedPositiveScoreFontColor());
				labelScore2.setForeground(colorProfile.getDelayedPositiveScoreFontColor());
				labelScore3.setForeground(colorProfile.getDelayedPositiveScoreFontColor());
			} else {
				labelScore1.setForeground(colorProfile.getDelayedNegativeScoreFontColor());
				labelScore2.setForeground(colorProfile.getDelayedNegativeScoreFontColor());
				labelScore3.setForeground(colorProfile.getDelayedNegativeScoreFontColor());
			}
		}

		labelRanking.setOpaque(darken);
		labelFlag.setOpaque(darken);
		labelPlayerId.setOpaque(darken);
		labelPlayerName.setOpaque(darken);
		for (final JLabel labelPlayerTeamMemberName : labelTeamMemberNames) {
			labelPlayerTeamMemberName.setOpaque(darken);
		}
		panelScore.setOpaque(darken);
	}

	@Override
	public void setScoreTeam(
		final RankingScoreSingleTeam score,
		final int average,
		final boolean darken
	) {
		labelRanking.setText(Integer.toString(score.getRanking()));

		String playerName = null;
		switch (rankingMode.getScore()) {
			case MATCH_POINT:
			case TABLE_POINT:
				playerName = translator.translate(UIText.GUI_SECONDARY_RANKING_MCR_ROW_TITLE_TEAM)
					+ " "
					+ score.getName(translator.translate(UIText.GUI_SECONDARY_RANKING_TITLE_PLAYER_EMPTY));
				break;
			default:
				playerName = "";
				break;
		}
		labelPlayerName.setText(playerName);

		int index = 0;
		for (final RankingScoreSingleIndividual scoreIndividual : score.getScores()) {
			labelTeamMemberNames.get(index++).setText(
				"        "
					+ scoreIndividual.getName(translator.translate(UIText.GUI_SECONDARY_RANKING_TITLE_PLAYER_EMPTY))
			);
		}

		switch (rankingMode.getScore()) {
			case MATCH_POINT:
				labelScore1.setText(
					fraction(
						score.getTotalScore(),
						12
					)
				);
				labelScore2.setText(
					format(
						score.getTotalScore2(),
						SCORE_DIGITS,
						SCORE_GROUP_WIDTH,
						SCORE_GROUP_SEPARATOR
					)
				);
				index = 0;
				for (final RankingScoreSingleIndividual scoreIndividual : score.getScores()) {
					labelTeamMemberScores.get(index++).setText(
						fraction(
							scoreIndividual.getTotalScore(),
							12
						)
					);
				}
				break;
			case TABLE_POINT:
				labelScore1.setText(
					format(
						score.getTotalScore(),
						SCORE_DIGITS,
						SCORE_GROUP_WIDTH,
						SCORE_GROUP_SEPARATOR
					)
				);
				labelScore2.setText(
					fraction(
						score.getTotalScore2(),
						12
					)
				);
				index = 0;
				for (final RankingScoreSingleIndividual scoreIndividual : score.getScores()) {
					labelTeamMemberScores.get(index++).setText(
						format(
							scoreIndividual.getTotalScore(),
							SCORE_DIGITS,
							SCORE_GROUP_WIDTH,
							SCORE_GROUP_SEPARATOR
						)
					);
				}
				break;
			default:
				break;
		}

		if (score.isUpToDate()) {
			if (score.getTotalScore() >= average) {
				labelScore1.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
				labelScore2.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
				labelScore3.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
				for (final JLabel labelTeamMemberScore : labelTeamMemberScores) {
					labelTeamMemberScore.setForeground(colorProfile.getDelayedPositiveScoreFontColor());
				}
			} else {
				labelScore1.setForeground(colorProfile.getUpToDateNegativeScoreFontColor());
				labelScore2.setForeground(colorProfile.getUpToDateNegativeScoreFontColor());
				labelScore3.setForeground(colorProfile.getUpToDateNegativeScoreFontColor());
				for (final JLabel labelTeamMemberScore : labelTeamMemberScores) {
					labelTeamMemberScore.setForeground(colorProfile.getDelayedNegativeScoreFontColor());
				}
			}
		} else {
			if (score.getTotalScore() >= average) {
				labelScore1.setForeground(colorProfile.getDelayedPositiveScoreFontColor());
				labelScore2.setForeground(colorProfile.getDelayedPositiveScoreFontColor());
				labelScore3.setForeground(colorProfile.getDelayedPositiveScoreFontColor());
				for (final JLabel labelTeamMemberScore : labelTeamMemberScores) {
					labelTeamMemberScore.setForeground(colorProfile.getDelayedPositiveScoreFontColor());
				}
			} else {
				labelScore1.setForeground(colorProfile.getDelayedNegativeScoreFontColor());
				labelScore2.setForeground(colorProfile.getDelayedNegativeScoreFontColor());
				labelScore3.setForeground(colorProfile.getDelayedNegativeScoreFontColor());
				for (final JLabel labelTeamMemberScore : labelTeamMemberScores) {
					labelTeamMemberScore.setForeground(colorProfile.getDelayedNegativeScoreFontColor());
				}
			}
		}

		labelRanking.setOpaque(darken);
		labelFlag.setOpaque(darken);
		labelPlayerId.setOpaque(darken);
		labelPlayerName.setOpaque(darken);
		for (final JLabel labelPlayerTeamMemberName : labelTeamMemberNames) {
			labelPlayerTeamMemberName.setOpaque(darken);
		}
		panelScore.setOpaque(darken);
	}

	private String format(
		final int number,
		final int width,
		final int groupWidth,
		final char groupSeparator
	) {
		final StringBuilder builder = new StringBuilder();
		if (number >= 0) {
			builder.append(' ');
		} else {
			builder.append('-');
		}

		final String raw = Integer.toString(Math.abs(number));
		final int rawWidth = raw.length();
		final int nbGroupSeparator = (width - 1) / groupWidth;
		final int nbRawGroupSeparator = (rawWidth - 1) / groupWidth;
		final int paddingWidth = width - rawWidth + nbGroupSeparator - nbRawGroupSeparator;
		for (int index = 0; index < paddingWidth; index++) {
			builder.append(' ');
		}

		for (int index = 0; index < rawWidth; index++) {
			builder.append(raw.charAt(index));
			final int numberIndex = rawWidth - 1 - index;
			if (numberIndex != 0 && numberIndex % groupWidth == 0) {
				builder.append(groupSeparator);
			}
		}
		return builder.toString();
	}

	private String fraction(
		final int numerator,
		final int denominator
	) {
		final int quotient = numerator / denominator;
		final int remainder = numerator % denominator;
		if (remainder != 0) {
			int a = remainder;
			int bcd = denominator;
			int t;
			while (a != 0) {
				t = a;
				a = bcd;
				bcd = t;
				a = a % bcd;
			}
			return Integer.toString(quotient)
				+ " "
				+ Integer.toString(remainder / bcd)
				+ "/"
				+ Integer.toString(denominator / bcd);
		} else {
			return Integer.toString(quotient);
		}
	}
}
