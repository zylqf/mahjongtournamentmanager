/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.ranking.mcr;

import java.util.ArrayList;
import java.util.List;

import fr.bri.mj.data.score.aggregated.QualificationMode;
import fr.bri.mj.data.score.aggregated.TeamMode;
import fr.bri.mj.data.tournament.mcr.ScoreMCR;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.secondary.ranking.UIRankingMode;

public enum UIRankingModeMCR implements UIRankingMode<ScoreMCR> {

	MATCH_POINT_INDIVIDUAL(UIText.GUI_RANKING_MCR_MODE_INDIVIDUAL_MATCH_POINT, ScoreMCR.MATCH_POINT, QualificationMode.WITHOUT, TeamMode.INDIVIDUAL),
	MATCH_POINT_INDIVIDUAL_WITH_QUALITIFICATION(UIText.GUI_RANKING_MCR_MODE_INDIVIDUAL_MATCH_POINT, ScoreMCR.MATCH_POINT, QualificationMode.WITH, TeamMode.INDIVIDUAL),
	TABLE_POINT_INDIVIDUAL(UIText.GUI_RANKING_MCR_MODE_INDIVIDUAL_TABLE_POINT, ScoreMCR.TABLE_POINT, QualificationMode.WITHOUT, TeamMode.INDIVIDUAL),

	MATCH_POINT_TEAM(UIText.GUI_RANKING_MCR_MODE_TEAM_MATCH_POINT, ScoreMCR.MATCH_POINT, QualificationMode.WITHOUT, TeamMode.TEAM),
	TABLE_POINT_TEAM(UIText.GUI_RANKING_MCR_MODE_TEAM_TABLE_POINT, ScoreMCR.TABLE_POINT, QualificationMode.WITHOUT, TeamMode.TEAM),;

	public static List<UIRankingMode<ScoreMCR>> getRankingModes(
		final TeamMode teamMode,
		final QualificationMode qualificationMode
	) {
		final List<UIRankingMode<ScoreMCR>> listeMode = new ArrayList<UIRankingMode<ScoreMCR>>();
		for (final UIRankingModeMCR mode : UIRankingModeMCR.values()) {
			if ((teamMode == null || mode.teamMode == teamMode) && mode.qualificationMode == qualificationMode) {
				listeMode.add(mode);
			}
		}
		return listeMode;
	}

	private UIRankingModeMCR(
		final UIText uiText,
		final ScoreMCR score,
		final QualificationMode qualificationMode,
		final TeamMode teamMode
	) {
		this.uiText = uiText;
		this.score = score;
		this.qualificationMode = qualificationMode;
		this.teamMode = teamMode;
	}

	private final UIText uiText;
	private final ScoreMCR score;
	private final QualificationMode qualificationMode;
	private final TeamMode teamMode;

	@Override
	public UIText getUIText() {
		return uiText;
	}

	@Override
	public ScoreMCR getScore() {
		return score;
	}

	@Override
	public QualificationMode getQualificationMode() {
		return qualificationMode;
	}

	@Override
	public TeamMode getRankingModeTeamMode() {
		return teamMode;
	}
}
