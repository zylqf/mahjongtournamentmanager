/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.ranking.rcr;

import java.awt.Font;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import fr.bri.awt.ProportionalGridLayoutConstraint;
import fr.bri.awt.ProportionalGridLayoutInteger;
import fr.bri.mj.data.player.Player;
import fr.bri.mj.data.score.aggregated.QualificationMode;
import fr.bri.mj.data.score.aggregated.ranking.calculator.single.RankingCalculatorSingleRCRFactory;
import fr.bri.mj.data.score.aggregated.ranking.rankingscore.single.RankingScoreSingleIndividual;
import fr.bri.mj.data.score.aggregated.ranking.rankingscore.single.RankingScoreSingleTeam;
import fr.bri.mj.data.tournament.Tournament;
import fr.bri.mj.data.tournament.TournamentPlayer;
import fr.bri.mj.data.tournament.rcr.ScoreRCR;
import fr.bri.mj.data.tournament.rcr.TournamentScoreRCR;
import fr.bri.mj.dataaccess.TournamentManager;
import fr.bri.mj.gui.config.UIConfig;
import fr.bri.mj.gui.config.item.UIConfigDisplayLanguage;
import fr.bri.mj.gui.dialog.UIMessageDialog;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogMessageType;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogOption;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogOptionsType;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;
import fr.bri.mj.gui.secondary.ranking.UIRankingChartRow;
import fr.bri.mj.gui.secondary.ranking.UIRankingChartSeparator;
import fr.bri.mj.gui.secondary.ranking.UIRankingColorProfile;
import fr.bri.mj.gui.secondary.ranking.UIRankingMode;
import fr.bri.mj.gui.secondary.ranking.UITabPanelRanking;
import fr.bri.mj.gui.secondary.ranking.VirticalBoxMovingRowLayout;
import fr.bri.mj.utils.CSVWriter;

public class UITabPanelRankingRCR extends UITabPanelRanking<TournamentScoreRCR, ScoreRCR> {

	private static final long serialVersionUID = -4251699598304298252L;

	public UITabPanelRankingRCR(
		final TournamentManager<TournamentScoreRCR, ScoreRCR> dataAccess,
		final UIConfig config,
		final UITextTranslator translator
	) {
		super(
			dataAccess,
			RankingCalculatorSingleRCRFactory.getInstance(),
			UIRankingModeRCR.getRankingModes(
				null,
				QualificationMode.WITH
			),
			UIRankingModeRCR.getRankingModes(
				null,
				QualificationMode.WITHOUT
			),
			config,
			translator
		);
	}

	@Override
	protected void displayRanking() {
		synchronized (scrollRanking) {
			final UIRankingColorProfile rankingColorProfile = getColorProfile();
			invalidate();
			panelCenterSupport.setBackground(rankingColorProfile.getTabBackgroundColor());

			panelRanking.removeAll();
			rankingTableRows = new ArrayList<UIRankingChartRow<TournamentScoreRCR, ScoreRCR>>();
			mapPlayerIdRowIndex = new HashMap<Integer, Integer>();
			rankingTableTitle = null;

			final int selectedTournamentIndex = comboTournament.getSelectedIndex();
			final int selectedSessionIndex = comboSession.getSelectedIndex();
			if (selectedTournamentIndex != -1 && selectedSessionIndex != -1) {
				final Tournament<TournamentScoreRCR, ScoreRCR> tournament = listTournament.get(selectedTournamentIndex);
				final UIRankingMode<ScoreRCR> ranking = rankingModes.get(comboMode.getSelectedIndex());
				final int session = comboSession.getSelectedIndex() + 1;
				if (tournament.isWithQualification()) {
					scoreListIndividual = rankingCalculatorSingleFactory.getRankingCalculator(
						ranking.getScore(),
						ranking.getQualificationMode()
					).getRankingScoreIndividual(
						tournament,
						session
					);
					if (scoreListIndividual != null && scoreListIndividual.size() > 0) {
						final int fontSize = getConfigFontSize();
						final Font labelFont = (CUSTOM_FONT != null
							? CUSTOM_FONT
							: getFont()).deriveFont(
								Font.PLAIN,
								fontSize
							);

						final Map<Integer, UIText> separatorIndex = new HashMap<Integer, UIText>();
						switch (tournament.getNbSessions() - session) {
							case 0:
							case 1:
								// Final phase
								separatorIndex.put(
									0,
									UIText.GUI_SECONDARY_RANKING_ROW_SEPARATOR_FINAL
								);
								separatorIndex.put(
									4,
									UIText.GUI_SECONDARY_RANKING_ROW_SEPARATOR_SEMI_FINAL
								);
								separatorIndex.put(
									8,
									UIText.GUI_SECONDARY_RANKING_ROW_SEPARATOR_QUARTER_FINAL
								);
								separatorIndex.put(
									16,
									UIText.GUI_SECONDARY_RANKING_ROW_SEPARATOR_PLACEMENT
								);
								break;
							case 2:
							case 3:
								// 1/2 final phase
								separatorIndex.put(
									0,
									UIText.GUI_SECONDARY_RANKING_ROW_SEPARATOR_SEMI_FINAL
								);
								separatorIndex.put(
									8,
									UIText.GUI_SECONDARY_RANKING_ROW_SEPARATOR_QUARTER_FINAL
								);
								separatorIndex.put(
									16,
									UIText.GUI_SECONDARY_RANKING_ROW_SEPARATOR_PLACEMENT
								);
								break;
							case 4:
							case 5:
								// 1/4 final phase
								separatorIndex.put(
									0,
									UIText.GUI_SECONDARY_RANKING_ROW_SEPARATOR_QUARTER_FINAL
								);
								separatorIndex.put(
									16,
									UIText.GUI_SECONDARY_RANKING_ROW_SEPARATOR_PLACEMENT
								);
								break;
							default:
								// Qualification phase
								separatorIndex.put(
									0,
									UIText.GUI_SECONDARY_RANKING_ROW_SEPARATOR_POOL_STAGE
								);
								break;
						}

						final ProportionalGridLayoutInteger panelRankingLayout = new ProportionalGridLayoutInteger(
							scoreListIndividual.size() + separatorIndex.size() + 1,
							1,
							0,
							0
						);
						panelRanking.setLayout(panelRankingLayout);

						final ProportionalGridLayoutConstraint panelRankingConstraint = new ProportionalGridLayoutConstraint(
							0,
							1,
							0,
							1
						);

						rankingTableTitle = new UIRankingChartTitleRCR(
							ranking,
							config.getNationalMode(),
							rankingColorProfile,
							labelFont,
							translator
						);

						panelRankingConstraint.y = 0;
						panelRanking.add(
							rankingTableTitle,
							panelRankingConstraint
						);

						for (int index = 0; index < scoreListIndividual.size(); index++) {
							if (separatorIndex.containsKey(index)) {
								final UIRankingChartSeparator separator = new UIRankingChartSeparator(
									rankingColorProfile,
									labelFont,
									separatorIndex.get(index),
									translator
								);

								panelRankingConstraint.y++;
								panelRanking.add(
									separator,
									panelRankingConstraint
								);
							}
							final RankingScoreSingleIndividual record = scoreListIndividual.get(index);
							final UIRankingChartRowRCR rankingTableRow = new UIRankingChartRowRCR(
								ranking,
								config.getNationalMode(),
								1,
								rankingColorProfile,
								labelFont,
								fontSize,
								translator
							);

							rankingTableRow.setScoreIndividual(
								record,
								0,
								index % 2 == 0
							);

							panelRankingConstraint.y++;
							panelRanking.add(
								rankingTableRow,
								panelRankingConstraint
							);
						}
					}
				} else {
					switch (ranking.getRankingModeTeamMode()) {
						case INDIVIDUAL: {
							scoreListIndividual = rankingCalculatorSingleFactory.getRankingCalculator(
								ranking.getScore(),
								ranking.getQualificationMode()
							).getRankingScoreIndividual(
								tournament,
								session
							);
							if (scoreListIndividual != null && scoreListIndividual.size() > 0) {
								panelRankingLayout = new VirticalBoxMovingRowLayout(
									panelRanking,
									scoreListIndividual.size() + 1
								);
								panelRanking.setLayout(panelRankingLayout);

								final int fontSize = getConfigFontSize();
								final Font labelFont = (CUSTOM_FONT != null
									? CUSTOM_FONT
									: getFont()).deriveFont(
										Font.PLAIN,
										fontSize
									);
								rankingTableTitle = new UIRankingChartTitleRCR(
									ranking,
									config.getNationalMode(),
									rankingColorProfile,
									labelFont,
									translator
								);
								panelRanking.add(
									rankingTableTitle,
									new Integer(0)
								);

								for (int index = 0; index < scoreListIndividual.size(); index++) {
									final RankingScoreSingleIndividual record = scoreListIndividual.get(index);
									final UIRankingChartRowRCR rankingTableRow = new UIRankingChartRowRCR(
										ranking,
										config.getNationalMode(),
										1,
										rankingColorProfile,
										labelFont,
										fontSize,
										translator
									);

									rankingTableRow.setScoreIndividual(
										record,
										0,
										index % 2 == 0
									);
									panelRanking.add(
										rankingTableRow,
										new Integer(index + 1)
									);

									rankingTableRows.add(rankingTableRow);
									mapPlayerIdRowIndex.put(
										record.hashCode(),
										rankingTableRows.size() - 1
									);
								}
							}
							break;
						}
						case TEAM: {
							scoreListTeam = rankingCalculatorSingleFactory.getRankingCalculator(
								ranking.getScore(),
								ranking.getQualificationMode()
							).getRankingScoreTeam(
								tournament,
								session
							);
							if (scoreListTeam != null && scoreListTeam.size() > 0) {
								final int fontSize = getConfigFontSize();
								final Font labelFont = (CUSTOM_FONT != null
									? CUSTOM_FONT
									: getFont()).deriveFont(
										Font.PLAIN,
										fontSize
									);

								panelRankingLayout = new VirticalBoxMovingRowLayout(
									panelRanking,
									scoreListTeam.size() + 1
								);
								panelRanking.setLayout(panelRankingLayout);
								rankingTableTitle = new UIRankingChartTitleRCR(
									ranking,
									config.getNationalMode(),
									rankingColorProfile,
									labelFont,
									translator
								);
								panelRanking.add(
									rankingTableTitle,
									new Integer(0)
								);

								for (int index = 0; index < scoreListTeam.size(); index++) {
									final RankingScoreSingleTeam record = scoreListTeam.get(index);
									final UIRankingChartRowRCR rankingTableRow = new UIRankingChartRowRCR(
										ranking,
										config.getNationalMode(),
										record.getScores().size() + 1,
										rankingColorProfile,
										labelFont,
										fontSize,
										translator
									);

									rankingTableRow.setScoreTeam(
										record,
										0,
										index % 2 == 0
									);
									panelRanking.add(
										rankingTableRow,
										new Integer(index + 1)
									);

									rankingTableRows.add(rankingTableRow);
									mapPlayerIdRowIndex.put(
										record.hashCode(),
										rankingTableRows.size() - 1
									);
								}
							}
							break;
						}
					}
				}
			}
			validate();
			scrollRanking.getVerticalScrollBar().setValue(0);
			repaint();
		}
	}

	@Override
	protected void updateRanking() {
		final int selectedTournamentIndex = comboTournament.getSelectedIndex();
		final int selectedSessionIndex = comboSession.getSelectedIndex();
		if (selectedTournamentIndex != -1 && selectedSessionIndex != -1) {
			final Tournament<TournamentScoreRCR, ScoreRCR> tournament = listTournament.get(selectedTournamentIndex);
			final UIRankingMode<ScoreRCR> ranking = rankingModes.get(comboMode.getSelectedIndex());
			final int session = comboSession.getSelectedIndex() + 1;
			if (tournament.isWithQualification()) {
				displayRanking();
			} else {
				switch (ranking.getRankingModeTeamMode()) {
					case INDIVIDUAL: {
						targetScoreListIndividual = rankingCalculatorSingleFactory.getRankingCalculator(
							ranking.getScore(),
							ranking.getQualificationMode()
						).getRankingScoreIndividual(
							tournament,
							session
						);
						if (targetScoreListIndividual != null && targetScoreListIndividual.size() > 0) {
							targetRankingTableRows = new ArrayList<UIRankingChartRow<TournamentScoreRCR, ScoreRCR>>(rankingTableRows.size());
							targetMapPlayerIdRowIndex = new HashMap<Integer, Integer>(mapPlayerIdRowIndex.size());

							panelRankingLayout.startUpdate();
							panelRankingLayout.updateRow(
								rankingTableTitle,
								0
							);

							for (int index = 0; index < targetScoreListIndividual.size(); index++) {
								final RankingScoreSingleIndividual record = targetScoreListIndividual.get(index);
								final UIRankingChartRow<TournamentScoreRCR, ScoreRCR> rankingTableRow = rankingTableRows.get(mapPlayerIdRowIndex.get(record.hashCode()));
								rankingTableRow.setScoreIndividual(
									record,
									0,
									index % 2 == 0
								);
								panelRankingLayout.updateRow(
									rankingTableRow,
									index + 1
								);

								targetRankingTableRows.add(rankingTableRow);
								targetMapPlayerIdRowIndex.put(
									record.hashCode(),
									targetRankingTableRows.size() - 1
								);
							}
							repaint();

							new Thread(() -> {
								animate();
							}).start();
						}
						break;
					}
					case TEAM: {
						targetScoreListTeam = rankingCalculatorSingleFactory.getRankingCalculator(
							ranking.getScore(),
							ranking.getQualificationMode()
						).getRankingScoreTeam(
							tournament,
							session
						);
						if (targetScoreListTeam != null && targetScoreListTeam.size() > 0) {
							targetRankingTableRows = new ArrayList<UIRankingChartRow<TournamentScoreRCR, ScoreRCR>>(rankingTableRows.size());
							targetMapPlayerIdRowIndex = new HashMap<Integer, Integer>(mapPlayerIdRowIndex.size());

							panelRankingLayout.startUpdate();
							panelRankingLayout.updateRow(
								rankingTableTitle,
								0
							);

							for (int index = 0; index < targetScoreListTeam.size(); index++) {
								final RankingScoreSingleTeam record = targetScoreListTeam.get(index);
								final UIRankingChartRow<TournamentScoreRCR, ScoreRCR> rankingTableRow = rankingTableRows.get(mapPlayerIdRowIndex.get(record.hashCode()));
								rankingTableRow.setScoreTeam(
									record,
									0,
									index % 2 == 0
								);
								panelRankingLayout.updateRow(
									rankingTableRow,
									index + 1
								);

								targetRankingTableRows.add(rankingTableRow);
								targetMapPlayerIdRowIndex.put(
									record.hashCode(),
									targetRankingTableRows.size() - 1
								);
							}
							repaint();

							new Thread(() -> {
								animate();
							}).start();
						}
						break;
					}
				}
			}
		}
	}

	@Override
	protected void exportCSV() {
		final int selectedTournamentIndex = comboTournament.getSelectedIndex();
		final int selectedSessionIndex = comboSession.getSelectedIndex();
		if (selectedTournamentIndex != -1 && selectedSessionIndex != -1) {
			final UIRankingMode<ScoreRCR> ranking = rankingModes.get(comboMode.getSelectedIndex());
			final Tournament<TournamentScoreRCR, ScoreRCR> tournament = listTournament.get(selectedTournamentIndex);
			final int session = comboSession.getSelectedIndex() + 1;
			if (scoreListIndividual != null && scoreListIndividual.size() > 0 || scoreListTeam != null && scoreListTeam.size() > 0) {
				final JFileChooser csvFileChooser = new JFileChooser();
				csvFileChooser.setMultiSelectionEnabled(false);
				csvFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				final String saveFileName = regulateString(tournament.getName())
					+ "_Ranking_"
					+ regulateString(
						translator.translate(
							UIConfigDisplayLanguage.ENGLISH,
							ranking.getUIText()
						)
					)
					+ "_Session_"
					+ Integer.toString(session)
					+ ".csv";
				if (lastChoosenFolder != null) {
					csvFileChooser.setSelectedFile(
						new File(
							lastChoosenFolder,
							saveFileName
						)
					);
				} else {
					csvFileChooser.setSelectedFile(new File(saveFileName));
				}
				File csvFile = null;
				boolean toContinue = true;
				while (toContinue) {
					final int fileChooserAnswer = csvFileChooser.showSaveDialog(this);
					if (fileChooserAnswer == JFileChooser.APPROVE_OPTION) {
						csvFile = csvFileChooser.getSelectedFile();
						if (csvFile.exists()) {
							final MessageDialogOption overwriteAnswer = new UIMessageDialog(
								(JFrame) SwingUtilities.getWindowAncestor(this),
								translator.translate(UIText.GUI_SECONDARY_RANKING_EXPORT_OVERWRITE_CONFIRM_MESSAGE),
								translator.translate(UIText.GUI_SECONDARY_RANKING_EXPORT_OVERWRITE_CONFIRM_TITLE),
								MessageDialogMessageType.WARNING,
								MessageDialogOptionsType.YES_NO_CANCEL,
								MessageDialogOption.NO,
								translator
							).showOptionDialog();
							switch (overwriteAnswer) {
								case YES:
									toContinue = false;
									break;
								case NO:
									csvFile = null;
									break;
								case CANCEL:
								case CLOSED:
									csvFile = null;
									toContinue = false;
									break;
								default:
									break;
							}
						} else {
							toContinue = false;
						}
					} else {
						csvFile = null;
						toContinue = false;
					}
				}

				if (csvFile != null) {
					lastChoosenFolder = csvFileChooser.getCurrentDirectory();

					final String csvContent[][] = new String[scoreListIndividual.size() + 2][];
					csvContent[0] = new String[] {
						"Tournament",
						tournament.getName(),
						"Ranking",
						translator.translate(
							UIConfigDisplayLanguage.ENGLISH,
							ranking.getUIText()
						),
						"Session",
						Integer.toString(session)
					};

					switch (ranking.getRankingModeTeamMode()) {
						case INDIVIDUAL: {
							switch (ranking.getScore()) {
								case FINAL_SCORE: {
									csvContent[1] = new String[] {
										"Ranking",
										"First Name",
										"Last Name",
										"Nationality",
										"License",
										translator.translate(
											UIConfigDisplayLanguage.ENGLISH,
											UIText.GUI_SECONDARY_RANKING_RCR_ROW_TITLE_FINAL_SCORE
										)
									};
									for (int index = 0; index < scoreListIndividual.size(); index++) {
										final RankingScoreSingleIndividual score = scoreListIndividual.get(index);
										final TournamentPlayer tournamentPlayer = score.getTournamentPlayer();
										final Player player = tournamentPlayer.getPlayer();
										if (player != null) {
											csvContent[index + 2] = new String[] {
												Integer.toString(score.getRanking()),
												player.getPlayerFirstName(),
												player.getPlayerLastName(),
												player.getPlayerNationality().getCountryCodeAlpha3(),
												player.getLicense(),
												Integer.toString(score.getTotalScore())
											};
										} else {
											csvContent[index + 2] = new String[] {
												Integer.toString(score.getRanking()),
												tournamentPlayer.getPlayerName(translator.translate(UIText.GUI_SECONDARY_DISPLAY_SCORE_TITLE_PLAYER_EMPTY)),
												"",
												"",
												"",
												Integer.toString(score.getTotalScore())
											};
										}
									}
								}
									break;

								case GAME_SCORE: {
									csvContent[1] = new String[] {
										"Ranking",
										"First Name",
										"Last Name",
										"Nationality",
										"License",
										translator.translate(
											UIConfigDisplayLanguage.ENGLISH,
											UIText.GUI_SECONDARY_RANKING_RCR_ROW_TITLE_GAME_SCORE
										),
										translator.translate(
											UIConfigDisplayLanguage.ENGLISH,
											UIText.GUI_SECONDARY_RANKING_RCR_ROW_TITLE_FINAL_SCORE
										)
									};
									for (int index = 0; index < scoreListIndividual.size(); index++) {
										final RankingScoreSingleIndividual score = scoreListIndividual.get(index);
										final TournamentPlayer tournamentPlayer = score.getTournamentPlayer();
										final Player player = tournamentPlayer.getPlayer();
										if (player != null) {
											csvContent[index + 2] = new String[] {
												Integer.toString(score.getRanking()),
												player.getPlayerFirstName(),
												player.getPlayerLastName(),
												player.getPlayerNationality().getCountryCodeAlpha3(),
												player.getLicense(),
												Integer.toString(score.getTotalScore()),
												Integer.toString(score.getTotalScore2())
											};
										} else {
											csvContent[index + 2] = new String[] {
												Integer.toString(score.getRanking()),
												tournamentPlayer.getPlayerName(translator.translate(UIText.GUI_SECONDARY_DISPLAY_SCORE_TITLE_PLAYER_EMPTY)),
												"",
												"",
												"",
												Integer.toString(score.getTotalScore()),
												Integer.toString(score.getTotalScore2())
											};
										}
									}
								}
									break;
								case UMA_SCORE: {
									csvContent[1] = new String[] {
										"Ranking",
										"First Name",
										"Last Name",
										"Nationality",
										"License",
										translator.translate(
											UIConfigDisplayLanguage.ENGLISH,
											UIText.GUI_SECONDARY_RANKING_RCR_ROW_TITLE_UMA_SCORE
										),
										translator.translate(
											UIConfigDisplayLanguage.ENGLISH,
											UIText.GUI_SECONDARY_RANKING_RCR_ROW_TITLE_FINAL_SCORE
										)
									};
									for (int index = 0; index < scoreListIndividual.size(); index++) {
										final RankingScoreSingleIndividual score = scoreListIndividual.get(index);
										final TournamentPlayer tournamentPlayer = score.getTournamentPlayer();
										final Player player = tournamentPlayer.getPlayer();
										if (player != null) {
											csvContent[index + 2] = new String[] {
												Integer.toString(score.getRanking()),
												player.getPlayerFirstName(),
												player.getPlayerLastName(),
												player.getPlayerNationality().getCountryCodeAlpha3(),
												player.getLicense(),
												Integer.toString(score.getTotalScore()),
												Integer.toString(score.getTotalScore2())
											};
										} else {
											csvContent[index + 2] = new String[] {
												Integer.toString(score.getRanking()),
												tournamentPlayer.getPlayerName(translator.translate(UIText.GUI_SECONDARY_DISPLAY_SCORE_TITLE_PLAYER_EMPTY)),
												"",
												"",
												"",
												Integer.toString(score.getTotalScore()),
												Integer.toString(score.getTotalScore2())
											};
										}
									}
								}
									break;
							}
						}
							break;
						case TEAM: {
							switch (ranking.getScore()) {
								case FINAL_SCORE: {
									csvContent[1] = new String[] {
										"Ranking",
										"Team Name",
										translator.translate(
											UIConfigDisplayLanguage.ENGLISH,
											UIText.GUI_SECONDARY_RANKING_RCR_ROW_TITLE_FINAL_SCORE
										)
									};
									for (int index = 0; index < scoreListTeam.size(); index++) {
										final RankingScoreSingleTeam score = scoreListTeam.get(index);
										csvContent[index + 2] = new String[] {
											Integer.toString(score.getRanking()),
											score.getName(
												translator.translate(
													UIConfigDisplayLanguage.ENGLISH,
													UIText.GUI_SECONDARY_RANKING_TITLE_PLAYER_EMPTY
												)
											),
											Integer.toString(score.getTotalScore())
										};
									}
								}
									break;
								case GAME_SCORE: {
									csvContent[1] = new String[] {
										"Ranking",
										"Team Name",
										translator.translate(
											UIConfigDisplayLanguage.ENGLISH,
											UIText.GUI_SECONDARY_RANKING_RCR_ROW_TITLE_GAME_SCORE
										),
										translator.translate(
											UIConfigDisplayLanguage.ENGLISH,
											UIText.GUI_SECONDARY_RANKING_RCR_ROW_TITLE_FINAL_SCORE
										)
									};
									for (int index = 0; index < scoreListTeam.size(); index++) {
										final RankingScoreSingleTeam score = scoreListTeam.get(index);
										csvContent[index + 2] = new String[] {
											Integer.toString(score.getRanking()),
											score.getName(
												translator.translate(
													UIConfigDisplayLanguage.ENGLISH,
													UIText.GUI_SECONDARY_RANKING_TITLE_PLAYER_EMPTY
												)
											),
											Integer.toString(score.getTotalScore()),
											Integer.toString(score.getTotalScore2())
										};
									}
								}
									break;
								case UMA_SCORE: {
									csvContent[1] = new String[] {
										"Ranking",
										"Team Name",
										translator.translate(
											UIConfigDisplayLanguage.ENGLISH,
											UIText.GUI_SECONDARY_RANKING_RCR_ROW_TITLE_UMA_SCORE
										),
										translator.translate(
											UIConfigDisplayLanguage.ENGLISH,
											UIText.GUI_SECONDARY_RANKING_RCR_ROW_TITLE_FINAL_SCORE
										)
									};
									for (int index = 0; index < scoreListTeam.size(); index++) {
										final RankingScoreSingleTeam score = scoreListTeam.get(index);
										csvContent[index + 2] = new String[] {
											Integer.toString(score.getRanking()),
											score.getName(
												translator.translate(
													UIConfigDisplayLanguage.ENGLISH,
													UIText.GUI_SECONDARY_RANKING_TITLE_PLAYER_EMPTY
												)
											),
											Integer.toString(score.getTotalScore()),
											Integer.toString(score.getTotalScore2())
										};
									}
								}
									break;
								default:
									break;
							}
						}
							break;
					}
					if (CSVWriter.write(
						csvFile,
						csvContent,
						null
					)) {
						new UIMessageDialog(
							(JFrame) SwingUtilities.getWindowAncestor(this),
							translator.translate(UIText.GUI_SECONDARY_RANKING_EXPORT_RESULT_FILE_SAVED_MESSAGE),
							translator.translate(UIText.GUI_SECONDARY_RANKING_EXPORT_RESULT_FILE_SAVED_TITLE),
							MessageDialogMessageType.INFORMATION,
							MessageDialogOptionsType.OK,
							MessageDialogOption.OK,
							translator
						).showOptionDialog();
					} else {
						new UIMessageDialog(
							(JFrame) SwingUtilities.getWindowAncestor(this),
							translator.translate(UIText.GUI_SECONDARY_RANKING_EXPORT_RESULT_FILE_NOT_SAVED_MESSAGE),
							translator.translate(UIText.GUI_SECONDARY_RANKING_EXPORT_RESULT_FILE_NOT_SAVED_TITLE),
							MessageDialogMessageType.ERROR,
							MessageDialogOptionsType.OK,
							MessageDialogOption.OK,
							translator
						).showOptionDialog();
					}
				}
			}
		}
	}
}
