/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.score;

import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import fr.bri.awt.ProportionalGridLayoutConstraint;
import fr.bri.awt.ProportionalGridLayoutInteger;
import fr.bri.mj.data.score.aggregated.TeamMode;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;

public class UIScoreChartTitle extends JPanel {

	private static final long serialVersionUID = -6470156294087220289L;

	public UIScoreChartTitle(
		final int nbSessions,
		final UIScoreColorProfile colorProfile,
		final Font labelFont,
		final TeamMode rankingModeTeamType,
		final UITextTranslator translator
	) {

		setOpaque(false);

		final ProportionalGridLayoutInteger layout = new ProportionalGridLayoutInteger(
			1,
			nbSessions * 2 + 3,
			0,
			0
		);
		final int weightX[] = new int[nbSessions * 2 + 3];
		weightX[0] = 2;
		weightX[1] = 10;
		for (int sessionIndex = 0; sessionIndex < nbSessions; sessionIndex++) {
			weightX[sessionIndex * 2 + 2] = 3;
			weightX[sessionIndex * 2 + 3] = 1;
		}
		weightX[weightX.length - 1] = 2;
		layout.setWeightX(weightX);
		setLayout(layout);

		final ProportionalGridLayoutConstraint constraints = new ProportionalGridLayoutConstraint(
			0,
			1,
			0,
			1
		);

		constraints.y = 0;
		{
			String lableTitleScoreText = null;
			switch (rankingModeTeamType) {
				case INDIVIDUAL:
					lableTitleScoreText = translator.translate(UIText.GUI_SECONDARY_DISPLAY_SCORE_ROW_TITLE_PLAYER_NAME);
					break;
				case TEAM:
					lableTitleScoreText = translator.translate(UIText.GUI_SECONDARY_DISPLAY_SCORE_ROW_TITLE_TEAM_NAME);
					break;
				default:
					break;
			}

			constraints.x = 1;
			final JLabel labelTitleScore = new JLabel(
				lableTitleScoreText,
				SwingConstants.LEFT
			);
			labelTitleScore.setFont(labelFont);
			labelTitleScore.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
			add(
				labelTitleScore,
				constraints
			);
		}

		for (int sessionIndex = 0; sessionIndex < nbSessions; sessionIndex++) {
			constraints.x = sessionIndex * 2 + 2;
			final JLabel labelTitleTrend = new JLabel(
				Integer.toString(sessionIndex + 1),
				SwingConstants.CENTER
			);
			labelTitleTrend.setFont(labelFont);
			labelTitleTrend.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
			add(
				labelTitleTrend,
				constraints
			);
		}
	}
}
