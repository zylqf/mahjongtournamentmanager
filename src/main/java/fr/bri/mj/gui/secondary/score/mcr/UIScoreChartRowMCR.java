/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.score.mcr;

import java.awt.Font;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import fr.bri.awt.ProportionalGridLayoutConstraint;
import fr.bri.awt.ProportionalGridLayoutInteger;
import fr.bri.mj.data.score.aggregated.sessionscore.score.SessionScore;
import fr.bri.mj.data.tournament.mcr.ScoreMCR;
import fr.bri.mj.data.tournament.mcr.TournamentScoreMCR;
import fr.bri.mj.gui.secondary.score.UIScoreChartRow;
import fr.bri.mj.gui.secondary.score.UIScoreColorProfile;
import fr.bri.mj.gui.secondary.score.UIScoreMode;

public class UIScoreChartRowMCR extends UIScoreChartRow<TournamentScoreMCR, ScoreMCR> {

	private static final long serialVersionUID = -635808108399675434L;

	private static final int SCORE_DIGITS = 4;
	private static final int SCORE_GROUP_WIDTH = 10;
	private static final char SCORE_GROUP_SEPARATOR = ' ';

	public UIScoreChartRowMCR(
		final UIScoreMode<ScoreMCR> scoreMode,
		final String playerName,
		final int nbSessions,
		final int nbPlayers,
		final List<? extends SessionScore> scoreList,
		final UIScoreColorProfile colorProfile,
		final Font labelFont,
		final boolean darken
	) {

		setOpaque(false);

		final ProportionalGridLayoutInteger layout = new ProportionalGridLayoutInteger(
			1,
			nbSessions * 2 + 3,
			0,
			0
		);
		final int weightX[] = new int[nbSessions * 2 + 3];
		weightX[0] = 2;
		weightX[1] = 10;
		for (int sessionIndex = 0; sessionIndex < nbSessions; sessionIndex++) {
			weightX[sessionIndex * 2 + 2] = 3;
			weightX[sessionIndex * 2 + 3] = 1;
		}
		weightX[weightX.length - 1] = 2;
		layout.setWeightX(weightX);
		setLayout(layout);

		final ProportionalGridLayoutConstraint constraints = new ProportionalGridLayoutConstraint(
			0,
			1,
			0,
			1
		);

		constraints.y = 0;
		{
			constraints.x = 1;
			final JLabel labelPlayerName = new JLabel(
				playerName,
				SwingConstants.LEFT
			);
			labelPlayerName.setFont(labelFont);
			labelPlayerName.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
			labelPlayerName.setBackground(colorProfile.getDarkenLineBackgroundColor());
			labelPlayerName.setOpaque(darken);
			add(
				labelPlayerName,
				constraints
			);
		}

		final int nbScores = Math.min(
			nbSessions,
			scoreList.size()
		);
		for (int sessionIndex = 0; sessionIndex < nbScores; sessionIndex++) {
			final SessionScore score = scoreList.get(sessionIndex);
			JLabel labelScore = null;
			if (score.isUpToDate()) {
				String scoreText = "";
				switch (scoreMode.getScoreMode()) {
					case MATCH_POINT:
						scoreText = fraction(
							score.getScore(),
							12
						);
						break;
					case TABLE_POINT:
						scoreText = format(
							score.getScore(),
							SCORE_DIGITS,
							SCORE_GROUP_WIDTH,
							SCORE_GROUP_SEPARATOR,
							true
						);
						break;
					default:
						break;
				}
				labelScore = new JLabel(
					scoreText,
					SwingConstants.CENTER
				);

				labelScore.setFont(labelFont);
				labelScore.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
			} else {
				labelScore = new JLabel();
				labelScore.setForeground(colorProfile.getUpToDateNegativeScoreFontColor());
			}
			labelScore.setBackground(colorProfile.getDarkenLineBackgroundColor());
			labelScore.setOpaque(darken);

			constraints.x = sessionIndex * 2 + 2;
			add(
				labelScore,
				constraints
			);

			final JLabel labelSpacer = new JLabel();
			labelSpacer.setBackground(colorProfile.getDarkenLineBackgroundColor());
			labelSpacer.setOpaque(darken);
			constraints.x = sessionIndex * 2 + 3;
			add(
				labelSpacer,
				constraints
			);
		}

		for (int sessionIndex = nbScores; sessionIndex < nbSessions; sessionIndex++) {
			final JLabel labelScore = new JLabel();
			labelScore.setBackground(colorProfile.getDarkenLineBackgroundColor());
			labelScore.setOpaque(darken);
			constraints.x = sessionIndex * 2 + 2;
			add(
				labelScore,
				constraints
			);

			final JLabel labelSpacer = new JLabel();
			labelSpacer.setBackground(colorProfile.getDarkenLineBackgroundColor());
			labelSpacer.setOpaque(darken);
			constraints.x = sessionIndex * 2 + 3;
			add(
				labelSpacer,
				constraints
			);
		}
	}

	private String format(
		final int number,
		final int width,
		final int groupWidth,
		final char groupSeparator,
		final boolean sign
	) {
		final StringBuilder builder = new StringBuilder();
		if (number >= 0 && sign) {
			builder.append(' ');
		} else if (number < 0) {
			builder.append('-');
		}

		final String raw = Integer.toString(Math.abs(number));
		final int rawWidth = raw.length();
		final int nbGroupSeparator = (width - 1) / groupWidth;
		final int nbRawGroupSeparator = (rawWidth - 1) / groupWidth;
		final int paddingWidth = width - rawWidth + nbGroupSeparator - nbRawGroupSeparator;
		for (int index = 0; index < paddingWidth; index++) {
			builder.append(' ');
		}

		for (int index = 0; index < rawWidth; index++) {
			builder.append(raw.charAt(index));
			final int numberIndex = rawWidth - 1 - index;
			if (numberIndex != 0 && numberIndex % groupWidth == 0) {
				builder.append(groupSeparator);
			}
		}
		return builder.toString();
	}

	private String fraction(
		final int numerator,
		final int denominator
	) {
		final int quotient = numerator / denominator;
		final int remainder = numerator % denominator;
		if (remainder != 0) {
			int a = remainder;
			int bcd = denominator;
			int t;
			while (a != 0) {
				t = a;
				a = bcd;
				bcd = t;
				a = a % bcd;
			}
			return Integer.toString(quotient)
				+ " "
				+ Integer.toString(remainder / bcd)
				+ "/"
				+ Integer.toString(denominator / bcd);
		} else {
			return Integer.toString(quotient);
		}
	}
}
