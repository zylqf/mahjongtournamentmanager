/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.score.mcr;

import java.awt.Dimension;
import java.awt.Font;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import fr.bri.awt.ProportionalGridLayoutConstraint;
import fr.bri.awt.ProportionalGridLayoutInteger;
import fr.bri.mj.data.score.aggregated.TeamMode;
import fr.bri.mj.data.score.aggregated.sessionscore.accessor.SessionScoreAccessorMCRFactory;
import fr.bri.mj.data.score.aggregated.sessionscore.score.SessionScoreIndividual;
import fr.bri.mj.data.score.aggregated.sessionscore.score.SessionScoreTeam;
import fr.bri.mj.data.tournament.Tournament;
import fr.bri.mj.data.tournament.TournamentPlayer;
import fr.bri.mj.data.tournament.mcr.ScoreMCR;
import fr.bri.mj.data.tournament.mcr.TournamentScoreMCR;
import fr.bri.mj.dataaccess.TournamentManager;
import fr.bri.mj.gui.config.UIConfig;
import fr.bri.mj.gui.config.item.UIConfigDisplayLanguage;
import fr.bri.mj.gui.config.item.UIConfigFontSize;
import fr.bri.mj.gui.dialog.UIMessageDialog;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogMessageType;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogOption;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogOptionsType;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;
import fr.bri.mj.gui.secondary.score.UIScoreChartTitle;
import fr.bri.mj.gui.secondary.score.UIScoreColorProfile;
import fr.bri.mj.gui.secondary.score.UIScoreMode;
import fr.bri.mj.gui.secondary.score.UITabPanelScore;
import fr.bri.mj.utils.CSVWriter;

public class UITabPanelScoreMCR extends UITabPanelScore<TournamentScoreMCR, ScoreMCR> {

	private static final long serialVersionUID = -4737087942374765856L;

	public UITabPanelScoreMCR(
		final TournamentManager<TournamentScoreMCR, ScoreMCR> dataAccess,
		final UIConfig config,
		final UITextTranslator translator
	) {
		super(
			dataAccess,
			UIScoreModeMCR.getRankingModes(),
			config,
			translator
		);
	}

	@Override
	protected void displayScore() {
		final UIScoreColorProfile scoreColorProfile = getColorProfile();
		invalidate();
		panelCenterSupport.setBackground(scoreColorProfile.getTabBackgroundColor());
		panelScore.removeAll();

		final int selectedTournamentIndex = comboTournament.getSelectedIndex();
		final int selectedSessionIndex = comboSession.getSelectedIndex();
		if (selectedTournamentIndex != -1 && selectedSessionIndex != -1) {
			final Tournament<TournamentScoreMCR, ScoreMCR> tournament = listTournament.get(selectedTournamentIndex);
			final int nbSessions = tournament.getNbSessions();
			final UIScoreMode<ScoreMCR> scoreMode = scoreModes.get(comboMode.getSelectedIndex());
			final int session = comboSession.getSelectedIndex() + 1;
			switch (scoreMode.getScoreTeamType()) {
				case INDIVIDUAL: {
					mapPlayerToListScoreIndividual.clear();
					{
						final List<SessionScoreIndividual> scoreListIndividual = SessionScoreAccessorMCRFactory.getScoreAccessorMCR(scoreMode.getScoreMode()).getScoreIndividual(
							tournament,
							1
						);
						for (final SessionScoreIndividual score : scoreListIndividual) {
							final List<SessionScoreIndividual> playerScoreIndividual = new ArrayList<SessionScoreIndividual>();
							playerScoreIndividual.add(score);
							mapPlayerToListScoreIndividual.put(
								score.getTournamentPlayer(),
								playerScoreIndividual
							);
						}
					}

					for (int sessionIndex = 2; sessionIndex <= session; sessionIndex++) {
						final List<SessionScoreIndividual> scoreListIndividual = SessionScoreAccessorMCRFactory.getScoreAccessorMCR(scoreMode.getScoreMode()).getScoreIndividual(
							tournament,
							sessionIndex
						);
						for (final SessionScoreIndividual score : scoreListIndividual) {
							mapPlayerToListScoreIndividual.get(score.getTournamentPlayer()).add(score);
						}
					}
					final int nbPlayers = mapPlayerToListScoreIndividual.size();

					final Font labelFont = (CUSTOM_FONT != null
						? CUSTOM_FONT
						: getFont()).deriveFont(
							Font.PLAIN,
							getFontSize(UIConfigFontSize.SMALL)
						);

					final ProportionalGridLayoutInteger layout = new ProportionalGridLayoutInteger(
						nbPlayers + 1,
						1,
						0,
						2
					);
					panelScore.setLayout(layout);

					final ProportionalGridLayoutConstraint constraint = new ProportionalGridLayoutConstraint(
						0,
						1,
						0,
						1
					);

					final Dimension rowSize = new Dimension(
						0,
						24
					);

					constraint.x = 0;

					final UIScoreChartTitle title = new UIScoreChartTitle(
						nbSessions,
						scoreColorProfile,
						labelFont,
						TeamMode.INDIVIDUAL,
						translator
					);
					title.setPreferredSize(rowSize);
					constraint.y = 0;
					panelScore.add(
						title,
						constraint
					);

					int rowIndex = 1;
					boolean darken = false;
					for (final TournamentPlayer player : tournament.getTournamentPlayers()) {
						darken = !darken;
						final UIScoreChartRowMCR row = new UIScoreChartRowMCR(
							scoreMode,
							player.getPlayerName(translator.translate(UIText.GUI_SECONDARY_DISPLAY_SCORE_TITLE_PLAYER_EMPTY)),
							nbSessions,
							nbPlayers,
							mapPlayerToListScoreIndividual.get(player),
							scoreColorProfile,
							labelFont,
							darken
						);
						row.setPreferredSize(rowSize);

						constraint.y = rowIndex;
						panelScore.add(
							row,
							constraint
						);
						rowIndex++;
					}
					break;
				}
				case TEAM: {
					mapTeamToListScoreTeam.clear();
					{
						final List<SessionScoreTeam> scoreListTeam = SessionScoreAccessorMCRFactory.getScoreAccessorMCR(scoreMode.getScoreMode()).getScoreTeam(
							tournament,
							1
						);
						setScoreTeamOrder.clear();
						for (final SessionScoreTeam teamScore : scoreListTeam) {
							final String teamName = teamScore.getName(translator.translate(UIText.GUI_SECONDARY_DISPLAY_SCORE_TITLE_PLAYER_EMPTY));
							setScoreTeamOrder.add(teamName);
							final List<SessionScoreTeam> playerScoreTeam = new ArrayList<SessionScoreTeam>();
							playerScoreTeam.add(teamScore);
							mapTeamToListScoreTeam.put(
								teamName,
								playerScoreTeam
							);
						}
					}

					for (int sessionIndex = 2; sessionIndex <= session; sessionIndex++) {
						final List<SessionScoreTeam> scoreListTeam = SessionScoreAccessorMCRFactory.getScoreAccessorMCR(scoreMode.getScoreMode()).getScoreTeam(
							tournament,
							sessionIndex
						);
						for (final SessionScoreTeam teamScore : scoreListTeam) {
							mapTeamToListScoreTeam.get(teamScore.getName(translator.translate(UIText.GUI_SECONDARY_DISPLAY_SCORE_TITLE_PLAYER_EMPTY))).add(teamScore);
						}
					}
					final int nbTeams = mapTeamToListScoreTeam.size();

					final Font labelFont = (CUSTOM_FONT != null
						? CUSTOM_FONT
						: getFont()).deriveFont(
							Font.PLAIN,
							getFontSize(UIConfigFontSize.SMALL)
						);

					final ProportionalGridLayoutInteger layout = new ProportionalGridLayoutInteger(
						nbTeams + 1,
						1,
						0,
						2
					);
					panelScore.setLayout(layout);

					final ProportionalGridLayoutConstraint constraint = new ProportionalGridLayoutConstraint(
						0,
						1,
						0,
						1
					);

					constraint.x = 0;

					final UIScoreChartTitle title = new UIScoreChartTitle(
						nbSessions,
						scoreColorProfile,
						labelFont,
						TeamMode.TEAM,
						translator
					);
					constraint.y = 0;
					panelScore.add(
						title,
						constraint
					);

					final Dimension rowSize = new Dimension(
						0,
						24
					);

					int rowIndex = 1;
					boolean darken = false;
					for (final String team : setScoreTeamOrder) {
						darken = !darken;
						final UIScoreChartRowMCR row = new UIScoreChartRowMCR(
							scoreMode,
							team,
							nbSessions,
							nbTeams,
							mapTeamToListScoreTeam.get(team),
							scoreColorProfile,
							labelFont,
							darken
						);
						row.setPreferredSize(rowSize);

						constraint.y = rowIndex;
						panelScore.add(
							row,
							constraint
						);
						rowIndex++;
					}

					break;
				}
			}
		}
		validate();
		scrollScore.getVerticalScrollBar().setValue(scrollScore.getVerticalScrollBar().getModel().getMinimum());
		repaint();
	}

	@Override
	protected void exportCSV() {
		final int selectedTournamentIndex = comboTournament.getSelectedIndex();
		final int selectedSessionIndex = comboSession.getSelectedIndex();
		if (selectedTournamentIndex != -1 && selectedSessionIndex != -1) {
			final UIScoreMode<ScoreMCR> scoreMode = scoreModes.get(comboMode.getSelectedIndex());
			final Tournament<TournamentScoreMCR, ScoreMCR> tournament = listTournament.get(selectedTournamentIndex);
			final int session = comboSession.getSelectedIndex() + 1;
			if (mapPlayerToListScoreIndividual != null && mapPlayerToListScoreIndividual.size() > 0 || mapTeamToListScoreTeam != null && mapTeamToListScoreTeam.size() > 0) {
				final JFileChooser csvFileChooser = new JFileChooser();
				csvFileChooser.setMultiSelectionEnabled(false);
				csvFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				final String saveFileName = regulateString(tournament.getName())
					+ "_Score_"
					+ regulateString(
						translator.translate(
							UIConfigDisplayLanguage.ENGLISH,
							scoreMode.getUIText()
						)
					)
					+ "_Session_"
					+ Integer.toString(session)
					+ ".csv";
				if (lastChoosenFolder != null) {
					csvFileChooser.setSelectedFile(
						new File(
							lastChoosenFolder,
							saveFileName
						)
					);
				} else {
					csvFileChooser.setSelectedFile(new File(saveFileName));
				}
				File csvFile = null;
				boolean toContinue = true;
				while (toContinue) {
					final int fileChooserAnswer = csvFileChooser.showSaveDialog(this);
					if (fileChooserAnswer == JFileChooser.APPROVE_OPTION) {
						csvFile = csvFileChooser.getSelectedFile();
						if (csvFile.exists()) {
							final MessageDialogOption overwriteAnswer = new UIMessageDialog(
								(JFrame) SwingUtilities.getWindowAncestor(this),
								translator.translate(UIText.GUI_SECONDARY_DISPLAY_SCORE_EXPORT_OVERWRITE_CONFIRM_MESSAGE),
								translator.translate(UIText.GUI_SECONDARY_DISPLAY_SCORE_EXPORT_OVERWRITE_CONFIRM_TITLE),
								MessageDialogMessageType.WARNING,
								MessageDialogOptionsType.YES_NO_CANCEL,
								MessageDialogOption.NO,
								translator
							).showOptionDialog();
							switch (overwriteAnswer) {
								case YES:
									toContinue = false;
									break;
								case NO:
									csvFile = null;
									break;
								case CANCEL:
								case CLOSED:
									csvFile = null;
									toContinue = false;
									break;
								default:
									break;
							}
						} else {
							toContinue = false;
						}
					} else {
						csvFile = null;
						toContinue = false;
					}
				}

				if (csvFile != null) {
					lastChoosenFolder = csvFileChooser.getCurrentDirectory();

					final String csvContent[][] = new String[tournament.getTournamentPlayers().size() + 2][];
					csvContent[0] = new String[] {
						"Tournament",
						tournament.getName(),
						"Score",
						translator.translate(
							UIConfigDisplayLanguage.ENGLISH,
							scoreMode.getUIText()
						),
						"Session",
						Integer.toString(session)
					};

					csvContent[1] = new String[tournament.getNbSessions() + 2];
					csvContent[1][0] = "ID";
					csvContent[1][1] = "Name";
					for (int sessionIndex = 0; sessionIndex < session; sessionIndex++) {
						csvContent[1][sessionIndex + 2] = "Session "
							+ Integer.toString(sessionIndex + 1);
					}

					switch (scoreMode.getScoreTeamType()) {
						case INDIVIDUAL: {
							switch (scoreMode.getScoreMode()) {
								case MATCH_POINT: {
									int playerIndex = 0;
									for (final TournamentPlayer player : tournament.getTournamentPlayers()) {
										csvContent[playerIndex + 2] = new String[tournament.getNbSessions() + 2];
										csvContent[playerIndex + 2][0] = Integer.toString(player.getTournamentPlayerId());
										csvContent[playerIndex + 2][1] = player.getPlayerName(translator.translate(UIText.GUI_SECONDARY_DISPLAY_SCORE_TITLE_PLAYER_EMPTY));
										final List<SessionScoreIndividual> scoreList = mapPlayerToListScoreIndividual.get(player);
										for (int scoreIndex = 0; scoreIndex < scoreList.size(); scoreIndex++) {
											final SessionScoreIndividual score = scoreList.get(scoreIndex);
											csvContent[playerIndex + 2][scoreIndex + 2] = fraction(
												score.getScore(),
												12
											);
										}
										for (int scoreIndex = scoreList.size(); scoreIndex < tournament.getNbSessions(); scoreIndex++) {
											csvContent[playerIndex + 2][scoreIndex + 2] = "";
										}
										playerIndex++;
									}
								}
									break;
								case TABLE_POINT: {
									int playerIndex = 0;
									for (final TournamentPlayer player : tournament.getTournamentPlayers()) {
										csvContent[playerIndex + 2] = new String[tournament.getNbSessions() + 1];
										csvContent[playerIndex + 2][0] = Integer.toString(player.getTournamentPlayerId());
										csvContent[playerIndex + 2][1] = player.getPlayerName(translator.translate(UIText.GUI_SECONDARY_DISPLAY_SCORE_TITLE_PLAYER_EMPTY));
										final List<SessionScoreIndividual> scoreList = mapPlayerToListScoreIndividual.get(player);
										for (int scoreIndex = 0; scoreIndex < scoreList.size(); scoreIndex++) {
											final SessionScoreIndividual score = scoreList.get(scoreIndex);
											csvContent[playerIndex + 2][scoreIndex + 2] = Integer.toString(score.getScore());
										}
										for (int scoreIndex = scoreList.size(); scoreIndex < tournament.getNbSessions(); scoreIndex++) {
											csvContent[playerIndex + 2][scoreIndex + 2] = "";
										}
										playerIndex++;
									}
								}
									break;
							}
						}
							break;
						case TEAM: {
							switch (scoreMode.getScoreMode()) {
								case MATCH_POINT: {
									int teamIndex = 0;
									for (final String name : setScoreTeamOrder) {
										csvContent[teamIndex + 2] = new String[tournament.getNbSessions() + 1];
										csvContent[teamIndex + 2][0] = Integer.toString(teamIndex + 1);
										csvContent[teamIndex + 2][1] = name;
										final List<SessionScoreTeam> scoreList = mapTeamToListScoreTeam.get(name);
										for (int scoreIndex = 0; scoreIndex < scoreList.size(); scoreIndex++) {
											final SessionScoreTeam score = scoreList.get(scoreIndex);
											csvContent[teamIndex + 2][scoreIndex + 2] = fraction(
												score.getScore(),
												12
											);
										}
										for (int scoreIndex = scoreList.size(); scoreIndex < tournament.getNbSessions(); scoreIndex++) {
											csvContent[teamIndex + 2][scoreIndex + 2] = "";
										}
										teamIndex++;
									}
								}
									break;
								case TABLE_POINT: {
									int teamIndex = 0;
									for (final String name : setScoreTeamOrder) {
										csvContent[teamIndex + 2] = new String[tournament.getNbSessions() + 1];
										csvContent[teamIndex + 2][0] = Integer.toString(teamIndex + 1);
										csvContent[teamIndex + 2][1] = name;
										final List<SessionScoreTeam> scoreList = mapTeamToListScoreTeam.get(name);
										for (int scoreIndex = 0; scoreIndex < scoreList.size(); scoreIndex++) {
											final SessionScoreTeam score = scoreList.get(scoreIndex);
											csvContent[teamIndex + 2][scoreIndex + 2] = Integer.toString(score.getScore());
										}
										for (int scoreIndex = scoreList.size(); scoreIndex < tournament.getNbSessions(); scoreIndex++) {
											csvContent[teamIndex + 2][scoreIndex + 2] = "";
										}
										teamIndex++;
									}
								}
									break;
							}
						}
					}

					if (CSVWriter.write(
						csvFile,
						csvContent,
						null
					)) {
						new UIMessageDialog(
							(JFrame) SwingUtilities.getWindowAncestor(this),
							translator.translate(UIText.GUI_SECONDARY_DISPLAY_SCORE_EXPORT_RESULT_FILE_SAVED_MESSAGE),
							translator.translate(UIText.GUI_SECONDARY_DISPLAY_SCORE_EXPORT_RESULT_FILE_SAVED_TITLE),
							MessageDialogMessageType.INFORMATION,
							MessageDialogOptionsType.OK,
							MessageDialogOption.OK,
							translator
						).showOptionDialog();
					} else {
						new UIMessageDialog(
							(JFrame) SwingUtilities.getWindowAncestor(this),
							translator.translate(UIText.GUI_SECONDARY_DISPLAY_SCORE_EXPORT_RESULT_FILE_NOT_SAVED_MESSAGE),
							translator.translate(UIText.GUI_SECONDARY_DISPLAY_SCORE_EXPORT_RESULT_FILE_NOT_SAVED_TITLE),
							MessageDialogMessageType.ERROR,
							MessageDialogOptionsType.OK,
							MessageDialogOption.OK,
							translator
						).showOptionDialog();
					}
				}
			}
		}
	}
}
