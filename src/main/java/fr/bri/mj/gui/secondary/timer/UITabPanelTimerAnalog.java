/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.timer;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;

import fr.bri.awt.ProportionalGridLayoutConstraint;
import fr.bri.awt.ProportionalGridLayoutInteger;
import fr.bri.mj.gui.config.UIConfig;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;
import fr.bri.swing.JCountDownAnalogClock;

public class UITabPanelTimerAnalog extends UITabPanelTimer {

	private static final long serialVersionUID = -5828157263757341171L;

	private static final int NB_TICKS_PER_SECOND = 20;
	private static final long NB_NANOSECONDS_IN_ONE_MILLISECOND = NB_NANOSECONDS_IN_ONE_SECOND / NB_MILLISECONDS_IN_ONE_SECOND;
	private static final int NB_MILLISECONDS_IN_ONE_TICK = NB_MILLISECONDS_IN_ONE_SECOND / NB_TICKS_PER_SECOND;
	private static final int CLOCK_SLEEP_INTERVAL_IN_MILLISECONDS = NB_MILLISECONDS_IN_ONE_SECOND / NB_TICKS_PER_SECOND / 5;

	private final JPanel panelCenter;
	private final JCountDownAnalogClock clock;

	private volatile UITimerAnalogColorProfile timerColorProfile;

	public UITabPanelTimerAnalog(
		final UIConfig config,
		final UITextTranslator translator
	) {
		super(
			config,
			translator
		);

		final int primaryMinute;
		switch (config.getRuleSet()) {
			case MCR:
				primaryMinute = DEFAULT_TIMER_MCR;
				break;
			case RCR:
				primaryMinute = DEFAULT_TIMER_RCR;
				break;
			default:
				primaryMinute = DEFAULT_TIMER_OTHER;
				break;
		}

		setLayout(new BorderLayout());
		{
			final JPanel panelNorth = new JPanel();
			final ProportionalGridLayoutInteger layout = new ProportionalGridLayoutInteger(
				1,
				5,
				4,
				4
			);
			layout.setWeightX(
				7,
				1,
				1,
				1,
				7
			);
			panelNorth.setLayout(layout);
			add(
				panelNorth,
				BorderLayout.NORTH
			);

			final ProportionalGridLayoutConstraint constraint = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);
			constraint.x = 1;
			panelNorth.add(
				new JLabel(
					translator.translate(UIText.GUI_SECONDARY_TIMER_TITLE_TIMER),
					SwingConstants.RIGHT
				),
				constraint
			);
			constraint.x = 2;
			spinnerMinute = new JSpinner(
				new SpinnerNumberModel(
					primaryMinute,
					1,
					150,
					1
				)
			);
			panelNorth.add(
				spinnerMinute,
				constraint
			);
			constraint.x = 3;
			panelNorth.add(
				new JLabel(
					translator.translate(UIText.GUI_SECONDARY_TIMER_TITLE_MINUTE),
					SwingConstants.LEFT
				),
				constraint
			);
		}

		{
			panelCenter = new JPanel();
			panelCenter.setBorder(
				BorderFactory.createEmptyBorder(
					32,
					32,
					32,
					32
				)
			);
			panelCenter.setOpaque(true);
			final ProportionalGridLayoutInteger layout = new ProportionalGridLayoutInteger(
				1,
				1,
				32,
				32
			);
			panelCenter.setLayout(layout);
			final ProportionalGridLayoutConstraint constraint = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			switch (config.getTimerDisplayMode()) {
				case HOUR_MINUTE_SECOND: {
					final int nbHours = primaryMinute / 60;
					int nbHourDigits = 0;
					if (nbHours > 0) {
						nbHourDigits = (int) (Math.floor(Math.log10(nbHours)) + 1);
					}
					if (nbHourDigits < 2) {
						nbHourDigits = 2;
					}
					final int maxHourNumber = (int) (Math.pow(
						10,
						nbHourDigits
					) - 1);
					clock = new JCountDownAnalogClock(
						maxHourNumber,
						60,
						60
					);
				}
					break;
				case MINUTE_SECOND: {
					int nbMinuteDigits = 0;
					if (primaryMinute > 0) {
						nbMinuteDigits = (int) (Math.floor(Math.log10(primaryMinute)) + 1);
					}
					if (nbMinuteDigits < 2) {
						nbMinuteDigits = 2;
					}
					final int maxMinuteNumber = (int) (Math.pow(
						10,
						nbMinuteDigits
					) - 1);
					clock = new JCountDownAnalogClock(
						maxMinuteNumber,
						60
					);
				}
					break;
				default: {
					clock = new JCountDownAnalogClock(
						99,
						60,
						60
					);
				}
					break;
			}
			panelCenter.add(
				clock,
				constraint
			);

			add(
				panelCenter,
				BorderLayout.CENTER
			);
		}

		{
			final JPanel panelSouth = new JPanel(
				new ProportionalGridLayoutInteger(
					1,
					7,
					0,
					0
				)
			);
			add(
				panelSouth,
				BorderLayout.SOUTH
			);
			final ProportionalGridLayoutConstraint constraints = new ProportionalGridLayoutConstraint();

			constraints.x = 1;
			buttonStart = new JButton(translator.translate(UIText.GUI_SECONDARY_TIMER_BUTTON_START));
			buttonStart.setEnabled(false);
			panelSouth.add(
				buttonStart,
				constraints
			);

			constraints.x = 3;
			buttonPause = new JButton(translator.translate(UIText.GUI_SECONDARY_TIMER_BUTTON_PAUSE));
			buttonPause.setEnabled(false);
			panelSouth.add(
				buttonPause,
				constraints
			);

			constraints.x = 5;
			buttonSet = new JButton(translator.translate(UIText.GUI_SECONDARY_TIMER_BUTTON_SET));
			panelSouth.add(
				buttonSet,
				constraints
			);
		}
		threadTimer = null;

		buttonStart.addActionListener((final ActionEvent e) -> {
			startTimer();
		});
		buttonPause.addActionListener((final ActionEvent e) -> {
			pauseTimer();
		});
		buttonSet.addActionListener((final ActionEvent e) -> {
			setTimer();
		});

		refresh(false);
	}

	@Override
	public void refresh(final boolean hard) {
		switch (config.getColorProfile()) {
			case WHITE_ON_BLACK:
				timerColorProfile = UITimerAnalogColorProfile.PROFILE_WHITE_ON_BLACK;
				break;
			case BLACK_ON_WHITE:
				timerColorProfile = UITimerAnalogColorProfile.PROFILE_BLACK_ON_WHITE;
				break;
		}

		final int lastRemainingSecond = (int) ((remainingNanoSeconds + NB_NANOSECONDS_IN_ONE_SECOND - 1) / NB_NANOSECONDS_IN_ONE_SECOND);
		panelCenter.setBackground(timerColorProfile.getClockPanelBGColor());
		clock.setBackground(timerColorProfile.getClockPanelBGColor());
		clock.setContourColor(timerColorProfile.getContourColor());
		clock.setHandColor(timerColorProfile.getHandColor());
		clock.setFaceColors(
			timerColorProfile.getFaceColors(
				lastRemainingSecond,
				threadTimer != null
			)
		);
		clock.setStrokeFGColor(timerColorProfile.getStrokeFGColor());
		timerVoiceProfile = UITimerVoiceProfile.getProfile(
			config.getVoiceLanguage(),
			config.getVoiceGender()
		);
	}

	@Override
	protected void setTimer() {
		if (threadTimer == null) {
			final int minute = (int) spinnerMinute.getValue();
			final int remainingSeconds = minute * NB_SECONDS_IN_ONE_MINUTE;
			remainingNanoSeconds = remainingSeconds * NB_NANOSECONDS_IN_ONE_SECOND;

			clock.setSeconds(remainingSeconds);
			clock.setFaceColors(
				timerColorProfile.getFaceColors(
					remainingSeconds,
					false
				)
			);
			if (remainingNanoSeconds > 0) {
				buttonStart.setEnabled(true);
			} else {
				buttonStart.setEnabled(false);
			}
			timerSet = true;
		}
	}

	@Override
	protected void timerRun(final boolean startingGongSound) {
		long lastNanoSecond = System.nanoTime();
		long currentNanoSecond;
		int lastRemainingSeconds = (int) ((remainingNanoSeconds + NB_NANOSECONDS_IN_ONE_SECOND - 1) / NB_NANOSECONDS_IN_ONE_SECOND);
		int currentRemainingSeconds;
		int currentRemainingMilliseconds = (int) ((remainingNanoSeconds + NB_NANOSECONDS_IN_ONE_MILLISECOND - 1) / NB_NANOSECONDS_IN_ONE_MILLISECOND);
		int currentRemainingTickMilliseconds;
		int lastRemainingTickMilliseconds = (currentRemainingMilliseconds + NB_MILLISECONDS_IN_ONE_TICK - 1) / NB_MILLISECONDS_IN_ONE_TICK * NB_MILLISECONDS_IN_ONE_TICK;
		boolean fifteenMinutesRemainingWarningSet = lastRemainingSeconds <= NB_SECONDS_IN_FIFTEEN_MINUTES;
		boolean fiveMinutesRemainingWarningSet = lastRemainingSeconds <= NB_SECONDS_IN_FIVE_MINUTES;

		if (startingGongSound) {
			soundPlayerRun(timerVoiceProfile.getUlrGong());
		}
		clock.setFaceColors(
			timerColorProfile.getFaceColors(
				lastRemainingSeconds,
				true
			)
		);
		while (!toStop && remainingNanoSeconds > 0) {
			try {
				Thread.sleep(CLOCK_SLEEP_INTERVAL_IN_MILLISECONDS);
			} catch (final InterruptedException e) {
			}

			currentNanoSecond = System.nanoTime();
			remainingNanoSeconds -= currentNanoSecond - lastNanoSecond;
			lastNanoSecond = currentNanoSecond;

			currentRemainingSeconds = (int) ((remainingNanoSeconds + NB_NANOSECONDS_IN_ONE_SECOND - 1) / NB_NANOSECONDS_IN_ONE_SECOND);
			currentRemainingMilliseconds = (int) ((remainingNanoSeconds + NB_NANOSECONDS_IN_ONE_MILLISECOND - 1) / NB_NANOSECONDS_IN_ONE_MILLISECOND);
			currentRemainingTickMilliseconds = (currentRemainingMilliseconds + NB_MILLISECONDS_IN_ONE_TICK - 1) / NB_MILLISECONDS_IN_ONE_TICK * NB_MILLISECONDS_IN_ONE_TICK;

			if (currentRemainingTickMilliseconds < lastRemainingTickMilliseconds) {
				clock.setMilliseconds(currentRemainingTickMilliseconds);
				lastRemainingTickMilliseconds = currentRemainingTickMilliseconds;
			}

			if (currentRemainingSeconds < lastRemainingSeconds) {
				clock.setFaceColors(
					timerColorProfile.getFaceColors(
						currentRemainingSeconds,
						true
					)
				);

				if (!fifteenMinutesRemainingWarningSet && lastRemainingSeconds <= NB_SECONDS_IN_FIFTEEN_MINUTES) {
					fifteenMinutesRemainingWarningSet = true;
					soundPlayerRun(timerVoiceProfile.getUrlVoiceFifteenMinutes());
				}

				if (!fiveMinutesRemainingWarningSet && lastRemainingSeconds <= NB_SECONDS_IN_FIVE_MINUTES) {
					fiveMinutesRemainingWarningSet = true;
					soundPlayerRun(timerVoiceProfile.getUrlVoiceFiveMinutes());
				}
				lastRemainingSeconds = currentRemainingSeconds;
			}
		}

		if (remainingNanoSeconds <= 0) {
			soundPlayerRun(timerVoiceProfile.getUlrGong());
		} else {
			buttonStart.setEnabled(true);
		}

		buttonPause.setEnabled(false);
		buttonSet.setEnabled(true);
		threadTimer = null;
	}
}
