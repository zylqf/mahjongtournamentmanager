/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.timer;

import fr.bri.mj.gui.config.item.UIConfigVoiceGender;
import fr.bri.mj.gui.config.item.UIConfigVoiceLanguage;

public class UITimerVoiceProfile {

	private static final String FIFTEEN_MINUTES_SOUND_URL = "fr/bri/mj/sound/fifteenminutes";
	private static final String FIVE_MINUTES_SOUND_URL = "fr/bri/mj/sound/fiveminutes";
	private static final String EXTENTION = ".wav";
	private static final String SEPARATOR = "_";
	private static final String GONG_SOUND_URL = "fr/bri/mj/sound/gong.wav";

	private final String urlVoiceFifteenMinutes;
	private final String urlVoiceFiveMinutes;
	private final String ulrGong;

	public static UITimerVoiceProfile getProfile(
		final UIConfigVoiceLanguage language,
		final UIConfigVoiceGender gender
	) {
		return new UITimerVoiceProfile(
			FIFTEEN_MINUTES_SOUND_URL + SEPARATOR + getLanguageString(language) + SEPARATOR + getGenderString(gender) + EXTENTION,
			FIVE_MINUTES_SOUND_URL + SEPARATOR + getLanguageString(language) + SEPARATOR + getGenderString(gender) + EXTENTION,
			GONG_SOUND_URL
		);
	}

	private static String getLanguageString(final UIConfigVoiceLanguage language) {
		switch (language) {
			case ENGLISH:
				return "en";
			case FRENCH:
				return "fr";
			case JAPANESE:
				return "jp";
			default:
				return "en";
		}
	}

	private static String getGenderString(final UIConfigVoiceGender gender) {
		switch (gender) {
			case MALE:
				return "male";
			case FEMALE:
				return "female";
			default:
				return "male";
		}
	}

	private UITimerVoiceProfile(
		final String urlVoiceFifteenMinutes,
		final String urlVoiceFiveMinutes,
		final String ulrGong
	) {
		this.urlVoiceFifteenMinutes = urlVoiceFifteenMinutes;
		this.urlVoiceFiveMinutes = urlVoiceFiveMinutes;
		this.ulrGong = ulrGong;
	}

	public String getUrlVoiceFifteenMinutes() {
		return urlVoiceFifteenMinutes;
	}

	public String getUrlVoiceFiveMinutes() {
		return urlVoiceFiveMinutes;
	}

	public String getUlrGong() {
		return ulrGong;
	}

}
