/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.trend.mcr;

import java.awt.Dimension;
import java.awt.Font;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import fr.bri.awt.ProportionalGridLayoutConstraint;
import fr.bri.awt.ProportionalGridLayoutInteger;
import fr.bri.mj.data.score.aggregated.QualificationMode;
import fr.bri.mj.data.score.aggregated.TeamMode;
import fr.bri.mj.data.score.aggregated.ranking.calculator.single.RankingCalculatorSingleMCRFactory;
import fr.bri.mj.data.score.aggregated.ranking.rankingscore.single.RankingScoreSingleIndividual;
import fr.bri.mj.data.score.aggregated.ranking.rankingscore.single.RankingScoreSingleTeam;
import fr.bri.mj.data.tournament.Tournament;
import fr.bri.mj.data.tournament.TournamentPlayer;
import fr.bri.mj.data.tournament.mcr.ScoreMCR;
import fr.bri.mj.data.tournament.mcr.TournamentScoreMCR;
import fr.bri.mj.dataaccess.TournamentManager;
import fr.bri.mj.gui.config.UIConfig;
import fr.bri.mj.gui.config.item.UIConfigDisplayLanguage;
import fr.bri.mj.gui.config.item.UIConfigFontSize;
import fr.bri.mj.gui.dialog.UIMessageDialog;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogMessageType;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogOption;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogOptionsType;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;
import fr.bri.mj.gui.secondary.ranking.UIRankingMode;
import fr.bri.mj.gui.secondary.ranking.mcr.UIRankingModeMCR;
import fr.bri.mj.gui.secondary.trend.UITabPanelTrend;
import fr.bri.mj.gui.secondary.trend.UITrendChartTitle;
import fr.bri.mj.gui.secondary.trend.UITrendColorProfile;
import fr.bri.mj.gui.secondary.trend.UITrendMode;
import fr.bri.mj.utils.CSVWriter;

public class UITabPanelTrendMCR extends UITabPanelTrend<TournamentScoreMCR, ScoreMCR> {

	private static final long serialVersionUID = -8659454519679810354L;

	public UITabPanelTrendMCR(
		final TournamentManager<TournamentScoreMCR, ScoreMCR> dataAccess,
		final UIConfig config,
		final UITextTranslator translator
	) {
		super(
			dataAccess,
			RankingCalculatorSingleMCRFactory.getInstance(),
			UIRankingModeMCR.getRankingModes(
				null,
				QualificationMode.WITH
			),
			UIRankingModeMCR.getRankingModes(
				null,
				QualificationMode.WITHOUT
			),
			config,
			translator
		);
	}

	@Override
	protected void displayTrend() {
		final UITrendColorProfile trendColorProfile = getColorProfile();
		invalidate();
		panelCenterSupport.setBackground(trendColorProfile.getTabBackgroundColor());
		panelTrend.removeAll();

		final int selectedTournamentIndex = comboTournament.getSelectedIndex();
		final int selectedSessionIndex = comboSession.getSelectedIndex();
		if (selectedTournamentIndex != -1 && selectedSessionIndex != -1) {
			final Tournament<TournamentScoreMCR, ScoreMCR> tournament = listTournament.get(selectedTournamentIndex);
			final int nbSessions = tournament.getNbSessions();
			final UITrendMode trendMode = trendModes.get(comboTrendMode.getSelectedIndex());
			final UIRankingMode<ScoreMCR> ranking = rankingModes.get(comboRankingMode.getSelectedIndex());
			final int session = comboSession.getSelectedIndex() + 1;
			switch (ranking.getRankingModeTeamMode()) {
				case INDIVIDUAL: {
					mapPlayerToListScoreIndividual.clear();
					{
						final List<RankingScoreSingleIndividual> scoreListIndividual = rankingCalculatorSingleFactory.getRankingCalculator(
							ranking.getScore(),
							ranking.getQualificationMode()
						).getRankingScoreIndividual(
							tournament,
							1
						);
						listScoreIndividualOrder.clear();
						listScoreIndividualOrder.addAll(scoreListIndividual);
						for (final RankingScoreSingleIndividual rankingScore : scoreListIndividual) {
							final List<RankingScoreSingleIndividual> playerScoreIndividual = new ArrayList<RankingScoreSingleIndividual>();
							playerScoreIndividual.add(rankingScore);
							mapPlayerToListScoreIndividual.put(
								rankingScore.getTournamentPlayer(),
								playerScoreIndividual
							);
						}
					}

					for (int sessionIndex = 2; sessionIndex <= session; sessionIndex++) {
						final List<RankingScoreSingleIndividual> scoreListIndividual = rankingCalculatorSingleFactory.getRankingCalculator(
							ranking.getScore(),
							ranking.getQualificationMode()
						).getRankingScoreIndividual(
							tournament,
							sessionIndex
						);
						listScoreIndividualOrder.clear();
						listScoreIndividualOrder.addAll(scoreListIndividual);
						for (final RankingScoreSingleIndividual rankingScore : scoreListIndividual) {
							mapPlayerToListScoreIndividual.get(rankingScore.getTournamentPlayer()).add(rankingScore);
						}
					}
					final int nbPlayers = mapPlayerToListScoreIndividual.size();

					final Font labelFont = (CUSTOM_FONT != null
						? CUSTOM_FONT
						: getFont()).deriveFont(
							Font.PLAIN,
							getFontSize(UIConfigFontSize.SMALL)
						);

					final ProportionalGridLayoutInteger layout = new ProportionalGridLayoutInteger(
						nbPlayers + 1,
						1,
						0,
						2
					);
					panelTrend.setLayout(layout);

					final ProportionalGridLayoutConstraint constraint = new ProportionalGridLayoutConstraint(
						0,
						1,
						0,
						1
					);

					final Dimension rowSize = new Dimension(
						0,
						24
					);

					constraint.x = 0;

					final UITrendChartTitle title = new UITrendChartTitle(
						nbSessions,
						trendColorProfile,
						labelFont,
						TeamMode.INDIVIDUAL,
						translator
					);
					title.setPreferredSize(rowSize);
					constraint.y = 0;
					panelTrend.add(
						title,
						constraint
					);

					final List<Integer> averages = new ArrayList<Integer>(session);
					switch (trendMode) {
						case SCORE:
							switch (ranking.getScore()) {
								case MATCH_POINT:
									for (int sessionIndex = 1; sessionIndex <= session; sessionIndex++) {
										averages.add(21 * sessionIndex);
									}
									break;
								case TABLE_POINT:
									for (int sessionIndex = 1; sessionIndex <= session; sessionIndex++) {
										averages.add(0);
									}
									break;
								default:
									break;
							}
							break;
						case RANKING:
							for (int sessionIndex = 1; sessionIndex <= session; sessionIndex++) {
								averages.add(nbPlayers / 2);
							}
							break;
						default:
							break;
					}

					for (int index = 0; index < listScoreIndividualOrder.size(); index++) {
						final TournamentPlayer player = listScoreIndividualOrder.get(index).getTournamentPlayer();
						final boolean darken = index % 2 == 0;
						final UITrendChartRowMCR row = new UITrendChartRowMCR(
							trendMode,
							ranking,
							player.getPlayerName(translator.translate(UIText.GUI_SECONDARY_TREND_TITLE_PLAYER_EMPTY)),
							nbSessions,
							nbPlayers,
							mapPlayerToListScoreIndividual.get(player),
							averages,
							trendColorProfile,
							labelFont,
							darken
						);
						row.setPreferredSize(rowSize);

						constraint.y = index + 1;
						panelTrend.add(
							row,
							constraint
						);
					}

					break;
				}
				case TEAM: {
					mapTeamToListScoreTeam.clear();
					{
						final List<RankingScoreSingleTeam> scoreListTeam = rankingCalculatorSingleFactory.getRankingCalculator(
							ranking.getScore(),
							ranking.getQualificationMode()
						).getRankingScoreTeam(
							tournament,
							1
						);
						listScoreTeamOrder.clear();
						listScoreTeamOrder.addAll(scoreListTeam);
						for (final RankingScoreSingleTeam rankingScore : scoreListTeam) {
							final List<RankingScoreSingleTeam> playerScoreTeam = new ArrayList<RankingScoreSingleTeam>();
							playerScoreTeam.add(rankingScore);
							mapTeamToListScoreTeam.put(
								rankingScore.getName(translator.translate(UIText.GUI_SECONDARY_TREND_TITLE_PLAYER_EMPTY)),
								playerScoreTeam
							);
						}
					}

					for (int sessionIndex = 2; sessionIndex <= session; sessionIndex++) {
						final List<RankingScoreSingleTeam> scoreListTeam = rankingCalculatorSingleFactory.getRankingCalculator(
							ranking.getScore(),
							ranking.getQualificationMode()
						).getRankingScoreTeam(
							tournament,
							sessionIndex
						);
						listScoreTeamOrder.clear();
						listScoreTeamOrder.addAll(scoreListTeam);
						for (final RankingScoreSingleTeam rankingScore : scoreListTeam) {
							mapTeamToListScoreTeam.get(rankingScore.getName(translator.translate(UIText.GUI_SECONDARY_TREND_TITLE_PLAYER_EMPTY))).add(rankingScore);
						}
					}
					final int nbTeams = mapTeamToListScoreTeam.size();

					final Font labelFont = (CUSTOM_FONT != null
						? CUSTOM_FONT
						: getFont()).deriveFont(
							Font.PLAIN,
							getFontSize(UIConfigFontSize.SMALL)
						);

					final ProportionalGridLayoutInteger layout = new ProportionalGridLayoutInteger(
						nbTeams + 1,
						1,
						0,
						2
					);
					panelTrend.setLayout(layout);

					final ProportionalGridLayoutConstraint constraint = new ProportionalGridLayoutConstraint(
						0,
						1,
						0,
						1
					);

					constraint.x = 0;

					final UITrendChartTitle title = new UITrendChartTitle(
						nbSessions,
						trendColorProfile,
						labelFont,
						TeamMode.TEAM,
						translator
					);
					constraint.y = 0;
					panelTrend.add(
						title,
						constraint
					);

					final int teamSize = tournament.getNbTables() * 4 / mapTeamToListScoreTeam.size();
					final List<Integer> averages = new ArrayList<Integer>(session);
					switch (trendMode) {
						case SCORE:
							switch (ranking.getScore()) {
								case MATCH_POINT:
									for (int sessionIndex = 1; sessionIndex <= session; sessionIndex++) {
										averages.add(21 * sessionIndex * teamSize);
									}
									break;
								case TABLE_POINT:
									for (int sessionIndex = 1; sessionIndex <= session; sessionIndex++) {
										averages.add(0);
									}
									break;
								default:
									break;
							}
							break;
						case RANKING:
							for (int sessionIndex = 1; sessionIndex <= session; sessionIndex++) {
								averages.add(nbTeams / 2);
							}
							break;
						default:
							break;
					}
					final Dimension rowSize = new Dimension(
						0,
						24
					);
					for (int index = 0; index < listScoreTeamOrder.size(); index++) {
						final String team = listScoreTeamOrder.get(index).getName(translator.translate(UIText.GUI_SECONDARY_TREND_TITLE_PLAYER_EMPTY));
						final boolean darken = index % 2 == 0;
						final UITrendChartRowMCR row = new UITrendChartRowMCR(
							trendMode,
							ranking,
							team,
							nbSessions,
							nbTeams,
							mapTeamToListScoreTeam.get(team),
							averages,
							trendColorProfile,
							labelFont,
							darken
						);
						row.setPreferredSize(rowSize);

						constraint.y = index + 1;
						panelTrend.add(
							row,
							constraint
						);
					}

					break;
				}
			}
		}
		validate();
		scrollTrend.getVerticalScrollBar().setValue(scrollTrend.getVerticalScrollBar().getModel().getMinimum());
		repaint();
	}

	@Override
	protected void exportCSV() {
		final int selectedTournamentIndex = comboTournament.getSelectedIndex();
		final int selectedSessionIndex = comboSession.getSelectedIndex();
		if (selectedTournamentIndex != -1 && selectedSessionIndex != -1) {
			final UIRankingMode<ScoreMCR> ranking = rankingModes.get(comboRankingMode.getSelectedIndex());
			final Tournament<TournamentScoreMCR, ScoreMCR> tournament = listTournament.get(selectedTournamentIndex);
			final int session = comboSession.getSelectedIndex() + 1;
			final UITrendMode trendMode = trendModes.get(comboTrendMode.getSelectedIndex());
			if (mapPlayerToListScoreIndividual != null && mapPlayerToListScoreIndividual.size() > 0 || mapTeamToListScoreTeam != null && mapTeamToListScoreTeam.size() > 0) {
				final JFileChooser csvFileChooser = new JFileChooser();
				csvFileChooser.setMultiSelectionEnabled(false);
				csvFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				final String saveFileName = regulateString(tournament.getName())
					+ "_Trend_"
					+ regulateString(
						translator.translate(
							UIConfigDisplayLanguage.ENGLISH,
							trendMode.getUIText()
						)
					)
					+ "_"
					+ regulateString(
						translator.translate(
							UIConfigDisplayLanguage.ENGLISH,
							ranking.getUIText()
						)
					)
					+ "_Session_"
					+ Integer.toString(session)
					+ ".csv";
				if (lastChoosenFolder != null) {
					csvFileChooser.setSelectedFile(
						new File(
							lastChoosenFolder,
							saveFileName
						)
					);
				} else {
					csvFileChooser.setSelectedFile(new File(saveFileName));
				}
				File csvFile = null;
				boolean toContinue = true;
				while (toContinue) {
					final int fileChooserAnswer = csvFileChooser.showSaveDialog(this);
					if (fileChooserAnswer == JFileChooser.APPROVE_OPTION) {
						csvFile = csvFileChooser.getSelectedFile();
						if (csvFile.exists()) {
							final MessageDialogOption overwriteAnswer = new UIMessageDialog(
								(JFrame) SwingUtilities.getWindowAncestor(this),
								translator.translate(UIText.GUI_SECONDARY_TREND_EXPORT_OVERWRITE_CONFIRM_MESSAGE),
								translator.translate(UIText.GUI_SECONDARY_TREND_EXPORT_OVERWRITE_CONFIRM_TITLE),
								MessageDialogMessageType.WARNING,
								MessageDialogOptionsType.YES_NO_CANCEL,
								MessageDialogOption.NO,
								translator
							).showOptionDialog();
							switch (overwriteAnswer) {
								case YES:
									toContinue = false;
									break;
								case NO:
									csvFile = null;
									break;
								case CANCEL:
								case CLOSED:
									csvFile = null;
									toContinue = false;
									break;
								default:
									break;
							}
						} else {
							toContinue = false;
						}
					} else {
						csvFile = null;
						toContinue = false;
					}
				}

				if (csvFile != null) {
					lastChoosenFolder = csvFileChooser.getCurrentDirectory();

					final String csvContent[][] = new String[listScoreIndividualOrder.size() + 2][];
					csvContent[0] = new String[] {
						"Tournament",
						tournament.getName(),
						"Trend",
						translator.translate(
							UIConfigDisplayLanguage.ENGLISH,
							ranking.getUIText()
						),
						"Session",
						Integer.toString(session)
					};

					csvContent[1] = new String[tournament.getNbSessions() + 2];
					csvContent[1][0] = "Name";
					for (int sessionIndex = 0; sessionIndex < session; sessionIndex++) {
						csvContent[1][sessionIndex + 1] = "Session "
							+ Integer.toString(sessionIndex + 1);
					}

					switch (ranking.getRankingModeTeamMode()) {
						case INDIVIDUAL: {
							switch (trendMode) {
								case SCORE: {
									switch (ranking.getScore()) {
										case MATCH_POINT: {
											for (int playerIndex = 0; playerIndex < listScoreIndividualOrder.size(); playerIndex++) {
												final TournamentPlayer player = listScoreIndividualOrder.get(playerIndex).getTournamentPlayer();
												csvContent[playerIndex + 2] = new String[tournament.getNbSessions() + 1];
												csvContent[playerIndex + 2][0] = player.getPlayerName(translator.translate(UIText.GUI_SECONDARY_TREND_TITLE_PLAYER_EMPTY));
												final List<RankingScoreSingleIndividual> scoreList = mapPlayerToListScoreIndividual.get(player);
												for (int scoreIndex = 0; scoreIndex < scoreList.size(); scoreIndex++) {
													final RankingScoreSingleIndividual score = scoreList.get(scoreIndex);
													csvContent[playerIndex + 2][scoreIndex + 1] = fraction(
														score.getTotalScore(),
														12
													);
												}
												for (int scoreIndex = scoreList.size(); scoreIndex < tournament.getNbSessions(); scoreIndex++) {
													csvContent[playerIndex + 2][scoreIndex + 1] = "";
												}
											}
										}
											break;
										case TABLE_POINT: {
											for (int playerIndex = 0; playerIndex < listScoreIndividualOrder.size(); playerIndex++) {
												final TournamentPlayer player = listScoreIndividualOrder.get(playerIndex).getTournamentPlayer();
												csvContent[playerIndex + 2] = new String[tournament.getNbSessions() + 1];
												csvContent[playerIndex + 2][0] = player.getPlayerName(translator.translate(UIText.GUI_SECONDARY_TREND_TITLE_PLAYER_EMPTY));
												final List<RankingScoreSingleIndividual> scoreList = mapPlayerToListScoreIndividual.get(player);
												for (int scoreIndex = 0; scoreIndex < scoreList.size(); scoreIndex++) {
													final RankingScoreSingleIndividual score = scoreList.get(scoreIndex);
													csvContent[playerIndex + 2][scoreIndex + 1] = Integer.toString(score.getTotalScore());
												}
												for (int scoreIndex = scoreList.size(); scoreIndex < tournament.getNbSessions(); scoreIndex++) {
													csvContent[playerIndex + 2][scoreIndex + 1] = "";
												}
											}
										}
											break;
									}
								}
									break;
								case RANKING: {
									for (int playerIndex = 0; playerIndex < listScoreIndividualOrder.size(); playerIndex++) {
										final TournamentPlayer player = listScoreIndividualOrder.get(playerIndex).getTournamentPlayer();
										csvContent[playerIndex + 2] = new String[tournament.getNbSessions() + 1];
										csvContent[playerIndex + 2][0] = player.getPlayerName(translator.translate(UIText.GUI_SECONDARY_TREND_TITLE_PLAYER_EMPTY));
										final List<RankingScoreSingleIndividual> scoreList = mapPlayerToListScoreIndividual.get(player);
										for (int scoreIndex = 0; scoreIndex < scoreList.size(); scoreIndex++) {
											final RankingScoreSingleIndividual score = scoreList.get(scoreIndex);
											csvContent[playerIndex + 2][scoreIndex + 1] = Integer.toString(score.getRanking());
										}
										for (int scoreIndex = scoreList.size(); scoreIndex < tournament.getNbSessions(); scoreIndex++) {
											csvContent[playerIndex + 2][scoreIndex + 1] = "";
										}
									}
								}
									break;
							}
						}
							break;
						case TEAM: {
							switch (trendMode) {
								case SCORE: {
									switch (ranking.getScore()) {
										case MATCH_POINT: {
											for (int teamIndex = 0; teamIndex < listScoreTeamOrder.size(); teamIndex++) {
												final String name = listScoreTeamOrder.get(teamIndex).getName(translator.translate(UIText.GUI_SECONDARY_TREND_TITLE_PLAYER_EMPTY));
												csvContent[teamIndex + 2] = new String[tournament.getNbSessions() + 1];
												csvContent[teamIndex + 2][0] = name;
												final List<RankingScoreSingleTeam> scoreList = mapTeamToListScoreTeam.get(name);
												for (int scoreIndex = 0; scoreIndex < scoreList.size(); scoreIndex++) {
													final RankingScoreSingleTeam score = scoreList.get(scoreIndex);
													csvContent[teamIndex + 2][scoreIndex + 1] = fraction(
														score.getTotalScore(),
														12
													);
												}
												for (int scoreIndex = scoreList.size(); scoreIndex < tournament.getNbSessions(); scoreIndex++) {
													csvContent[teamIndex + 2][scoreIndex + 1] = "";
												}
											}
										}
											break;
										case TABLE_POINT: {
											for (int teamIndex = 0; teamIndex < listScoreTeamOrder.size(); teamIndex++) {
												final String name = listScoreTeamOrder.get(teamIndex).getName(translator.translate(UIText.GUI_SECONDARY_TREND_TITLE_PLAYER_EMPTY));
												csvContent[teamIndex + 2] = new String[tournament.getNbSessions() + 1];
												csvContent[teamIndex + 2][0] = name;
												final List<RankingScoreSingleTeam> scoreList = mapTeamToListScoreTeam.get(name);
												for (int scoreIndex = 0; scoreIndex < scoreList.size(); scoreIndex++) {
													final RankingScoreSingleTeam score = scoreList.get(scoreIndex);
													csvContent[teamIndex + 2][scoreIndex + 1] = Integer.toString(score.getTotalScore());
												}
												for (int scoreIndex = scoreList.size(); scoreIndex < tournament.getNbSessions(); scoreIndex++) {
													csvContent[teamIndex + 2][scoreIndex + 1] = "";
												}
											}
										}
											break;
									}
								}
									break;
								case RANKING: {
									for (int teamIndex = 0; teamIndex < listScoreTeamOrder.size(); teamIndex++) {
										final String name = listScoreTeamOrder.get(teamIndex).getName(translator.translate(UIText.GUI_SECONDARY_TREND_TITLE_PLAYER_EMPTY));
										csvContent[teamIndex + 2] = new String[tournament.getNbSessions() + 1];
										csvContent[teamIndex + 2][0] = name;
										final List<RankingScoreSingleTeam> scoreList = mapTeamToListScoreTeam.get(name);
										for (int scoreIndex = 0; scoreIndex < scoreList.size(); scoreIndex++) {
											final RankingScoreSingleTeam score = scoreList.get(scoreIndex);
											csvContent[teamIndex + 2][scoreIndex + 1] = Integer.toString(score.getRanking());
										}
										for (int scoreIndex = scoreList.size(); scoreIndex < tournament.getNbSessions(); scoreIndex++) {
											csvContent[teamIndex + 2][scoreIndex + 1] = "";
										}
									}
								}
									break;
							}
						}
					}

					if (CSVWriter.write(
						csvFile,
						csvContent,
						null
					)) {
						new UIMessageDialog(
							(JFrame) SwingUtilities.getWindowAncestor(this),
							translator.translate(UIText.GUI_SECONDARY_TREND_EXPORT_RESULT_FILE_SAVED_MESSAGE),
							translator.translate(UIText.GUI_SECONDARY_TREND_EXPORT_RESULT_FILE_SAVED_TITLE),
							MessageDialogMessageType.INFORMATION,
							MessageDialogOptionsType.OK,
							MessageDialogOption.OK,
							translator
						).showOptionDialog();
					} else {
						new UIMessageDialog(
							(JFrame) SwingUtilities.getWindowAncestor(this),
							translator.translate(UIText.GUI_SECONDARY_TREND_EXPORT_RESULT_FILE_NOT_SAVED_MESSAGE),
							translator.translate(UIText.GUI_SECONDARY_TREND_EXPORT_RESULT_FILE_NOT_SAVED_TITLE),
							MessageDialogMessageType.ERROR,
							MessageDialogOptionsType.OK,
							MessageDialogOption.OK,
							translator
						).showOptionDialog();
					}
				}
			}
		}
	}
}
