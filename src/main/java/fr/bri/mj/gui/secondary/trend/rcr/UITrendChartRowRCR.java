/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.trend.rcr;

import java.awt.Font;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import fr.bri.awt.ProportionalGridLayoutConstraint;
import fr.bri.awt.ProportionalGridLayoutInteger;
import fr.bri.mj.data.score.aggregated.ranking.rankingscore.RankingScore;
import fr.bri.mj.data.tournament.rcr.ScoreRCR;
import fr.bri.mj.data.tournament.rcr.TournamentScoreRCR;
import fr.bri.mj.gui.icon.IconLoaderTrend;
import fr.bri.mj.gui.secondary.ranking.UIRankingMode;
import fr.bri.mj.gui.secondary.trend.UITrendChartRow;
import fr.bri.mj.gui.secondary.trend.UITrendColorProfile;
import fr.bri.mj.gui.secondary.trend.UITrendMode;

public class UITrendChartRowRCR extends UITrendChartRow<TournamentScoreRCR, ScoreRCR> {

	private static final long serialVersionUID = -4873868256147930918L;

	private static final int SCORE_DIGITS = 6;
	private static final int SCORE_GROUP_WIDTH = 3;
	private static final char SCORE_GROUP_SEPARATOR = ' ';

	// private static final int RANKING_DIGITS = 2;
	// private static final int RANKING_GROUP_WIDTH = 2;
	// private static final char RANKING_GROUP_SEPARATOR = ' ';

	public UITrendChartRowRCR(
		final UITrendMode trendMode,
		final UIRankingMode<ScoreRCR> rankingMode,
		final String playerName,
		final int nbSessions,
		final int nbPlayers,
		final List<? extends RankingScore> scoreList,
		final List<Integer> averages,
		final UITrendColorProfile colorProfile,
		final Font labelFont,
		final boolean darken
	) {
		setOpaque(false);

		final ProportionalGridLayoutInteger layout = new ProportionalGridLayoutInteger(
			1,
			nbSessions * 2 + 3,
			0,
			0
		);
		final int weightX[] = new int[nbSessions * 2 + 3];
		weightX[0] = 2;
		weightX[1] = 10;
		for (int sessionIndex = 0; sessionIndex < nbSessions; sessionIndex++) {
			weightX[sessionIndex * 2 + 2] = 3;
			weightX[sessionIndex * 2 + 3] = 1;
		}
		weightX[weightX.length - 1] = 2;
		layout.setWeightX(weightX);
		setLayout(layout);

		final ProportionalGridLayoutConstraint constraints = new ProportionalGridLayoutConstraint(
			0,
			1,
			0,
			1
		);

		constraints.y = 0;
		{
			constraints.x = 1;
			final JLabel labelPlayerName = new JLabel(
				playerName,
				SwingConstants.LEFT
			);
			labelPlayerName.setFont(labelFont);
			labelPlayerName.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
			labelPlayerName.setBackground(colorProfile.getDarkenLineBackgroundColor());
			labelPlayerName.setOpaque(darken);
			add(
				labelPlayerName,
				constraints
			);
		}

		final int rankChangeScale = nbPlayers / 4;
		final int nbScores = Math.min(
			nbSessions,
			scoreList.size()
		);
		for (int sessionIndex = 0; sessionIndex < nbScores; sessionIndex++) {
			final RankingScore score = scoreList.get(sessionIndex);
			final int average = averages.get(sessionIndex);
			JLabel labelScore = null;
			JLabel labelIcon = null;
			if (score.isUpToDate()) {
				String scoreText = "";
				switch (trendMode) {
					case SCORE: {
						scoreText = format(
							score.getTotalScore(),
							SCORE_DIGITS,
							SCORE_GROUP_WIDTH,
							SCORE_GROUP_SEPARATOR,
							true
						);
					}
						break;
					case RANKING: {
						scoreText = Integer.toString(score.getRanking());
					}
						break;
					default:
						break;
				}
				labelScore = new JLabel(
					scoreText,
					SwingConstants.CENTER
				);
				labelScore.setFont(labelFont);
				switch (trendMode) {
					case SCORE:
						if (score.getTotalScore() >= average) {
							labelScore.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
						} else {
							labelScore.setForeground(colorProfile.getUpToDateNegativeScoreFontColor());
						}
						break;
					case RANKING:
						if (score.getRanking() <= average) {
							labelScore.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
						} else {
							labelScore.setForeground(colorProfile.getUpToDateNegativeScoreFontColor());
						}
						break;
					default:
						break;
				}

				if (sessionIndex > 0) {
					final RankingScore lastScore = scoreList.get(sessionIndex - 1);
					final int rankChange = score.getRanking() - lastScore.getRanking();
					if (rankChange < -rankChangeScale) {
						labelIcon = new JLabel(
							new ImageIcon(IconLoaderTrend.getIconImageArrowUpUp()),
							SwingConstants.CENTER
						);
					} else if (rankChange < 0) {
						labelIcon = new JLabel(
							new ImageIcon(IconLoaderTrend.getIconImageArrowUp()),
							SwingConstants.CENTER
						);
					} else if (rankChange == 0) {
						labelIcon = new JLabel(
							new ImageIcon(IconLoaderTrend.getIconImageArrowFlat()),
							SwingConstants.CENTER
						);
					} else if (rankChange <= rankChangeScale) {
						labelIcon = new JLabel(
							new ImageIcon(IconLoaderTrend.getIconImageArrowDown()),
							SwingConstants.CENTER
						);
					} else {
						labelIcon = new JLabel(
							new ImageIcon(IconLoaderTrend.getIconImageArrowDownDown()),
							SwingConstants.CENTER
						);
					}
				} else {
					labelIcon = new JLabel();
				}
			} else {
				labelScore = new JLabel();
				labelIcon = new JLabel();
			}

			labelScore.setBackground(colorProfile.getDarkenLineBackgroundColor());
			labelScore.setOpaque(darken);

			constraints.x = sessionIndex * 2 + 2;
			add(
				labelScore,
				constraints
			);

			labelIcon.setBackground(colorProfile.getDarkenLineBackgroundColor());
			labelIcon.setOpaque(darken);
			constraints.x = sessionIndex * 2 + 3;
			add(
				labelIcon,
				constraints
			);
		}

		for (int sessionIndex = nbScores; sessionIndex < nbSessions; sessionIndex++) {
			final JLabel labelScore = new JLabel();
			labelScore.setBackground(colorProfile.getDarkenLineBackgroundColor());
			labelScore.setOpaque(darken);
			constraints.x = sessionIndex * 2 + 2;
			add(
				labelScore,
				constraints
			);

			final JLabel labelIcon = new JLabel();
			labelIcon.setBackground(colorProfile.getDarkenLineBackgroundColor());
			labelIcon.setOpaque(darken);
			constraints.x = sessionIndex * 2 + 3;
			add(
				labelIcon,
				constraints
			);
		}
	}

	private String format(
		final int number,
		final int width,
		final int groupWidth,
		final char groupSeparator,
		final boolean sign
	) {
		final StringBuilder builder = new StringBuilder();
		if (number >= 0 && sign) {
			builder.append(' ');
		} else if (number < 0) {
			builder.append('-');
		}

		final String raw = Integer.toString(Math.abs(number));
		final int rawWidth = raw.length();
		final int nbGroupSeparator = (width - 1) / groupWidth;
		final int nbRawGroupSeparator = (rawWidth - 1) / groupWidth;
		final int paddingWidth = width - rawWidth + nbGroupSeparator - nbRawGroupSeparator;
		for (int index = 0; index < paddingWidth; index++) {
			builder.append(' ');
		}

		for (int index = 0; index < rawWidth; index++) {
			builder.append(raw.charAt(index));
			final int numberIndex = rawWidth - 1 - index;
			if (numberIndex != 0 && numberIndex % groupWidth == 0) {
				builder.append(groupSeparator);
			}
		}
		return builder.toString();
	}
}
