/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.utils;

import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import fr.bri.mj.data.tournament.Tournament;
import fr.bri.mj.gui.icon.IconLoaderTreeNode;
import fr.bri.mj.gui.primary.score.SessionId;

public class UITreeNodeRenderer extends DefaultTreeCellRenderer {

	private static final long serialVersionUID = 4328349189935026165L;

	@Override
	public Component getTreeCellRendererComponent(
		final JTree tree,
		final Object value,
		final boolean selected,
		final boolean expanded,
		final boolean leaf,
		final int row,
		final boolean hasFocus
	) {
		super.getTreeCellRendererComponent(
			tree,
			value,
			selected,
			expanded,
			leaf,
			row,
			hasFocus
		);
		if (value != null) {
			final DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) value;
			final Object nodeObject = treeNode.getUserObject();
			if (nodeObject instanceof Tournament<?, ?>) {
				setIcon(new ImageIcon(IconLoaderTreeNode.getIconImageTournament()));
			} else if (nodeObject instanceof SessionId) {
				setIcon(new ImageIcon(IconLoaderTreeNode.getIconImageSession()));
			}
		}

		return this;
	}
}
