/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.printing;

public enum PaperSizeType {
	A0(PaperSizeSeries.A_SERIES, 0),
	A1(PaperSizeSeries.A_SERIES, 1),
	A2(PaperSizeSeries.A_SERIES, 2),
	A3(PaperSizeSeries.A_SERIES, 3),
	A4(PaperSizeSeries.A_SERIES, 4),
	A5(PaperSizeSeries.A_SERIES, 5),
	A6(PaperSizeSeries.A_SERIES, 6),
	A7(PaperSizeSeries.A_SERIES, 7),
	A8(PaperSizeSeries.A_SERIES, 8),
	A9(PaperSizeSeries.A_SERIES, 9),
	A10(PaperSizeSeries.A_SERIES, 10),

	B0(PaperSizeSeries.B_SERIES, 0),
	B1(PaperSizeSeries.B_SERIES, 1),
	B2(PaperSizeSeries.B_SERIES, 2),
	B3(PaperSizeSeries.B_SERIES, 3),
	B4(PaperSizeSeries.B_SERIES, 4),
	B5(PaperSizeSeries.B_SERIES, 5),
	B6(PaperSizeSeries.B_SERIES, 6),
	B7(PaperSizeSeries.B_SERIES, 7),
	B8(PaperSizeSeries.B_SERIES, 8),
	B9(PaperSizeSeries.B_SERIES, 9),
	B10(PaperSizeSeries.B_SERIES, 10);

	private final PaperSizeSeries series;
	private final int size;

	private PaperSizeType(
		final PaperSizeSeries series,
		final int size
	) {
		this.series = series;
		this.size = size;
	}

	public PaperSizeSeries getSeries() {
		return series;
	}

	public int getSize() {
		return size;
	}
}
