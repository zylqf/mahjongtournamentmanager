/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.printing;

import java.awt.Color;

import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;

import fr.bri.mj.data.tournament.TournamentPlayer;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;

public class ScoreFormPrinterRCR {

	public static final Color FONT_COLOR = new Color(
		0,
		0,
		0
	);

	public static final Color DARKEN_CELL_COLOR = new Color(
		127,
		127,
		127
	);

	private static final UIText TEXT_TITLE[] = {
		UIText.PDF_SCORE_TITLE_ID,
		UIText.PDF_SCORE_TITLE_SCORE,
		UIText.PDF_SCORE_TITLE_BORROW,
		UIText.PDF_SCORE_TITLE_NET,
		UIText.PDF_SCORE_TITLE_UMA,
		UIText.PDF_SCORE_TITLE_PENALTY,
		UIText.PDF_SCORE_TITLE_TOTAL,
		UIText.PDF_SCORE_TITLE_SIGNATURE
	};

	private static final boolean DARKEN[] = {
		false,
		false,
		false,
		true,
		true,
		false,
		true,
		false
	};

	private static final int ROW_LINE_INDEX_FROM[] = {
		4,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0
	};

	private static final int ROW_LINE_INDEX_TO[] = {
		5,
		5,
		5,
		5,
		5,
		5,
		5,
		5,
		5,
		5
	};

	private static final int COLUMN_LINE_INDEX_FROM[] = {
		1,
		1,
		1,
		1,
		0,
		0
	};

	private static final int COLUMN_LINE_INDEX_TO[] = {
		9,
		9,
		9,
		9,
		9,
		9
	};

	public void drawForm(
		final UITextTranslator translator,
		final PDPageContentStream stream,
		final PDFont font,
		final float fontSize,
		final float topLeftX,
		final float topLeftY,
		final float formWidth,
		final float formHeight,
		final int tableNumber,
		final int nbTables,
		final int sessionNumber,
		final int nbSessions,
		final TournamentPlayer[] tablePlan
	) throws Exception {
		final float titleFontSizeQuater = font.getFontDescriptor().getFontBoundingBox().getHeight() * fontSize / 4000f;
		final float cellWidth = formWidth / 5;
		final float cellHeight = formHeight / (TEXT_TITLE.length + 1);

		// Draw table title
		{
			// Session number
			final float textBaseY = topLeftY - cellHeight * 0.5f - titleFontSizeQuater;
			float textBaseX = topLeftX;
			stream.setNonStrokingColor(FONT_COLOR);
			stream.beginText();
			stream.newLineAtOffset(
				textBaseX,
				textBaseY
			);
			if (sessionNumber <= nbSessions) {
				stream.showText(
					translator.translate(UIText.PDF_SCORE_TITLE_SESSION)
						+ ": "
						+ Integer.toString(sessionNumber)
				);
			} else {
				stream.showText(
					translator.translate(UIText.PDF_SCORE_TITLE_SESSION)
						+ ": "
				);
			}
			stream.endText();

			// Table number
			textBaseX = topLeftX + formWidth * 0.4f;
			stream.setNonStrokingColor(FONT_COLOR);
			stream.beginText();
			stream.newLineAtOffset(
				textBaseX,
				textBaseY
			);
			if (sessionNumber <= nbSessions) {
				stream.showText(
					translator.translate(UIText.PDF_PLAN_TITLE_TABLE)
						+ ": "
						+ Integer.toString(tableNumber)
				);
			} else {
				stream.showText(
					translator.translate(UIText.PDF_PLAN_TITLE_TABLE)
						+ ": "
				);
			}
			stream.endText();
		}

		// Draw line title
		for (int rowIndex = 1; rowIndex <= TEXT_TITLE.length; rowIndex++) {
			final String title = translator.translate(TEXT_TITLE[rowIndex - 1]);
			final float textBaseY = topLeftY - cellHeight * (rowIndex + 0.5f) - titleFontSizeQuater;
			final float textBaseX = topLeftX + (cellWidth - font.getStringWidth(title) * fontSize / 1000f) / 2f;
			stream.setNonStrokingColor(FONT_COLOR);
			stream.beginText();
			stream.newLineAtOffset(
				textBaseX,
				textBaseY
			);
			stream.showText(title);
			stream.endText();

			if (DARKEN[rowIndex - 1]) {
				stream.setNonStrokingColor(DARKEN_CELL_COLOR);
				stream.addRect(
					topLeftX + cellWidth,
					topLeftY - cellHeight * (rowIndex + 1),
					cellWidth * 4,
					cellHeight
				);
				stream.fill();
			}
		}

		// Draw ID
		if (tablePlan != null) {
			for (int columnIndex = 0; columnIndex < 4; columnIndex++) {
				final TournamentPlayer tournamentPlayer = tablePlan[columnIndex];
				final String id = Integer.toString(tournamentPlayer.getTournamentPlayerId());
				final float textBaseY = topLeftY - cellHeight * 1.5f - titleFontSizeQuater;
				final float textBaseX = topLeftX + cellWidth * (columnIndex + 1) + (cellWidth - font.getStringWidth(id) * fontSize / 1000f) / 2;
				stream.setNonStrokingColor(FONT_COLOR);
				stream.beginText();
				stream.newLineAtOffset(
					textBaseX,
					textBaseY
				);
				stream.showText(id);
				stream.endText();
			}
		}

		// Draw row lines
		for (int rowIndex = 0; rowIndex < ROW_LINE_INDEX_FROM.length; rowIndex++) {
			final float y = topLeftY - rowIndex * cellHeight;
			stream.setNonStrokingColor(FONT_COLOR);
			stream.moveTo(
				topLeftX + ROW_LINE_INDEX_FROM[rowIndex] * cellWidth,
				y
			);
			stream.lineTo(
				topLeftX + ROW_LINE_INDEX_TO[rowIndex] * cellWidth,
				y
			);
			stream.stroke();
		}

		// Draw column lines
		for (int columnIndex = 0; columnIndex < COLUMN_LINE_INDEX_FROM.length; columnIndex++) {
			final float x = topLeftX + columnIndex * cellWidth;
			stream.setNonStrokingColor(FONT_COLOR);
			stream.moveTo(
				x,
				topLeftY - COLUMN_LINE_INDEX_FROM[columnIndex] * cellHeight
			);
			stream.lineTo(
				x,
				topLeftY - COLUMN_LINE_INDEX_TO[columnIndex] * cellHeight
			);
			stream.stroke();
		}
	}
}
