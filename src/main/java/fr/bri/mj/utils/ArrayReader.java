/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.utils;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ArrayReader {

	private static enum Token {
		LEFT_BRACKET,
		NUMBER,
		COMMA,
		RIGHT_BRACKET,
		END,
		ERROR
	}

	private static InputStreamReader reader;
	private static Token token;
	private static int tokenValue;
	private static int lastCharRead;

	private static void readNextToken() {
		try {
			while (lastCharRead == ' ' || lastCharRead == '\t' || lastCharRead == '\r' || lastCharRead == '\n') {
				lastCharRead = reader.read();
			}
			switch (lastCharRead) {
				case -1:
					token = Token.END;
					lastCharRead = reader.read();
					break;
				case '[':
					token = Token.LEFT_BRACKET;
					lastCharRead = reader.read();
					break;
				case ']':
					token = Token.RIGHT_BRACKET;
					lastCharRead = reader.read();
					break;
				case ',':
					token = Token.COMMA;
					lastCharRead = reader.read();
					break;
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
					token = Token.NUMBER;
					tokenValue = lastCharRead - '0';
					lastCharRead = reader.read();
					while (lastCharRead >= '0' && lastCharRead <= '9') {
						tokenValue = tokenValue * 10 + lastCharRead - '0';
						lastCharRead = reader.read();
					}
					break;
				default:
					token = Token.ERROR;
					break;
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	private static Object read() {
		Object item = null;
		readNextToken();
		if (token == Token.LEFT_BRACKET) {
			final List<Object> array = new ArrayList<Object>();
			item = array;
			while (true) {
				final Object subItem = read();
				if (subItem != null) {
					array.add(subItem);
					readNextToken();
					if (token == Token.RIGHT_BRACKET) {
						break;
					} else if (token != Token.COMMA) {
						item = null;
						break;
					}
				} else {
					item = null;
					break;
				}
			}
		} else if (token == Token.NUMBER) {
			item = new Integer(tokenValue);
		}
		return item;
	}

	public synchronized static List<?> readIntegerArray(final InputStreamReader reader) {
		token = null;
		lastCharRead = ' ';
		ArrayReader.reader = reader;
		final Object result = read();
		if (result instanceof List) {
			return (List<?>) result;
		} else {
			return null;
		}
	}
}
