/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.utils;

import java.util.HashMap;
import java.util.Map;

public class StringNormalizer {

	private static final char[][] NONE_ASCII_CHARACTERS = {
		{
			'À',
			'Á',
			'Â',
			'Ã',
			'Ä',
			'Å',
			'Ą'
		},
		{
			'Ç'
		},
		{
			'È',
			'É',
			'Ê',
			'Ë',
			'Ę'
		},
		{
			'Ì',
			'Í',
			'Î',
			'Ï'
		},
		{
			'Ł',
		},
		{
			'Ń',
			'Ň',
			'Ñ'
		},
		{
			'Ò',
			'Ó',
			'Ô',
			'Õ',
			'Ö',
			'Ø'
		},
		{
			'Ś',
			'Š'
		},
		{
			'Ù',
			'Ú',
			'Û',
			'Ü'
		},
		{
			'Ý',
			'Ÿ'
		},
		{
			'Ż',
			'Ź'
		},
		{
			'à',
			'á',
			'â',
			'ã',
			'ä',
			'å',
			'ą'
		},
		{
			'ç'
		},
		{
			'è',
			'é',
			'ê',
			'ë',
			'ę'
		},
		{
			'ì',
			'í',
			'î',
			'ï'
		},
		{
			'ł'
		},
		{
			'ń',
			'ň',
			'ñ'
		},
		{
			'ò',
			'ó',
			'ô',
			'õ',
			'ö',
			'ø'
		},
		{
			'ś',
			'š'
		},
		{
			'ù',
			'ú',
			'û',
			'ü'
		},
		{
			'ý',
			'ÿ'
		},
		{
			'ż',
			'ź'
		}
	};

	private static final char[] ASCII_CHARACTERS = {
		'A',
		'C',
		'E',
		'I',
		'L',
		'N',
		'O',
		'S',
		'U',
		'Y',
		'Z',
		'a',
		'c',
		'e',
		'i',
		'l',
		'n',
		'o',
		's',
		'u',
		'y',
		'z'
	};

	private static final Map<Character, Character> MAP_CHARACTER;

	static {
		MAP_CHARACTER = new HashMap<Character, Character>();
		for (int charIndex = 0; charIndex < NONE_ASCII_CHARACTERS.length; charIndex++) {
			for (int varIndex = 0; varIndex < NONE_ASCII_CHARACTERS[charIndex].length; varIndex++) {
				MAP_CHARACTER.put(
					NONE_ASCII_CHARACTERS[charIndex][varIndex],
					ASCII_CHARACTERS[charIndex]
				);
			}
		}
	}

	public static String normalize(final String text) {
		if (text != null) {
			final char normalizedText[] = new char[text.length()];
			for (int index = 0; index < text.length(); index++) {
				final char currentCharacter = text.charAt(index);
				final Character normalizedCharacter = MAP_CHARACTER.get(currentCharacter);
				if (normalizedCharacter != null) {
					normalizedText[index] = normalizedCharacter;
				} else {
					normalizedText[index] = currentCharacter;
				}
			}
			return new String(normalizedText);
		} else {
			return null;
		}
	}
}
