create table Player(
id integer,
first_name varchar(64) not null,
last_name varchar(64) not null,
gender integer not null,
nationality integer not null,
club varchar(8),
license varchar(16),
constraint player_pk primary key(id),
constraint player_gender_integrity check (gender>=0 and gender<=1),
constraint player_nationality_integrity check (nationality>=1 and nationality<=999)
);

create table RCR_Tournament(
id integer,
name varchar(64) not null,
nb_sessions integer not null,
nb_tables integer not null,
is_archived boolean not null,
constraint rcr_tournament_pk primary key(id),
constraint rcr_tournament_name unique(name),
constraint rcr_tournament_sessions check(nb_sessions>0 and nb_sessions<100),
constraint rcr_tournament_tables check(nb_tables>0 and nb_tables<100)
);

create table RCR_Tournament_Player(
rcr_tournament_id integer,
rcr_tournament_player_id integer,
player_id integer,
team_name varchar(64),
constraint rtpa_pk primary key (rcr_tournament_id, rcr_tournament_player_id),
constraint rtpa_tid_fk foreign key(rcr_tournament_id) references RCR_Tournament(id) on delete cascade on update restrict,
constraint rtpa_player_fk foreign key(player_id) references Player(id) on delete restrict on update restrict
);

create table RCR_Tournament_Planning(
rcr_tournament_id integer,
session_id integer,
start_time timestamp not null,
constraint rtpn_pk primary key (rcr_tournament_id, session_id),
constraint rtpn_tid_fk foreign key(rcr_tournament_id) references RCR_Tournament(id) on delete cascade on update restrict
);

create table RCR_Tournament_Table(
rcr_tournament_id integer,
session_id integer not null,
table_id integer not null,
table_player_id integer not null,
rcr_tournament_player_id integer not null,
substituted integer not null,
ranking integer not null,
game_score integer not null,
uma_score integer not null,
penalty integer not null,
final_score integer not null,
score_recorded boolean not null,
win integer not null,
self_draw integer not null,
deal_in integer not null,
constraint rtt_pk primary key (rcr_tournament_id, session_id, table_id, table_player_id),
constraint rtt_tid_fk foreign key(rcr_tournament_id) references RCR_Tournament(id) on delete cascade on update restrict,
constraint rtt_sub_integrity check (substituted>=0 and substituted<=2),
constraint rtt_player_fk foreign key(rcr_tournament_id, rcr_tournament_player_id) references RCR_Tournament_Player(rcr_tournament_id, rcr_tournament_player_id) on delete restrict on update restrict,
constraint rtt_seat_integrity check (table_player_id>=1 and table_player_id<=4),
constraint rtt_player_uniqueness unique(rcr_tournament_id, session_id, rcr_tournament_player_id),
constraint rtt_ranking_integrity check(ranking>=1 and ranking<=4),
constraint rtt_agari_integrity check(win>=0 and self_draw>=0 and win>=self_draw),
constraint rtt_furigomi_integrity check(deal_in>=0)
);

create table MCR_Tournament(
id integer,
name varchar(64) not null,
nb_sessions integer not null,
nb_tables integer not null,
constraint mcr_tournament_pk primary key(id),
constraint mcr_tournament_name unique(name),
constraint mcr_tournament_sessions check(nb_sessions>0 and nb_sessions<100),
constraint mcr_tournament_tables check(nb_tables>0 and nb_tables<100)
);

create table MCR_Tournament_Player(
mcr_tournament_id integer,
mcr_tournament_player_id integer,
player_id integer,
team_name varchar(64),
constraint mtpa_pk primary key (mcr_tournament_id, mcr_tournament_player_id),
constraint mtpa_tid_fk foreign key(mcr_tournament_id) references MCR_Tournament(id) on delete cascade on update restrict,
constraint mtpa_player_fk foreign key(player_id) references Player(id) on delete restrict on update restrict
);

create table MCR_Tournament_Planning(
mcr_tournament_id integer,
session_id integer,
start_time timestamp not null,
constraint mtpn_pk primary key (mcr_tournament_id, session_id),
constraint mtpn_tid_fk foreign key(mcr_tournament_id) references MCR_Tournament(id) on delete cascade on update restrict
);

create table MCR_Tournament_Table(
mcr_tournament_id integer,
session_id integer not null,
table_id integer not null,
table_player_id integer not null,
mcr_tournament_player_id integer not null,
substituted integer not null,
ranking integer not null,
table_point integer not null,
penalty integer not null,
total_point integer not null,
match_point integer not null,
score_recorded boolean not null,
win integer not null,
self_draw integer not null,
deal_in integer not null,
constraint mtt_pk primary key (mcr_tournament_id, session_id, table_id, table_player_id),
constraint mtt_tid_fk foreign key(mcr_tournament_id) references MCR_Tournament(id) on delete cascade on update restrict,
constraint mtt_sub_integrity check (substituted>=0 and substituted<=2),
constraint mtt_player_fk foreign key(mcr_tournament_id, mcr_tournament_player_id) references MCR_Tournament_Player(mcr_tournament_id, mcr_tournament_player_id) on delete restrict on update restrict,
constraint mtt_seat_integrity check (table_player_id>=1 and table_player_id<=4),
constraint mtt_player_uniqueness unique(mcr_tournament_id, session_id, mcr_tournament_player_id),
constraint mtt_ranking_integrity check(ranking>=1 and ranking<=4),
constraint mtt_win_integrity check(win>=0 and self_draw>=0 and win>=self_draw),
constraint mtt_dealin_integrity check(deal_in>=0)
);